﻿#ifndef SUCROSE_CONTROLLER_RAW_ACTIONBUFFER_H
#define SUCROSE_CONTROLLER_RAW_ACTIONBUFFER_H

#include "fg/controller/raw/actionbuffer.h"
#include "fg/controller/raw/action.h"

#include <vector>

struct FgRawControllerActionBuffer
{
    using Buffer = std::vector< fg::RawControllerAction::ConstUnique >;

    Buffer  buffer;

    static fg::RawControllerActionBuffer::Unique create(
    );

    void addAction(
        fg::RawControllerAction::ConstUnique &&
    );

    void clear(
    );
};

#endif  // SUCROSE_CONTROLLER_RAW_ACTIONBUFFER_H
