﻿#ifndef SUCROSE_CONTROLLER_RAW_ID_H
#define SUCROSE_CONTROLLER_RAW_ID_H

#include "fg/controller/raw/id.h"

#include <string>

struct FgRawControllerID
{
    std::string module;
    std::string port;
    std::string device;
};

#endif  // SUCROSE_CONTROLLER_RAW_ID_H
