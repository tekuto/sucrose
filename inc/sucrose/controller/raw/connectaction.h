﻿#ifndef SUCROSE_CONTROLLER_RAW_CONNECTACTION_H
#define SUCROSE_CONTROLLER_RAW_CONNECTACTION_H

#include "fg/controller/raw/connectaction.h"
#include "fg/controller/raw/id.h"

struct FgRawControllerConnectAction
{
    fg::RawControllerID::ConstUnique    idUnique;
    bool                                connected;

    static fg::RawControllerConnectAction::ConstUnique createConnectAction(
        fg::RawControllerID::ConstUnique &&
    );

    static fg::RawControllerConnectAction::ConstUnique createDisconnectAction(
        fg::RawControllerID::ConstUnique &&
    );
};

#endif  // SUCROSE_CONTROLLER_RAW_CONNECTACTION_H
