﻿#ifndef SUCROSE_CONTROLLER_RAW_BUTTON_H
#define SUCROSE_CONTROLLER_RAW_BUTTON_H

#include "fg/controller/raw/button.h"
#include "fg/controller/raw/id.h"

#include <cstddef>

struct FgRawControllerButton
{
    fg::RawControllerID::ConstUnique    idUnique;
    std::size_t                         index;

    static fg::RawControllerButton::ConstUnique create(
        fg::RawControllerID::ConstUnique &&
        , std::size_t
    );
};

#endif  // SUCROSE_CONTROLLER_RAW_BUTTON_H
