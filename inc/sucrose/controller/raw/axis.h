﻿#ifndef SUCROSE_CONTROLLER_RAW_AXIS_H
#define SUCROSE_CONTROLLER_RAW_AXIS_H

#include "fg/controller/raw/axis.h"
#include "fg/controller/raw/id.h"

#include <cstddef>

struct FgRawControllerAxis
{
    fg::RawControllerID::ConstUnique    idUnique;
    std::size_t                         index;

    static fg::RawControllerAxis::ConstUnique create(
        fg::RawControllerID::ConstUnique &&
        , std::size_t
    );
};

#endif  // SUCROSE_CONTROLLER_RAW_AXIS_H
