﻿#ifndef SUCROSE_CONTROLLER_RAW_AXISACTION_H
#define SUCROSE_CONTROLLER_RAW_AXISACTION_H

#include "fg/controller/raw/axisaction.h"
#include "fg/controller/raw/axis.h"

struct FgRawControllerAxisAction
{
    fg::RawControllerAxis::ConstUnique  axisUnique;
    int                                 value;

    static fg::RawControllerAxisAction::ConstUnique create(
        fg::RawControllerAxis::ConstUnique &&
        , int
    );
};

#endif  // SUCROSE_CONTROLLER_RAW_AXISACTION_H
