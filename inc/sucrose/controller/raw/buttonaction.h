﻿#ifndef SUCROSE_CONTROLLER_RAW_BUTTONACTION_H
#define SUCROSE_CONTROLLER_RAW_BUTTONACTION_H

#include "fg/controller/raw/buttonaction.h"
#include "fg/controller/raw/button.h"

struct FgRawControllerButtonAction
{
    fg::RawControllerButton::ConstUnique    buttonUnique;
    bool                                    pressed;

    static fg::RawControllerButtonAction::ConstUnique createPressAction(
        fg::RawControllerButton::ConstUnique &&
    );

    static fg::RawControllerButtonAction::ConstUnique createReleaseAction(
        fg::RawControllerButton::ConstUnique &&
    );
};

#endif  // SUCROSE_CONTROLLER_RAW_BUTTONACTION_H
