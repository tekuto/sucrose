﻿#ifndef SUCROSE_CONTROLLER_RAW_ACTION_H
#define SUCROSE_CONTROLLER_RAW_ACTION_H

#include "fg/controller/raw/action.h"
#include "fg/controller/raw/connectaction.h"
#include "fg/controller/raw/buttonaction.h"
#include "fg/controller/raw/axisaction.h"

struct FgRawControllerAction
{
    fg::RawControllerConnectAction::ConstUnique connectActionUnique;
    fg::RawControllerButtonAction::ConstUnique  buttonActionUnique;
    fg::RawControllerAxisAction::ConstUnique    axisActionUnique;

    static fg::RawControllerAction::ConstUnique create(
        fg::RawControllerConnectAction::ConstUnique &&
    );

    static fg::RawControllerAction::ConstUnique create(
        fg::RawControllerButtonAction::ConstUnique &&
    );

    static fg::RawControllerAction::ConstUnique create(
        fg::RawControllerAxisAction::ConstUnique &&
    );
};

#endif  // SUCROSE_CONTROLLER_RAW_ACTION_H
