﻿#ifndef SUCROSE_CONTROLLER_RAW_MANAGER_H
#define SUCROSE_CONTROLLER_RAW_MANAGER_H

#include "sucrose/def/controller/raw/manager.h"
#include "fg/controller/raw/actionbuffer.h"
#include "fg/core/state/state.h"
#include "fg/core/state/joiner.h"
#include "fg/common/unique.h"
#include "fg/util/import.h"

#include <mutex>
#include <cstddef>

namespace sucrose {
    struct RawControllerManager : public fg::UniqueWrapper< RawControllerManager >
    {
        fg::State<> &   state;

        RawControllerManagerEventProcImpl   eventProcPtr;
        void *                              userDataPtr;

        std::mutex  mutex;

        fg::RawControllerActionBuffer::Unique   actionBufferUnique;

        fg::StateJoiner::Unique joinerUnique;

        RawControllerManager(
            FgState &
            , RawControllerManagerEventProcImpl
            , void *
        );

        static FG_FUNCTION_PTR(
            RawControllerManager
            , create_(
                FgState &
                , RawControllerManagerEventProcImpl
                , void *
            )
        )

        template<
            typename STATE_DATA_T
            , typename USER_DATA_T
        >
        static Unique create(
            fg::State< STATE_DATA_T > &                     _state
            , RawControllerManagerEventProc< USER_DATA_T >  _eventProcPtr
            , USER_DATA_T &                                 _userData
        )
        {
            return create_(
                *_state
                , reinterpret_cast< RawControllerManagerEventProcImpl >( _eventProcPtr )
                , &_userData
            );
        }

        static FG_FUNCTION_VOID(
            destroy(
                RawControllerManager *
            )
        )

        FG_FUNCTION_VOID(
            connect(
                const char *
                , const char *
                , const char *
            )
        )

        FG_FUNCTION_VOID(
            disconnect(
                const char *
                , const char *
                , const char *
            )
        )

        FG_FUNCTION_VOID(
            pressButton(
                const char *
                , const char *
                , const char *
                , std::size_t
            )
        )

        FG_FUNCTION_VOID(
            releaseButton(
                const char *
                , const char *
                , const char *
                , std::size_t
            )
        )

        FG_FUNCTION_VOID(
            operateAxis(
                const char *
                , const char *
                , const char *
                , std::size_t
                , int
            )
        )
    };
}

#endif  // SUCROSE_CONTROLLER_RAW_MANAGER_H
