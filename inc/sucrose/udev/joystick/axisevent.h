﻿#ifndef SUCROSE_UDEV_JOYSTICK_AXISEVENT_H
#define SUCROSE_UDEV_JOYSTICK_AXISEVENT_H

#include "sucrose/def/udev/joystick/axisevent.h"
#include "sucrose/def/udev/joystick/id.h"
#include "fg/util/import.h"

namespace sucrose {
    struct UdevJoystickAxisEvent
    {
        const UdevJoystickID &  ID;
        unsigned char           index;
        short                   value;

        UdevJoystickAxisEvent(
            const UdevJoystickID &
            , unsigned char
            , short
        );

        FG_FUNCTION_REF(
            const UdevJoystickID
            , getID(
            ) const
        )

        FG_FUNCTION_NUM(
            unsigned char
            , getIndex(
            ) const
        )

        FG_FUNCTION_NUM(
            short
            , getValue(
            ) const
        )
    };
}

#endif  // SUCROSE_UDEV_JOYSTICK_AXISEVENT_H
