﻿#ifndef SUCROSE_UDEV_JOYSTICK_BUTTONEVENT_H
#define SUCROSE_UDEV_JOYSTICK_BUTTONEVENT_H

#include "sucrose/def/udev/joystick/buttonevent.h"
#include "sucrose/def/udev/joystick/id.h"
#include "fg/util/import.h"

namespace sucrose {
    struct UdevJoystickButtonEvent
    {
        const UdevJoystickID &  ID;
        unsigned char           index;
        bool                    pressed;

        UdevJoystickButtonEvent(
            const UdevJoystickID &
            , unsigned char
            , bool
        );

        FG_FUNCTION_REF(
            const UdevJoystickID
            , getID(
            ) const
        )

        FG_FUNCTION_NUM(
            unsigned char
            , getIndex(
            ) const
        )

        FG_FUNCTION_BOOL(
            getPressed(
            ) const
        )
    };
}

#endif  // SUCROSE_UDEV_JOYSTICK_BUTTONEVENT_H
