﻿#ifndef SUCROSE_UDEV_JOYSTICK_MANAGER_H
#define SUCROSE_UDEV_JOYSTICK_MANAGER_H

#include "sucrose/def/udev/joystick/manager.h"
#include "sucrose/udev/joystick/id.h"
#include "sucrose/udev/joystick/joystick.h"
#include "sucrose/udev/joystick/eventparameters.h"
#include "sucrose/def/udev/event.h"
#include "fg/core/state/state.h"
#include "fg/common/unique.h"
#include "fg/util/import.h"

#include <map>

namespace sucrose {
    struct UdevJoystickManager : public fg::UniqueWrapper< UdevJoystickManager >
    {
        struct LessUdevJoystickIDUnique
        {
            bool operator()(
                const UdevJoystickID::ConstUnique &
                , const UdevJoystickID::ConstUnique &
            ) const;
        };

        using JoystickMap = std::map<
            UdevJoystickID::ConstUnique
            , UdevJoystick::Unique
            , LessUdevJoystickIDUnique
        >;

        fg::State<> &   state;

        UdevJoystickEventParameters eventParameters;

        JoystickMap  joystickMap;

        UdevJoystickManager(
            FgState &
            , const UdevJoystickEventParameters &
        );

        static FG_FUNCTION_PTR(
            UdevJoystickManager
            , create_(
                FgState &
                , const UdevJoystickEventParameters &
            )
        )

        template< typename STATE_DATA_T >
        static Unique create(
            fg::State< STATE_DATA_T > &             _state
            , const UdevJoystickEventParameters &   _EVENT_PARAMETERS
        )
        {
            return create_(
                *_state
                , _EVENT_PARAMETERS
            );
        }

        static FG_FUNCTION_VOID(
            destroy(
                UdevJoystickManager *
            )
        )

        FG_FUNCTION_VOID(
            analyzeUdevEvent(
                const UdevEvent &
            )
        )
    };
}

#endif  // SUCROSE_UDEV_JOYSTICK_MANAGER_H
