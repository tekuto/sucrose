﻿#ifndef SUCROSE_UDEV_JOYSTICK_CONNECTEVENT_H
#define SUCROSE_UDEV_JOYSTICK_CONNECTEVENT_H

#include "sucrose/def/udev/joystick/connectevent.h"
#include "sucrose/def/udev/joystick/id.h"
#include "fg/util/import.h"

namespace sucrose {
    struct UdevJoystickConnectEvent
    {
        const UdevJoystickID &  ID;

        //TODO ジョイスティック名を追加する？
        //TODO ボタン数と軸数は？

        UdevJoystickConnectEvent(
            const UdevJoystickID &
        );

        FG_FUNCTION_REF(
            const UdevJoystickID
            , getID(
            ) const
        )
    };
}

#endif  // SUCROSE_UDEV_JOYSTICK_CONNECTEVENT_H
