﻿#ifndef SUCROSE_UDEV_JOYSTICK_JOYSTICK_H
#define SUCROSE_UDEV_JOYSTICK_JOYSTICK_H

#include "sucrose/def/udev/joystick/joystick.h"
#include "sucrose/def/udev/joystick/id.h"
#include "sucrose/def/udev/joystick/eventparameters.h"
#include "fg/core/state/state.h"
#include "fg/core/state/joiner.h"
#include "fg/common/unique.h"

#include <memory>
#include <mutex>
#include <linux/joystick.h>

namespace sucrose {
    struct UdevJoystick : public fg::UniqueWrapper< UdevJoystick >
    {
        struct CloseDescriptor
        {
            void operator()(
                int *
            ) const;
        };

        using DescriptorCloser = std::unique_ptr<
            int
            , CloseDescriptor
        >;

        struct TaskData
        {
            js_event    jsEvent;
        };

        struct EndThreads
        {
            void operator()(
                UdevJoystick *
            ) const;
        };

        using Ender = std::unique_ptr<
            UdevJoystick
            , EndThreads
        >;

        fg::State<> &   state;

        UdevJoystickEventParameters &   eventParameters;
        const UdevJoystickID &          ID;

        int                 descriptor;
        DescriptorCloser    descriptorCloser;

        TaskData    taskData;

        std::mutex  mutex;
        bool        ended;

        fg::StateJoiner::Unique joinerUnique;

        Ender   ender;

        UdevJoystick(
            FgState &
            , UdevJoystickEventParameters &
            , const UdevJoystickID &
            , int
        );

        inline static Unique create(
            FgState &                       _state
            , UdevJoystickEventParameters & _eventParameters
            , const UdevJoystickID &        _ID
            , int                           _descriptor
        )
        {
            return new UdevJoystick(
                _state
                , _eventParameters
                , _ID
                , _descriptor
            );
        }
    };
}

#endif  // SUCROSE_UDEV_JOYSTICK_JOYSTICK_H
