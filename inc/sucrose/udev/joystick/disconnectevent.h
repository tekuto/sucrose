﻿#ifndef SUCROSE_UDEV_JOYSTICK_DISCONNECTEVENT_H
#define SUCROSE_UDEV_JOYSTICK_DISCONNECTEVENT_H

#include "sucrose/def/udev/joystick/disconnectevent.h"
#include "sucrose/def/udev/joystick/id.h"
#include "fg/util/import.h"

namespace sucrose {
    struct UdevJoystickDisconnectEvent
    {
        const UdevJoystickID &  ID;

        UdevJoystickDisconnectEvent(
            const UdevJoystickID &
        );

        FG_FUNCTION_REF(
            const UdevJoystickID
            , getID(
            ) const
        )
    };
}

#endif  // SUCROSE_UDEV_JOYSTICK_DISCONNECTEVENT_H
