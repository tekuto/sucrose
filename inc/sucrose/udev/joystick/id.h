﻿#ifndef SUCROSE_UDEV_JOYSTICK_ID_H
#define SUCROSE_UDEV_JOYSTICK_ID_H

#include "sucrose/def/udev/joystick/id.h"
#include "fg/common/unique.h"
#include "fg/util/import.h"

#include <string>

namespace sucrose {
    struct UdevJoystickID : public fg::UniqueWrapper< UdevJoystickID >
    {
        const std::string   PORT;
        const std::string   DEVICE;

        UdevJoystickID(
            const std::string &
            , const std::string &
        );

        static ConstUnique create(
            const std::string &
            , const std::string &
        );

        FG_FUNCTION_PTR(
            const char
            , getPort(
            ) const
        )

        FG_FUNCTION_PTR(
            const char
            , getDevice(
            ) const
        )

        bool operator<(
            const UdevJoystickID &
        ) const;
    };
}

#endif  // SUCROSE_UDEV_JOYSTICK_ID_H
