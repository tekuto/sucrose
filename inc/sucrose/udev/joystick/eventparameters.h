﻿#ifndef SUCROSE_UDEV_JOYSTICK_EVENTPARAMETERS_H
#define SUCROSE_UDEV_JOYSTICK_EVENTPARAMETERS_H

#include "sucrose/def/udev/joystick/eventparameters.h"
#include "sucrose/def/udev/joystick/connectevent.h"
#include "sucrose/def/udev/joystick/disconnectevent.h"
#include "sucrose/def/udev/joystick/buttonevent.h"
#include "sucrose/def/udev/joystick/axisevent.h"
#include "fg/common/unique.h"
#include "fg/util/import.h"

namespace sucrose {
    struct UdevJoystickEventParameters : public fg::UniqueWrapper< UdevJoystickEventParameters >
    {
        void *  dataPtr = nullptr;

        UdevJoystickConnectEventProcImpl     connectEventProcPtr = nullptr;
        UdevJoystickDisconnectEventProcImpl  disconnectEventProcPtr = nullptr;
        UdevJoystickButtonEventProcImpl      buttonEventProcPtr = nullptr;
        UdevJoystickAxisEventProcImpl        axisEventProcPtr = nullptr;

        static FG_FUNCTION_PTR(
            UdevJoystickEventParameters
            , create_(
            )
        )

        inline static Unique create(
        )
        {
            return create_();
        }

        static FG_FUNCTION_VOID(
            destroy(
                UdevJoystickEventParameters *
            )
        )

        FG_FUNCTION_VOID(
            setEventData(
                void *
            )
        )

        template< typename DATA_T >
        void setEventData(
            DATA_T &    _data
        )
        {
            this->setEventData( &_data );
        }

        FG_FUNCTION_VOID(
            setEventProc(
                UdevJoystickConnectEventProcImpl
            )
        )

        template< typename DATA_T >
        void setEventProc(
            UdevJoystickConnectEventProc< DATA_T >  _proc
        )
        {
            this->setEventProc( reinterpret_cast< UdevJoystickConnectEventProcImpl >( _proc ) );
        }

        FG_FUNCTION_VOID(
            setEventProc(
                UdevJoystickDisconnectEventProcImpl
            )
        )

        template< typename DATA_T >
        void setEventProc(
            UdevJoystickDisconnectEventProc< DATA_T >   _proc
        )
        {
            this->setEventProc( reinterpret_cast< UdevJoystickDisconnectEventProcImpl >( _proc ) );
        }

        FG_FUNCTION_VOID(
            setEventProc(
                UdevJoystickButtonEventProcImpl
            )
        )

        template< typename DATA_T >
        void setEventProc(
            UdevJoystickButtonEventProc< DATA_T >   _proc
        )
        {
            this->setEventProc( reinterpret_cast< UdevJoystickButtonEventProcImpl >( _proc ) );
        }

        FG_FUNCTION_VOID(
            setEventProc(
                UdevJoystickAxisEventProcImpl
            )
        )

        template< typename DATA_T >
        void setEventProc(
            UdevJoystickAxisEventProc< DATA_T > _proc
        )
        {
            this->setEventProc( reinterpret_cast< UdevJoystickAxisEventProcImpl >( _proc ) );
        }

        inline void setEventProcs(
        )
        {
        }

        template<
            typename FIRST_PROC_T
            , typename ... REST_PROC_T
        >
        void setEventProcs(
            FIRST_PROC_T        _firstProc
            , REST_PROC_T ...   _restProc
        )
        {
            this->setEventProc( _firstProc );

            this->setEventProcs( _restProc ... );
        }

        template< typename DATA_T >
        void setEventProcs(
        )
        {
        }

        template<
            typename DATA_T
            , typename FIRST_PROC_T
            , typename ... REST_PROC_T
        >
        void setEventProcs(
            FIRST_PROC_T        _firstProc
            , REST_PROC_T ...   _restProc
        )
        {
            this->setEventProc< DATA_T >( _firstProc );

            this->setEventProcs< DATA_T >( _restProc ... );
        }

        template<
            typename FIRST_PROC_T
            , typename ... REST_PROC_T
        >
        void setEventParameters(
            void *              _dataPtr
            , FIRST_PROC_T      _firstProc
            , REST_PROC_T ...   _restProc
        )
        {
            this->setEventData( _dataPtr );

            this->setEventProcs(
                _firstProc
                , _restProc ...
            );
        }

        template<
            typename DATA_T
            , typename FIRST_PROC_T
            , typename ... REST_PROC_T
        >
        void setEventParameters(
            DATA_T &            _data
            , FIRST_PROC_T      _firstProc
            , REST_PROC_T ...   _restProc
        )
        {
            this->setEventData< DATA_T >( _data );

            this->setEventProcs< DATA_T >(
                _firstProc
                , _restProc ...
            );
        }

        void call(
            const UdevJoystickConnectEvent &
        );

        void call(
            const UdevJoystickDisconnectEvent &
        );

        void call(
            const UdevJoystickButtonEvent &
        );

        void call(
            const UdevJoystickAxisEvent &
        );
    };
}

#endif  // SUCROSE_UDEV_JOYSTICK_EVENTPARAMETERS_H
