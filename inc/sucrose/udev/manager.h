﻿#ifndef SUCROSE_UDEV_MANAGER_H
#define SUCROSE_UDEV_MANAGER_H

#include "sucrose/def/udev/manager.h"
#include "sucrose/udev/eventparameters.h"
#include "fg/core/state/state.h"
#include "fg/core/state/joiner.h"
#include "fg/common/unique.h"
#include "fg/util/import.h"

#include <memory>
#include <mutex>
#include <libudev.h>

namespace sucrose {
    struct UdevManager : public fg::UniqueWrapper< UdevManager >
    {
        struct UnrefUdev
        {
            void operator()(
                udev *
            ) const;
        };

        using UdevUnique = std::unique_ptr<
            udev
            , UnrefUdev
        >;

        struct UnrefUdevEnumerate
        {
            void operator()(
                udev_enumerate *
            ) const;
        };

        using UdevEnumerateUnique = std::unique_ptr<
            udev_enumerate
            , UnrefUdevEnumerate
        >;

        struct UnrefUdevMonitor
        {
            void operator()(
                udev_monitor *
            ) const;
        };

        using UdevMonitorUnique = std::unique_ptr<
            udev_monitor
            , UnrefUdevMonitor
        >;

        struct EndThreads
        {
            void operator()(
                UdevManager *
            ) const;
        };

        using Ender = std::unique_ptr<
            UdevManager
            , EndThreads
        >;

        fg::State<> &   state;

        UdevEventParameters eventParameters;

        UdevUnique          udevUnique;
        UdevEnumerateUnique udevEnumerateUnique;
        UdevMonitorUnique   udevMonitorUnique;

        std::mutex  mutex;
        bool        ended;

        fg::StateJoiner::Unique joinerUnique;

        Ender   ender;

        UdevManager(
            FgState &
            , const UdevEventParameters &
        );

        static FG_FUNCTION_PTR(
            UdevManager
            , create_(
                FgState &
                , const UdevEventParameters &
            )
        )

        template< typename STATE_DATA_T >
        static Unique create(
            fg::State< STATE_DATA_T > &     _state
            , const UdevEventParameters &   _EVENT_PARAMETERS
        )
        {
            return create_(
                *_state
                , _EVENT_PARAMETERS
            );
        }

        static FG_FUNCTION_VOID(
            destroy(
                UdevManager *
            )
        )
    };
}

#endif  // SUCROSE_UDEV_MANAGER_H
