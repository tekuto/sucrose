﻿#ifndef SUCROSE_UDEV_EVENTPARAMETERS_H
#define SUCROSE_UDEV_EVENTPARAMETERS_H

#include "sucrose/def/udev/eventparameters.h"
#include "sucrose/def/udev/event.h"
#include "fg/common/unique.h"
#include "fg/util/import.h"

namespace sucrose {
    struct UdevEventParameters : public fg::UniqueWrapper< UdevEventParameters >
    {
        void *  dataPtr = nullptr;

        UdevEventProcImpl   eventProcPtr = nullptr;

        static FG_FUNCTION_PTR(
            UdevEventParameters
            , create_(
            )
        )

        inline static Unique create(
        )
        {
            return create_();
        }

        static FG_FUNCTION_VOID(
            destroy(
                UdevEventParameters *
            )
        )

        FG_FUNCTION_VOID(
            setEventData(
                void *
            )
        )

        template< typename DATA_T >
        void setEventData(
            DATA_T &    _data
        )
        {
            this->setEventData( &_data );
        }

        FG_FUNCTION_VOID(
            setEventProc(
                UdevEventProcImpl
            )
        )

        template< typename DATA_T >
        void setEventProc(
            UdevEventProc< DATA_T > _proc
        )
        {
            this->setEventProc( reinterpret_cast< UdevEventProcImpl >( _proc ) );
        }

        inline void setEventParameters(
            void *                  _dataPtr
            , UdevEventProcImpl     _proc
        )
        {
            this->setEventData( _dataPtr );

            this->setEventProc( _proc );
        }

        template< typename DATA_T >
        void setEventParameters(
            DATA_T &                    _data
            , UdevEventProc< DATA_T >   _proc
        )
        {
            this->setEventData< DATA_T >( _data );

            this->setEventProc< DATA_T >( _proc );
        }

        void call(
            const UdevEvent &
        );
    };
}

#endif  // SUCROSE_UDEV_EVENTPARAMETERS_H
