﻿#ifndef SUCROSE_UDEV_EVENT_H
#define SUCROSE_UDEV_EVENT_H

#include "sucrose/def/udev/event.h"
#include "fg/common/unique.h"
#include "fg/util/import.h"

#include <map>
#include <string>
#include <utility>

namespace sucrose {
    struct UdevEvent : public fg::UniqueWrapper< UdevEvent >
    {
        using Properties = std::map<
            std::string
            , const char *
        >;

        Properties  properties;

        UdevEvent(
            Properties &&
        );

        inline static ConstUnique create(
            Properties &&   _properties
        )
        {
            return new UdevEvent( std::move( _properties ) );
        }

        FG_FUNCTION_BOOL(
            getProperty(
                const char * &
                , const char *
            ) const
        )
    };
}

#endif  // SUCROSE_UDEV_EVENT_H
