﻿#ifndef SUCROSE_UDEV_EVENTPROPERTYNAMES_H
#define SUCROSE_UDEV_EVENTPROPERTYNAMES_H

#include "sucrose/def/udev/eventpropertynames.h"
#include "sucrose/def/udev/event.h"
#include "fg/common/unique.h"
#include "fg/util/import.h"

#include <vector>
#include <string>
#include <cstddef>

namespace sucrose {
    struct UdevEventPropertyNames : public fg::UniqueWrapper< UdevEventPropertyNames >
    {
        using PropertyNames = std::vector< std::string >;

        PropertyNames   propertyNames;

        UdevEventPropertyNames(
            const UdevEvent &
        );

        static FG_FUNCTION_PTR(
            UdevEventPropertyNames
            , create_(
                const UdevEvent &
            )
        )

        inline static ConstUnique create(
            const UdevEvent &   _EVENT
        )
        {
            return create_( _EVENT );
        }

        static FG_FUNCTION_VOID(
            destroy(
                UdevEventPropertyNames *
            )
        )

        FG_FUNCTION_NUM(
            std::size_t
            , getSize(
            ) const
        )

        FG_FUNCTION_PTR(
            const char
            , getName(
                std::size_t
            ) const
        )
    };
}

#endif  // SUCROSE_DEF_UDEV_EVENTPROPERTYNAMES_H
