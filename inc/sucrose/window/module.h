﻿#ifndef SUCROSE_WINDOW_MODULE_H
#define SUCROSE_WINDOW_MODULE_H

#include "fg/core/module/context.h"
#include "fg/util/import.h"

namespace sucrose {
    FG_FUNCTION_VOID(
        initializeWindow(
            const fg::ModuleContext &
        )
    )
}

#endif  // SUCROSE_WINDOW_MODULE_H
