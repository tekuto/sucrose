﻿#ifndef SUCROSE_WINDOW_EVENTPROCESSOR_H
#define SUCROSE_WINDOW_EVENTPROCESSOR_H

#include "fg/window/eventprocessor.h"
#include "fg/window/window.h"
#include "fg/window/eventmanagers.h"
#include "fg/core/state/state.h"
#include "fg/core/state/joiner.h"
#include "sucrose/window/paintevent.h"
#include "sucrose/window/closeevent.h"
#include "fg/common/unique.h"

#include <memory>
#include <mutex>

struct FgWindowEventProcessor
{
    struct EndThread
    {
        void operator()(
            FgWindowEventProcessor *
        ) const;
    };

    using ThreadEnder = std::unique_ptr<
        FgWindowEventProcessor
        , EndThread
    >;

    union EventData
    {
        fg::Window *    windowPtr_;

        FgWindowPaintEventData  paintEvent;
        FgWindowCloseEventData  closeEvent;

        EventData(
            fg::Window &
        );
    };

    fg::State<> &               state;
    fg::Window &                window;
    fg::WindowEventManagers &   eventManagers;

    EventData   eventData;

    fg::StateJoiner::Unique stateJoinerUnique;

    std::mutex  mutex;
    bool        ended;

    fg::StateJoiner::Unique threadJoinerUnique;

    ThreadEnder ender;

    FgWindowEventProcessor(
        fg::State<> &
        , fg::Window &
        , fg::WindowEventManagers &
    );
};

#endif  // SUCROSE_WINDOW_EVENTPROCESSOR_H
