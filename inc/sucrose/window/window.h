﻿#ifndef SUCROSE_WINDOW_WINDOW_H
#define SUCROSE_WINDOW_WINDOW_H

#include "fg/window/window.h"
#include "fg/common/unique.h"

#include <X11/Xlib.h>
#include <memory>

struct FgWindow
{
    class XDisplay : public fg::UniqueWrapper< XDisplay, Display >
    {
    public:
        static Unique create(
        );

        static void destroy(
            XDisplay *
        );
    };

    struct DestroyXWindow
    {
        void operator()(
            FgWindow *
        ) const;
    };

    using XWindowDestroyer = std::unique_ptr<
        FgWindow
        , DestroyXWindow
    >;

    XDisplay::Unique    xDisplayForEventUnique;
    XDisplay::Unique    xDisplayForPaintUnique;
    Atom                wmDeleteWindow;
    Window              xWindow;
    XWindowDestroyer    xWindowDestroyer;

    FgWindow(
        const char *
        , int
        , int
    );

    bool pollXEvent(
    );

    void getXEvent(
        XEvent &
    );

    bool isWmDeleteWindow(
        const Atom &
    ) const;

    FG_FUNCTION_REF(
        Display
        , getXDisplayForPaint(
        )
    )

    FG_FUNCTION_REF(
        Window
        , getXWindow(
        )
    )
};

#endif  // SUCROSE_WINDOW_WINDOW_H
