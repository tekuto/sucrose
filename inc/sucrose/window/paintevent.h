﻿#ifndef SUCROSE_WINDOW_PAINTEVENT_H
#define SUCROSE_WINDOW_PAINTEVENT_H

#include "fg/window/paintevent.h"
#include "fg/window/window.h"

struct FgWindowPaintEventData
{
    fg::Window &    window;
};

#endif  // SUCROSE_WINDOW_PAINTEVENT_H
