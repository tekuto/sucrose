﻿#ifndef SUCROSE_WINDOW_CLOSEEVENT_H
#define SUCROSE_WINDOW_CLOSEEVENT_H

#include "fg/window/closeevent.h"
#include "fg/window/window.h"

struct FgWindowCloseEventData
{
    fg::Window &    window;
};

#endif  // SUCROSE_WINDOW_CLOSEEVENT_H
