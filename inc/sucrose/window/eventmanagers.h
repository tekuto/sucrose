﻿#ifndef SUCROSE_WINDOW_EVENTMANAGERS_H
#define SUCROSE_WINDOW_EVENTMANAGERS_H

#include "fg/window/eventmanagers.h"
#include "fg/window/paintevent.h"
#include "fg/window/closeevent.h"

struct FgWindowEventManagers
{
    fg::WindowPaintEventManager::Unique paintEventManagerUnique;
    fg::WindowCloseEventManager::Unique closeEventManagerUnique;
};

#endif  // SUCROSE_WINDOW_EVENTMANAGERS_H
