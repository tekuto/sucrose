﻿#ifndef SUCROSE_GL_PICTURE_H
#define SUCROSE_GL_PICTURE_H

#include "fg/gl/picture.h"
#include "fg/gl/gl.h"

#include <vector>

struct FgGLPicture
{
    using Data = std::vector< char >;

    Data    data;

    fg::GLsizei width;
    fg::GLsizei height;
    fg::GLenum  format;
    fg::GLenum  dataType;
};

#endif  // SUCROSE_GL_PICTURE_H
