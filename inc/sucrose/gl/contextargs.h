﻿#ifndef SUCROSE_GL_CONTEXTARGS_H
#define SUCROSE_GL_CONTEXTARGS_H

#include "fg/gl/contextargs.h"

struct FgGLContextArgs
{
    struct Size
    {
        bool    exists = false;
        int     value;
    };

    Size    redSize;
    Size    greenSize;
    Size    blueSize;
    Size    alphaSize;
    Size    depthSize;
    Size    stencilSize;
};

#endif  // SUCROSE_GL_CONTEXTARGS_H
