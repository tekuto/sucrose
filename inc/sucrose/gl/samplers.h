﻿#ifndef SUCROSE_GL_SAMPLERS_H
#define SUCROSE_GL_SAMPLERS_H

#include "fg/gl/samplers.h"
#include "fg/gl/gl.h"
#include "fg/gl/context.h"
#include "fg/gl/currentcontext.h"

#include <vector>
#include <memory>

struct FgGLSamplers
{
    using Names = std::vector< fg::GLuint >;

    struct DeleteNames
    {
        void operator()(
            FgGLSamplers *
        ) const;
    };

    using NamesDeleter = std::unique_ptr<
        FgGLSamplers
        , DeleteNames
    >;

    fg::GLContext & glContext;

    Names           names;
    NamesDeleter    namesDeleter;

    FgGLSamplers(
        fg::GLCurrentContext &
        , fg::GLsizei
    );
};

#endif  // SUCROSE_GL_SAMPLERS_H
