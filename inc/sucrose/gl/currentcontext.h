﻿#ifndef SUCROSE_GL_CURRENTCONTEXT_H
#define SUCROSE_GL_CURRENTCONTEXT_H

#include "fg/gl/currentcontext.h"
#include "fg/gl/context.h"

struct FgGLCurrentContext
{
    fg::GLContext & getGLContext(
    );

    void swapBuffers(
    );
};

#endif  // SUCROSE_GL_CURRENTCONTEXT_H
