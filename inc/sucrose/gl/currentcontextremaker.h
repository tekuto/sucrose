﻿#ifndef SUCROSE_GL_CURRENTCONTEXTREMAKER_H
#define SUCROSE_GL_CURRENTCONTEXTREMAKER_H

#include "fg/gl/context.h"

#include <memory>

namespace sucrose {
    struct RemakeGLCurrentContext
    {
        void operator()(
            FgGLContext *
        ) const;
    };

    using GLCurrentContextRemaker = std::unique_ptr<
        FgGLContext
        , RemakeGLCurrentContext
    >;
}

#endif  // SUCROSE_GL_CURRENTCONTEXTREMAKER_H
