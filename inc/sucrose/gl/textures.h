﻿#ifndef SUCROSE_GL_TEXTURES_H
#define SUCROSE_GL_TEXTURES_H

#include "fg/gl/textures.h"
#include "fg/gl/gl.h"
#include "fg/gl/context.h"
#include "fg/gl/currentcontext.h"

#include <vector>
#include <memory>

struct FgGLTextures
{
    using Names = std::vector< fg::GLuint >;

    struct DeleteNames
    {
        void operator()(
            FgGLTextures *
        ) const;
    };

    using NamesDeleter = std::unique_ptr<
        FgGLTextures
        , DeleteNames
    >;

    fg::GLContext & glContext;

    Names           names;
    NamesDeleter    namesDeleter;

    FgGLTextures(
        fg::GLCurrentContext &
        , fg::GLsizei
    );

    const fg::GLContext & getGLContext(
    ) const;

    fg::GLContext & getGLContext(
    );
};

#endif  // SUCROSE_GL_TEXTURES_H
