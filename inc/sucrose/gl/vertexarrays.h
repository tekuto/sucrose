﻿#ifndef SUCROSE_GL_VERTEXARRAYS_H
#define SUCROSE_GL_VERTEXARRAYS_H

#include "fg/gl/vertexarrays.h"
#include "fg/gl/gl.h"
#include "fg/gl/context.h"
#include "fg/gl/currentcontext.h"

#include <vector>
#include <memory>

struct FgGLVertexArrays
{
    using Names = std::vector< fg::GLuint >;

    struct DeleteNames
    {
        void operator()(
            FgGLVertexArrays *
        ) const;
    };

    using NamesDeleter = std::unique_ptr<
        FgGLVertexArrays
        , DeleteNames
    >;

    fg::GLContext & glContext;

    Names           names;
    NamesDeleter    namesDeleter;

    FgGLVertexArrays(
        fg::GLCurrentContext &
        , fg::GLsizei
    );

    const fg::GLContext & getGLContext(
    ) const;

    fg::GLContext & getGLContext(
    );
};

#endif  // SUCROSE_GL_VERTEXARRAYS_H
