﻿#ifndef SUCROSE_GL_THREADEXECUTOR_H
#define SUCROSE_GL_THREADEXECUTOR_H

#include "fg/gl/threadexecutor.h"
#include "fg/core/state/joiner.h"
#include "sucrose/gl/thread.h"

struct FgGLThreadExecutor
{
    FgGLThread  thread;

    fg::StateJoiner::Unique joinerUnique;
};

#endif  // SUCROSE_GL_THREADEXECUTOR_H
