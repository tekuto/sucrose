﻿#ifndef SUCROSE_GL_CONTEXT_H
#define SUCROSE_GL_CONTEXT_H

#include "fg/gl/context.h"
#include "fg/def/gl/functions.h"
#include "fg/window/window.h"

#include <memory>
#include <X11/Xutil.h>

using GLXContext = struct __GLXcontextRec *;

struct FgGLContext
{
    struct FreeXVisualInfo
    {
        void operator()(
            XVisualInfo *
        ) const;
    };

    using XVisualInfoUnique = std::unique_ptr<
        XVisualInfo
        , FreeXVisualInfo
    >;

    struct GLXContextManager
    {
        struct DestroyGLXContext
        {
            void operator()(
                GLXContextManager *
            ) const;
        };

        using GLXContextDestroyer = std::unique_ptr<
            GLXContextManager
            , DestroyGLXContext
        >;

        Display &   xDisplay;

        GLXContext          glxContext;
        GLXContextDestroyer glxContextDestroyer;
    };

    using GLXContextManagerUnique = std::unique_ptr< GLXContextManager >;

    fg::Window &    window;

    XVisualInfoUnique   xVisualInfoUnique;

    GLXContextManagerUnique glxContextManagerUnique;

#define GL_ARGS( _dummy, ... ) \
    __VA_ARGS__
#define GL_DECLARE_FUNCTION_POINTER( _name, _returnType, _args ) \
    _returnType ( * _name )( GL_ARGS _args );
#define FG_GL_FUNCTION_NUM( _name, _returnType, _args, _values ) \
    GL_DECLARE_FUNCTION_POINTER( gl##_name, _returnType, _args )
#define FG_GL_FUNCTION_PTR( _name, _returnType, _args, _values ) \
    GL_DECLARE_FUNCTION_POINTER( gl##_name, _returnType *, _args )
#define FG_GL_FUNCTION_VOID( _name, _args, _values ) \
    GL_DECLARE_FUNCTION_POINTER( gl##_name, void, _args )

    // OpenGL関数のポインタ宣言
    FG_GL_FUNCTIONS

#undef  FG_GL_FUNCTION_VOID
#undef  FG_GL_FUNCTION_PTR
#undef  FG_GL_FUNCTION_NUM
#undef  GL_DECLARE_FUNCTION_POINTER
#undef  GL_ARGS

    void makeGLCurrent(
    );

    static FgGLContext * releaseGLCurrent(
    );

    void swapBuffers(
    );
};

#endif  // SUCROSE_GL_CONTEXT_H
