﻿#ifndef SUCROSE_GL_FRAGMENTSHADER_H
#define SUCROSE_GL_FRAGMENTSHADER_H

#include "fg/gl/fragmentshader.h"
#include "fg/gl/gl.h"
#include "fg/gl/context.h"
#include "fg/gl/currentcontext.h"

#include <memory>

struct FgGLFragmentShader
{
    using Object = fg::GLuint;

    struct DeleteObject
    {
        void operator()(
            FgGLFragmentShader *
        ) const;
    };

    using ObjectDeleter = std::unique_ptr<
        FgGLFragmentShader
        , DeleteObject
    >;

    fg::GLContext & glContext;

    Object          object;
    ObjectDeleter   objectDeleter;

    FgGLFragmentShader(
        fg::GLCurrentContext &
    );
};

#endif  // SUCROSE_GL_FRAGMENTSHADER_H
