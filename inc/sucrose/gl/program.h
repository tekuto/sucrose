﻿#ifndef SUCROSE_GL_PROGRAM_H
#define SUCROSE_GL_PROGRAM_H

#include "fg/gl/program.h"
#include "fg/gl/gl.h"
#include "fg/gl/context.h"
#include "fg/gl/currentcontext.h"

#include <memory>

struct FgGLProgram
{
    using Object = fg::GLuint;

    struct DeleteObject
    {
        void operator()(
            FgGLProgram *
        ) const;
    };

    using ObjectDeleter = std::unique_ptr<
        FgGLProgram
        , DeleteObject
    >;

    fg::GLContext & glContext;

    Object          object;
    ObjectDeleter   objectDeleter;

    FgGLProgram(
        fg::GLCurrentContext &
    );
};

#endif  // SUCROSE_GL_PROGRAM_H
