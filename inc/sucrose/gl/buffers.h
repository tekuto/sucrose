﻿#ifndef SUCROSE_GL_BUFFERS_H
#define SUCROSE_GL_BUFFERS_H

#include "fg/gl/buffers.h"
#include "fg/gl/gl.h"
#include "fg/gl/context.h"
#include "fg/gl/currentcontext.h"

#include <vector>
#include <memory>

struct FgGLBuffers
{
    using Names = std::vector< fg::GLuint >;

    struct DeleteNames
    {
        void operator()(
            FgGLBuffers *
        ) const;
    };

    using NamesDeleter = std::unique_ptr<
        FgGLBuffers
        , DeleteNames
    >;

    fg::GLContext & glContext;

    Names           names;
    NamesDeleter    namesDeleter;

    FgGLBuffers(
        fg::GLCurrentContext &
        , fg::GLsizei
    );

    const fg::GLContext & getGLContext(
    ) const;

    fg::GLContext & getGLContext(
    );
};

#endif  // SUCROSE_GL_BUFFERS_H
