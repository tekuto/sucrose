﻿#ifndef SUCROSE_GL_VERTEXSHADER_H
#define SUCROSE_GL_VERTEXSHADER_H

#include "fg/gl/vertexshader.h"
#include "fg/gl/gl.h"
#include "fg/gl/context.h"
#include "fg/gl/currentcontext.h"

#include <memory>

struct FgGLVertexShader
{
    using Object = fg::GLuint;

    struct DeleteObject
    {
        void operator()(
            FgGLVertexShader *
        ) const;
    };

    using ObjectDeleter = std::unique_ptr<
        FgGLVertexShader
        , DeleteObject
    >;

    fg::GLContext & glContext;

    Object          object;
    ObjectDeleter   objectDeleter;

    FgGLVertexShader(
        fg::GLCurrentContext &
    );
};

#endif  // SUCROSE_GL_VERTEXSHADER_H
