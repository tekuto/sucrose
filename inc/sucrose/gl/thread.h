﻿#ifndef SUCROSE_GL_THREAD_H
#define SUCROSE_GL_THREAD_H

#include "fg/gl/thread.h"
#include "fg/gl/context.h"
#include "fg/core/state/state.h"
#include "fg/core/state/joiner.h"

struct FgGLThread
{
    fg::State<> &   state;
    fg::GLContext & glContext;
    FgGLThreadProc  procPtr;
    void *          userDataPtr;

    FgGLThread(
        fg::State<> &
        , fg::GLContext &
        , FgGLThreadProc
        , void *
    );

    void execute(
        fg::StateJoiner &
    );
};

#endif  // SUCROSE_GL_THREAD_H
