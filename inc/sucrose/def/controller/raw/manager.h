﻿#ifndef SUCROSE_DEF_CONTROLLER_RAW_MANAGER_H
#define SUCROSE_DEF_CONTROLLER_RAW_MANAGER_H

#include "fg/controller/raw/actionbuffer.h"

namespace sucrose {
    struct RawControllerManager;

    using RawControllerManagerEventProcImpl = void ( * )(
        const fg::RawControllerActionBuffer &
        , void *
    );

    template< typename USER_DATA_T >
    using RawControllerManagerEventProc = void ( * )(
        const fg::RawControllerActionBuffer &
        , USER_DATA_T &
    );
}

#endif  // SUCROSE_DEF_CONTROLLER_RAW_MANAGER_H
