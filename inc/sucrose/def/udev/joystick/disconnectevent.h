﻿#ifndef SUCROSE_DEF_UDEV_JOYSTICK_DISCONNECTEVENT_H
#define SUCROSE_DEF_UDEV_JOYSTICK_DISCONNECTEVENT_H

namespace sucrose {
    struct UdevJoystickDisconnectEvent;

    using UdevJoystickDisconnectEventProcImpl = void ( * )(
        const UdevJoystickDisconnectEvent &
        , void *
    );

    template< typename DATA_T >
    using UdevJoystickDisconnectEventProc = void ( * )(
        const UdevJoystickDisconnectEvent &
        , DATA_T &
    );
}

#endif  // SUCROSE_DEF_UDEV_JOYSTICK_DISCONNECTEVENT_H
