﻿#ifndef SUCROSE_DEF_UDEV_JOYSTICK_BUTTONEVENT_H
#define SUCROSE_DEF_UDEV_JOYSTICK_BUTTONEVENT_H

namespace sucrose {
    struct UdevJoystickButtonEvent;

    using UdevJoystickButtonEventProcImpl = void ( * )(
        const UdevJoystickButtonEvent &
        , void *
    );

    template< typename DATA_T >
    using UdevJoystickButtonEventProc = void ( * )(
        const UdevJoystickButtonEvent &
        , DATA_T &
    );
}

#endif  // SUCROSE_DEF_UDEV_JOYSTICK_BUTTONEVENT_H
