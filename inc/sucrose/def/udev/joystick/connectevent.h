﻿#ifndef SUCROSE_DEF_UDEV_JOYSTICK_CONNECTEVENT_H
#define SUCROSE_DEF_UDEV_JOYSTICK_CONNECTEVENT_H

namespace sucrose {
    struct UdevJoystickConnectEvent;

    using UdevJoystickConnectEventProcImpl = void ( * )(
        const UdevJoystickConnectEvent &
        , void *
    );

    template< typename DATA_T >
    using UdevJoystickConnectEventProc = void ( * )(
        const UdevJoystickConnectEvent &
        , DATA_T &
    );
}

#endif  // SUCROSE_DEF_UDEV_JOYSTICK_CONNECTEVENT_H
