﻿#ifndef SUCROSE_DEF_UDEV_JOYSTICK_AXISEVENT_H
#define SUCROSE_DEF_UDEV_JOYSTICK_AXISEVENT_H

namespace sucrose {
    struct UdevJoystickAxisEvent;

    using UdevJoystickAxisEventProcImpl = void ( * )(
        const UdevJoystickAxisEvent &
        , void *
    );

    template< typename DATA_T >
    using UdevJoystickAxisEventProc = void ( * )(
        const UdevJoystickAxisEvent &
        , DATA_T &
    );
}

#endif  // SUCROSE_DEF_UDEV_JOYSTICK_AXISEVENT_H
