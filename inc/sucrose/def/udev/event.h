﻿#ifndef SUCROSE_DEF_UDEV_EVENT_H
#define SUCROSE_DEF_UDEV_EVENT_H

namespace sucrose {
    struct UdevEvent;

    using UdevEventProcImpl = void ( * )(
        const UdevEvent &
        , void *
    );

    template< typename DATA_T >
    using UdevEventProc = void ( * )(
        const UdevEvent &
        , DATA_T &
    );
}

#endif  // SUCROSE_DEF_UDEV_EVENT_H
