# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

from waflib.Tools import waf_unit_test

import os.path

APPNAME = 'sucrose'
VERSION = '0.52.1'

out = 'build'

taf.PACKAGE_NAME = 'sucrose'

taf.LOAD_TOOLS = [
    'compiler_cxx',
    'waf_unit_test',
    'taf.tools.cpp',
]

cpp.INCLUDES = [
    os.path.join(
        '..',
        'fg',
        'inc',
    ),
]

cpp.TEST_INCLUDES = [
    os.path.join(
        '..',
        'brownsugar',
        'inc',
    ),
]

cpp.TEST_LIBPATH = [
    os.path.join(
        '..',
        'brownsugar',
        'build',
        'brownsugar',
    ),
]

taf.POST_FUNCTIONS = [
    waf_unit_test.summary,
]
