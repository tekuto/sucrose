﻿#include "fg/util/export.h"
#include "sucrose/controller/raw/action.h"
#include "sucrose/controller/raw/connectaction.h"
#include "sucrose/controller/raw/buttonaction.h"
#include "sucrose/controller/raw/axisaction.h"

#include <utility>

const FgRawControllerConnectAction * fgRawControllerActionGetConnectAction(
    const FgRawControllerAction *   _IMPL_PTR
)
{
    return &**( _IMPL_PTR->connectActionUnique );
}

const FgRawControllerButtonAction * fgRawControllerActionGetButtonAction(
    const FgRawControllerAction *   _IMPL_PTR
)
{
    return &**( _IMPL_PTR->buttonActionUnique );
}

const FgRawControllerAxisAction * fgRawControllerActionGetAxisAction(
    const FgRawControllerAction *   _IMPL_PTR
)
{
    return &**( _IMPL_PTR->axisActionUnique );
}

fg::RawControllerAction::ConstUnique FgRawControllerAction::create(
    fg::RawControllerConnectAction::ConstUnique &&  _connectActionUnique
)
{
    return new FgRawControllerAction{
        std::move( _connectActionUnique ),
        nullptr,
        nullptr,
    };
}

fg::RawControllerAction::ConstUnique FgRawControllerAction::create(
    fg::RawControllerButtonAction::ConstUnique &&   _buttonActionUnique
)
{
    return new FgRawControllerAction{
        nullptr,
        std::move( _buttonActionUnique ),
        nullptr,
    };
}

fg::RawControllerAction::ConstUnique FgRawControllerAction::create(
    fg::RawControllerAxisAction::ConstUnique && _axisActionUnique
)
{
    return new FgRawControllerAction{
        nullptr,
        nullptr,
        std::move( _axisActionUnique ),
    };
}
