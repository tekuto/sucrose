﻿#include "fg/util/export.h"
#include "sucrose/controller/raw/buttonaction.h"
#include "fg/controller/raw/button.h"

#include <utility>

const FgRawControllerButton * fgRawControllerButtonGetButton(
    const FgRawControllerButtonAction * _IMPL_PTR
)
{
    return &**( _IMPL_PTR->buttonUnique );
}

bool fgRawControllerButtonIsPressedAction(
    const FgRawControllerButtonAction * _IMPL_PTR
)
{
    return _IMPL_PTR->pressed;
}

fg::RawControllerButtonAction::ConstUnique FgRawControllerButtonAction::createPressAction(
    fg::RawControllerButton::ConstUnique && _buttonUnique
)
{
    return new FgRawControllerButtonAction{
        std::move( _buttonUnique ),
        true,
    };
}

fg::RawControllerButtonAction::ConstUnique FgRawControllerButtonAction::createReleaseAction(
    fg::RawControllerButton::ConstUnique && _buttonUnique
)
{
    return new FgRawControllerButtonAction{
        std::move( _buttonUnique ),
        false,
    };
}
