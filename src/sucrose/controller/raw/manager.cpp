﻿#include "fg/util/export.h"
#include "sucrose/controller/raw/manager.h"
#include "fg/controller/raw/actionbuffer.h"
#include "fg/controller/raw/id.h"
#include "fg/controller/raw/button.h"
#include "fg/controller/raw/axis.h"
#include "fg/core/state/state.h"
#include "fg/core/state/threadbackgroundsingleton.h"
#include "fg/core/state/joiner.h"
#include "sucrose/controller/raw/actionbuffer.h"
#include "sucrose/controller/raw/connectaction.h"
#include "sucrose/controller/raw/buttonaction.h"
#include "sucrose/controller/raw/axisaction.h"
#include "sucrose/controller/raw/action.h"
#include "sucrose/controller/raw/button.h"
#include "sucrose/controller/raw/axis.h"

#include <mutex>
#include <utility>
#include <cstddef>

namespace {
    void swapBuffer(
        sucrose::RawControllerManager &             _manager
        , fg::RawControllerActionBuffer::Unique &   _actionBufferUnique
    )
    {
        auto    lock = std::unique_lock< std::mutex >( _manager.mutex );

        std::swap(
            _manager.actionBufferUnique
            , _actionBufferUnique
        );
    }

    void addAction(
        sucrose::RawControllerManager &             _manager
        , fg::RawControllerAction::ConstUnique &&   _actionUnique
    )
    {
        auto    lock = std::unique_lock< std::mutex >( _manager.mutex );

        ( *( _manager.actionBufferUnique ) )->addAction( std::move( _actionUnique ) );
    }

    void rawControllerManagerEventThread(
        fg::StateThreadBackgroundSingleton< void, sucrose::RawControllerManager > & _thread
    )
    {
        auto &  manager =  _thread.getData();

        auto    actionBufferUnique = FgRawControllerActionBuffer::create();

        swapBuffer(
            manager
            , actionBufferUnique
        );

        if( actionBufferUnique->getSize() <= 0 ) {
            return;
        }

        manager.eventProcPtr(
            *actionBufferUnique
            , manager.userDataPtr
        );
    }

    struct CreateRawControllerConnectActionUnique
    {
        auto operator()(
            fg::RawControllerID::ConstUnique && _idUnique
        ) const
        {
            return FgRawControllerConnectAction::createConnectAction( std::move( _idUnique ) );
        }
    };

    struct CreateRawControllerDisconnectActionUnique
    {
        auto operator()(
            fg::RawControllerID::ConstUnique && _idUnique
        ) const
        {
            return FgRawControllerConnectAction::createDisconnectAction( std::move( _idUnique ) );
        }
    };

    template< typename CREATE_RAW_CONTROLLER_BUTTON_ACTION_UNIQUE_T >
    struct CreateRawControllerButtonActionUnique
    {
        auto operator()(
            fg::RawControllerID::ConstUnique && _idUnique
            , std::size_t                       _index
        ) const
        {
            return CREATE_RAW_CONTROLLER_BUTTON_ACTION_UNIQUE_T()(
                FgRawControllerButton::create(
                    std::move( _idUnique )
                    , _index
                )
            );
        }
    };

    struct CreateRawControllerButtonPressActionUnique
    {
        auto operator()(
            fg::RawControllerButton::ConstUnique && _buttonUnique
        ) const
        {
            return FgRawControllerButtonAction::createPressAction( std::move( _buttonUnique ) );
        }
    };

    struct CreateRawControllerButtonReleaseActionUnique
    {
        auto operator()(
            fg::RawControllerButton::ConstUnique && _buttonUnique
        ) const
        {
            return FgRawControllerButtonAction::createReleaseAction( std::move( _buttonUnique ) );
        }
    };

    struct CreateRawControllerAxisActionUnique
    {
        auto operator()(
            fg::RawControllerID::ConstUnique && _idUnique
            , std::size_t                       _index
            , int                               _value
        ) const
        {
            return FgRawControllerAxisAction::create(
                FgRawControllerAxis::create(
                    std::move( _idUnique )
                    , _index
                )
                , _value
            );
        }
    };

    template<
        typename CREATE_RAW_CONTROLLER_ACTION_UNIQUE_T
        , typename ... ARGS_T
    >
    void executeRawControllerManagerEventThread(
        sucrose::RawControllerManager & _manager
        , const char *                  _MODULE
        , const char *                  _PORT
        , const char *                  _DEVICE
        , ARGS_T ...                    _args
    )
    {
        auto    idUnique = fg::RawControllerID::create(
            _MODULE
            , _PORT
            , _DEVICE
        );

        auto    actionUnique = FgRawControllerAction::create(
            CREATE_RAW_CONTROLLER_ACTION_UNIQUE_T()(
                std::move( idUnique )
                , _args ...
            )
        );

        addAction(
            _manager
            , std::move( actionUnique )
        );

        _manager.state.execute(
            rawControllerManagerEventThread
            , *( _manager.joinerUnique )
            , _manager
        );
    }
}

namespace sucrose {
    RawControllerManager::RawControllerManager(
        FgState &                           _state
        , RawControllerManagerEventProcImpl _eventProcPtr
        , void *                            _userDataPtr
    )
        : state( reinterpret_cast< fg::State<> & >( _state ) )
        , eventProcPtr( _eventProcPtr )
        , userDataPtr( _userDataPtr )
        , actionBufferUnique( FgRawControllerActionBuffer::create() )
        , joinerUnique( fg::StateJoiner::create() )
    {
    }

    RawControllerManager * RawControllerManager::create_(
        FgState &                           _state
        , RawControllerManagerEventProcImpl _eventProcPtr
        , void *                            _userDataPtr
    )
    {
        return new RawControllerManager(
            _state
            , _eventProcPtr
            , _userDataPtr
        );
    }

    void RawControllerManager::destroy(
        RawControllerManager *  _implPtr
    )
    {
        delete _implPtr;
    }

    void RawControllerManager::connect(
        const char *    _MODULE
        , const char *  _PORT
        , const char *  _DEVICE
    )
    {
        executeRawControllerManagerEventThread< CreateRawControllerConnectActionUnique >(
            *this
            , _MODULE
            , _PORT
            , _DEVICE
        );
    }

    void RawControllerManager::disconnect(
        const char *    _MODULE
        , const char *  _PORT
        , const char *  _DEVICE
    )
    {
        executeRawControllerManagerEventThread< CreateRawControllerDisconnectActionUnique >(
            *this
            , _MODULE
            , _PORT
            , _DEVICE
        );
    }

    void RawControllerManager::pressButton(
        const char *    _MODULE
        , const char *  _PORT
        , const char *  _DEVICE
        , std::size_t   _index
    )
    {
        executeRawControllerManagerEventThread< CreateRawControllerButtonActionUnique< CreateRawControllerButtonPressActionUnique > >(
            *this
            , _MODULE
            , _PORT
            , _DEVICE
            , _index
        );
    }

    void RawControllerManager::releaseButton(
        const char *    _MODULE
        , const char *  _PORT
        , const char *  _DEVICE
        , std::size_t   _index
    )
    {
        executeRawControllerManagerEventThread< CreateRawControllerButtonActionUnique< CreateRawControllerButtonReleaseActionUnique > >(
            *this
            , _MODULE
            , _PORT
            , _DEVICE
            , _index
        );
    }

    void RawControllerManager::operateAxis(
        const char *    _MODULE
        , const char *  _PORT
        , const char *  _DEVICE
        , std::size_t   _index
        , int           _value
    )
    {
        executeRawControllerManagerEventThread< CreateRawControllerAxisActionUnique >(
            *this
            , _MODULE
            , _PORT
            , _DEVICE
            , _index
            , _value
        );
    }
}
