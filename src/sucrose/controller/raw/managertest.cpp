﻿#include "fg/util/test.h"
#include "sucrose/controller/raw/manager.h"
#include "fg/controller/raw/actionbuffer.h"
#include "fg/controller/raw/action.h"
#include "fg/controller/raw/connectaction.h"
#include "fg/controller/raw/buttonaction.h"
#include "fg/controller/raw/axisaction.h"
#include "fg/controller/raw/id.h"
#include "fg/controller/raw/button.h"
#include "fg/controller/raw/axis.h"
#include "fg/core/state/state.h"
#include "fg/core/state/creating.h"
#include "brownsugar/statemanager.h"

#include <mutex>
#include <condition_variable>
#include <chrono>
#include <string>

void createTestProc(
    const fg::RawControllerActionBuffer &
    , int &
)
{
}

TEST(
    RawControllerManagerTest
    , Create
)
{
    auto    stateManagerUnique = brownsugar::StateManager::create();
    ASSERT_NE( nullptr, stateManagerUnique.get() );

    auto &  state = stateManagerUnique->getState();

    auto    i = 10;

    const auto  MANAGER_UNIQUE = sucrose::RawControllerManager::create(
        state
        , createTestProc
        , i
    );
    ASSERT_NE( nullptr, MANAGER_UNIQUE.get() );
    const auto &    MANAGER = *MANAGER_UNIQUE;

    ASSERT_EQ( &state, &( MANAGER.state ) );
    ASSERT_EQ( reinterpret_cast< sucrose::RawControllerManagerEventProcImpl >( createTestProc ), MANAGER.eventProcPtr );
    ASSERT_EQ( 10, *static_cast< const int * >( MANAGER.userDataPtr ) );
    ASSERT_NE( nullptr, MANAGER.actionBufferUnique.get() );
    ASSERT_NE( nullptr, MANAGER.joinerUnique.get() );
}

struct ConnectTestData
{
    std::mutex              mutex;
    std::condition_variable cond;

    bool    ended = false;

    std::string module;
    std::string port;
    std::string device;
    bool        connected;
};

void connectTestProc(
    const fg::RawControllerActionBuffer &   _BUFFER
    , ConnectTestData &                     _data
)
{
    auto    lock = std::unique_lock< std::mutex >( _data.mutex );

    _data.ended = true;
    _data.cond.notify_all();

    if( _BUFFER.getSize() < 1 ) {
        return;
    }

    const auto  ACTION_PTR = _BUFFER.getAction( 0 ).getConnectAction();
    if( ACTION_PTR == nullptr ) {
        return;
    }

    const auto &    ID = ACTION_PTR->getID();

    _data.module = ID.getModule();
    _data.port = ID.getPort();
    _data.device = ID.getDevice();
    _data.connected = ACTION_PTR->isConnectedAction();
}

TEST(
    RawControllerManagerTest
    , Connect
)
{
    auto    stateManagerUnique = brownsugar::StateManager::create();
    ASSERT_NE( nullptr, stateManagerUnique.get() );

    auto &  rootState = stateManagerUnique->getState();

    rootState.enter(
        [](
            fg::CreatingState<> &
        )
        {
        }
    );

    ConnectTestData data;

    auto    managerUnique = sucrose::RawControllerManager::create(
        rootState   // 無効ステートでも稼働することを確認
        , connectTestProc
        , data
    );
    ASSERT_NE( nullptr, managerUnique.get() );

    managerUnique->connect(
        "MODULE"
        , "PORT"
        , "DEVICE"
    );

    {
        auto    lock = std::unique_lock< std::mutex >( data.mutex );

        if( data.ended == false ) {
            data.cond.wait_for(
                lock
                , std::chrono::milliseconds( 100 )
            );
        }
    }

    ASSERT_STREQ( "MODULE", data.module.c_str() );
    ASSERT_STREQ( "PORT", data.port.c_str() );
    ASSERT_STREQ( "DEVICE", data.device.c_str() );
    ASSERT_TRUE( data.connected );

    ASSERT_EQ( 0, managerUnique->actionBufferUnique->getSize() );
}

TEST(
    RawControllerManagerTest
    , Disconnect
)
{
    auto    stateManagerUnique = brownsugar::StateManager::create();
    ASSERT_NE( nullptr, stateManagerUnique.get() );

    auto &  rootState = stateManagerUnique->getState();

    rootState.enter(
        [](
            fg::CreatingState<> &
        )
        {
        }
    );

    ConnectTestData data;

    auto    managerUnique = sucrose::RawControllerManager::create(
        rootState   // 無効ステートでも稼働することを確認
        , connectTestProc
        , data
    );
    ASSERT_NE( nullptr, managerUnique.get() );

    managerUnique->disconnect(
        "MODULE"
        , "PORT"
        , "DEVICE"
    );

    {
        auto    lock = std::unique_lock< std::mutex >( data.mutex );

        if( data.ended == false ) {
            data.cond.wait_for(
                lock
                , std::chrono::milliseconds( 100 )
            );
        }
    }

    ASSERT_STREQ( "MODULE", data.module.c_str() );
    ASSERT_STREQ( "PORT", data.port.c_str() );
    ASSERT_STREQ( "DEVICE", data.device.c_str() );
    ASSERT_FALSE( data.connected );

    ASSERT_EQ( 0, managerUnique->actionBufferUnique->getSize() );
}

struct ButtonTestData
{
    std::mutex              mutex;
    std::condition_variable cond;

    bool    ended = false;

    std::string module;
    std::string port;
    std::string device;
    std::size_t index;
    bool        pressed;
};

void buttonTestProc(
    const fg::RawControllerActionBuffer &   _BUFFER
    , ButtonTestData &                      _data
)
{
    auto    lock = std::unique_lock< std::mutex >( _data.mutex );

    _data.ended = true;
    _data.cond.notify_all();

    if( _BUFFER.getSize() < 1 ) {
        return;
    }

    const auto  ACTION_PTR = _BUFFER.getAction( 0 ).getButtonAction();
    if( ACTION_PTR == nullptr ) {
        return;
    }

    const auto &    BUTTON = ACTION_PTR->getButton();

    const auto &    ID = BUTTON.getID();

    _data.module = ID.getModule();
    _data.port = ID.getPort();
    _data.device = ID.getDevice();
    _data.index = BUTTON.getIndex();
    _data.pressed = ACTION_PTR->isPressedAction();
}

TEST(
    RawControllerManagerTest
    , PressButton
)
{
    auto    stateManagerUnique = brownsugar::StateManager::create();
    ASSERT_NE( nullptr, stateManagerUnique.get() );

    auto &  rootState = stateManagerUnique->getState();

    rootState.enter(
        [](
            fg::CreatingState<> &
        )
        {
        }
    );

    ButtonTestData  data;

    auto    managerUnique = sucrose::RawControllerManager::create(
        rootState   // 無効ステートでも稼働することを確認
        , buttonTestProc
        , data
    );
    ASSERT_NE( nullptr, managerUnique.get() );

    managerUnique->pressButton(
        "MODULE"
        , "PORT"
        , "DEVICE"
        , 10
    );

    {
        auto    lock = std::unique_lock< std::mutex >( data.mutex );

        if( data.ended == false ) {
            data.cond.wait_for(
                lock
                , std::chrono::milliseconds( 100 )
            );
        }
    }

    ASSERT_STREQ( "MODULE", data.module.c_str() );
    ASSERT_STREQ( "PORT", data.port.c_str() );
    ASSERT_STREQ( "DEVICE", data.device.c_str() );
    ASSERT_EQ( 10, data.index );
    ASSERT_TRUE( data.pressed );

    ASSERT_EQ( 0, managerUnique->actionBufferUnique->getSize() );
}

TEST(
    RawControllerManagerTest
    , ReleaseButton
)
{
    auto    stateManagerUnique = brownsugar::StateManager::create();
    ASSERT_NE( nullptr, stateManagerUnique.get() );

    auto &  rootState = stateManagerUnique->getState();

    rootState.enter(
        [](
            fg::CreatingState<> &
        )
        {
        }
    );

    ButtonTestData  data;

    auto    managerUnique = sucrose::RawControllerManager::create(
        rootState   // 無効ステートでも稼働することを確認
        , buttonTestProc
        , data
    );
    ASSERT_NE( nullptr, managerUnique.get() );

    managerUnique->releaseButton(
        "MODULE"
        , "PORT"
        , "DEVICE"
        , 10
    );

    {
        auto    lock = std::unique_lock< std::mutex >( data.mutex );

        if( data.ended == false ) {
            data.cond.wait_for(
                lock
                , std::chrono::milliseconds( 100 )
            );
        }
    }

    ASSERT_STREQ( "MODULE", data.module.c_str() );
    ASSERT_STREQ( "PORT", data.port.c_str() );
    ASSERT_STREQ( "DEVICE", data.device.c_str() );
    ASSERT_EQ( 10, data.index );
    ASSERT_FALSE( data.pressed );

    ASSERT_EQ( 0, managerUnique->actionBufferUnique->getSize() );
}

struct AxisTestData
{
    std::mutex              mutex;
    std::condition_variable cond;

    bool    ended = false;

    std::string module;
    std::string port;
    std::string device;
    std::size_t index;
    int         value;
};

void axisTestProc(
    const fg::RawControllerActionBuffer &   _BUFFER
    , AxisTestData &                        _data
)
{
    auto    lock = std::unique_lock< std::mutex >( _data.mutex );

    _data.ended = true;
    _data.cond.notify_all();

    if( _BUFFER.getSize() < 1 ) {
        return;
    }

    const auto  ACTION_PTR = _BUFFER.getAction( 0 ).getAxisAction();
    if( ACTION_PTR == nullptr ) {
        return;
    }

    const auto &    AXIS = ACTION_PTR->getAxis();

    const auto &    ID = AXIS.getID();

    _data.module = ID.getModule();
    _data.port = ID.getPort();
    _data.device = ID.getDevice();
    _data.index = AXIS.getIndex();
    _data.value = ACTION_PTR->getValue();
}

TEST(
    RawControllerManagerTest
    , OperateAxis
)
{
    auto    stateManagerUnique = brownsugar::StateManager::create();
    ASSERT_NE( nullptr, stateManagerUnique.get() );

    auto &  rootState = stateManagerUnique->getState();

    rootState.enter(
        [](
            fg::CreatingState<> &
        )
        {
        }
    );

    AxisTestData    data;

    auto    managerUnique = sucrose::RawControllerManager::create(
        rootState   // 無効ステートでも稼働することを確認
        , axisTestProc
        , data
    );
    ASSERT_NE( nullptr, managerUnique.get() );

    managerUnique->operateAxis(
        "MODULE"
        , "PORT"
        , "DEVICE"
        , 10
        , 20
    );

    {
        auto    lock = std::unique_lock< std::mutex >( data.mutex );

        if( data.ended == false ) {
            data.cond.wait_for(
                lock
                , std::chrono::milliseconds( 100 )
            );
        }
    }

    ASSERT_STREQ( "MODULE", data.module.c_str() );
    ASSERT_STREQ( "PORT", data.port.c_str() );
    ASSERT_STREQ( "DEVICE", data.device.c_str() );
    ASSERT_EQ( 10, data.index );
    ASSERT_EQ( 20, data.value );
}
