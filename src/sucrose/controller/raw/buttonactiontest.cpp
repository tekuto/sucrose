﻿#include "fg/util/test.h"
#include "sucrose/controller/raw/buttonaction.h"
#include "fg/controller/raw/id.h"
#include "sucrose/controller/raw/button.h"

#include <utility>

TEST(
    RawControllerButtonActionTest
    , CreateButtonPressAction
)
{
    auto    idUnique = fg::RawControllerID::create(
        "module"
        , "port"
        , "device"
    );
    ASSERT_NE( nullptr, idUnique.get() );

    auto    buttonUnique = FgRawControllerButton::create(
        std::move( idUnique )
        , 10
    );
    ASSERT_NE( nullptr, buttonUnique.get() );

    const auto  ACTION_UNIQUE = FgRawControllerButtonAction::createPressAction( std::move( buttonUnique ) );
    ASSERT_NE( nullptr, ACTION_UNIQUE.get() );
    const auto &    ACTION = *ACTION_UNIQUE;

    const auto &    BUTTON = *( ACTION->buttonUnique );
    const auto &    ID = BUTTON.getID();
    ASSERT_STREQ( "module", ID.getModule() );
    ASSERT_STREQ( "port", ID.getPort() );
    ASSERT_STREQ( "device", ID.getDevice() );
    ASSERT_EQ( 10, BUTTON.getIndex() );
    ASSERT_TRUE( ACTION->pressed );
}

TEST(
    RawControllerButtonActionTest
    , CreateButtonReleaseAction
)
{
    auto    idUnique = fg::RawControllerID::create(
        "module"
        , "port"
        , "device"
    );
    ASSERT_NE( nullptr, idUnique.get() );

    auto    buttonUnique = FgRawControllerButton::create(
        std::move( idUnique )
        , 10
    );
    ASSERT_NE( nullptr, buttonUnique.get() );

    const auto  ACTION_UNIQUE = FgRawControllerButtonAction::createReleaseAction( std::move( buttonUnique ) );
    ASSERT_NE( nullptr, ACTION_UNIQUE.get() );
    const auto &    ACTION = *ACTION_UNIQUE;

    const auto &    BUTTON = *( ACTION->buttonUnique );
    const auto &    ID = BUTTON.getID();
    ASSERT_STREQ( "module", ID.getModule() );
    ASSERT_STREQ( "port", ID.getPort() );
    ASSERT_STREQ( "device", ID.getDevice() );
    ASSERT_EQ( 10, BUTTON.getIndex() );
    ASSERT_FALSE( ACTION->pressed );
}

TEST(
    RawControllerButtonActionTest
    , GetButton
)
{
    auto    idUnique = fg::RawControllerID::create(
        "module"
        , "port"
        , "device"
    );
    ASSERT_NE( nullptr, idUnique.get() );

    auto    buttonUnique = FgRawControllerButton::create(
        std::move( idUnique )
        , 10
    );
    ASSERT_NE( nullptr, buttonUnique.get() );

    const auto  ACTION_UNIQUE = FgRawControllerButtonAction::createPressAction( std::move( buttonUnique ) );
    ASSERT_NE( nullptr, ACTION_UNIQUE.get() );

    const auto &    BUTTON = ACTION_UNIQUE->getButton();
    const auto &    ID = BUTTON.getID();
    ASSERT_STREQ( "module", ID.getModule() );
    ASSERT_STREQ( "port", ID.getPort() );
    ASSERT_STREQ( "device", ID.getDevice() );
    ASSERT_EQ( 10, BUTTON.getIndex() );
}

TEST(
    RawControllerButtonActionTest
    , IsPressedAction
)
{
    auto    idUnique = fg::RawControllerID::create(
        "module"
        , "port"
        , "device"
    );
    ASSERT_NE( nullptr, idUnique.get() );

    auto    buttonUnique = FgRawControllerButton::create(
        std::move( idUnique )
        , 10
    );
    ASSERT_NE( nullptr, buttonUnique.get() );

    const auto  ACTION_UNIQUE = FgRawControllerButtonAction::createPressAction( std::move( buttonUnique ) );
    ASSERT_NE( nullptr, ACTION_UNIQUE.get() );

    ASSERT_TRUE( ACTION_UNIQUE->isPressedAction() );
}
