﻿#include "fg/util/test.h"
#include "sucrose/controller/raw/id.h"

TEST(
    RawControllerIDTest
    , Create
)
{
    const auto  ID_UNIQUE = fg::RawControllerID::create(
        "MODULE"
        , "PORT"
        , "DEVICE"
    );
    ASSERT_NE( nullptr, ID_UNIQUE.get() );
    const auto &    ID = *ID_UNIQUE;

    ASSERT_STREQ( "MODULE", ID->module.c_str() );
    ASSERT_STREQ( "PORT", ID->port.c_str() );
    ASSERT_STREQ( "DEVICE", ID->device.c_str() );
}

TEST(
    RawControllerIDTest
    , Clone
)
{
    const auto  ID_UNIQUE = fg::RawControllerID::create(
        "MODULE"
        , "PORT"
        , "DEVICE"
    );
    ASSERT_NE( nullptr, ID_UNIQUE.get() );

    const auto  CLONE_UNIQUE = ID_UNIQUE->clone();
    ASSERT_NE( nullptr, CLONE_UNIQUE.get() );
    const auto &    CLONE = *CLONE_UNIQUE;

    ASSERT_STREQ( "MODULE", CLONE->module.c_str() );
    ASSERT_STREQ( "PORT", CLONE->port.c_str() );
    ASSERT_STREQ( "DEVICE", CLONE->device.c_str() );
}

TEST(
    RawControllerIDTest
    , GetModule
)
{
    const auto  ID_UNIQUE = fg::RawControllerID::create(
        "MODULE"
        , "PORT"
        , "DEVICE"
    );
    ASSERT_NE( nullptr, ID_UNIQUE.get() );

    ASSERT_STREQ( "MODULE", ID_UNIQUE->getModule() );
}

TEST(
    RawControllerIDTest
    , GetPort
)
{
    const auto  ID_UNIQUE = fg::RawControllerID::create(
        "MODULE"
        , "PORT"
        , "DEVICE"
    );
    ASSERT_NE( nullptr, ID_UNIQUE.get() );

    ASSERT_STREQ( "PORT", ID_UNIQUE->getPort() );
}

TEST(
    RawControllerIDTest
    , GetDevice
)
{
    const auto  ID_UNIQUE = fg::RawControllerID::create(
        "MODULE"
        , "PORT"
        , "DEVICE"
    );
    ASSERT_NE( nullptr, ID_UNIQUE.get() );

    ASSERT_STREQ( "DEVICE", ID_UNIQUE->getDevice() );
}

TEST(
    RawControllerIDTest
    , Equals
)
{
    const auto  ID_UNIQUE = fg::RawControllerID::create(
        "MODULE"
        , "PORT"
        , "DEVICE"
    );
    ASSERT_NE( nullptr, ID_UNIQUE.get() );

    const auto  ID2_UNIQUE = fg::RawControllerID::create(
        "MODULE"
        , "PORT"
        , "DEVICE"
    );
    ASSERT_NE( nullptr, ID2_UNIQUE.get() );

    ASSERT_TRUE( *ID_UNIQUE == *ID2_UNIQUE );
}

TEST(
    RawControllerIDTest
    , Equals_diffModule
)
{
    const auto  ID_UNIQUE = fg::RawControllerID::create(
        "MODULE"
        , "PORT"
        , "DEVICE"
    );
    ASSERT_NE( nullptr, ID_UNIQUE.get() );

    const auto  ID2_UNIQUE = fg::RawControllerID::create(
        "DIFF"
        , "PORT"
        , "DEVICE"
    );
    ASSERT_NE( nullptr, ID2_UNIQUE.get() );

    ASSERT_FALSE( *ID_UNIQUE == *ID2_UNIQUE );
}

TEST(
    RawControllerIDTest
    , Equals_diffPort
)
{
    const auto  ID_UNIQUE = fg::RawControllerID::create(
        "MODULE"
        , "PORT"
        , "DEVICE"
    );
    ASSERT_NE( nullptr, ID_UNIQUE.get() );

    const auto  ID2_UNIQUE = fg::RawControllerID::create(
        "MODULE"
        , "DIFF"
        , "DEVICE"
    );
    ASSERT_NE( nullptr, ID2_UNIQUE.get() );

    ASSERT_FALSE( *ID_UNIQUE == *ID2_UNIQUE );
}

TEST(
    RawControllerIDTest
    , Equals_diffDevice
)
{
    const auto  ID_UNIQUE = fg::RawControllerID::create(
        "MODULE"
        , "PORT"
        , "DEVICE"
    );
    ASSERT_NE( nullptr, ID_UNIQUE.get() );

    const auto  ID2_UNIQUE = fg::RawControllerID::create(
        "DIFF"
        , "PORT"
        , "DIFF"
    );
    ASSERT_NE( nullptr, ID2_UNIQUE.get() );

    ASSERT_FALSE( *ID_UNIQUE == *ID2_UNIQUE );
}

TEST(
    RawControllerIDTest
    , Less_module
)
{
    const auto  ID_A_UNIQUE = fg::RawControllerID::create(
        "A"
        , "A"
        , "A"
    );
    ASSERT_NE( nullptr, ID_A_UNIQUE.get() );

    const auto  ID_B_UNIQUE = fg::RawControllerID::create(
        "B"
        , "A"
        , "A"
    );
    ASSERT_NE( nullptr, ID_B_UNIQUE.get() );

    const auto  ID_B2_UNIQUE = fg::RawControllerID::create(
        "B"
        , "A"
        , "A"
    );
    ASSERT_NE( nullptr, ID_B2_UNIQUE.get() );

    const auto  ID_C_UNIQUE = fg::RawControllerID::create(
        "C"
        , "A"
        , "A"
    );
    ASSERT_NE( nullptr, ID_C_UNIQUE.get() );

    ASSERT_TRUE( *ID_B_UNIQUE < *ID_C_UNIQUE );
    ASSERT_FALSE( *ID_B_UNIQUE < *ID_A_UNIQUE );
    ASSERT_FALSE( *ID_B_UNIQUE < *ID_B2_UNIQUE );
}

TEST(
    RawControllerIDTest
    , Less_port
)
{
    const auto  ID_A_UNIQUE = fg::RawControllerID::create(
        "A"
        , "A"
        , "A"
    );
    ASSERT_NE( nullptr, ID_A_UNIQUE.get() );

    const auto  ID_B_UNIQUE = fg::RawControllerID::create(
        "A"
        , "B"
        , "A"
    );
    ASSERT_NE( nullptr, ID_B_UNIQUE.get() );

    const auto  ID_B2_UNIQUE = fg::RawControllerID::create(
        "A"
        , "B"
        , "A"
    );
    ASSERT_NE( nullptr, ID_B2_UNIQUE.get() );

    const auto  ID_C_UNIQUE = fg::RawControllerID::create(
        "A"
        , "C"
        , "A"
    );
    ASSERT_NE( nullptr, ID_C_UNIQUE.get() );

    ASSERT_TRUE( *ID_B_UNIQUE < *ID_C_UNIQUE );
    ASSERT_FALSE( *ID_B_UNIQUE < *ID_A_UNIQUE );
    ASSERT_FALSE( *ID_B_UNIQUE < *ID_B2_UNIQUE );
}

TEST(
    RawControllerIDTest
    , Less_device
)
{
    const auto  ID_A_UNIQUE = fg::RawControllerID::create(
        "A"
        , "A"
        , "A"
    );
    ASSERT_NE( nullptr, ID_A_UNIQUE.get() );

    const auto  ID_B_UNIQUE = fg::RawControllerID::create(
        "A"
        , "A"
        , "B"
    );
    ASSERT_NE( nullptr, ID_B_UNIQUE.get() );

    const auto  ID_B2_UNIQUE = fg::RawControllerID::create(
        "A"
        , "A"
        , "B"
    );
    ASSERT_NE( nullptr, ID_B2_UNIQUE.get() );

    const auto  ID_C_UNIQUE = fg::RawControllerID::create(
        "A"
        , "A"
        , "C"
    );
    ASSERT_NE( nullptr, ID_C_UNIQUE.get() );

    ASSERT_TRUE( *ID_B_UNIQUE < *ID_C_UNIQUE );
    ASSERT_FALSE( *ID_B_UNIQUE < *ID_A_UNIQUE );
    ASSERT_FALSE( *ID_B_UNIQUE < *ID_B2_UNIQUE );
}
