﻿#include "fg/util/test.h"
#include "sucrose/controller/raw/action.h"
#include "fg/controller/raw/id.h"
#include "sucrose/controller/raw/connectaction.h"
#include "sucrose/controller/raw/button.h"
#include "sucrose/controller/raw/buttonaction.h"
#include "sucrose/controller/raw/axis.h"
#include "sucrose/controller/raw/axisaction.h"

#include <utility>

TEST(
    RawControllerActionTest
    , CreateConnectAction
)
{
    auto    idUnique = fg::RawControllerID::create(
        "module"
        , "port"
        , "device"
    );
    ASSERT_NE( nullptr, idUnique.get() );

    auto    connectActionUnique = FgRawControllerConnectAction::createConnectAction( std::move( idUnique ) );
    ASSERT_NE( nullptr, connectActionUnique.get() );
    auto &  CONNECT_ACTION = *connectActionUnique;

    const auto  ACTION_UNIQUE = FgRawControllerAction::create( std::move( connectActionUnique ) );
    ASSERT_NE( nullptr, ACTION_UNIQUE.get() );
    const auto &    ACTION = *ACTION_UNIQUE;

    ASSERT_EQ( &CONNECT_ACTION, ACTION->connectActionUnique.get() );
    ASSERT_EQ( nullptr, ACTION->buttonActionUnique.get() );
    ASSERT_EQ( nullptr, ACTION->axisActionUnique.get() );
}

TEST(
    RawControllerActionTest
    , CreateButtonAction
)
{
    auto    idUnique = fg::RawControllerID::create(
        "module"
        , "port"
        , "device"
    );
    ASSERT_NE( nullptr, idUnique.get() );

    auto    buttonUnique = FgRawControllerButton::create(
        std::move( idUnique )
        , 10
    );
    ASSERT_NE( nullptr, buttonUnique.get() );

    auto    buttonActionUnique = FgRawControllerButtonAction::createPressAction( std::move( buttonUnique ) );
    ASSERT_NE( nullptr, buttonActionUnique.get() );
    const auto &    BUTTON_ACTION = *buttonActionUnique;

    const auto  ACTION_UNIQUE = FgRawControllerAction::create( std::move( buttonActionUnique ) );
    ASSERT_NE( nullptr, ACTION_UNIQUE.get() );
    const auto &    ACTION = *ACTION_UNIQUE;

    ASSERT_EQ( nullptr, ACTION->connectActionUnique.get() );
    ASSERT_EQ( &BUTTON_ACTION, ACTION->buttonActionUnique.get() );
    ASSERT_EQ( nullptr, ACTION->axisActionUnique.get() );
}

TEST(
    RawControllerActionTest
    , CreateAxisAction
)
{
    auto    idUnique = fg::RawControllerID::create(
        "module"
        , "port"
        , "device"
    );
    ASSERT_NE( nullptr, idUnique.get() );

    auto    axisUnique = FgRawControllerAxis::create(
        std::move( idUnique )
        , 10
    );
    ASSERT_NE( nullptr, axisUnique.get() );

    auto    axisActionUnique = FgRawControllerAxisAction::create(
        std::move( axisUnique )
        , 20
    );
    ASSERT_NE( nullptr, axisActionUnique.get() );
    const auto &    AXIS_ACTION = *axisActionUnique;

    const auto  ACTION_UNIQUE = FgRawControllerAction::create( std::move( axisActionUnique ) );
    ASSERT_NE( nullptr, ACTION_UNIQUE.get() );
    const auto &    ACTION = *ACTION_UNIQUE;

    ASSERT_EQ( nullptr, ACTION->connectActionUnique.get() );
    ASSERT_EQ( nullptr, ACTION->buttonActionUnique.get() );
    ASSERT_EQ( &AXIS_ACTION, ACTION->axisActionUnique.get() );
}

TEST(
    RawControllerActionTest
    , GetConnectAction
)
{
    auto    idUnique = fg::RawControllerID::create(
        "module"
        , "port"
        , "device"
    );
    ASSERT_NE( nullptr, idUnique.get() );

    auto    connectActionUnique = FgRawControllerConnectAction::createConnectAction( std::move( idUnique ) );
    ASSERT_NE( nullptr, connectActionUnique.get() );
    const auto &  CONNECT_ACTION = *connectActionUnique;

    const auto  ACTION_UNIQUE = FgRawControllerAction::create( std::move( connectActionUnique ) );
    ASSERT_NE( nullptr, ACTION_UNIQUE.get() );

    ASSERT_EQ( &CONNECT_ACTION, ACTION_UNIQUE->getConnectAction() );
}

TEST(
    RawControllerActionTest
    , GetConnectAction_failed
)
{
    auto    idUnique = fg::RawControllerID::create(
        "module"
        , "port"
        , "device"
    );
    ASSERT_NE( nullptr, idUnique.get() );

    auto    buttonUnique = FgRawControllerButton::create(
        std::move( idUnique )
        , 10
    );
    ASSERT_NE( nullptr, buttonUnique.get() );

    auto    buttonActionUnique = FgRawControllerButtonAction::createPressAction( std::move( buttonUnique ) );
    ASSERT_NE( nullptr, buttonActionUnique.get() );

    const auto  ACTION_UNIQUE = FgRawControllerAction::create( std::move( buttonActionUnique ) );
    ASSERT_NE( nullptr, ACTION_UNIQUE.get() );

    ASSERT_EQ( nullptr, ACTION_UNIQUE->getConnectAction() );
}

TEST(
    RawControllerActionTest
    , GewButtonAction
)
{
    auto    idUnique = fg::RawControllerID::create(
        "module"
        , "port"
        , "device"
    );
    ASSERT_NE( nullptr, idUnique.get() );

    auto    buttonUnique = FgRawControllerButton::create(
        std::move( idUnique )
        , 10
    );
    ASSERT_NE( nullptr, buttonUnique.get() );

    auto    buttonActionUnique = FgRawControllerButtonAction::createPressAction( std::move( buttonUnique ) );
    ASSERT_NE( nullptr, buttonActionUnique.get() );
    const auto &    BUTTON_ACTION = *buttonActionUnique;

    const auto  ACTION_UNIQUE = FgRawControllerAction::create( std::move( buttonActionUnique ) );
    ASSERT_NE( nullptr, ACTION_UNIQUE.get() );

    ASSERT_EQ( &BUTTON_ACTION, ACTION_UNIQUE->getButtonAction() );
}

TEST(
    RawControllerActionTest
    , GewButtonAction_failed
)
{
    auto    idUnique = fg::RawControllerID::create(
        "module"
        , "port"
        , "device"
    );
    ASSERT_NE( nullptr, idUnique.get() );

    auto    connectActionUnique = FgRawControllerConnectAction::createConnectAction( std::move( idUnique ) );
    ASSERT_NE( nullptr, connectActionUnique.get() );

    const auto  ACTION_UNIQUE = FgRawControllerAction::create( std::move( connectActionUnique ) );
    ASSERT_NE( nullptr, ACTION_UNIQUE.get() );

    ASSERT_EQ( nullptr, ACTION_UNIQUE->getButtonAction() );
}

TEST(
    RawControllerActionTest
    , GetAxisAction
)
{
    auto    idUnique = fg::RawControllerID::create(
        "module"
        , "port"
        , "device"
    );
    ASSERT_NE( nullptr, idUnique.get() );

    auto    axisUnique = FgRawControllerAxis::create(
        std::move( idUnique )
        , 10
    );
    ASSERT_NE( nullptr, axisUnique.get() );

    auto    axisActionUnique = FgRawControllerAxisAction::create(
        std::move( axisUnique )
        , 20
    );
    ASSERT_NE( nullptr, axisActionUnique.get() );
    const auto &    AXIS_ACTION = *axisActionUnique;

    const auto  ACTION_UNIQUE = FgRawControllerAction::create( std::move( axisActionUnique ) );
    ASSERT_NE( nullptr, ACTION_UNIQUE.get() );

    ASSERT_EQ( &AXIS_ACTION, ACTION_UNIQUE->getAxisAction() );
}

TEST(
    RawControllerActionTest
    , GetAxisAction_failed
)
{
    auto    idUnique = fg::RawControllerID::create(
        "module"
        , "port"
        , "device"
    );
    ASSERT_NE( nullptr, idUnique.get() );

    auto    connectActionUnique = FgRawControllerConnectAction::createConnectAction( std::move( idUnique ) );
    ASSERT_NE( nullptr, connectActionUnique.get() );

    const auto  ACTION_UNIQUE = FgRawControllerAction::create( std::move( connectActionUnique ) );
    ASSERT_NE( nullptr, ACTION_UNIQUE.get() );

    ASSERT_EQ( nullptr, ACTION_UNIQUE->getAxisAction() );
}
