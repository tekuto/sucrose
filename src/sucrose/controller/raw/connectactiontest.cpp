﻿#include "fg/util/test.h"
#include "sucrose/controller/raw/connectaction.h"
#include "fg/controller/raw/id.h"

#include <utility>

TEST(
    RawControllerConnectActionTest
    , CreateConnectAction
)
{
    auto    idUnique = fg::RawControllerID::create(
        "module"
        , "port"
        , "device"
    );
    ASSERT_NE( nullptr, idUnique.get() );

    const auto  ACTION_UNIQUE = FgRawControllerConnectAction::createConnectAction( std::move( idUnique ) );
    ASSERT_NE( nullptr, ACTION_UNIQUE.get() );
    const auto &    ACTION = *ACTION_UNIQUE;

    const auto &    ID = *( ACTION->idUnique );
    ASSERT_STREQ( "module", ID.getModule() );
    ASSERT_STREQ( "port", ID.getPort() );
    ASSERT_STREQ( "device", ID.getDevice() );
    ASSERT_TRUE( ACTION->connected );
}

TEST(
    RawControllerConnectActionTest
    , CreateDisconnectAction
)
{
    auto    idUnique = fg::RawControllerID::create(
        "module"
        , "port"
        , "device"
    );
    ASSERT_NE( nullptr, idUnique.get() );

    const auto  ACTION_UNIQUE = FgRawControllerConnectAction::createDisconnectAction( std::move( idUnique ) );
    ASSERT_NE( nullptr, ACTION_UNIQUE.get() );
    const auto &    ACTION = *ACTION_UNIQUE;

    const auto &    ID = *( ACTION->idUnique );
    ASSERT_STREQ( "module", ID.getModule() );
    ASSERT_STREQ( "port", ID.getPort() );
    ASSERT_STREQ( "device", ID.getDevice() );
    ASSERT_FALSE( ACTION->connected );
}

TEST(
    RawControllerConnectActionTest
    , GetID
)
{
    auto    idUnique = fg::RawControllerID::create(
        "module"
        , "port"
        , "device"
    );
    ASSERT_NE( nullptr, idUnique.get() );

    const auto  ACTION_UNIQUE = FgRawControllerConnectAction::createConnectAction( std::move( idUnique ) );
    ASSERT_NE( nullptr, ACTION_UNIQUE.get() );

    const auto &    ID = ACTION_UNIQUE->getID();
    ASSERT_STREQ( "module", ID.getModule() );
    ASSERT_STREQ( "port", ID.getPort() );
    ASSERT_STREQ( "device", ID.getDevice() );
}

TEST(
    RawControllerConnectActionTest
    , IsConnectedAction
)
{
    auto    idUnique = fg::RawControllerID::create(
        "module"
        , "port"
        , "device"
    );
    ASSERT_NE( nullptr, idUnique.get() );

    const auto  ACTION_UNIQUE = FgRawControllerConnectAction::createConnectAction( std::move( idUnique ) );
    ASSERT_NE( nullptr, ACTION_UNIQUE.get() );

    ASSERT_TRUE( ACTION_UNIQUE->isConnectedAction() );
}
