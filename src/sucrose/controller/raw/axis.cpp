﻿#include "fg/util/export.h"
#include "sucrose/controller/raw/axis.h"
#include "fg/controller/raw/id.h"

#include <utility>

FgRawControllerAxis * fgRawControllerAxisCreate(
    const FgRawControllerID *   _ID_PTR
    , size_t                    _index
)
{
    return new FgRawControllerAxis{
        reinterpret_cast< const fg::RawControllerID * >( _ID_PTR )->clone(),
        _index,
    };
}

void fgRawControllerAxisDestroy(
    FgRawControllerAxis *   _implPtr
)
{
    delete _implPtr;
}

FgRawControllerAxis * fgRawControllerAxisClone(
    const FgRawControllerAxis * _ORG_PTR
)
{
    return fgRawControllerAxisCreate(
        &**( _ORG_PTR->idUnique )
        , _ORG_PTR->index
    );
}

const FgRawControllerID * fgRawControllerAxisGetID(
    const FgRawControllerAxis * _IMPL_PTR
)
{
    return &**( _IMPL_PTR->idUnique );
}

size_t fgRawControllerAxisGetIndex(
    const FgRawControllerAxis * _IMPL_PTR
)
{
    return _IMPL_PTR->index;
}

bool fgRawControllerAxisEquals(
    const FgRawControllerAxis *     _IMPL_PTR
    , const FgRawControllerAxis *   _OTHER_PTR
)
{
    if( ( *( _IMPL_PTR->idUnique ) == *( _OTHER_PTR->idUnique ) ) == false ) {
        return false;
    }

    if( _IMPL_PTR->index != _OTHER_PTR->index ) {
        return false;
    }

    return true;
}

bool fgRawControllerAxisLess(
    const FgRawControllerAxis *     _IMPL_PTR
    , const FgRawControllerAxis *   _OTHER_PTR
)
{
    const auto &    ID1 = *( _IMPL_PTR->idUnique );
    const auto &    ID2 = *( _OTHER_PTR->idUnique );
    if( ( ID1 == ID2 ) == false ) {
        return ID1 < ID2;
    }

    const auto &    INDEX1 = _IMPL_PTR->index;
    const auto &    INDEX2 = _OTHER_PTR->index;
    if( INDEX1 != INDEX2 ) {
        return INDEX1 < INDEX2;
    }

    return false;
}

fg::RawControllerAxis::ConstUnique FgRawControllerAxis::create(
    fg::RawControllerID::ConstUnique && _idUnique
    , std::size_t                       _index
)
{
    return new FgRawControllerAxis{
        std::move( _idUnique ),
        _index,
    };
}
