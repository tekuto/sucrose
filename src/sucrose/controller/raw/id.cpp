﻿#include "fg/util/export.h"
#include "sucrose/controller/raw/id.h"

FgRawControllerID * fgRawControllerIDCreate(
    const char *    _MODULE
    , const char *  _PORT
    , const char *  _DEVICE
)
{
    return new FgRawControllerID{
        _MODULE,
        _PORT,
        _DEVICE,
    };
}

void fgRawControllerIDDestroy(
    FgRawControllerID * _implPtr
)
{
    delete _implPtr;
}

FgRawControllerID * fgRawControllerIDClone(
    const FgRawControllerID *   _ORG_PTR
)
{
    return new FgRawControllerID( *_ORG_PTR );
}

const char * fgRawControllerIDGetModule(
    const FgRawControllerID *   _IMPL_PTR
)
{
    return _IMPL_PTR->module.c_str();
}

const char * fgRawControllerIDGetPort(
    const FgRawControllerID *   _IMPL_PTR
)
{
    return _IMPL_PTR->port.c_str();
}

const char * fgRawControllerIDGetDevice(
    const FgRawControllerID *   _IMPL_PTR
)
{
    return _IMPL_PTR->device.c_str();
}

bool fgRawControllerIDEquals(
    const FgRawControllerID *   _IMPL_PTR
    , const FgRawControllerID * _OTHER_PTR
)
{
    if( _IMPL_PTR->module != _OTHER_PTR->module ) {
        return false;
    }

    if( _IMPL_PTR->port != _OTHER_PTR->port ) {
        return false;
    }

    if( _IMPL_PTR->device != _OTHER_PTR->device ) {
        return false;
    }

    return true;
}

bool fgRawControllerIDLess(
    const FgRawControllerID *   _IMPL_PTR
    , const FgRawControllerID * _OTHER_PTR
)
{
    const auto  COMPARE_MODULE = _IMPL_PTR->module.compare( _OTHER_PTR->module );
    if( COMPARE_MODULE < 0 ) {
        return true;
    } else if( COMPARE_MODULE > 0 ) {
        return false;
    }

    const auto  COMPARE_PORT = _IMPL_PTR->port.compare( _OTHER_PTR->port );
    if( COMPARE_PORT < 0 ) {
        return true;
    } else if( COMPARE_PORT > 0 ) {
        return false;
    }

    const auto  COMPARE_DEVICE = _IMPL_PTR->device.compare( _OTHER_PTR->device );
    if( COMPARE_DEVICE < 0 ) {
        return true;
    } else if( COMPARE_DEVICE > 0 ) {
        return false;
    }

    return false;
}
