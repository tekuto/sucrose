﻿#include "fg/util/test.h"
#include "sucrose/controller/raw/button.h"
#include "sucrose/controller/raw/id.h"

#include <utility>

TEST(
    RawControllerButtonTest
    , Create_fg
)
{
    const auto  ID_UNIQUE = fg::RawControllerID::create(
        "module"
        , "port"
        , "device"
    );
    ASSERT_NE( nullptr, ID_UNIQUE.get() );

    const auto  BUTTON_UNIQUE = fg::RawControllerButton::create(
        *ID_UNIQUE
        , 10
    );
    ASSERT_NE( nullptr, BUTTON_UNIQUE.get() );
    const auto &    BUTTON = *BUTTON_UNIQUE;

    const auto &    ID = *( BUTTON->idUnique );
    ASSERT_STREQ( "module", ID->module.c_str() );
    ASSERT_STREQ( "port", ID->port.c_str() );
    ASSERT_STREQ( "device", ID->device.c_str() );
    ASSERT_EQ( 10, BUTTON->index );
}

TEST(
    RawControllerButtonTest
    , Create_sucrose
)
{
    auto    idUnique = fg::RawControllerID::create(
        "module"
        , "port"
        , "device"
    );
    ASSERT_NE( nullptr, idUnique.get() );

    const auto  BUTTON_UNIQUE = FgRawControllerButton::create(
        std::move( idUnique )
        , 10
    );
    ASSERT_NE( nullptr, BUTTON_UNIQUE.get() );
    const auto &    BUTTON = *BUTTON_UNIQUE;

    const auto &    ID = *( BUTTON->idUnique );
    ASSERT_STREQ( "module", ID->module.c_str() );
    ASSERT_STREQ( "port", ID->port.c_str() );
    ASSERT_STREQ( "device", ID->device.c_str() );
    ASSERT_EQ( 10, BUTTON->index );
}

TEST(
    RawControllerButtonTest
    , Clone
)
{
    const auto  ID_UNIQUE = fg::RawControllerID::create(
        "module"
        , "port"
        , "device"
    );
    ASSERT_NE( nullptr, ID_UNIQUE.get() );

    const auto  BUTTON_UNIQUE = fg::RawControllerButton::create(
        *ID_UNIQUE
        , 10
    );
    ASSERT_NE( nullptr, BUTTON_UNIQUE.get() );

    const auto  CLONE_UNIQUE = BUTTON_UNIQUE->clone();
    ASSERT_NE( nullptr, CLONE_UNIQUE.get() );
    const auto &    CLONE = *CLONE_UNIQUE;

    const auto &    ID = *( CLONE->idUnique );
    ASSERT_STREQ( "module", ID->module.c_str() );
    ASSERT_STREQ( "port", ID->port.c_str() );
    ASSERT_STREQ( "device", ID->device.c_str() );
    ASSERT_EQ( 10, CLONE->index );
}

TEST(
    RawControllerButtonTest
    , GetID
)
{
    auto    idUnique = fg::RawControllerID::create(
        "module"
        , "port"
        , "device"
    );
    ASSERT_NE( nullptr, idUnique.get() );

    const auto  BUTTON_UNIQUE = FgRawControllerButton::create(
        std::move( idUnique )
        , 10
    );
    ASSERT_NE( nullptr, BUTTON_UNIQUE.get() );

    const auto &    ID = BUTTON_UNIQUE->getID();
    ASSERT_STREQ( "module", ID->module.c_str() );
    ASSERT_STREQ( "port", ID->port.c_str() );
    ASSERT_STREQ( "device", ID->device.c_str() );
}

TEST(
    RawControllerButtonTest
    , GetIndex
)
{
    auto    idUnique = fg::RawControllerID::create(
        "module"
        , "port"
        , "device"
    );
    ASSERT_NE( nullptr, idUnique.get() );

    const auto  BUTTON_UNIQUE = FgRawControllerButton::create(
        std::move( idUnique )
        , 10
    );
    ASSERT_NE( nullptr, BUTTON_UNIQUE.get() );

    ASSERT_EQ( 10, BUTTON_UNIQUE->getIndex() );
}

TEST(
    RawControllerButtonTest
    , Equals
)
{
    const auto  ID_UNIQUE = fg::RawControllerID::create(
        "module"
        , "port"
        , "device"
    );
    ASSERT_NE( nullptr, ID_UNIQUE.get() );
    const auto &    ID = *ID_UNIQUE;

    const auto  BUTTON_UNIQUE = fg::RawControllerButton::create(
        ID
        , 10
    );
    ASSERT_NE( nullptr, BUTTON_UNIQUE.get() );

    const auto  BUTTON2_UNIQUE = fg::RawControllerButton::create(
        ID
        , 10
    );
    ASSERT_NE( nullptr, BUTTON2_UNIQUE.get() );

    ASSERT_TRUE( *BUTTON_UNIQUE == *BUTTON2_UNIQUE );
}

TEST(
    RawControllerButtonTest
    , Equals_diffID
)
{
    const auto  ID_UNIQUE = fg::RawControllerID::create(
        "module"
        , "port"
        , "device"
    );
    ASSERT_NE( nullptr, ID_UNIQUE.get() );

    const auto  BUTTON_UNIQUE = fg::RawControllerButton::create(
        *ID_UNIQUE
        , 10
    );
    ASSERT_NE( nullptr, BUTTON_UNIQUE.get() );

    const auto  ID2_UNIQUE = fg::RawControllerID::create(
        "diff"
        , "port"
        , "device"
    );
    ASSERT_NE( nullptr, ID2_UNIQUE.get() );

    const auto  BUTTON2_UNIQUE = fg::RawControllerButton::create(
        *ID2_UNIQUE
        , 10
    );
    ASSERT_NE( nullptr, BUTTON2_UNIQUE.get() );

    ASSERT_FALSE( *BUTTON_UNIQUE == *BUTTON2_UNIQUE );
}

TEST(
    RawControllerButtonTest
    , Equals_diffIndex
)
{
    const auto  ID_UNIQUE = fg::RawControllerID::create(
        "module"
        , "port"
        , "device"
    );
    ASSERT_NE( nullptr, ID_UNIQUE.get() );
    const auto &    ID = *ID_UNIQUE;

    const auto  BUTTON_UNIQUE = fg::RawControllerButton::create(
        ID
        , 10
    );
    ASSERT_NE( nullptr, BUTTON_UNIQUE.get() );

    const auto  BUTTON2_UNIQUE = fg::RawControllerButton::create(
        ID
        , 20
    );
    ASSERT_NE( nullptr, BUTTON2_UNIQUE.get() );

    ASSERT_FALSE( *BUTTON_UNIQUE == *BUTTON2_UNIQUE );
}

TEST(
    RawControllerButtonTest
    , Less_ID
)
{
    const auto  ID_A_UNIQUE = fg::RawControllerID::create(
        "A"
        , "A"
        , "A"
    );
    ASSERT_NE( nullptr, ID_A_UNIQUE.get() );

    const auto  BUTTON_A_UNIQUE = fg::RawControllerButton::create(
        *ID_A_UNIQUE
        , 10
    );
    ASSERT_NE( nullptr, BUTTON_A_UNIQUE.get() );

    const auto  ID_B_UNIQUE = fg::RawControllerID::create(
        "B"
        , "A"
        , "A"
    );
    ASSERT_NE( nullptr, ID_B_UNIQUE.get() );

    const auto  BUTTON_B_UNIQUE = fg::RawControllerButton::create(
        *ID_B_UNIQUE
        , 10
    );
    ASSERT_NE( nullptr, BUTTON_B_UNIQUE.get() );

    const auto  ID_B2_UNIQUE = fg::RawControllerID::create(
        "B"
        , "A"
        , "A"
    );
    ASSERT_NE( nullptr, ID_B2_UNIQUE.get() );

    const auto  BUTTON_B2_UNIQUE = fg::RawControllerButton::create(
        *ID_B2_UNIQUE
        , 10
    );
    ASSERT_NE( nullptr, BUTTON_B2_UNIQUE.get() );

    const auto  ID_C_UNIQUE = fg::RawControllerID::create(
        "C"
        , "A"
        , "A"
    );
    ASSERT_NE( nullptr, ID_C_UNIQUE.get() );

    const auto  BUTTON_C_UNIQUE = fg::RawControllerButton::create(
        *ID_C_UNIQUE
        , 10
    );
    ASSERT_NE( nullptr, BUTTON_C_UNIQUE.get() );

    ASSERT_TRUE( *BUTTON_B_UNIQUE < *BUTTON_C_UNIQUE );
    ASSERT_FALSE( *BUTTON_B_UNIQUE < *BUTTON_A_UNIQUE );
    ASSERT_FALSE( *BUTTON_B_UNIQUE < *BUTTON_B2_UNIQUE );
}

TEST(
    RawControllerButtonTest
    , Less_index
)
{
    const auto  ID_UNIQUE = fg::RawControllerID::create(
        "module"
        , "port"
        , "device"
    );
    ASSERT_NE( nullptr, ID_UNIQUE.get() );
    const auto &    ID = *ID_UNIQUE;

    const auto  BUTTON_A_UNIQUE = fg::RawControllerButton::create(
        ID
        , 10
    );
    ASSERT_NE( nullptr, BUTTON_A_UNIQUE.get() );

    const auto  BUTTON_B_UNIQUE = fg::RawControllerButton::create(
        ID
        , 20
    );
    ASSERT_NE( nullptr, BUTTON_B_UNIQUE.get() );

    const auto  BUTTON_B2_UNIQUE = fg::RawControllerButton::create(
        ID
        , 20
    );
    ASSERT_NE( nullptr, BUTTON_B2_UNIQUE.get() );

    const auto  BUTTON_C_UNIQUE = fg::RawControllerButton::create(
        ID
        , 30
    );
    ASSERT_NE( nullptr, BUTTON_C_UNIQUE.get() );

    ASSERT_TRUE( *BUTTON_B_UNIQUE < *BUTTON_C_UNIQUE );
    ASSERT_FALSE( *BUTTON_B_UNIQUE < *BUTTON_A_UNIQUE );
    ASSERT_FALSE( *BUTTON_B_UNIQUE < *BUTTON_B2_UNIQUE );
}
