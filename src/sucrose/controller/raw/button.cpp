﻿#include "fg/util/export.h"
#include "sucrose/controller/raw/button.h"
#include "fg/controller/raw/id.h"

#include <utility>
#include <cstddef>

FgRawControllerButton * fgRawControllerButtonCreate(
    const FgRawControllerID *   _ID_PTR
    , size_t                    _index
)
{
    return new FgRawControllerButton{
        reinterpret_cast< const fg::RawControllerID * >( _ID_PTR )->clone(),
        _index,
    };
}

void fgRawControllerButtonDestroy(
    FgRawControllerButton * _implPtr
)
{
    delete _implPtr;
}

FgRawControllerButton * fgRawControllerButtonClone(
    const FgRawControllerButton *   _ORG_PTR
)
{
    return fgRawControllerButtonCreate(
        &**( _ORG_PTR->idUnique )
        , _ORG_PTR->index
    );
}

const FgRawControllerID * fgRawControllerButtonGetID(
    const FgRawControllerButton *   _IMPL_PTR
)
{
    return &**( _IMPL_PTR->idUnique );
}

size_t fgRawControllerButtonGetIndex(
    const FgRawControllerButton *   _IMPL_PTR
)
{
    return _IMPL_PTR->index;
}

bool fgRawControllerButtonEquals(
    const FgRawControllerButton *   _IMPL_PTR
    , const FgRawControllerButton * _OTHER_PTR
)
{
    if( ( *( _IMPL_PTR->idUnique ) == *( _OTHER_PTR->idUnique ) ) == false ) {
        return false;
    }

    if( _IMPL_PTR->index != _OTHER_PTR->index ) {
        return false;
    }

    return true;
}

bool fgRawControllerButtonLess(
    const FgRawControllerButton *   _IMPL_PTR
    , const FgRawControllerButton * _OTHER_PTR
)
{
    const auto &    ID1 = *( _IMPL_PTR->idUnique );
    const auto &    ID2 = *( _OTHER_PTR->idUnique );
    if( ( ID1 == ID2 ) == false ) {
        return ID1 < ID2;
    }

    const auto &    INDEX1 = _IMPL_PTR->index;
    const auto &    INDEX2 = _OTHER_PTR->index;
    if( INDEX1 != INDEX2 ) {
        return INDEX1 < INDEX2;
    }

    return false;
}

fg::RawControllerButton::ConstUnique FgRawControllerButton::create(
    fg::RawControllerID::ConstUnique && _idUnique
    , std::size_t                       _index
)
{
    return new FgRawControllerButton{
        std::move( _idUnique ),
        _index,
    };
}
