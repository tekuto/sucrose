﻿#include "fg/util/export.h"
#include "sucrose/controller/raw/connectaction.h"
#include "fg/controller/raw/id.h"

#include <utility>

const FgRawControllerID * fgRawControllerConnectActionGetID(
    const FgRawControllerConnectAction *    _IMPL_PTR
)
{
    return &**( _IMPL_PTR->idUnique );
}

bool fgRawControllerConnectActionIsConnectedAction(
    const FgRawControllerConnectAction *    _IMPL_PTR
)
{
    return _IMPL_PTR->connected;
}

fg::RawControllerConnectAction::ConstUnique FgRawControllerConnectAction::createConnectAction(
    fg::RawControllerID::ConstUnique && _idUnique
)
{
    return new FgRawControllerConnectAction{
        std::move( _idUnique ),
        true,
    };
}

fg::RawControllerConnectAction::ConstUnique FgRawControllerConnectAction::createDisconnectAction(
    fg::RawControllerID::ConstUnique && _idUnique
)
{
    return new FgRawControllerConnectAction{
        std::move( _idUnique ),
        false,
    };
}
