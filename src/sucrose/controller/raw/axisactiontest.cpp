﻿#include "fg/util/test.h"
#include "sucrose/controller/raw/axisaction.h"
#include "fg/controller/raw/id.h"
#include "sucrose/controller/raw/axis.h"

#include <utility>

TEST(
    RawControllerAxisActionTest
    , Create
)
{
    auto    idUnique = fg::RawControllerID::create(
        "module"
        , "port"
        , "device"
    );
    ASSERT_NE( nullptr, idUnique.get() );

    auto    axisUnique = FgRawControllerAxis::create(
        std::move( idUnique )
        , 10
    );
    ASSERT_NE( nullptr, axisUnique.get() );

    const auto  ACTION_UNIQUE = FgRawControllerAxisAction::create(
        std::move( axisUnique )
        , 20
    );
    ASSERT_NE( nullptr, ACTION_UNIQUE.get() );
    const auto &    ACTION = *ACTION_UNIQUE;

    const auto &    AXIS = *( ACTION->axisUnique );
    const auto &    ID = AXIS.getID();
    ASSERT_STREQ( "module", ID.getModule() );
    ASSERT_STREQ( "port", ID.getPort() );
    ASSERT_STREQ( "device", ID.getDevice() );
    ASSERT_EQ( 10, AXIS.getIndex() );
    ASSERT_EQ( 20, ACTION->value );
}

TEST(
    RawControllerAxisActionTest
    , GetAxis
)
{
    auto    idUnique = fg::RawControllerID::create(
        "module"
        , "port"
        , "device"
    );
    ASSERT_NE( nullptr, idUnique.get() );

    auto    axisUnique = FgRawControllerAxis::create(
        std::move( idUnique )
        , 10
    );
    ASSERT_NE( nullptr, axisUnique.get() );

    const auto  ACTION_UNIQUE = FgRawControllerAxisAction::create(
        std::move( axisUnique )
        , 20
    );
    ASSERT_NE( nullptr, ACTION_UNIQUE.get() );

    const auto &    AXIS = ACTION_UNIQUE->getAxis();
    const auto &    ID = AXIS.getID();
    ASSERT_STREQ( "module", ID.getModule() );
    ASSERT_STREQ( "port", ID.getPort() );
    ASSERT_STREQ( "device", ID.getDevice() );
    ASSERT_EQ( 10, AXIS.getIndex() );
}

TEST(
    RawControllerAxisActionTest
    , GetValue
)
{
    auto    idUnique = fg::RawControllerID::create(
        "module"
        , "port"
        , "device"
    );
    ASSERT_NE( nullptr, idUnique.get() );

    auto    axisUnique = FgRawControllerAxis::create(
        std::move( idUnique )
        , 10
    );
    ASSERT_NE( nullptr, axisUnique.get() );

    const auto  ACTION_UNIQUE = FgRawControllerAxisAction::create(
        std::move( axisUnique )
        , 20
    );
    ASSERT_NE( nullptr, ACTION_UNIQUE.get() );

    ASSERT_EQ( 20, ACTION_UNIQUE->getValue() );
}
