﻿#include "fg/util/test.h"
#include "sucrose/controller/raw/axis.h"
#include "sucrose/controller/raw/id.h"

#include <utility>

TEST(
    RawControllerAxisTest
    , Create_fg
)
{
    const auto  ID_UNIQUE = fg::RawControllerID::create(
        "module"
        , "port"
        , "device"
    );
    ASSERT_NE( nullptr, ID_UNIQUE.get() );

    const auto  AXIS_UNIQUE = fg::RawControllerAxis::create(
        *ID_UNIQUE
        , 10
    );
    ASSERT_NE( nullptr, AXIS_UNIQUE.get() );
    const auto &    AXIS = *AXIS_UNIQUE;

    const auto &    ID = *( AXIS->idUnique );
    ASSERT_STREQ( "module", ID->module.c_str() );
    ASSERT_STREQ( "port", ID->port.c_str() );
    ASSERT_STREQ( "device", ID->device.c_str() );
    ASSERT_EQ( 10, AXIS->index );
}

TEST(
    RawControllerAxisTest
    , Create_sucrose
)
{
    auto    idUnique = fg::RawControllerID::create(
        "module"
        , "port"
        , "device"
    );
    ASSERT_NE( nullptr, idUnique.get() );

    const auto  AXIS_UNIQUE = FgRawControllerAxis::create(
        std::move( idUnique )
        , 10
    );
    ASSERT_NE( nullptr, AXIS_UNIQUE.get() );
    const auto &    AXIS = *AXIS_UNIQUE;

    const auto &    ID = *( AXIS->idUnique );
    ASSERT_STREQ( "module", ID->module.c_str() );
    ASSERT_STREQ( "port", ID->port.c_str() );
    ASSERT_STREQ( "device", ID->device.c_str() );
    ASSERT_EQ( 10, AXIS->index );
}

TEST(
    RawControllerAxisTest
    , Clone
)
{
    const auto  ID_UNIQUE = fg::RawControllerID::create(
        "module"
        , "port"
        , "device"
    );
    ASSERT_NE( nullptr, ID_UNIQUE.get() );

    const auto  AXIS_UNIQUE = fg::RawControllerAxis::create(
        *ID_UNIQUE
        , 10
    );
    ASSERT_NE( nullptr, AXIS_UNIQUE.get() );

    const auto  CLONE_UNIQUE = AXIS_UNIQUE->clone();
    ASSERT_NE( nullptr, CLONE_UNIQUE.get() );
    const auto &    CLONE = *CLONE_UNIQUE;

    const auto &    ID = *( CLONE->idUnique );
    ASSERT_STREQ( "module", ID->module.c_str() );
    ASSERT_STREQ( "port", ID->port.c_str() );
    ASSERT_STREQ( "device", ID->device.c_str() );
    ASSERT_EQ( 10, CLONE->index );
}

TEST(
    RawControllerAxisTest
    , GetID
)
{
    auto    idUnique = fg::RawControllerID::create(
        "module"
        , "port"
        , "device"
    );
    ASSERT_NE( nullptr, idUnique.get() );

    const auto  AXIS_UNIQUE = FgRawControllerAxis::create(
        std::move( idUnique )
        , 10
    );
    ASSERT_NE( nullptr, AXIS_UNIQUE.get() );

    const auto &    ID = AXIS_UNIQUE->getID();
    ASSERT_STREQ( "module", ID->module.c_str() );
    ASSERT_STREQ( "port", ID->port.c_str() );
    ASSERT_STREQ( "device", ID->device.c_str() );
}

TEST(
    RawControllerAxisTest
    , GetIndex
)
{
    auto    idUnique = fg::RawControllerID::create(
        "module"
        , "port"
        , "device"
    );
    ASSERT_NE( nullptr, idUnique.get() );

    const auto  AXIS_UNIQUE = FgRawControllerAxis::create(
        std::move( idUnique )
        , 10
    );
    ASSERT_NE( nullptr, AXIS_UNIQUE.get() );

    ASSERT_EQ( 10, AXIS_UNIQUE->getIndex() );
}

TEST(
    RawControllerAxisTest
    , Equals
)
{
    const auto  ID_UNIQUE = fg::RawControllerID::create(
        "module"
        , "port"
        , "device"
    );
    ASSERT_NE( nullptr, ID_UNIQUE.get() );
    const auto &    ID = *ID_UNIQUE;

    const auto  AXIS_UNIQUE = fg::RawControllerAxis::create(
        ID
        , 10
    );
    ASSERT_NE( nullptr, AXIS_UNIQUE.get() );

    const auto  AXIS2_UNIQUE = fg::RawControllerAxis::create(
        ID
        , 10
    );
    ASSERT_NE( nullptr, AXIS2_UNIQUE.get() );

    ASSERT_TRUE( *AXIS_UNIQUE == *AXIS2_UNIQUE );
}

TEST(
    RawControllerAxisTest
    , Equals_diffID
)
{
    auto    idUnique = fg::RawControllerID::create(
        "module"
        , "port"
        , "device"
    );
    ASSERT_NE( nullptr, idUnique.get() );

    const auto  AXIS_UNIQUE = FgRawControllerAxis::create(
        std::move( idUnique )
        , 10
    );
    ASSERT_NE( nullptr, AXIS_UNIQUE.get() );

    auto    id2Unique = fg::RawControllerID::create(
        "diff"
        , "port"
        , "device"
    );
    ASSERT_NE( nullptr, id2Unique.get() );

    const auto  AXIS2_UNIQUE = FgRawControllerAxis::create(
        std::move( id2Unique )
        , 10
    );
    ASSERT_NE( nullptr, AXIS2_UNIQUE.get() );

    ASSERT_FALSE( *AXIS_UNIQUE == *AXIS2_UNIQUE );
}

TEST(
    RawControllerAxisTest
    , Equals_diffIndex
)
{
    const auto  ID_UNIQUE = fg::RawControllerID::create(
        "module"
        , "port"
        , "device"
    );
    ASSERT_NE( nullptr, ID_UNIQUE.get() );
    const auto &    ID = *ID_UNIQUE;

    const auto  AXIS_UNIQUE = fg::RawControllerAxis::create(
        ID
        , 10
    );
    ASSERT_NE( nullptr, AXIS_UNIQUE.get() );

    const auto  AXIS2_UNIQUE = fg::RawControllerAxis::create(
        ID
        , 20
    );
    ASSERT_NE( nullptr, AXIS2_UNIQUE.get() );

    ASSERT_FALSE( *AXIS_UNIQUE == *AXIS2_UNIQUE );
}

TEST(
    RawControllerAxisTest
    , LessThan_ID
)
{
    auto    idAUnique = fg::RawControllerID::create(
        "A"
        , "A"
        , "A"
    );
    ASSERT_NE( nullptr, idAUnique.get() );

    const auto  AXIS_A_UNIQUE = FgRawControllerAxis::create(
        std::move( idAUnique )
        , 10
    );
    ASSERT_NE( nullptr, AXIS_A_UNIQUE.get() );

    auto    idBUnique = fg::RawControllerID::create(
        "B"
        , "A"
        , "A"
    );
    ASSERT_NE( nullptr, idBUnique.get() );

    const auto  AXIS_B_UNIQUE = FgRawControllerAxis::create(
        std::move( idBUnique )
        , 10
    );
    ASSERT_NE( nullptr, AXIS_B_UNIQUE.get() );

    auto    idB2Unique = fg::RawControllerID::create(
        "B"
        , "A"
        , "A"
    );
    ASSERT_NE( nullptr, idB2Unique.get() );

    const auto  AXIS_B2_UNIQUE = FgRawControllerAxis::create(
        std::move( idB2Unique )
        , 10
    );
    ASSERT_NE( nullptr, AXIS_B2_UNIQUE.get() );

    auto    idCUnique = fg::RawControllerID::create(
        "C"
        , "A"
        , "A"
    );
    ASSERT_NE( nullptr, idCUnique.get() );

    const auto  AXIS_C_UNIQUE = FgRawControllerAxis::create(
        std::move( idCUnique )
        , 10
    );
    ASSERT_NE( nullptr, AXIS_C_UNIQUE.get() );

    ASSERT_TRUE( *AXIS_B_UNIQUE < *AXIS_C_UNIQUE );
    ASSERT_FALSE( *AXIS_B_UNIQUE < *AXIS_A_UNIQUE );
    ASSERT_FALSE( *AXIS_B_UNIQUE < *AXIS_B2_UNIQUE );
}

TEST(
    RawControllerAxisTest
    , LessThan_index
)
{
    const auto  ID_UNIQUE = fg::RawControllerID::create(
        "module"
        , "port"
        , "device"
    );
    ASSERT_NE( nullptr, ID_UNIQUE.get() );
    const auto &    ID = *ID_UNIQUE;

    const auto  AXIS_A_UNIQUE = fg::RawControllerAxis::create(
        ID
        , 10
    );
    ASSERT_NE( nullptr, AXIS_A_UNIQUE.get() );

    const auto  AXIS_B_UNIQUE = fg::RawControllerAxis::create(
        ID
        , 20
    );
    ASSERT_NE( nullptr, AXIS_B_UNIQUE.get() );

    const auto  AXIS_B2_UNIQUE = fg::RawControllerAxis::create(
        ID
        , 20
    );
    ASSERT_NE( nullptr, AXIS_B2_UNIQUE.get() );

    const auto  AXIS_C_UNIQUE = fg::RawControllerAxis::create(
        ID
        , 30
    );
    ASSERT_NE( nullptr, AXIS_C_UNIQUE.get() );

    ASSERT_TRUE( *AXIS_B_UNIQUE < *AXIS_C_UNIQUE );
    ASSERT_FALSE( *AXIS_B_UNIQUE < *AXIS_A_UNIQUE );
    ASSERT_FALSE( *AXIS_B_UNIQUE < *AXIS_B2_UNIQUE );
}
