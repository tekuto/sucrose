﻿#include "fg/util/export.h"
#include "sucrose/controller/raw/axisaction.h"
#include "fg/controller/raw/axis.h"

#include <utility>

const FgRawControllerAxis * fgRawControllerAxisGetAxis(
    const FgRawControllerAxisAction *   _IMPL_PTR
)
{
    return &**( _IMPL_PTR->axisUnique );
}

int fgRawControllerAxisGetValue(
    const FgRawControllerAxisAction *   _IMPL_PTR
)
{
    return _IMPL_PTR->value;
}

fg::RawControllerAxisAction::ConstUnique FgRawControllerAxisAction::create(
    fg::RawControllerAxis::ConstUnique &&   _axisUnique
    , int                                   _value
)
{
    return new FgRawControllerAxisAction{
        std::move( _axisUnique ),
        _value,
    };
}
