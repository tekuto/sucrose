﻿#include "fg/util/test.h"
#include "sucrose/controller/raw/actionbuffer.h"
#include "fg/controller/raw/id.h"
#include "sucrose/controller/raw/action.h"
#include "sucrose/controller/raw/connectaction.h"
#include "sucrose/controller/raw/buttonaction.h"
#include "sucrose/controller/raw/axisaction.h"

#include <utility>

TEST(
    RawControllerActionBufferTest
    , Create
)
{
    const auto  ACTION_BUFFER_UNIQUE = FgRawControllerActionBuffer::create();
    ASSERT_NE( nullptr, ACTION_BUFFER_UNIQUE.get() );
    const auto &    ACTION_BUFFER = *ACTION_BUFFER_UNIQUE;

    ASSERT_EQ( 0, ACTION_BUFFER->buffer.size() );
}

TEST(
    RawControllerActionBufferTest
    , GetSize
)
{
    auto    actionBufferUnique = FgRawControllerActionBuffer::create();
    ASSERT_NE( nullptr, actionBufferUnique.get() );
    auto &  actionBuffer = *actionBufferUnique;

    auto    idUnique = fg::RawControllerID::create(
        "module"
        , "port"
        , "device"
    );
    ASSERT_NE( nullptr, idUnique.get() );

    auto    connectActionUnique = FgRawControllerConnectAction::createConnectAction( std::move( idUnique ) );
    ASSERT_NE( nullptr, connectActionUnique.get() );

    auto    actionUnique = FgRawControllerAction::create( std::move( connectActionUnique ) );
    ASSERT_NE( nullptr, actionUnique.get() );

    actionBuffer->addAction( std::move( actionUnique ) );

    ASSERT_EQ( 1, actionBufferUnique->getSize() );
}

TEST(
    RawControllerActionBufferTest
    , GetAction
)
{
    auto    actionBufferUnique = FgRawControllerActionBuffer::create();
    ASSERT_NE( nullptr, actionBufferUnique.get() );
    auto &  actionBuffer = *actionBufferUnique;

    auto    idUnique = fg::RawControllerID::create(
        "module"
        , "port"
        , "device"
    );
    ASSERT_NE( nullptr, idUnique.get() );

    auto    connectActionUnique = FgRawControllerConnectAction::createConnectAction( std::move( idUnique ) );
    ASSERT_NE( nullptr, connectActionUnique.get() );

    auto    actionUnique = FgRawControllerAction::create( std::move( connectActionUnique ) );
    ASSERT_NE( nullptr, actionUnique.get() );

    actionBuffer->addAction( std::move( actionUnique ) );

    const auto &    ACTION = actionBufferUnique->getAction( 0 );
    ASSERT_NE( nullptr, &ACTION );

    const auto  CONNECT_ACTION_PTR = ACTION.getConnectAction();
    ASSERT_NE( nullptr, CONNECT_ACTION_PTR );
    const auto &    CONNECT_ACTION = *CONNECT_ACTION_PTR;

    const auto &    ID = *( CONNECT_ACTION->idUnique );
    ASSERT_STREQ( "module", ID.getModule() );
    ASSERT_STREQ( "port", ID.getPort() );
    ASSERT_STREQ( "device", ID.getDevice() );
    ASSERT_TRUE( CONNECT_ACTION->connected );
}

TEST(
    RawControllerActionBufferTest
    , AddAction
)
{
    auto    actionBufferUnique = FgRawControllerActionBuffer::create();
    ASSERT_NE( nullptr, actionBufferUnique.get() );
    auto &  actionBuffer = *actionBufferUnique;

    auto    idUnique = fg::RawControllerID::create(
        "module"
        , "port"
        , "device"
    );
    ASSERT_NE( nullptr, idUnique.get() );

    auto    connectActionUnique = FgRawControllerConnectAction::createConnectAction( std::move( idUnique ) );
    ASSERT_NE( nullptr, connectActionUnique.get() );

    auto    actionUnique = FgRawControllerAction::create( std::move( connectActionUnique ) );
    ASSERT_NE( nullptr, actionUnique.get() );

    actionBuffer->addAction( std::move( actionUnique ) );

    const auto &    BUFFER = actionBuffer->buffer;
    ASSERT_EQ( 1, BUFFER.size() );
    const auto &    ACTION = *( BUFFER[ 0 ] );

    const auto  CONNECT_ACTION_PTR = ACTION.getConnectAction();
    ASSERT_NE( nullptr, CONNECT_ACTION_PTR );
    const auto &    CONNECT_ACTION = *CONNECT_ACTION_PTR;

    const auto &    ID = *( CONNECT_ACTION->idUnique );
    ASSERT_STREQ( "module", ID.getModule() );
    ASSERT_STREQ( "port", ID.getPort() );
    ASSERT_STREQ( "device", ID.getDevice() );
    ASSERT_TRUE( CONNECT_ACTION->connected );
}

TEST(
    RawControllerActionBufferTest
    , Clear
)
{
    auto    actionBufferUnique = FgRawControllerActionBuffer::create();
    ASSERT_NE( nullptr, actionBufferUnique.get() );
    auto &  actionBuffer = *actionBufferUnique;

    auto    idUnique = fg::RawControllerID::create(
        "module"
        , "port"
        , "device"
    );
    ASSERT_NE( nullptr, idUnique.get() );

    auto    connectActionUnique = FgRawControllerConnectAction::createConnectAction( std::move( idUnique ) );
    ASSERT_NE( nullptr, connectActionUnique.get() );

    auto    actionUnique = FgRawControllerAction::create( std::move( connectActionUnique ) );
    ASSERT_NE( nullptr, actionUnique.get() );

    actionBuffer->addAction( std::move( actionUnique ) );

    actionBuffer->clear();

    ASSERT_EQ( 0, actionBuffer->buffer.size() );
}
