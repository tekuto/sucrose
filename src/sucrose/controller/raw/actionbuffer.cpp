﻿#include "fg/util/export.h"
#include "sucrose/controller/raw/actionbuffer.h"
#include "sucrose/controller/raw/action.h"
#include "sucrose/controller/raw/connectaction.h"
#include "sucrose/controller/raw/buttonaction.h"
#include "sucrose/controller/raw/axisaction.h"

#include <utility>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

size_t fgRawControllerActionBufferGetSize(
    const FgRawControllerActionBuffer * _IMPL_PTR
)
{
    return _IMPL_PTR->buffer.size();
}

const FgRawControllerAction * fgRawControllerActionBufferGetAction(
    const FgRawControllerActionBuffer * _IMPL_PTR
    , size_t                            _index
)
{
    return &**( _IMPL_PTR->buffer.at( _index ) );
}

fg::RawControllerActionBuffer::Unique FgRawControllerActionBuffer::create(
)
{
    return new FgRawControllerActionBuffer;
}

void FgRawControllerActionBuffer::addAction(
    fg::RawControllerAction::ConstUnique && _actionUnique
)
{
    this->buffer.push_back( std::move( _actionUnique ) );
}

void FgRawControllerActionBuffer::clear(
)
{
    this->buffer.clear();
}
