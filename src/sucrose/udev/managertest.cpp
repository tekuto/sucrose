﻿#include "fg/util/test.h"
#include "sucrose/udev/manager.h"
#include "sucrose/udev/eventparameters.h"
#include "sucrose/udev/event.h"
#include "brownsugar/statemanager.h"

void createTestEventProc(
    const sucrose::UdevEvent &
    , int &
)
{
}

TEST(
    UdevManagerTest
    , Create
)
{
    auto    stateManagerUnique = brownsugar::StateManager::create();
    ASSERT_NE( nullptr, stateManagerUnique.get() );

    auto &  state = stateManagerUnique->getState();

    auto    eventParametersUnique = sucrose::UdevEventParameters::create();
    ASSERT_NE( nullptr, eventParametersUnique.get() );

    auto    i = 10;

    eventParametersUnique->setEventParameters(
        i
        , createTestEventProc
    );

    const auto  MANAGER_UNIQUE = sucrose::UdevManager::create(
        state
        , *eventParametersUnique
    );
    ASSERT_NE( nullptr, MANAGER_UNIQUE.get() );
    const auto &    MANAGER = *MANAGER_UNIQUE;

    ASSERT_EQ( &state, &( MANAGER.state ) );

    ASSERT_EQ( 10, *static_cast< const int * >( MANAGER.eventParameters.dataPtr ) );
    ASSERT_EQ( reinterpret_cast< sucrose::UdevEventProcImpl >( createTestEventProc ), MANAGER.eventParameters.eventProcPtr );

    ASSERT_NE( nullptr, MANAGER.udevUnique.get() );
    ASSERT_NE( nullptr, MANAGER.udevEnumerateUnique.get() );
    ASSERT_NE( nullptr, MANAGER.udevMonitorUnique.get() );

    ASSERT_FALSE( MANAGER.ended );
    ASSERT_NE( nullptr, MANAGER.joinerUnique.get() );
    ASSERT_EQ( &MANAGER, MANAGER.ender.get() );
}
