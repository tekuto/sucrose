﻿#include "fg/util/export.h"
#include "sucrose/udev/manager.h"
#include "sucrose/udev/event.h"
#include "sucrose/udev/eventparameters.h"
#include "fg/core/state/joiner.h"
#include "fg/core/state/threadbackground.h"

#include <libudev.h>
#include <poll.h>
#include <memory>
#include <string>
#include <utility>
#include <stdexcept>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace {
    enum {
        TIMEOUT_NANOSECONDS = 100000000,    // 100ミリ秒
    };

    struct UnrefUdevDevice
    {
        void operator()(
            udev_device *   _udevDevicePtr
        ) const
        {
            udev_device_unref( _udevDevicePtr );
        }
    };

    using UdevDeviceUnique = std::unique_ptr<
        udev_device
        , UnrefUdevDevice
    >;

    sucrose::UdevManager::UdevUnique createUdev(
    )
    {
        auto    thisUnique = sucrose::UdevManager::UdevUnique( udev_new() );
        if( thisUnique.get() == nullptr ) {
            throw std::runtime_error( "udevコンテキストの生成に失敗" );
        }

        return thisUnique;
    }

    void unrefUdev(
        udev &  _this
    )
    {
        udev_unref( &_this );
    }

    sucrose::UdevManager::UdevEnumerateUnique createUdevEnumerate(
        udev &  _udev
    )
    {
        auto    thisUnique = sucrose::UdevManager::UdevEnumerateUnique( udev_enumerate_new( &_udev ) );
        if( thisUnique.get() == nullptr ) {
            throw std::runtime_error( "udevのデバイス列挙コンテキストの生成に失敗" );
        }
        auto &  udevEnumerate = *thisUnique;

        if( udev_enumerate_scan_devices( &udevEnumerate ) != 0 ) {
            throw std::runtime_error( "udevのデバイス列挙に失敗" );
        }

        return thisUnique;
    }

    void unrefUdevEnumerate(
        udev_enumerate &    _this
    )
    {
        udev_enumerate_unref( &_this );
    }

    sucrose::UdevManager::UdevMonitorUnique createUdevMonitor(
        udev &  _udev
    )
    {
        auto    thisUnique = sucrose::UdevManager::UdevMonitorUnique(
            udev_monitor_new_from_netlink(
                &_udev
                , "udev"
            )
        );
        if( thisUnique.get() == nullptr ) {
            throw std::runtime_error( "udevモニタコンテキストの生成に失敗" );
        }

        return thisUnique;
    }

    void unrefUdevMonitor(
        udev_monitor &  _this
    )
    {
        udev_monitor_unref( &_this );
    }

    void executePollDeviceThread(
        sucrose::UdevManager &
    );

    void executeReceiveDeviceThread(
        sucrose::UdevManager &
    );

    std::string createPropertyName(
        udev_list_entry *   _deviceProperty
    )
    {
        const auto  NAME_PTR = udev_list_entry_get_name( _deviceProperty );
        if( NAME_PTR == nullptr ) {
            throw std::runtime_error( "udevイベントのプロパティ名がnull" );
        }

        return std::string( NAME_PTR );
    }

    sucrose::UdevEvent::Properties createProperties(
        udev_device &   _udevDevice
    )
    {
        auto    properties = sucrose::UdevEvent::Properties();

        auto    deviceProperties = udev_device_get_properties_list_entry( &_udevDevice );

        auto    deviceProperty = static_cast< udev_list_entry * >( nullptr );
        udev_list_entry_foreach( deviceProperty, deviceProperties ) {
            auto    name = createPropertyName( deviceProperty );

            const auto  VALUE_PTR = udev_list_entry_get_value( deviceProperty );

            const auto  RESULT = properties.insert(
                std::make_pair(
                    std::move( name )
                    , VALUE_PTR
                )
            );
            if( RESULT.second == false ) {
                throw std::runtime_error( "udevイベントのプロパティ集合へのプロパティ追加に失敗" );
            }
        }

        return properties;
    }

    void callEventProc(
        sucrose::UdevManager &  _manager
        , udev_device &         _udevDevice
    )
    {
        auto    properties = createProperties( _udevDevice );

        const auto  EVENT_UNIQUE = sucrose::UdevEvent::create( std::move( properties ) );

        _manager.eventParameters.call( *EVENT_UNIQUE );
    }

    void enumerateDevices(
        sucrose::UdevManager &  _manager
    )
    {
        auto &  udevEnumerate = *( _manager.udevEnumerateUnique );
        auto &  udev = *( _manager.udevUnique );

        auto    enumerateDevices = udev_enumerate_get_list_entry( &udevEnumerate );

        auto    enumerateDevice = static_cast< udev_list_entry * >( nullptr );
        udev_list_entry_foreach( enumerateDevice, enumerateDevices ) {
            const auto  SYSPATH = udev_list_entry_get_name( enumerateDevice );
            if( SYSPATH == nullptr ) {
                continue;
            }

            auto    udevDeviceUnique = UdevDeviceUnique(
                udev_device_new_from_syspath(
                    &udev
                    , SYSPATH
                )
            );
            if( udevDeviceUnique.get() == nullptr ) {
#ifdef  DEBUG
                std::printf( "D:sysデバイスパスからのudevデバイス情報生成に失敗\n" );
#endif  // DEBUG

                continue;
            }
            auto &  udevDevice = *udevDeviceUnique;

            callEventProc(
                _manager
                , udevDevice
            );
        }
    }

    void unrefEnumerate(
        sucrose::UdevManager &  _manager
    )
    {
        _manager.udevEnumerateUnique.reset();
    }

    void enableReceiving(
        sucrose::UdevManager &  _manager
    )
    {
        auto &  udevMonitor = *( _manager.udevMonitorUnique );

        if( udev_monitor_enable_receiving( &udevMonitor ) != 0 ) {
            throw std::runtime_error( "udev_monitor_enable_receiving()が失敗" );
        }
    }

    void enumerateDevicesThread(
        fg::StateThreadBackground< void, sucrose::UdevManager > &   _thread
    )
    {
        auto &  manager = _thread.getData();

        enumerateDevices( manager );

        unrefEnumerate( manager );

        enableReceiving( manager );

        executePollDeviceThread( manager );
    }

    bool poll(
        int _descriptor
    )
    {
        auto    pollFd = pollfd{
            _descriptor,
            POLLIN,
        };

        const auto  TIMEOUT = timespec{
            0,
            TIMEOUT_NANOSECONDS,
        };

        if( ppoll(
            &pollFd
            , 1
            , &TIMEOUT
            , nullptr
        ) <= 0 ) {
            return false;
        }

        return true;
    }

    bool pollDevice(
        sucrose::UdevManager &  _manager
    )
    {
        auto &  udevMonitor = *( _manager.udevMonitorUnique );

        return poll( udev_monitor_get_fd( &udevMonitor ) );
    }

    void pollDeviceThread(
        fg::StateThreadBackground< void, sucrose::UdevManager > &   _thread
    )
    {
        auto &  manager = _thread.getData();

        if( pollDevice( manager ) == false ) {
            executePollDeviceThread( manager );

            return;
        }

        executeReceiveDeviceThread( manager );
    }

    void executePollDeviceThread(
        sucrose::UdevManager &  _manager
    )
    {
        auto    lock = std::unique_lock< std::mutex >( _manager.mutex );

        if( _manager.ended == true ) {
            return;
        }

        _manager.state.execute(
            pollDeviceThread
            , *( _manager.joinerUnique )
            , _manager
        );
    }

    void receiveDevice(
        sucrose::UdevManager &  _manager
    )
    {
        auto &  udevMonitor = *( _manager.udevMonitorUnique );

        auto    udevDeviceUnique = UdevDeviceUnique( udev_monitor_receive_device( &udevMonitor ) );
        if( udevDeviceUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "D:udevモニタからレシーブされたデバイス情報の取得に失敗\n" );
#endif  // DEBUG

            return;
        }
        auto &  udevDevice = *udevDeviceUnique;

        callEventProc(
            _manager
            , udevDevice
        );
    }

    void receiveDeviceThread(
        fg::StateThreadBackground< void, sucrose::UdevManager > &   _thread
    )
    {
        auto &  manager = _thread.getData();

        receiveDevice( manager );

        executePollDeviceThread( manager );
    }

    void executeReceiveDeviceThread(
        sucrose::UdevManager &  _manager
    )
    {
        auto    lock = std::unique_lock< std::mutex >( _manager.mutex );

        if( _manager.ended == true ) {
            return;
        }

        _manager.state.execute(
            receiveDeviceThread
            , *( _manager.joinerUnique )
            , _manager
        );
    }
}

namespace sucrose {
    void UdevManager::UnrefUdev::operator()(
        udev *  _udevPtr
    ) const
    {
        unrefUdev( *_udevPtr );
    }

    void UdevManager::UnrefUdevEnumerate::operator()(
        udev_enumerate *    _udevEnumeratePtr
    ) const
    {
        unrefUdevEnumerate( *_udevEnumeratePtr );
    }

    void UdevManager::UnrefUdevMonitor::operator()(
        udev_monitor *  _udevMonitorPtr
    ) const
    {
        unrefUdevMonitor( *_udevMonitorPtr );
    }

    void UdevManager::EndThreads::operator()(
        UdevManager *   _managerPtr
    ) const
    {
        auto &  manager = *_managerPtr;

        auto    lock = std::unique_lock< std::mutex >( manager.mutex );

        manager.ended = true;
    }

    UdevManager::UdevManager(
        FgState &                       _state
        , const UdevEventParameters &   _EVENT_PARAMETERS
    )
        : state( reinterpret_cast< fg::State<> & >( _state ) )
        , eventParameters( _EVENT_PARAMETERS )
        , udevUnique( createUdev() )
        , udevEnumerateUnique( createUdevEnumerate( *( this->udevUnique ) ) )
        , udevMonitorUnique( createUdevMonitor( *( this->udevUnique ) ) )
        , ended( false )
        , joinerUnique( fg::StateJoiner::create() )
        , ender( this )
    {
        this->state.execute(
            enumerateDevicesThread
            , *( this->joinerUnique )
            , *this
        );
    }

    UdevManager * UdevManager::create_(
        FgState &                       _state
        , const UdevEventParameters &   _EVENT_PARAMETERS
    )
    {
        return new UdevManager(
            _state
            , _EVENT_PARAMETERS
        );
    }

    void UdevManager::destroy(
        UdevManager *   _implPtr
    )
    {
        delete _implPtr;
    }
}
