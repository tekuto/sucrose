﻿#include "fg/util/export.h"
#include "sucrose/udev/event.h"

#include <utility>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace sucrose {
    UdevEvent::UdevEvent(
        UdevEvent::Properties &&    _properties
    )
        : properties( std::move( _properties ) )
    {
    }

    bool UdevEvent::getProperty(
        const char * &  _VALUE
        , const char *  _NAME
    ) const
    {
        const auto &    PROPERTIES = this->properties;

        const auto  IT = PROPERTIES.find( _NAME );
        if( IT == PROPERTIES.end() ) {
#ifdef  DEBUG
            std::printf(
                "D:udevイベントのプロパティに\"%s\"に対応する値が存在しない\n"
                , _NAME
            );
#endif  // DEBUG

            return false;
        }

        _VALUE = IT->second;

        return true;
    }
}
