﻿#include "fg/util/test.h"
#include "sucrose/udev/eventpropertynames.h"
#include "sucrose/udev/event.h"

#include <utility>

TEST(
    UdevEventPropertyNamesTest
    , Create
)
{
    auto    properties = sucrose::UdevEvent::Properties{
        { "name", "value" },
    };

    const auto  EVENT_UNIQUE = sucrose::UdevEvent::create( std::move( properties ) );
    ASSERT_NE( nullptr, EVENT_UNIQUE.get() );

    const auto  EVENT_PROPERTY_NAMES_UNIQUE = sucrose::UdevEventPropertyNames::create( *EVENT_UNIQUE );
    ASSERT_NE( nullptr, EVENT_PROPERTY_NAMES_UNIQUE.get() );

    const auto &    PROPERTY_NAMES = EVENT_PROPERTY_NAMES_UNIQUE->propertyNames;
    ASSERT_EQ( 1, PROPERTY_NAMES.size() );
    ASSERT_STREQ( "name", PROPERTY_NAMES[ 0 ].c_str() );
}

TEST(
    UdevEventPropertyNamesTest
    , GetSize
)
{
    auto    properties = sucrose::UdevEvent::Properties{
        { "name", "value" },
    };

    const auto  EVENT_UNIQUE = sucrose::UdevEvent::create( std::move( properties ) );
    ASSERT_NE( nullptr, EVENT_UNIQUE.get() );

    const auto  EVENT_PROPERTY_NAMES_UNIQUE = sucrose::UdevEventPropertyNames::create( *EVENT_UNIQUE );
    ASSERT_NE( nullptr, EVENT_PROPERTY_NAMES_UNIQUE.get() );

    ASSERT_EQ( 1, EVENT_PROPERTY_NAMES_UNIQUE->getSize() );
}

TEST(
    UdevEventPropertyNamesTest
    , GetName
)
{
    auto    properties = sucrose::UdevEvent::Properties{
        { "name", "value" },
    };

    const auto  EVENT_UNIQUE = sucrose::UdevEvent::create( std::move( properties ) );
    ASSERT_NE( nullptr, EVENT_UNIQUE.get() );

    const auto  EVENT_PROPERTY_NAMES_UNIQUE = sucrose::UdevEventPropertyNames::create( *EVENT_UNIQUE );
    ASSERT_NE( nullptr, EVENT_PROPERTY_NAMES_UNIQUE.get() );

    ASSERT_STREQ( "name", EVENT_PROPERTY_NAMES_UNIQUE->getName( 0 ) );
}
