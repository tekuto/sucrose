﻿#include "fg/util/test.h"
#include "sucrose/udev/eventparameters.h"
#include "sucrose/udev/event.h"
#include "sucrose/udev/eventpropertynames.h"

#include <string>
#include <utility>

TEST(
    UdevEventParametersTest
    , Create
)
{
    const auto  EVENT_PARAMETERS_UNIQUE = sucrose::UdevEventParameters::create();
    ASSERT_NE( nullptr, EVENT_PARAMETERS_UNIQUE.get() );

    ASSERT_EQ( nullptr, EVENT_PARAMETERS_UNIQUE->dataPtr );
    ASSERT_EQ( nullptr, EVENT_PARAMETERS_UNIQUE->eventProcPtr );
}

TEST(
    UdevEventParametersTest
    , SetEventData
)
{
    auto    eventParametersUnique = sucrose::UdevEventParameters::create();
    ASSERT_NE( nullptr, eventParametersUnique.get() );

    auto    i = 30;

    eventParametersUnique->setEventData( i );

    ASSERT_EQ( 30, *static_cast< const int * >( eventParametersUnique->dataPtr ) );
}

void setEventProcTestProc(
    const sucrose::UdevEvent &
    , void *
)
{
}

TEST(
    UdevEventParametersTest
    , SetEventProc
)
{
    auto    eventParametersUnique = sucrose::UdevEventParameters::create();
    ASSERT_NE( nullptr, eventParametersUnique.get() );

    eventParametersUnique->setEventProc( setEventProcTestProc );

    ASSERT_EQ( setEventProcTestProc, eventParametersUnique->eventProcPtr );
}

void setEventParametersTestEventProc(
    const sucrose::UdevEvent &
    , void *
)
{
}

TEST(
    UdevEventParametersTest
    , SetEventParameters
)
{
    auto    eventParametersUnique = sucrose::UdevEventParameters::create();
    ASSERT_NE( nullptr, eventParametersUnique.get() );

    auto    i = 30;

    eventParametersUnique->setEventParameters(
        static_cast< void * >( &i )
        , setEventParametersTestEventProc
    );

    ASSERT_EQ( 30, *static_cast< const int * >( eventParametersUnique->dataPtr ) );
    ASSERT_EQ( setEventParametersTestEventProc, eventParametersUnique->eventProcPtr );
}

void setEventParametersTemplateTestEventProc(
    const sucrose::UdevEvent &
    , int &
)
{
}

TEST(
    UdevEventParametersTest
    , SetEventParametersTemplate
)
{
    auto    eventParametersUnique = sucrose::UdevEventParameters::create();
    ASSERT_NE( nullptr, eventParametersUnique.get() );

    auto    i = 30;

    eventParametersUnique->setEventParameters(
        i
        , setEventParametersTemplateTestEventProc
    );

    ASSERT_EQ( 30, *static_cast< const int * >( eventParametersUnique->dataPtr ) );
    ASSERT_EQ( reinterpret_cast< sucrose::UdevEventProcImpl >( setEventParametersTemplateTestEventProc ), eventParametersUnique->eventProcPtr );
}

struct CallEventProcTestData
{
    std::string name;
    std::string value;
};

void callEventProcTestProc(
    const sucrose::UdevEvent &  _EVENT
    , CallEventProcTestData &   _data
)
{
    const auto  NAMES_UNIQUE = sucrose::UdevEventPropertyNames::create( _EVENT );
    if( NAMES_UNIQUE.get() == nullptr ) {
        return;
    }

    const auto  NAME = NAMES_UNIQUE->getName( 0 );
    _data.name = NAME;

    auto    valuePtr = static_cast< const char * >( nullptr );
    if( _EVENT.getProperty(
        valuePtr
        , NAME
    ) == false ) {
        return;
    }
    _data.value = valuePtr;
}

TEST(
    UdevEventParametersTest
    , CallEventProc
)
{
    auto    eventParametersUnique = sucrose::UdevEventParameters::create();
    ASSERT_NE( nullptr, eventParametersUnique.get() );

    CallEventProcTestData   data;

    eventParametersUnique->setEventParameters(
        data
        , callEventProcTestProc
    );

    const auto  EVENT_UNIQUE = sucrose::UdevEvent::create(
        sucrose::UdevEvent::Properties{
            { "name", "value" },
        }
    );
    ASSERT_NE( nullptr, EVENT_UNIQUE.get() );

    eventParametersUnique->call( *EVENT_UNIQUE );

    ASSERT_STREQ( "name", data.name.c_str() );
    ASSERT_STREQ( "value", data.value.c_str() );
}
