﻿#include "fg/util/export.h"
#include "sucrose/udev/eventpropertynames.h"
#include "sucrose/udev/event.h"

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace {
    sucrose::UdevEventPropertyNames::PropertyNames createPropertyNames(
        const sucrose::UdevEvent &  _EVENT
    )
    {
        auto    propertyNames = sucrose::UdevEventPropertyNames::PropertyNames();

        for( const auto & PAIR : _EVENT.properties ) {
            const auto &    NAME = PAIR.first;

            propertyNames.push_back( NAME );
        }

        return propertyNames;
    }
}

namespace sucrose {
    UdevEventPropertyNames::UdevEventPropertyNames(
        const UdevEvent &   _EVENT
    )
        : propertyNames( createPropertyNames( _EVENT ) )
    {
    }

    UdevEventPropertyNames * UdevEventPropertyNames::create_(
        const UdevEvent &   _EVENT
    )
    {
        return new UdevEventPropertyNames( _EVENT );
    }

    void UdevEventPropertyNames::destroy(
        UdevEventPropertyNames *    _implPtr
    )
    {
        delete _implPtr;
    }

    std::size_t UdevEventPropertyNames::getSize(
    ) const
    {
        return this->propertyNames.size();
    }

    const char * UdevEventPropertyNames::getName(
        std::size_t _index
    ) const
    {
        if( _index >= this->getSize() ) {
#ifdef  DEBUG
            std::printf( "D:インデックスが範囲外\n" );
#endif  // DEBUG

            return nullptr;
        }

        return this->propertyNames.at( _index ).c_str();
    }
}
