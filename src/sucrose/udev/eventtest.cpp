﻿#include "fg/util/test.h"
#include "sucrose/udev/event.h"

TEST(
    UdevEventTest
    , Create
)
{
    const auto  EVENT_UNIQUE = sucrose::UdevEvent::create(
        sucrose::UdevEvent::Properties{
            { "name", "value" },
        }
    );
    ASSERT_NE( nullptr, EVENT_UNIQUE.get() );

    const auto &    PROPERTIES = EVENT_UNIQUE->properties;
    ASSERT_EQ( 1, PROPERTIES.size() );
    const auto  IT = PROPERTIES.find( "name" );
    ASSERT_NE( PROPERTIES.end(), IT );
    ASSERT_STREQ( "value", IT->second );
}

TEST(
    UdevEventTest
    , GetProperty
)
{
    const auto  EVENT_UNIQUE = sucrose::UdevEvent::create(
        sucrose::UdevEvent::Properties{
            { "name", "value" },
        }
    );
    ASSERT_NE( nullptr, EVENT_UNIQUE.get() );

    auto    valuePtr = static_cast< const char * >( nullptr );
    ASSERT_TRUE(
        EVENT_UNIQUE->getProperty(
            valuePtr
            , "name"
        )
    );
    ASSERT_STREQ( "value", valuePtr );
}
