﻿#include "sucrose/udev/joystick/joystick.h"
#include "sucrose/udev/joystick/eventparameters.h"
#include "sucrose/udev/joystick/buttonevent.h"
#include "sucrose/udev/joystick/axisevent.h"
#include "fg/core/state/state.h"
#include "fg/core/state/joiner.h"
#include "fg/core/state/threadbackground.h"

#include <mutex>
#include <utility>
#include <linux/joystick.h>
#include <poll.h>
#include <unistd.h>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace {
    enum {
        TIMEOUT_NANOSECONDS = 100000000,
    };

    void executeReadInputThread(
        sucrose::UdevJoystick &
    );

    void executeAnalyzeInputThread(
        sucrose::UdevJoystick &
    );

    bool poll(
        int _descriptor
    )
    {
        auto    pollFd = pollfd{
            _descriptor,
            POLLIN,
        };

        const auto  TIMEOUT = timespec{
            0,
            TIMEOUT_NANOSECONDS,
        };

        if( ppoll(
            &pollFd
            , 1
            , &TIMEOUT
            , nullptr
        ) <= 0 ) {
            return false;
        }

        return true;
    }

    bool readInput(
        sucrose::UdevJoystick & _joystick
    )
    {
        auto &  descriptor = _joystick.descriptor;
        auto &  taskData = _joystick.taskData;
        auto &  jsEvent = taskData.jsEvent;

        if( poll( descriptor ) == false ) {
            return false;
        }

        const auto  READ_SIZE = read(
            descriptor
            , &jsEvent
            , sizeof( jsEvent )
        );
        if( READ_SIZE < ssize_t( sizeof( jsEvent ) ) ) {
#ifdef  DEBUG
            std::printf( "E:ジョイスティックのデータ読み込みに失敗\n" );
#endif  // DEBUG

            return false;
        }

        return true;
    }

    //TODO ポーリングスレッドとデータ読み込み、解析スレッドの2つに分ける
    void readInputThread(
        fg::StateThreadBackground< void, sucrose::UdevJoystick > &  _thread
    )
    {
        auto &  joystick = _thread.getData();

        if( readInput( joystick ) == false ) {
            executeReadInputThread( joystick );

            return;
        }

        executeAnalyzeInputThread( joystick );
    }

    void executeReadInputThread(
        sucrose::UdevJoystick & _joystick
    )
    {
        auto    lock = std::unique_lock< std::mutex >( _joystick.mutex );

        if( _joystick.ended == true ) {
            return;
        }

        _joystick.state.execute(
            readInputThread
            , *( _joystick.joinerUnique )
            , _joystick
        );
    }

    void callButtonEventProc(
        sucrose::UdevJoystick & _joystick
        , const js_event &      _JS_EVENT
    )
    {
        const auto &    INDEX = _JS_EVENT.number;
        const auto      PRESSED = _JS_EVENT.value != 0;

        const auto  EVENT = sucrose::UdevJoystickButtonEvent(
            _joystick.ID
            , INDEX
            , PRESSED
        );

        _joystick.eventParameters.call( EVENT );
    }

    void callAxisEventProc(
        sucrose::UdevJoystick & _joystick
        , const js_event &      _JS_EVENT
    )
    {
        const auto &    INDEX = _JS_EVENT.number;
        const auto &    VALUE = _JS_EVENT.value;

        const auto  EVENT = sucrose::UdevJoystickAxisEvent(
            _joystick.ID
            , INDEX
            , VALUE
        );

        _joystick.eventParameters.call( EVENT );
    }

    void analyzeInput(
        sucrose::UdevJoystick & _joystick
    )
    {
        const auto &    JS_EVENT = _joystick.taskData.jsEvent;

        const auto &    TYPE = JS_EVENT.type;

        switch( TYPE ) {
        case JS_EVENT_BUTTON:
            callButtonEventProc(
                _joystick
                , JS_EVENT
            );
            break;

        case JS_EVENT_AXIS:
            callAxisEventProc(
                _joystick
                , JS_EVENT
            );
            break;

        default:
#ifdef  DEBUG
            std::printf(
                "D:対象外のデータ[0x%x]\n"
                , TYPE
            );
#endif  // DEBUG

            break;
        }
    }

    void analyzeInputThread(
        fg::StateThreadBackground< void, sucrose::UdevJoystick > &  _thread
    )
    {
        auto &  joystick = _thread.getData();

        analyzeInput( joystick );

        executeReadInputThread( joystick );
    }

    void executeAnalyzeInputThread(
        sucrose::UdevJoystick & _joystick
    )
    {
        auto    lock = std::unique_lock< std::mutex >( _joystick.mutex );

        if( _joystick.ended == true ) {
            return;
        }

        _joystick.state.execute(
            analyzeInputThread
            , *( _joystick.joinerUnique )
            , _joystick
        );
    }
}

namespace sucrose {
    void UdevJoystick::CloseDescriptor::operator()(
        int *   _descriptorPtr
    ) const
    {
        close( *_descriptorPtr );
    }

    void UdevJoystick::EndThreads::operator()(
        UdevJoystick *  _joystickPtr
    ) const
    {
        auto &  joystick = *_joystickPtr;

        auto    lock = std::unique_lock< std::mutex >( joystick.mutex );

        joystick.ended = true;
    }

    UdevJoystick::UdevJoystick(
        FgState &                       _state
        , UdevJoystickEventParameters & _eventParameters
        , const UdevJoystickID &        _ID
        , int                           _decsriptor
    )
        : state( reinterpret_cast< fg::State<> & >( _state ) )
        , eventParameters( _eventParameters )
        , ID( _ID )
        , descriptor( _decsriptor )
        , descriptorCloser( &( this->descriptor ) )
        , ended( false )
        , joinerUnique( fg::StateJoiner::create() )
        , ender( this )
    {
        executeReadInputThread( *this );
    }
}
