﻿#include "fg/util/export.h"
#include "sucrose/udev/joystick/disconnectevent.h"

namespace sucrose {
    UdevJoystickDisconnectEvent::UdevJoystickDisconnectEvent(
        const UdevJoystickID &  _ID
    )
        : ID( _ID )
    {
    }

    const UdevJoystickID & UdevJoystickDisconnectEvent::getID(
    ) const
    {
        return this->ID;
    }
}
