﻿#include "fg/util/export.h"
#include "sucrose/udev/joystick/eventparameters.h"

namespace {
    template<
        typename EVENT_T
        , typename PROC_PTR_T
    >
    void call(
        sucrose::UdevJoystickEventParameters &  _eventParameters
        , const EVENT_T &                       _EVENT
        , PROC_PTR_T &                          _procPtr
    )
    {
        if( _procPtr == nullptr ) {
            return;
        }

        _procPtr(
            _EVENT
            , _eventParameters.dataPtr
        );
    }
}

namespace sucrose {
    UdevJoystickEventParameters * UdevJoystickEventParameters::create_(
    )
    {
        return new UdevJoystickEventParameters;
    }

    void UdevJoystickEventParameters::destroy(
        UdevJoystickEventParameters *   _implPtr
    )
    {
        delete _implPtr;
    }

    void UdevJoystickEventParameters::setEventData(
        void *  _dataPtr
    )
    {
        this->dataPtr = _dataPtr;
    }

    void UdevJoystickEventParameters::setEventProc(
        UdevJoystickConnectEventProcImpl    _procPtr
    )
    {
        this->connectEventProcPtr = _procPtr;
    }

    void UdevJoystickEventParameters::setEventProc(
        UdevJoystickDisconnectEventProcImpl _procPtr
    )
    {
        this->disconnectEventProcPtr = _procPtr;
    }

    void UdevJoystickEventParameters::setEventProc(
        UdevJoystickButtonEventProcImpl _procPtr
    )
    {
        this->buttonEventProcPtr = _procPtr;
    }

    void UdevJoystickEventParameters::setEventProc(
        UdevJoystickAxisEventProcImpl   _procPtr
    )
    {
        this->axisEventProcPtr = _procPtr;
    }

    void UdevJoystickEventParameters::call(
        const UdevJoystickConnectEvent &    _EVENT
    )
    {
        ::call(
            *this
            , _EVENT
            , this->connectEventProcPtr
        );
    }

    void UdevJoystickEventParameters::call(
        const UdevJoystickDisconnectEvent & _EVENT
    )
    {
        ::call(
            *this
            , _EVENT
            , this->disconnectEventProcPtr
        );
    }

    void UdevJoystickEventParameters::call(
        const UdevJoystickButtonEvent & _EVENT
    )
    {
        ::call(
            *this
            , _EVENT
            , this->buttonEventProcPtr
        );
    }

    void UdevJoystickEventParameters::call(
        const UdevJoystickAxisEvent &   _EVENT
    )
    {
        ::call(
            *this
            , _EVENT
            , this->axisEventProcPtr
        );
    }
}
