﻿#include "fg/util/test.h"
#include "sucrose/udev/joystick/axisevent.h"
#include "sucrose/udev/joystick/id.h"

TEST(
    UdevJoystickAxisEventTest
    , Constructor
)
{
    const auto  ID_UNIQUE = sucrose::UdevJoystickID::create(
        "PORT"
        , "DEVICE"
    );
    ASSERT_NE( nullptr, ID_UNIQUE.get() );
    const auto &    ID = *ID_UNIQUE;

    const auto  EVENT = sucrose::UdevJoystickAxisEvent(
        ID
        , 10
        , 20
    );

    ASSERT_EQ( &ID, &( EVENT.ID ) );
    ASSERT_EQ( 10, EVENT.index );
    ASSERT_EQ( 20, EVENT.value );
}

TEST(
    UdevJoystickAxisEventTest
    , GetID
)
{
    const auto  ID_UNIQUE = sucrose::UdevJoystickID::create(
        "PORT"
        , "DEVICE"
    );
    ASSERT_NE( nullptr, ID_UNIQUE.get() );
    const auto &    ID = *ID_UNIQUE;

    const auto  EVENT = sucrose::UdevJoystickAxisEvent(
        ID
        , 10
        , true
    );

    ASSERT_EQ( &ID, &( EVENT.getID() ) );
}

TEST(
    UdevJoystickAxisEventTest
    , GetIndex
)
{
    const auto  ID_UNIQUE = sucrose::UdevJoystickID::create(
        "PORT"
        , "DEVICE"
    );
    ASSERT_NE( nullptr, ID_UNIQUE.get() );

    const auto  EVENT = sucrose::UdevJoystickAxisEvent(
        *ID_UNIQUE
        , 10
        , 20
    );

    ASSERT_EQ( 10, EVENT.getIndex() );
}

TEST(
    UdevJoystickAxisEventTest
    , GetValue
)
{
    const auto  ID_UNIQUE = sucrose::UdevJoystickID::create(
        "PORT"
        , "DEVICE"
    );
    ASSERT_NE( nullptr, ID_UNIQUE.get() );

    const auto  EVENT = sucrose::UdevJoystickAxisEvent(
        *ID_UNIQUE
        , 10
        , 20
    );

    ASSERT_EQ( 20, EVENT.getValue() );
}
