﻿#include "fg/util/test.h"
#include "sucrose/udev/joystick/joystick.h"
#include "fg/core/state/state.h"
#include "fg/core/state/creating.h"
#include "sucrose/udev/joystick/eventparameters.h"
#include "sucrose/udev/joystick/id.h"
#include "sucrose/udev/joystick/buttonevent.h"
#include "sucrose/udev/joystick/axisevent.h"
#include "brownsugar/statemanager.h"

#include <utility>
#include <memory>
#include <mutex>
#include <condition_variable>
#include <chrono>
#include <fcntl.h>

struct CloseDescriptor
{
    void operator()(
        int *   _descriptorPtr
    ) const
    {
        close( *_descriptorPtr );
    }
};

using DescriptorCloser = std::unique_ptr<
    int
    , CloseDescriptor
>;

TEST(
    UdevJoystickTest
    , Create
)
{
    auto    stateManagerUnique = brownsugar::StateManager::create();
    ASSERT_NE( nullptr, stateManagerUnique.get() );

    auto &  state = stateManagerUnique->getState();

    auto    eventParametersUnique = sucrose::UdevJoystickEventParameters::create();
    ASSERT_NE( nullptr, eventParametersUnique.get() );
    auto &  eventParameters = *eventParametersUnique;

    const auto  ID_UNIQUE = sucrose::UdevJoystickID::create(
        "PORT"
        , "DEVICE"
    );
    ASSERT_NE( nullptr, ID_UNIQUE.get() );
    const auto &    ID = *ID_UNIQUE;

    auto    descriptor = open(
        "udevjoysticktest/new.dat"
        , O_RDONLY
    );
    ASSERT_NE( -1, descriptor );
    auto    descriptorCloser = DescriptorCloser( &descriptor );

    const auto  JOYSTICK_UNIQUE = sucrose::UdevJoystick::create(
        *state
        , eventParameters
        , ID
        , descriptor
    );
    ASSERT_NE( nullptr, JOYSTICK_UNIQUE.get() );
    const auto &    JOYSTICK = *JOYSTICK_UNIQUE;

    descriptorCloser.release();

    ASSERT_EQ( &state, &( JOYSTICK.state ) );
    ASSERT_EQ( &eventParameters, &( JOYSTICK.eventParameters ) );
    ASSERT_EQ( &ID, &( JOYSTICK.ID ) );
    ASSERT_EQ( descriptor, JOYSTICK.descriptor );
    ASSERT_EQ( &( JOYSTICK.descriptor ), JOYSTICK.descriptorCloser.get() );

    ASSERT_FALSE( JOYSTICK.ended );
    ASSERT_NE( nullptr, JOYSTICK.joinerUnique.get() );
    ASSERT_EQ( &JOYSTICK, JOYSTICK.ender.get() );
}

struct ButtonPressedTestData
{
    const sucrose::UdevJoystickID * ID_PTR = nullptr;
    unsigned char                   index = 0;
    bool                            pressed = false;

    std::mutex              mutex;
    std::condition_variable condition;
    bool                    ended = false;
};

void buttonPressedTestProc(
    const sucrose::UdevJoystickButtonEvent &    _EVENT
    , ButtonPressedTestData &                   _data
)
{
    _data.ID_PTR = &( _EVENT.getID() );
    _data.index = _EVENT.getIndex();
    _data.pressed = _EVENT.getPressed();

    auto    lock = std::unique_lock< std::mutex >( _data.mutex );

    _data.ended = true;

    _data.condition.notify_all();
}

TEST(
    UdevJoystickTest
    , ButtonPressed
)
{
    auto    stateManagerUnique = brownsugar::StateManager::create();
    ASSERT_NE( nullptr, stateManagerUnique.get() );

    auto &  rootState = stateManagerUnique->getState();

    rootState.enter(
        [](
            fg::CreatingState<> &
        )
        {
        }
    );

    auto    eventParametersUnique = sucrose::UdevJoystickEventParameters::create();
    ASSERT_NE( nullptr, eventParametersUnique.get() );

    ButtonPressedTestData   data;

    eventParametersUnique->setEventParameters(
        data
        , buttonPressedTestProc
    );

    const auto  ID_UNIQUE = sucrose::UdevJoystickID::create(
        "PORT"
        , "DEVICE"
    );
    ASSERT_NE( nullptr, ID_UNIQUE.get() );
    const auto &    ID = *ID_UNIQUE;

    auto    descriptor = open(
        "udevjoysticktest/buttonpress.dat"
        , O_RDONLY
    );
    ASSERT_NE( -1, descriptor );
    auto    descriptorCloser = DescriptorCloser( &descriptor );

    const auto  JOYSTICK_UNIQUE = sucrose::UdevJoystick::create(
        *rootState  // 無効ステートでも稼働することを確認
        , *eventParametersUnique
        , ID
        , descriptor
    );
    ASSERT_NE( nullptr, JOYSTICK_UNIQUE.get() );

    descriptorCloser.release();

    {
        auto    lock = std::unique_lock< std::mutex >( data.mutex );

        if( data.ended == false ) {
            data.condition.wait_for(
                lock
                , std::chrono::seconds( 1 )
            );
        }
    }

    ASSERT_EQ( &ID, data.ID_PTR );
    ASSERT_EQ( 10, data.index );
    ASSERT_TRUE( data.pressed );
}

struct AxisOperatedTestData
{
    const sucrose::UdevJoystickID * ID_PTR = nullptr;
    unsigned char                   index = 0;
    short                           value = 0;

    std::mutex              mutex;
    std::condition_variable condition;
    bool                    ended = false;
};

void axisOperatedTestProc(
    const sucrose::UdevJoystickAxisEvent &  _EVENT
    , AxisOperatedTestData &                _data
)
{
    _data.ID_PTR = &( _EVENT.getID() );
    _data.index = _EVENT.getIndex();
    _data.value = _EVENT.getValue();

    auto    lock = std::unique_lock< std::mutex >( _data.mutex );

    _data.ended = true;

    _data.condition.notify_all();
}

TEST(
    UdevJoystickTest
    , AxisOperated
)
{
    auto    stateManagerUnique = brownsugar::StateManager::create();
    ASSERT_NE( nullptr, stateManagerUnique.get() );

    auto &  rootState = stateManagerUnique->getState();

    rootState.enter(
        [](
            fg::CreatingState<> &
        )
        {
        }
    );

    auto    eventParametersUnique = sucrose::UdevJoystickEventParameters::create();
    ASSERT_NE( nullptr, eventParametersUnique.get() );

    AxisOperatedTestData    data;

    eventParametersUnique->setEventParameters(
        data
        , axisOperatedTestProc
    );

    const auto  ID_UNIQUE = sucrose::UdevJoystickID::create(
        "PORT"
        , "DEVICE"
    );
    ASSERT_NE( nullptr, ID_UNIQUE.get() );
    const auto &    ID = *ID_UNIQUE;

    auto    descriptor = open(
        "udevjoysticktest/axisoperate.dat"
        , O_RDONLY
    );
    ASSERT_NE( -1, descriptor );
    auto    descriptorCloser = DescriptorCloser( &descriptor );

    const auto  JOYSTICK_UNIQUE = sucrose::UdevJoystick::create(
        *rootState  // 無効ステートでも稼働することを確認
        , *eventParametersUnique
        , ID
        , descriptor
    );
    ASSERT_NE( nullptr, JOYSTICK_UNIQUE.get() );

    descriptorCloser.release();

    {
        auto    lock = std::unique_lock< std::mutex >( data.mutex );

        if( data.ended == false ) {
            data.condition.wait_for(
                lock
                , std::chrono::seconds( 1 )
            );
        }
    }

    ASSERT_EQ( &ID, data.ID_PTR );
    ASSERT_EQ( 10, data.index );
    ASSERT_EQ( 20, data.value );
}
