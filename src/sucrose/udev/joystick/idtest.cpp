﻿#include "fg/util/test.h"
#include "sucrose/udev/joystick/id.h"

TEST(
    UdevJoystickIDTest
    , Create
)
{
    const auto  ID_UNIQUE = sucrose::UdevJoystickID::create(
        "PORT"
        , "DEVICE"
    );
    ASSERT_NE( nullptr, ID_UNIQUE.get() );

    ASSERT_STREQ( "PORT", ID_UNIQUE->PORT.c_str() );
    ASSERT_STREQ( "DEVICE", ID_UNIQUE->DEVICE.c_str() );
}

TEST(
    UdevJoystickIDTest
    , GetPort
)
{
    const auto  ID_UNIQUE = sucrose::UdevJoystickID::create(
        "PORT"
        , "DEVICE"
    );
    ASSERT_NE( nullptr, ID_UNIQUE.get() );

    ASSERT_STREQ( "PORT", ID_UNIQUE->getPort() );
}

TEST(
    UdevJoystickIDTest
    , GetDevice
)
{
    const auto  ID_UNIQUE = sucrose::UdevJoystickID::create(
        "PORT"
        , "DEVICE"
    );
    ASSERT_NE( nullptr, ID_UNIQUE.get() );

    ASSERT_STREQ( "DEVICE", ID_UNIQUE->getDevice() );
}

TEST(
    UdevJoystickIDTest
    , LessThan_port
)
{
    auto    idAUnique = sucrose::UdevJoystickID::create(
        "A"
        , "A"
    );
    ASSERT_NE( nullptr, idAUnique.get() );
    const auto &    ID_A = *idAUnique;

    auto    idBUnique = sucrose::UdevJoystickID::create(
        "B"
        , "A"
    );
    ASSERT_NE( nullptr, idBUnique.get() );
    const auto &    ID_B = *idBUnique;

    auto    idB2Unique = sucrose::UdevJoystickID::create(
        "B"
        , "A"
    );
    ASSERT_NE( nullptr, idB2Unique.get() );
    const auto &    ID_B2 = *idB2Unique;

    auto    idCUnique = sucrose::UdevJoystickID::create(
        "C"
        , "A"
    );
    ASSERT_NE( nullptr, idCUnique.get() );
    const auto &    ID_C = *idCUnique;

    ASSERT_TRUE( ID_B < ID_C );
    ASSERT_FALSE( ID_B < ID_A );
    ASSERT_FALSE( ID_B < ID_B2 );
}

TEST(
    UdevJoystickIDTest
    , LessThan_device
)
{
    auto    idAUnique = sucrose::UdevJoystickID::create(
        "A"
        , "A"
    );
    ASSERT_NE( nullptr, idAUnique.get() );
    const auto &    ID_A = *idAUnique;

    auto    idBUnique = sucrose::UdevJoystickID::create(
        "A"
        , "B"
    );
    ASSERT_NE( nullptr, idBUnique.get() );
    const auto &    ID_B = *idBUnique;

    auto    idB2Unique = sucrose::UdevJoystickID::create(
        "A"
        , "B"
    );
    ASSERT_NE( nullptr, idB2Unique.get() );
    const auto &    ID_B2 = *idB2Unique;

    auto    idCUnique = sucrose::UdevJoystickID::create(
        "A"
        , "C"
    );
    ASSERT_NE( nullptr, idCUnique.get() );
    const auto &    ID_C = *idCUnique;

    ASSERT_TRUE( ID_B < ID_C );
    ASSERT_FALSE( ID_B < ID_A );
    ASSERT_FALSE( ID_B < ID_B2 );
}
