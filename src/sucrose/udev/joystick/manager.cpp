﻿#include "fg/util/export.h"
#include "sucrose/udev/joystick/manager.h"
#include "sucrose/udev/joystick/id.h"
#include "sucrose/udev/joystick/joystick.h"
#include "sucrose/udev/joystick/connectevent.h"
#include "sucrose/udev/joystick/disconnectevent.h"
#include "sucrose/udev/event.h"

#include <utility>
#include <memory>
#include <cstddef>
#include <cstring>
#include <stdexcept>
#include <fcntl.h>
#include <unistd.h>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace {
    const auto  PROPERTY_ID_INPUT_JOYSTICK = "ID_INPUT_JOYSTICK";
    const auto  PROPERTY_DEVNAME = "DEVNAME";
    const auto  PROPERTY_ACTION = "ACTION";

    const auto  ID_INPUT_JOYSTICK_TRUE = "1";

    const auto  PROPERTY_ID_PATH = "ID_PATH";
    const auto  PROPERTY_ID_VENDOR_ID = "ID_VENDOR_ID";
    const auto  PROPERTY_ID_MODEL_ID = "ID_MODEL_ID";
    const auto  PROPERTY_ID_REVISION = "ID_REVISION";

    const auto  DEVICE_SEPARATOR = '/';

    const auto  ACTION_ADD = "add";
    const auto  ACTION_REMOVE = "remove";

    enum Action : std::size_t
    {
        INVALID = 0,

        CONNECT,
        DISCONNECT,
    };

    struct CloseDescriptor
    {
        void operator()(
            int *   _descriptorPtr
        ) const
        {
            close( *_descriptorPtr );
        }
    };

    using DescriptorCloser = std::unique_ptr<
        int
        , CloseDescriptor
    >;

    bool isJoystickEvent(
        const sucrose::UdevEvent &  _EVENT
    )
    {
        auto    idInputJoystickPtr = static_cast< const char * >( nullptr );
        if( _EVENT.getProperty(
            idInputJoystickPtr
            , PROPERTY_ID_INPUT_JOYSTICK
        ) == false ) {
#ifdef  DEBUG
            std::printf(
                "D:%sの取得に失敗\n"
                , PROPERTY_ID_INPUT_JOYSTICK
            );
#endif  // DEBUG

            return false;
        }

        if( std::strcmp(
            idInputJoystickPtr
            , ID_INPUT_JOYSTICK_TRUE
        ) != 0 ) {
#ifdef  DEBUG
            std::printf(
                "D:%sの値が\"%s\"ではない\n"
                , PROPERTY_ID_INPUT_JOYSTICK
                , ID_INPUT_JOYSTICK_TRUE
            );
#endif  // DEBUG

            return false;
        }

        return true;
    }

    bool getDevicePath(
        const char * &                  _devicePathPtr
        , const sucrose::UdevEvent &    _EVENT
    )
    {
        auto    devicePathPtr = static_cast< const char * >( nullptr );
        if( _EVENT.getProperty(
            devicePathPtr
            , PROPERTY_DEVNAME
        ) == false ) {
#ifdef  DEBUG
            std::printf(
                "D:%sの取得に失敗\n"
                , PROPERTY_DEVNAME
            );
#endif  // DEBUG

            return false;
        }

        _devicePathPtr = std::move( devicePathPtr );

        return true;
    }

    bool isJoystickInterface(
        const char *    _DEVNODE
    )
    {
        const auto  NAME = std::strrchr(
            _DEVNODE
            , '/'
        );
        if( NAME == nullptr ) {
            return false;
        }

        auto    tmp = int( 0 );
        if( std::sscanf(
            NAME
            , "/js%u"
            , &tmp
        ) != 1 ) {
            return false;
        }

        return true;
    }

    bool initPort(
        std::string &                   _port
        , const sucrose::UdevEvent &    _EVENT
    )
    {
        auto    pathPtr = static_cast< const char * >( nullptr );
        if( _EVENT.getProperty(
            pathPtr
            , PROPERTY_ID_PATH
        ) == false ) {
#ifdef  DEBUG
            std::printf(
                "D:プロパティ\"%s\"の値取得に失敗\n"
                , PROPERTY_ID_PATH
            );
#endif  // DEBUG

            return false;
        }

        _port = std::string( pathPtr );

        return true;
    }

    bool initDevice(
        std::string &                   _device
        , const sucrose::UdevEvent &    _EVENT
    )
    {
        auto    vendorIdPtr = static_cast< const char * >( nullptr );
        if( _EVENT.getProperty(
            vendorIdPtr
            , PROPERTY_ID_VENDOR_ID
        ) == false ) {
#ifdef  DEBUG
            std::printf(
                "D:プロパティ\"%s\"の値取得に失敗\n"
                , PROPERTY_ID_VENDOR_ID
            );
#endif  // DEBUG

            return false;
        }

        auto    modelIdPtr = static_cast< const char * >( nullptr );
        if( _EVENT.getProperty(
            modelIdPtr
            , PROPERTY_ID_MODEL_ID
        ) == false ) {
#ifdef  DEBUG
            std::printf(
                "D:プロパティ\"%s\"の値取得に失敗\n"
                , PROPERTY_ID_MODEL_ID
            );
#endif  // DEBUG

            return false;
        }

        auto    revisionPtr = static_cast< const char * >( nullptr );
        if( _EVENT.getProperty(
            revisionPtr
            , PROPERTY_ID_REVISION
        ) == false ) {
#ifdef  DEBUG
            std::printf(
                "D:プロパティ\"%s\"の値取得に失敗\n"
                , PROPERTY_ID_REVISION
            );
#endif  // DEBUG

            return false;
        }

        auto    device = std::string();
        device.append( vendorIdPtr );
        device.push_back( DEVICE_SEPARATOR );
        device.append( modelIdPtr );
        device.push_back( DEVICE_SEPARATOR );
        device.append( revisionPtr );

        _device = std::move( device );

        return true;
    }

    sucrose::UdevJoystickID::ConstUnique generateUdevJoystickID(
        const sucrose::UdevEvent &  _EVENT
    )
    {
        auto    port = std::string();
        if( initPort(
            port
            , _EVENT
        ) == false ) {
#ifdef  DEBUG
            std::printf( "D:ポート名の初期化に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        auto    device = std::string();
        if( initDevice(
            device
            , _EVENT
        ) == false ) {
#ifdef  DEBUG
            std::printf( "D:デバイス名の初期化に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        return sucrose::UdevJoystickID::create(
            port
            , device
        );
    }

    bool getAction(
        Action &                        _action
        , const sucrose::UdevEvent &    _EVENT
    )
    {
        auto    action = Action::INVALID;

        auto    actionPtr = static_cast< const char * >( nullptr );
        if( _EVENT.getProperty(
            actionPtr
            , PROPERTY_ACTION
        ) == false ) {
            action = Action::CONNECT;
        } else if( std::strcmp(
            actionPtr
            , ACTION_ADD
        ) == 0 ) {
            action = Action::CONNECT;
        } else if( std::strcmp(
            actionPtr
            , ACTION_REMOVE
        ) == 0 ) {
            action = Action::DISCONNECT;
        } else {
            return false;
        }

        _action = std::move( action );

        return true;
    }

    bool isExistsJoystick(
        sucrose::UdevJoystickManager &                  _manager
        , const sucrose::UdevJoystickID::ConstUnique &  _ID_UNIQUE
    )
    {
        const auto &    MAP = _manager.joystickMap;
        if( MAP.find( _ID_UNIQUE ) == MAP.end() ) {
            return false;
        }

        return true;
    }

    void callConnectEventProc(
        sucrose::UdevJoystickManager &      _manager
        , const sucrose::UdevJoystickID &   _ID
    )
    {
        const auto  EVENT = sucrose::UdevJoystickConnectEvent( _ID );

        _manager.eventParameters.call( EVENT );
    }

    void addNewJoystick(
        sucrose::UdevJoystickManager &              _manager
        , sucrose::UdevJoystickID::ConstUnique &&   _idUnique
        , DescriptorCloser &&                       _descriptorCloser
    )
    {
        auto    joystickUnique = sucrose::UdevJoystick::create(
            *( _manager.state )
            , _manager.eventParameters
            , *_idUnique
            , *_descriptorCloser
        );

        _descriptorCloser.release();

        const auto  RESULT = _manager.joystickMap.insert(
            std::make_pair(
                std::move( _idUnique )
                , std::move( joystickUnique )
            )
        );
        if( RESULT.second == false ) {
            throw std::runtime_error( "ジョイスティックの追加に失敗" );
        }
    }

    bool connect(
        sucrose::UdevJoystickManager &              _manager
        , const char *                              _DEVICE_PATH
        , sucrose::UdevJoystickID::ConstUnique &&   _idUnique
    )
    {
        if( isExistsJoystick(
            _manager
            , _idUnique
        ) == true ) {
#ifdef  DEBUG
            std::printf( "D:既に同じジョイスティックが存在している\n" );
#endif  // DEBUG

            return false;
        }

        auto    descriptor = open(
            _DEVICE_PATH
            , O_RDONLY
            , 0
        );
        if( descriptor == -1 ) {
#ifdef  DEBUG
            std::printf( "D:デバイスオープンに失敗\n" );
#endif  // DEBUG

            return false;
        }
        auto    descriptorCloser = DescriptorCloser( &descriptor );

        callConnectEventProc(
            _manager
            , *_idUnique
        );

        addNewJoystick(
            _manager
            , std::move( _idUnique )
            , std::move( descriptorCloser )
        );

        return true;
    }

    void callDisconnectEventProc(
        sucrose::UdevJoystickEventParameters &   _eventParameters
        , const sucrose::UdevJoystickID &        _ID
    )
    {
        const auto  EVENT = sucrose::UdevJoystickDisconnectEvent( _ID );

        _eventParameters.call( EVENT );
    }

    bool disconnect(
        sucrose::UdevJoystickManager &                  _manager
        , const sucrose::UdevJoystickID::ConstUnique &  _ID_UNIQUE
    )
    {
        auto &  joystickMap = _manager.joystickMap;

        auto    it = joystickMap.find( _ID_UNIQUE );
        if( it == joystickMap.end() ) {
#ifdef  DEBUG
            std::printf( "D:IDに対応するジョイスティックが存在しない\n" );
#endif  // DEBUG

            return false;
        }

        joystickMap.erase( it );

        callDisconnectEventProc(
            _manager.eventParameters
            , *_ID_UNIQUE
        );

        return true;
    }
}

namespace sucrose {
    bool UdevJoystickManager::LessUdevJoystickIDUnique::operator()(
        const UdevJoystickID::ConstUnique &     _ID_UNIQUE1
        , const UdevJoystickID::ConstUnique &   _ID_UNIQUE2
    ) const
    {
        return *_ID_UNIQUE1 < *_ID_UNIQUE2;
    }

    UdevJoystickManager::UdevJoystickManager(
        FgState &                               _state
        , const UdevJoystickEventParameters &   _EVENT_PARAMETERS
    )
        : state( reinterpret_cast< fg::State<> & >( _state ) )
        , eventParameters( _EVENT_PARAMETERS )
    {
    }

    UdevJoystickManager * UdevJoystickManager::create_(
        FgState &                               _state
        , const UdevJoystickEventParameters &   _EVENT_PARAMETERS
    )
    {
        return new UdevJoystickManager(
            _state
            , _EVENT_PARAMETERS
        );
    }

    void UdevJoystickManager::destroy(
        UdevJoystickManager *   _implPtr
    )
    {
        delete _implPtr;
    }

    void UdevJoystickManager::analyzeUdevEvent(
        const UdevEvent &   _EVENT
    )
    {
        if( isJoystickEvent( _EVENT ) == false ) {
#ifdef  DEBUG
            std::printf( "D:ジョイスティックのイベントではない\n" );
#endif  // DEBUG

            return;
        }

        auto    devicePathPtr = static_cast< const char * >( nullptr );
        if( getDevicePath(
            devicePathPtr
            , _EVENT
        ) == false ) {
#ifdef  DEBUG
            std::printf( "D:デバイスのパス取得に失敗\n" );
#endif  // DEBUG

            return;
        }

        if( isJoystickInterface( devicePathPtr ) == false ) {
#ifdef  DEBUG
            std::printf( "D:Joystickインターフェースのデバイスではない\n" );
#endif  // DEBUG

            return;
        }

        auto    idUnique = generateUdevJoystickID( _EVENT );
        if( idUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "D:ジョイスティックIDの生成に失敗\n" );
#endif  // DEBUG

            return;
        }

        auto    action = Action::INVALID;
        if( getAction(
            action
            , _EVENT
        ) == false ) {
#ifdef  DEBUG
            std::printf( "D:デバイスのアクション取得に失敗\n" );
#endif  // DEBUG

            return;
        }

        switch( action ) {
        case Action::CONNECT:
            if( connect(
                *this
                , devicePathPtr
                , std::move( idUnique )
            ) == false ) {
#ifdef  DEBUG
                std::printf( "D:ジョイスティックの接続に失敗\n" );
#endif  // DEBUG

                return;
            }
            break;

        case Action::DISCONNECT:
            if( disconnect(
                *this
                , idUnique
            ) == false ) {
#ifdef  DEBUG
                std::printf( "D:ジョイスティックの切断に失敗\n" );
#endif  // DEBUG

                return;
            }
            break;

        default:
#ifdef  DEBUG
            std::printf( "D:対象外のイベント\n" );
#endif  // DEBUG

            break;
        }
    }
}
