﻿#include "fg/util/export.h"
#include "sucrose/udev/joystick/id.h"

#include <string>

namespace sucrose {
    UdevJoystickID::UdevJoystickID(
        const std::string &     _PORT
        , const std::string &   _DEVICE
    )
        : PORT( _PORT )
        , DEVICE( _DEVICE )
    {
    }

    UdevJoystickID::ConstUnique UdevJoystickID::create(
        const std::string &     _PORT
        , const std::string &   _DEVICE
    )
    {
        return new UdevJoystickID(
            _PORT
            , _DEVICE
        );
    }

    const char * UdevJoystickID::getPort(
    ) const
    {
        return this->PORT.c_str();
    }

    const char * UdevJoystickID::getDevice(
    ) const
    {
        return this->DEVICE.c_str();
    }

    bool UdevJoystickID::operator<(
        const UdevJoystickID &  _OTHER
    ) const
    {
        const auto  COMPARE_PORT = this->PORT.compare( _OTHER.PORT );
        if( COMPARE_PORT < 0 ) {
            return true;
        } else if( COMPARE_PORT > 0 ) {
            return false;
        }

        const auto  COMPARE_DEVICE = this->DEVICE.compare( _OTHER.DEVICE );
        if( COMPARE_DEVICE < 0 ) {
            return true;
        } else if( COMPARE_DEVICE > 0 ) {
            return false;
        }

        return false;
    }
}
