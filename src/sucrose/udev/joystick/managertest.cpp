﻿#include "fg/util/test.h"
#include "sucrose/udev/joystick/manager.h"
#include "sucrose/udev/joystick/connectevent.h"
#include "sucrose/udev/joystick/disconnectevent.h"
#include "sucrose/udev/joystick/buttonevent.h"
#include "sucrose/udev/joystick/axisevent.h"
#include "sucrose/udev/joystick/id.h"
#include "sucrose/udev/event.h"
#include "brownsugar/statemanager.h"

void createTestConnectEventProc(
    const sucrose::UdevJoystickConnectEvent &
    , int &
)
{
}

void createTestDisconnectEventProc(
    const sucrose::UdevJoystickDisconnectEvent &
    , int &
)
{
}

void createTestButtonEventProc(
    const sucrose::UdevJoystickButtonEvent &
    , int &
)
{
}

void createTestAxisEventProc(
    const sucrose::UdevJoystickAxisEvent &
    , int &
)
{
}

TEST(
    UdevJoystickManagerTest
    , Create
)
{
    auto    stateManagerUnique = brownsugar::StateManager::create();
    ASSERT_NE( nullptr, stateManagerUnique.get() );

    auto &  state = stateManagerUnique->getState();

    auto    eventParametersUnique = sucrose::UdevJoystickEventParameters::create();
    ASSERT_NE( nullptr, eventParametersUnique.get() );

    auto    i = 10;

    eventParametersUnique->setEventParameters(
        i
        , createTestConnectEventProc
        , createTestDisconnectEventProc
        , createTestButtonEventProc
        , createTestAxisEventProc
    );

    const auto  MANAGER_UNIQUE = sucrose::UdevJoystickManager::create(
        state
        , *eventParametersUnique
    );
    ASSERT_NE( nullptr, MANAGER_UNIQUE.get() );

    ASSERT_EQ( &state, &( MANAGER_UNIQUE->state ) );
    ASSERT_EQ( 10, *static_cast< const int * >( MANAGER_UNIQUE->eventParameters.dataPtr ) );
    ASSERT_EQ( reinterpret_cast< sucrose::UdevJoystickConnectEventProcImpl >( createTestConnectEventProc ), MANAGER_UNIQUE->eventParameters.connectEventProcPtr );
    ASSERT_EQ( reinterpret_cast< sucrose::UdevJoystickDisconnectEventProcImpl >( createTestDisconnectEventProc ), MANAGER_UNIQUE->eventParameters.disconnectEventProcPtr );
    ASSERT_EQ( reinterpret_cast< sucrose::UdevJoystickButtonEventProcImpl >( createTestButtonEventProc ), MANAGER_UNIQUE->eventParameters.buttonEventProcPtr );
    ASSERT_EQ( reinterpret_cast< sucrose::UdevJoystickAxisEventProcImpl >( createTestAxisEventProc ), MANAGER_UNIQUE->eventParameters.axisEventProcPtr );
    ASSERT_EQ( 0, MANAGER_UNIQUE->joystickMap.size() );
}

struct AnalyzeUdevEventConnectTestData
{
    const sucrose::UdevJoystickID * ID_PTR = nullptr;
};

void analyzeUdevEventConnectTestProc(
    const sucrose::UdevJoystickConnectEvent &   _EVENT
    , AnalyzeUdevEventConnectTestData &         _data
)
{
    _data.ID_PTR = &( _EVENT.getID() );
}

TEST(
    UdevJoystickManagerTest
    , AnalyzeUdevEvent_connect
)
{
    auto    stateManagerUnique = brownsugar::StateManager::create();
    ASSERT_NE( nullptr, stateManagerUnique.get() );

    auto &  state = stateManagerUnique->getState();

    auto    eventParametersUnique = sucrose::UdevJoystickEventParameters::create();
    ASSERT_NE( nullptr, eventParametersUnique.get() );

    AnalyzeUdevEventConnectTestData data;

    eventParametersUnique->setEventParameters(
        data
        , analyzeUdevEventConnectTestProc
    );

    auto    managerUnique = sucrose::UdevJoystickManager::create(
        state
        , *eventParametersUnique
    );
    ASSERT_NE( nullptr, managerUnique.get() );

    const auto  EVENT_UNIQUE = sucrose::UdevEvent::create(
        sucrose::UdevEvent::Properties{
            { "ID_PATH", "PORT" },
            { "ID_VENDOR_ID", "VENDOR_ID" },
            { "ID_MODEL_ID", "PRODUCT_ID" },
            { "ID_REVISION", "REVISION" },
            { "ID_INPUT_JOYSTICK", "1" },
            { "DEVNAME", "udevjoysticktest/js10" },
            { "ACTION", "add" },
        }
    );
    ASSERT_NE( nullptr, EVENT_UNIQUE.get() );

    managerUnique->analyzeUdevEvent( *EVENT_UNIQUE );

    const auto &    ID_PTR = data.ID_PTR;
    ASSERT_NE( nullptr, ID_PTR );
    const auto &    ID = *ID_PTR;
    ASSERT_STREQ( "PORT", ID.PORT.c_str() );
    ASSERT_STREQ( "VENDOR_ID/PRODUCT_ID/REVISION", ID.DEVICE.c_str() );

    const auto &    JOYSTICK_MAP = managerUnique->joystickMap;
    ASSERT_EQ( 1, JOYSTICK_MAP.size() );

    const auto &    PAIR = *( JOYSTICK_MAP.begin() );
    ASSERT_EQ( &ID, PAIR.first.get() );
}

TEST(
    UdevJoystickManagerTest
    , AnalyzeUdevEvent_initConnect
)
{
    auto    stateManagerUnique = brownsugar::StateManager::create();
    ASSERT_NE( nullptr, stateManagerUnique.get() );

    auto &  state = stateManagerUnique->getState();

    auto    eventParametersUnique = sucrose::UdevJoystickEventParameters::create();
    ASSERT_NE( nullptr, eventParametersUnique.get() );

    AnalyzeUdevEventConnectTestData data;

    eventParametersUnique->setEventParameters(
        data
        , analyzeUdevEventConnectTestProc
    );

    auto    managerUnique = sucrose::UdevJoystickManager::create(
        state
        , *eventParametersUnique
    );
    ASSERT_NE( nullptr, managerUnique.get() );

    const auto  EVENT_UNIQUE = sucrose::UdevEvent::create(
        sucrose::UdevEvent::Properties{
            { "ID_PATH", "PORT" },
            { "ID_VENDOR_ID", "VENDOR_ID" },
            { "ID_MODEL_ID", "PRODUCT_ID" },
            { "ID_REVISION", "REVISION" },
            { "ID_INPUT_JOYSTICK", "1" },
            { "DEVNAME", "udevjoysticktest/js10" },
        }
    );
    ASSERT_NE( nullptr, EVENT_UNIQUE.get() );

    managerUnique->analyzeUdevEvent( *EVENT_UNIQUE );

    const auto &    ID_PTR = data.ID_PTR;
    ASSERT_NE( nullptr, ID_PTR );
    const auto &    ID = *ID_PTR;
    ASSERT_STREQ( "PORT", ID.PORT.c_str() );
    ASSERT_STREQ( "VENDOR_ID/PRODUCT_ID/REVISION", ID.DEVICE.c_str() );

    const auto &    JOYSTICK_MAP = managerUnique->joystickMap;
    ASSERT_EQ( 1, JOYSTICK_MAP.size() );

    const auto &    PAIR = *( JOYSTICK_MAP.begin() );
    ASSERT_EQ( &ID, PAIR.first.get() );
}

struct AnalyzeUdevEventDisconnectTestData
{
    const sucrose::UdevJoystickID * ID_PTR = nullptr;
};

void analyzeUdevEventDisconnectTestProc(
    const sucrose::UdevJoystickDisconnectEvent &    _EVENT
    , AnalyzeUdevEventDisconnectTestData &          _data
)
{
    _data.ID_PTR = &( _EVENT.getID() );
}

TEST(
    UdevJoystickManagerTest
    , AnalyzeUdevEvent_disconnect
)
{
    auto    stateManagerUnique = brownsugar::StateManager::create();
    ASSERT_NE( nullptr, stateManagerUnique.get() );

    auto &  state = stateManagerUnique->getState();

    auto    eventParametersUnique = sucrose::UdevJoystickEventParameters::create();
    ASSERT_NE( nullptr, eventParametersUnique.get() );

    AnalyzeUdevEventDisconnectTestData data;

    eventParametersUnique->setEventParameters(
        data
        , analyzeUdevEventDisconnectTestProc
    );

    auto    managerUnique = sucrose::UdevJoystickManager::create(
        state
        , *eventParametersUnique
    );
    ASSERT_NE( nullptr, managerUnique.get() );

    const auto  CONNECT_EVENT_UNIQUE = sucrose::UdevEvent::create(
        sucrose::UdevEvent::Properties{
            { "ID_PATH", "PORT" },
            { "ID_VENDOR_ID", "VENDOR_ID" },
            { "ID_MODEL_ID", "PRODUCT_ID" },
            { "ID_REVISION", "REVISION" },
            { "ID_INPUT_JOYSTICK", "1" },
            { "DEVNAME", "udevjoysticktest/js10" },
            { "ACTION", "add" },
        }
    );
    ASSERT_NE( nullptr, CONNECT_EVENT_UNIQUE.get() );

    managerUnique->analyzeUdevEvent( *CONNECT_EVENT_UNIQUE );

    const auto  DISCONNECT_EVENT_UNIQUE = sucrose::UdevEvent::create(
        sucrose::UdevEvent::Properties{
            { "ID_PATH", "PORT" },
            { "ID_VENDOR_ID", "VENDOR_ID" },
            { "ID_MODEL_ID", "PRODUCT_ID" },
            { "ID_REVISION", "REVISION" },
            { "ID_INPUT_JOYSTICK", "1" },
            { "DEVNAME", "udevjoysticktest/js10" },
            { "ACTION", "remove" },
        }
    );
    ASSERT_NE( nullptr, DISCONNECT_EVENT_UNIQUE.get() );

    managerUnique->analyzeUdevEvent( *DISCONNECT_EVENT_UNIQUE );

    ASSERT_NE( nullptr, data.ID_PTR );

    ASSERT_EQ( 0, managerUnique->joystickMap.size() );
}
