﻿#include "fg/util/export.h"
#include "sucrose/udev/joystick/buttonevent.h"

namespace sucrose {
    UdevJoystickButtonEvent::UdevJoystickButtonEvent(
        const UdevJoystickID &  _ID
        , unsigned char         _index
        , bool                  _pressed
    )
        : ID( _ID )
        , index( _index )
        , pressed( _pressed )
    {
    }

    const UdevJoystickID & UdevJoystickButtonEvent::getID(
    ) const
    {
        return this->ID;
    }

    unsigned char UdevJoystickButtonEvent::getIndex(
    ) const
    {
        return this->index;
    }

    bool UdevJoystickButtonEvent::getPressed(
    ) const
    {
        return this->pressed;
    }
}
