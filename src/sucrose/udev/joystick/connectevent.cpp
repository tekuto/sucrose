﻿#include "fg/util/export.h"
#include "sucrose/udev/joystick/connectevent.h"

namespace sucrose {
    UdevJoystickConnectEvent::UdevJoystickConnectEvent(
        const UdevJoystickID &  _ID
    )
        : ID( _ID )
    {
    }

    const UdevJoystickID & UdevJoystickConnectEvent::getID(
    ) const
    {
        return this->ID;
    }
}
