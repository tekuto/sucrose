﻿#include "fg/util/test.h"
#include "sucrose/udev/joystick/buttonevent.h"
#include "sucrose/udev/joystick/id.h"

TEST(
    UdevJoystickButtonEventTest
    , Constructor
)
{
    const auto  ID_UNIQUE = sucrose::UdevJoystickID::create(
        "PORT"
        , "DEVICE"
    );
    ASSERT_NE( nullptr, ID_UNIQUE.get() );
    const auto &    ID = *ID_UNIQUE;

    const auto  EVENT = sucrose::UdevJoystickButtonEvent(
        ID
        , 10
        , true
    );

    ASSERT_EQ( &ID, &( EVENT.ID ) );
    ASSERT_EQ( 10, EVENT.index );
    ASSERT_TRUE( EVENT.pressed );
}

TEST(
    UdevJoystickButtonEventTest
    , GetID
)
{
    const auto  ID_UNIQUE = sucrose::UdevJoystickID::create(
        "PORT"
        , "DEVICE"
    );
    ASSERT_NE( nullptr, ID_UNIQUE.get() );
    const auto &    ID = *ID_UNIQUE;

    const auto  EVENT = sucrose::UdevJoystickButtonEvent(
        ID
        , 10
        , true
    );

    ASSERT_EQ( &ID, &( EVENT.getID() ) );
}

TEST(
    UdevJoystickButtonEventTest
    , GetIndex
)
{
    const auto  ID_UNIQUE = sucrose::UdevJoystickID::create(
        "PORT"
        , "DEVICE"
    );
    ASSERT_NE( nullptr, ID_UNIQUE.get() );

    const auto  EVENT = sucrose::UdevJoystickButtonEvent(
        *ID_UNIQUE
        , 10
        , true
    );

    ASSERT_EQ( 10, EVENT.getIndex() );
}

TEST(
    UdevJoystickButtonEventTest
    , GetPressed
)
{
    const auto  ID_UNIQUE = sucrose::UdevJoystickID::create(
        "PORT"
        , "DEVICE"
    );
    ASSERT_NE( nullptr, ID_UNIQUE.get() );

    const auto  EVENT = sucrose::UdevJoystickButtonEvent(
        *ID_UNIQUE
        , 10
        , true
    );

    ASSERT_TRUE( EVENT.getPressed() );
}
