﻿#include "fg/util/test.h"
#include "sucrose/udev/joystick/connectevent.h"
#include "sucrose/udev/joystick/id.h"

TEST(
    UdevJoystickConnectEventTest
    , Constructor
)
{
    const auto  ID_UNIQUE = sucrose::UdevJoystickID::create(
        "PORT"
        , "DEVICE"
    );
    ASSERT_NE( nullptr, ID_UNIQUE.get() );
    const auto &    ID = *ID_UNIQUE;

    const auto  EVENT = sucrose::UdevJoystickConnectEvent( ID );

    ASSERT_EQ( &ID, &( EVENT.ID ) );
}

TEST(
    UdevJoystickConnectEventTest
    , GetID
)
{
    const auto  ID_UNIQUE = sucrose::UdevJoystickID::create(
        "PORT"
        , "DEVICE"
    );
    ASSERT_NE( nullptr, ID_UNIQUE.get() );
    const auto &    ID = *ID_UNIQUE;

    const auto  EVENT = sucrose::UdevJoystickConnectEvent( ID );

    ASSERT_EQ( &ID, &( EVENT.getID() ) );
}
