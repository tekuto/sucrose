﻿#include "fg/util/test.h"
#include "sucrose/udev/joystick/disconnectevent.h"
#include "sucrose/udev/joystick/id.h"

TEST(
    UdevJoystickDisconnectEventTest
    , Constructor
)
{
    const auto  ID_UNIQUE = sucrose::UdevJoystickID::create(
        "PORT"
        , "DEVICE"
    );
    ASSERT_NE( nullptr, ID_UNIQUE.get() );
    const auto &    ID = *ID_UNIQUE;

    const auto  EVENT = sucrose::UdevJoystickDisconnectEvent( ID );

    ASSERT_EQ( &ID, &( EVENT.ID ) );
}

TEST(
    UdevJoystickDisconnectEventTest
    , GetID
)
{
    const auto  ID_UNIQUE = sucrose::UdevJoystickID::create(
        "PORT"
        , "DEVICE"
    );
    ASSERT_NE( nullptr, ID_UNIQUE.get() );
    const auto &    ID = *ID_UNIQUE;

    const auto  EVENT = sucrose::UdevJoystickDisconnectEvent( ID );

    ASSERT_EQ( &ID, &( EVENT.getID() ) );
}
