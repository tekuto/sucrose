﻿#include "fg/util/export.h"
#include "sucrose/udev/joystick/axisevent.h"

namespace sucrose {
    UdevJoystickAxisEvent::UdevJoystickAxisEvent(
        const UdevJoystickID &  _ID
        , unsigned char         _index
        , short                 _value
    )
        : ID( _ID )
        , index( _index )
        , value( _value )
    {
    }

    const UdevJoystickID & UdevJoystickAxisEvent::getID(
    ) const
    {
        return this->ID;
    }

    unsigned char UdevJoystickAxisEvent::getIndex(
    ) const
    {
        return this->index;
    }

    short UdevJoystickAxisEvent::getValue(
    ) const
    {
        return this->value;
    }
}
