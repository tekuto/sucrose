﻿#include "fg/util/test.h"
#include "sucrose/udev/joystick/eventparameters.h"
#include "sucrose/udev/joystick/id.h"
#include "sucrose/udev/joystick/connectevent.h"
#include "sucrose/udev/joystick/disconnectevent.h"
#include "sucrose/udev/joystick/buttonevent.h"
#include "sucrose/udev/joystick/axisevent.h"

TEST(
    UdevJoystickEventParametersTest
    , Create
)
{
    const auto  EVENT_PARAMETERS_UNIQUE = sucrose::UdevJoystickEventParameters::create();
    ASSERT_NE( nullptr, EVENT_PARAMETERS_UNIQUE.get() );

    ASSERT_EQ( nullptr, EVENT_PARAMETERS_UNIQUE->dataPtr );
    ASSERT_EQ( nullptr, EVENT_PARAMETERS_UNIQUE->connectEventProcPtr );
    ASSERT_EQ( nullptr, EVENT_PARAMETERS_UNIQUE->disconnectEventProcPtr );
    ASSERT_EQ( nullptr, EVENT_PARAMETERS_UNIQUE->buttonEventProcPtr );
    ASSERT_EQ( nullptr, EVENT_PARAMETERS_UNIQUE->axisEventProcPtr );
}

TEST(
    UdevJoystickEventParametersTest
    , SetEventData
)
{
    auto    eventParametersUnique = sucrose::UdevJoystickEventParameters::create();
    ASSERT_NE( nullptr, eventParametersUnique.get() );

    auto    i = 10;

    eventParametersUnique->setEventData( i );

    ASSERT_EQ( 10, *static_cast< const int * >( eventParametersUnique->dataPtr ) );
}

void setConnectEventProcTestProc(
    const sucrose::UdevJoystickConnectEvent &
    , void *
)
{
}

TEST(
    UdevJoystickEventParametersTest
    , SetConnectEventProc
)
{
    auto    eventParametersUnique = sucrose::UdevJoystickEventParameters::create();
    ASSERT_NE( nullptr, eventParametersUnique.get() );

    eventParametersUnique->setEventProc( setConnectEventProcTestProc );

    ASSERT_EQ( setConnectEventProcTestProc, eventParametersUnique->connectEventProcPtr );
}

void setDisconnectEventProcTestProc(
    const sucrose::UdevJoystickDisconnectEvent &
    , void *
)
{
}

TEST(
    UdevJoystickEventParametersTest
    , SetDisconnectEventProc
)
{
    auto    eventParametersUnique = sucrose::UdevJoystickEventParameters::create();
    ASSERT_NE( nullptr, eventParametersUnique.get() );

    eventParametersUnique->setEventProc( setDisconnectEventProcTestProc );

    ASSERT_EQ( setDisconnectEventProcTestProc, eventParametersUnique->disconnectEventProcPtr );
}

void setButtonEventProcTestProc(
    const sucrose::UdevJoystickButtonEvent &
    , void *
)
{
}

TEST(
    UdevJoystickEventParametersTest
    , SetButtonEventProc
)
{
    auto    eventParametersUnique = sucrose::UdevJoystickEventParameters::create();
    ASSERT_NE( nullptr, eventParametersUnique.get() );

    eventParametersUnique->setEventProc( setButtonEventProcTestProc );

    ASSERT_EQ( setButtonEventProcTestProc, eventParametersUnique->buttonEventProcPtr );
}

void setAxisEventProcTestProc(
    const sucrose::UdevJoystickAxisEvent &
    , void *
)
{
}

TEST(
    UdevJoystickEventParametersTest
    , SetAxisEventProc
)
{
    auto    eventParametersUnique = sucrose::UdevJoystickEventParameters::create();
    ASSERT_NE( nullptr, eventParametersUnique.get() );

    eventParametersUnique->setEventProc( setAxisEventProcTestProc );

    ASSERT_EQ( setAxisEventProcTestProc, eventParametersUnique->axisEventProcPtr );
}

void setEventProcsTestConnectEventProc(
    const sucrose::UdevJoystickConnectEvent &
    , void *
)
{
}

void setEventProcsTestDisconnectEventProc(
    const sucrose::UdevJoystickDisconnectEvent &
    , void *
)
{
}

void setEventProcsTestButtonEventProc(
    const sucrose::UdevJoystickButtonEvent &
    , void *
)
{
}

void setEventProcsTestAxisEventProc(
    const sucrose::UdevJoystickAxisEvent &
    , void *
)
{
}

TEST(
    UdevJoystickEventParametersTest
    , SetEventProcs
)
{
    auto    eventParametersUnique = sucrose::UdevJoystickEventParameters::create();
    ASSERT_NE( nullptr, eventParametersUnique.get() );

    eventParametersUnique->setEventProcs(
        setEventProcsTestConnectEventProc
        , setEventProcsTestDisconnectEventProc
        , setEventProcsTestButtonEventProc
        , setEventProcsTestAxisEventProc
    );

    ASSERT_EQ( setEventProcsTestConnectEventProc, eventParametersUnique->connectEventProcPtr );
    ASSERT_EQ( setEventProcsTestDisconnectEventProc, eventParametersUnique->disconnectEventProcPtr );
    ASSERT_EQ( setEventProcsTestButtonEventProc, eventParametersUnique->buttonEventProcPtr );
    ASSERT_EQ( setEventProcsTestAxisEventProc, eventParametersUnique->axisEventProcPtr );
}

void setEventProcsTemplateTestConnectEventProc(
    const sucrose::UdevJoystickConnectEvent &
    , int &
)
{
}

void setEventProcsTemplateTestDisconnectEventProc(
    const sucrose::UdevJoystickDisconnectEvent &
    , int &
)
{
}

void setEventProcsTemplateTestButtonEventProc(
    const sucrose::UdevJoystickButtonEvent &
    , int &
)
{
}

void setEventProcsTemplateTestAxisEventProc(
    const sucrose::UdevJoystickAxisEvent &
    , int &
)
{
}

TEST(
    UdevJoystickEventParametersTest
    , SetEventProcsTemplate
)
{
    auto    eventParametersUnique = sucrose::UdevJoystickEventParameters::create();
    ASSERT_NE( nullptr, eventParametersUnique.get() );

    eventParametersUnique->setEventProcs< int >(
        setEventProcsTemplateTestConnectEventProc
        , setEventProcsTemplateTestDisconnectEventProc
        , setEventProcsTemplateTestButtonEventProc
        , setEventProcsTemplateTestAxisEventProc
    );

    ASSERT_EQ( reinterpret_cast< sucrose::UdevJoystickConnectEventProcImpl >( setEventProcsTemplateTestConnectEventProc ), eventParametersUnique->connectEventProcPtr );
    ASSERT_EQ( reinterpret_cast< sucrose::UdevJoystickDisconnectEventProcImpl >( setEventProcsTemplateTestDisconnectEventProc ), eventParametersUnique->disconnectEventProcPtr );
    ASSERT_EQ( reinterpret_cast< sucrose::UdevJoystickButtonEventProcImpl >( setEventProcsTemplateTestButtonEventProc ), eventParametersUnique->buttonEventProcPtr );
    ASSERT_EQ( reinterpret_cast< sucrose::UdevJoystickAxisEventProcImpl >( setEventProcsTemplateTestAxisEventProc ), eventParametersUnique->axisEventProcPtr );
}

void setEventParametersTestConnectEventProc(
    const sucrose::UdevJoystickConnectEvent &
    , void *
)
{
}

void setEventParametersTestDisconnectEventProc(
    const sucrose::UdevJoystickDisconnectEvent &
    , void *
)
{
}

void setEventParametersTestButtonEventProc(
    const sucrose::UdevJoystickButtonEvent &
    , void *
)
{
}

void setEventParametersTestAxisEventProc(
    const sucrose::UdevJoystickAxisEvent &
    , void *
)
{
}

TEST(
    UdevJoystickEventParametersTest
    , SetEventParameters
)
{
    auto    eventParametersUnique = sucrose::UdevJoystickEventParameters::create();
    ASSERT_NE( nullptr, eventParametersUnique.get() );

    auto    i = 10;

    eventParametersUnique->setEventParameters(
        static_cast< void * >( &i )
        , setEventParametersTestConnectEventProc
        , setEventParametersTestDisconnectEventProc
        , setEventParametersTestButtonEventProc
        , setEventParametersTestAxisEventProc
    );

    ASSERT_EQ( 10, *static_cast< const int * >( eventParametersUnique->dataPtr ) );
    ASSERT_EQ( setEventParametersTestConnectEventProc, eventParametersUnique->connectEventProcPtr );
    ASSERT_EQ( setEventParametersTestDisconnectEventProc, eventParametersUnique->disconnectEventProcPtr );
    ASSERT_EQ( setEventParametersTestButtonEventProc, eventParametersUnique->buttonEventProcPtr );
    ASSERT_EQ( setEventParametersTestAxisEventProc, eventParametersUnique->axisEventProcPtr );
}

void setEventParametersTemplateTestConnectEventProc(
    const sucrose::UdevJoystickConnectEvent &
    , int &
)
{
}

void setEventParametersTemplateTestDisconnectEventProc(
    const sucrose::UdevJoystickDisconnectEvent &
    , int &
)
{
}

void setEventParametersTemplateTestButtonEventProc(
    const sucrose::UdevJoystickButtonEvent &
    , int &
)
{
}

void setEventParametersTemplateTestAxisEventProc(
    const sucrose::UdevJoystickAxisEvent &
    , int &
)
{
}

TEST(
    UdevJoystickEventParametersTest
    , SetEventParametersTemplate
)
{
    auto    eventParametersUnique = sucrose::UdevJoystickEventParameters::create();
    ASSERT_NE( nullptr, eventParametersUnique.get() );

    auto    i = 10;

    eventParametersUnique->setEventParameters(
        i
        , setEventParametersTemplateTestConnectEventProc
        , setEventParametersTemplateTestDisconnectEventProc
        , setEventParametersTemplateTestButtonEventProc
        , setEventParametersTemplateTestAxisEventProc
    );

    ASSERT_EQ( 10, *static_cast< const int * >( eventParametersUnique->dataPtr ) );
    ASSERT_EQ( reinterpret_cast< sucrose::UdevJoystickConnectEventProcImpl >( setEventParametersTemplateTestConnectEventProc ), eventParametersUnique->connectEventProcPtr );
    ASSERT_EQ( reinterpret_cast< sucrose::UdevJoystickDisconnectEventProcImpl >( setEventParametersTemplateTestDisconnectEventProc ), eventParametersUnique->disconnectEventProcPtr );
    ASSERT_EQ( reinterpret_cast< sucrose::UdevJoystickButtonEventProcImpl >( setEventParametersTemplateTestButtonEventProc ), eventParametersUnique->buttonEventProcPtr );
    ASSERT_EQ( reinterpret_cast< sucrose::UdevJoystickAxisEventProcImpl >( setEventParametersTemplateTestAxisEventProc ), eventParametersUnique->axisEventProcPtr );
}

struct CallConnectEventProcTestData
{
    const sucrose::UdevJoystickID * ID_PTR = nullptr;
};

void callConnectEventProcTestProc(
    const sucrose::UdevJoystickConnectEvent &   _EVENT
    , CallConnectEventProcTestData &            _data
)
{
    _data.ID_PTR = &( _EVENT.getID() );
}

TEST(
    UdevJoystickEventParametersTest
    , CallConnectEventProc
)
{
    auto    eventParametersUnique = sucrose::UdevJoystickEventParameters::create();
    ASSERT_NE( nullptr, eventParametersUnique.get() );

    CallConnectEventProcTestData    data;

    eventParametersUnique->setEventParameters(
        data
        , callConnectEventProcTestProc
    );

    const auto  ID_UNIQUE = sucrose::UdevJoystickID::create(
        "PORT"
        , "DEVICE"
    );
    ASSERT_NE( nullptr, ID_UNIQUE.get() );
    const auto &    ID = *ID_UNIQUE;

    const auto  EVENT = sucrose::UdevJoystickConnectEvent( ID );

    eventParametersUnique->call( EVENT );

    ASSERT_EQ( &ID, data.ID_PTR );
}

struct CallDisconnectEventProcTestData
{
    const sucrose::UdevJoystickID * ID_PTR = nullptr;
};

void callDisconnectEventProcTestProc(
    const sucrose::UdevJoystickDisconnectEvent &    _EVENT
    , CallDisconnectEventProcTestData &             _data
)
{
    _data.ID_PTR = &( _EVENT.getID() );
}

TEST(
    UdevJoystickEventParametersTest
    , CallDisconnectEventProc
)
{
    auto    eventParametersUnique = sucrose::UdevJoystickEventParameters::create();
    ASSERT_NE( nullptr, eventParametersUnique.get() );

    CallDisconnectEventProcTestData data;

    eventParametersUnique->setEventParameters(
        data
        , callDisconnectEventProcTestProc
    );

    const auto  ID_UNIQUE = sucrose::UdevJoystickID::create(
        "PORT"
        , "DEVICE"
    );
    ASSERT_NE( nullptr, ID_UNIQUE.get() );
    const auto &    ID = *ID_UNIQUE;

    const auto  EVENT = sucrose::UdevJoystickDisconnectEvent( ID );

    eventParametersUnique->call( EVENT );

    ASSERT_EQ( &ID, data.ID_PTR );
}

struct CallButtonEventProcTestData
{
    const sucrose::UdevJoystickID * ID_PTR = nullptr;
    unsigned char                   index = 0;
    bool                            pressed = false;
};

void callButtonEventProcTestProc(
    const sucrose::UdevJoystickButtonEvent &    _EVENT
    , CallButtonEventProcTestData &             _data
)
{
    _data.ID_PTR = &( _EVENT.getID() );
    _data.index = _EVENT.getIndex();
    _data.pressed = _EVENT.getPressed();
}

TEST(
    UdevJoystickEventParametersTest
    , CallButtonEventProc
)
{
    auto    eventParametersUnique = sucrose::UdevJoystickEventParameters::create();
    ASSERT_NE( nullptr, eventParametersUnique.get() );

    CallButtonEventProcTestData data;

    eventParametersUnique->setEventParameters(
        data
        , callButtonEventProcTestProc
    );

    const auto  ID_UNIQUE = sucrose::UdevJoystickID::create(
        "PORT"
        , "DEVICE"
    );
    ASSERT_NE( nullptr, ID_UNIQUE.get() );
    const auto &    ID = *ID_UNIQUE;

    const auto  EVENT = sucrose::UdevJoystickButtonEvent(
        ID
        , 10
        , true
    );

    eventParametersUnique->call( EVENT );

    ASSERT_EQ( &ID, data.ID_PTR );
    ASSERT_EQ( 10, data.index );
    ASSERT_TRUE( data.pressed );
}

struct CallAxisEventProcTestData
{
    const sucrose::UdevJoystickID * ID_PTR = nullptr;
    unsigned char                   index = 0;
    short                           value = 0;
};

void callAxisEventProcTestProc(
    const sucrose::UdevJoystickAxisEvent &  _EVENT
    , CallAxisEventProcTestData &           _data
)
{
    _data.ID_PTR = &( _EVENT.getID() );
    _data.index = _EVENT.getIndex();
    _data.value = _EVENT.getValue();
}

TEST(
    UdevJoystickEventParametersTest
    , CallAxisEventProc
)
{
    auto    eventParametersUnique = sucrose::UdevJoystickEventParameters::create();
    ASSERT_NE( nullptr, eventParametersUnique.get() );

    CallAxisEventProcTestData data;

    eventParametersUnique->setEventParameters(
        data
        , callAxisEventProcTestProc
    );

    const auto  ID_UNIQUE = sucrose::UdevJoystickID::create(
        "PORT"
        , "DEVICE"
    );
    ASSERT_NE( nullptr, ID_UNIQUE.get() );
    const auto &    ID = *ID_UNIQUE;

    const auto  EVENT = sucrose::UdevJoystickAxisEvent(
        ID
        , 10
        , 20
    );

    eventParametersUnique->call( EVENT );

    ASSERT_EQ( &ID, data.ID_PTR );
    ASSERT_EQ( 10, data.index );
    ASSERT_EQ( 20, data.value );
}
