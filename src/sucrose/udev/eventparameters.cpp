﻿#include "fg/util/export.h"
#include "sucrose/udev/eventparameters.h"

namespace sucrose {
    UdevEventParameters * UdevEventParameters::create_(
    )
    {
        return new UdevEventParameters;
    }

    void UdevEventParameters::destroy(
        UdevEventParameters *   _implPtr
    )
    {
        delete _implPtr;
    }

    void UdevEventParameters::setEventData(
        void *  _dataPtr
    )
    {
        this->dataPtr = _dataPtr;
    }

    void UdevEventParameters::setEventProc(
        UdevEventProcImpl   _procPtr
    )
    {
        this->eventProcPtr = _procPtr;
    }

    void UdevEventParameters::call(
        const UdevEvent &   _EVENT
    )
    {
        auto &  eventProcPtr = this->eventProcPtr;
        if( eventProcPtr == nullptr ) {
            return;
        }

        eventProcPtr(
            _EVENT
            , this->dataPtr
        );
    }
}
