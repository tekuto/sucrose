﻿#include "fg/util/test.h"
#include "sucrose/window/window.h"

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <thread>
#include <chrono>

TEST(
    WindowTest
    , Create
)
{
    auto    windowUnique = fg::Window::create(
        "WindowTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = **windowUnique;

    ASSERT_NE( nullptr, window.xDisplayForEventUnique.get() );
    ASSERT_NE( nullptr, window.xDisplayForPaintUnique.get() );
    ASSERT_NE( None, window.wmDeleteWindow );
    ASSERT_EQ( &window, window.xWindowDestroyer.get() );

    auto &  xDisplay = **( window.xDisplayForEventUnique );
    auto &  xWindow = window.xWindow;

    auto    namePtr = static_cast< char * >( nullptr );
    ASSERT_NE(
        0
        , XFetchName(
            &xDisplay
            , xWindow
            , &namePtr
        )
    );
    ASSERT_NE( nullptr, namePtr );
    ASSERT_STREQ( "WindowTest", namePtr );
    XFree( namePtr );

    auto    attributes = XWindowAttributes();
    ASSERT_NE(
        0
        , XGetWindowAttributes(
            &xDisplay
            , xWindow
            , &attributes
        )
    );
    ASSERT_EQ( 100, attributes.width );
    ASSERT_EQ( 200, attributes.height );

    auto    sizeHints = XSizeHints();
    auto    tmp = static_cast< long >( 0 );
    ASSERT_NE(
        0
        , XGetWMNormalHints(
            &xDisplay
            , xWindow
            , &sizeHints
            , &tmp
        )
    );

    ASSERT_EQ( 100, sizeHints.min_width );
    ASSERT_EQ( 200, sizeHints.min_height );
    ASSERT_EQ( 100, sizeHints.max_width );
    ASSERT_EQ( 200, sizeHints.max_height );
    ASSERT_EQ( PMinSize | PMaxSize, sizeHints.flags );
}

Bool checkEventForFlush(
    Display *
    , XEvent *
    , XPointer
)
{
    return True;
}

void flushEventQueue(
    FgWindow &  _window
)
{
    auto    xEvent = XEvent();

    while(
        XCheckIfEvent(
            &**( _window.xDisplayForEventUnique )
            , &xEvent
            , checkEventForFlush
            , nullptr
        ) == True
    ) {
    }
}

TEST(
    WindowTest
    , Repaint
)
{
    auto    windowUnique = fg::Window::create(
        "WindowTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = **windowUnique;

    std::this_thread::sleep_for( std::chrono::milliseconds( 100 ) );

    flushEventQueue( window );

    windowUnique->repaint();

    std::this_thread::sleep_for( std::chrono::milliseconds( 100 ) );

    auto    xEvent = XEvent();

    ASSERT_EQ(
        True
        , XCheckWindowEvent(
            &**( window.xDisplayForEventUnique )
            , window.xWindow
            , ExposureMask
            , &xEvent
        )
    );
}

TEST(
    WindowTest
    , PollXEvent
)
{
    auto    windowUnique = fg::Window::create(
        "WindowTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = **windowUnique;

    std::this_thread::sleep_for( std::chrono::milliseconds( 100 ) );

    flushEventQueue( window );

    ASSERT_FALSE( window.pollXEvent() );

    windowUnique->repaint();

    ASSERT_TRUE( window.pollXEvent() );
}

TEST(
    WindowTest
    , GetXEvent
)
{
    auto    windowUnique = fg::Window::create(
        "WindowTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = **windowUnique;

    std::this_thread::sleep_for( std::chrono::milliseconds( 100 ) );

    flushEventQueue( window );

    windowUnique->repaint();

    auto    xEvent = XEvent();
    window.getXEvent( xEvent );

    ASSERT_EQ( Expose, xEvent.type );
}

TEST(
    WindowTest
    , IsWmDeleteWindow
)
{
    auto    windowUnique = fg::Window::create(
        "WindowTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    const auto  BLANK = Atom();

    ASSERT_FALSE( window->isWmDeleteWindow( BLANK ) );

    const auto  WM_DELETE_WINDOW = XInternAtom(
        &**( window->xDisplayForEventUnique )
        , "WM_DELETE_WINDOW"
        , True
    );
    ASSERT_NE( None, WM_DELETE_WINDOW );

    ASSERT_TRUE( window->isWmDeleteWindow( WM_DELETE_WINDOW ) );
}

TEST(
    WindowTest
    , GetXDisplayForPaint
)
{
    auto    windowUnique = fg::Window::create(
        "WindowTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = **windowUnique;

    ASSERT_EQ( &**( window.xDisplayForPaintUnique ), &( window.getXDisplayForPaint() ) );
}

TEST(
    WindowTest
    , GetXWindow
)
{
    auto    windowUnique = fg::Window::create(
        "WindowTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = **windowUnique;

    ASSERT_EQ( &( window.xWindow ), &( window.getXWindow() ) );
}
