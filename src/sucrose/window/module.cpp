﻿#include "fg/util/export.h"
#include "sucrose/window/module.h"
#include "sucrose/window/xlib.h"

namespace sucrose {
    void initializeWindow(
        const fg::ModuleContext &
    )
    {
        initializeXlib();
    }
}
