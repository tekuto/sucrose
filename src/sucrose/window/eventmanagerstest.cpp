﻿#include "fg/util/test.h"
#include "sucrose/window/eventmanagers.h"

TEST(
    WindowEventManagersTest
    , Create
)
{
    const auto  EVENT_MANAGERS_UNIQUE = fg::WindowEventManagers::create();
    ASSERT_NE( nullptr, EVENT_MANAGERS_UNIQUE.get() );
    const auto &    EVENT_MANAGERS = *EVENT_MANAGERS_UNIQUE;

    ASSERT_NE( nullptr, EVENT_MANAGERS->paintEventManagerUnique.get() );
    ASSERT_NE( nullptr, EVENT_MANAGERS->closeEventManagerUnique.get() );
}

TEST(
    WindowEventManagersTest
    , GetPaintEventManager
)
{
    auto    eventManagersUnique = fg::WindowEventManagers::create();
    ASSERT_NE( nullptr, eventManagersUnique.get() );
    auto &  eventManagers = *eventManagersUnique;

    ASSERT_EQ( eventManagers->paintEventManagerUnique.get(), &( eventManagers.getPaintEventManager() ) );
}

TEST(
    WindowEventManagersTest
    , GetCloseEventManager
)
{
    auto    eventManagersUnique = fg::WindowEventManagers::create();
    ASSERT_NE( nullptr, eventManagersUnique.get() );
    auto &  eventManagers = *eventManagersUnique;

    ASSERT_EQ( eventManagers->closeEventManagerUnique.get(), &( eventManagers.getCloseEventManager() ) );
}
