﻿#include "fg/util/export.h"
#include "sucrose/window/window.h"

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <errno.h>
#include <poll.h>
#include <stdexcept>

namespace {
    enum {
        TIMEOUT_NANOSECONDS = 100000000,    // 100ミリ秒
    };

    Atom initializeWmDeleteWindow(
        Display &   _xDisplay
    )
    {
        const auto  ATOM = XInternAtom(
            &_xDisplay
            , "WM_DELETE_WINDOW"
            , True
        );
        if( ATOM == None ) {
            throw std::runtime_error( "WM_DELETE_WINDOWに対応したアトムの取得に失敗" );
        }

        return ATOM;
    }

    Window createXWindow(
        Display &   _xDisplay
        , int       _width
        , int       _height
    )
    {
        auto    attributes = XSetWindowAttributes();
        attributes.event_mask =
            ExposureMask
        ;

        errno = 0;
        auto    xWindow = XCreateWindow(
            &_xDisplay
            , XDefaultRootWindow( &_xDisplay )
            , 0
            , 0
            , _width
            , _height
            , 0
            , CopyFromParent
            , InputOutput
            , XDefaultVisual(
                &_xDisplay
                , XDefaultScreen( &_xDisplay )
            )
            , CWEventMask
            , &attributes
        );
        if( errno != 0 ) {
            throw std::runtime_error( "XCreateWindow()が失敗" );
        }

        return xWindow;
    }

    void destroyXWindow(
        Display &   _xDisplay
        , Window &  _xWindow
    )
    {
        XDestroyWindow(
            &_xDisplay
            , _xWindow
        );
    }

    void fixWindowSize(
        Display &   _xDisplay
        , Window &  _xWindow
        , int       _width
        , int       _height
    )
    {
        auto    sizeHints = XSizeHints();

        sizeHints.min_width = _width;
        sizeHints.min_height = _height;
        sizeHints.max_width = _width;
        sizeHints.max_height = _height;

        sizeHints.flags = PMinSize | PMaxSize;

        XSetWMNormalHints(
            &_xDisplay
            , _xWindow
            , &sizeHints
        );
    }

    void setWmProtocols(
        Display &   _xDisplay
        , Window &  _xWindow
        , Atom &    _wmDeleteWindow
    )
    {
        if( XSetWMProtocols(
            &_xDisplay
            , _xWindow
            , &_wmDeleteWindow
            , 1
        ) == 0 ) {
            throw std::runtime_error( "XSetWMProtocols()が失敗" );
        }
    }

    void setWindowTitle(
        Display &       _xDisplay
        , Window &      _xWindow
        , const char *  _TITLE
    )
    {
        if( XStoreName(
            &_xDisplay
            , _xWindow
            , _TITLE
        ) == 0 ) {
            throw std::runtime_error( "XStoreName()が失敗" );
        }
    }

    void showWindow(
        Display &   _xDisplay
        , Window &  _xWindow
    )
    {
        if( XMapWindow(
            &_xDisplay
            , _xWindow
        ) == 0 ) {
            throw std::runtime_error( "XMapWindow()が失敗" );
        }
    }

    void initializeXWindow(
        FgWindow &      _window
        , const char *  _TITLE
        , int           _width
        , int           _height
    )
    {
        auto &  xDisplay = **( _window.xDisplayForEventUnique );
        auto &  xWindow = _window.xWindow;

        fixWindowSize(
            xDisplay
            , xWindow
            , _width
            , _height
        );

        setWmProtocols(
            xDisplay
            , xWindow
            , _window.wmDeleteWindow
        );

        setWindowTitle(
            xDisplay
            , xWindow
            , _TITLE
        );

        showWindow(
            xDisplay
            , xWindow
        );
    }

    void flush(
        FgWindow &  _window
    )
    {
        XFlush( &**( _window.xDisplayForEventUnique ) );
    }

    void sendXEvent(
        XEvent &        _xEvent
        , FgWindow &    _window
        , int           _eventMask
    )
    {
        auto    xDisplayPtr = &**( _window.xDisplayForEventUnique );
        auto &  xWindow = _window.xWindow;

        _xEvent.xany.display = xDisplayPtr;
        _xEvent.xany.window = xWindow;

        XSendEvent(
            xDisplayPtr
            , xWindow
            , False
            , _eventMask
            , &_xEvent
        );
    }

    void sendExposeEvent(
        FgWindow &  _window
    )
    {
        auto    xEvent = XEvent();
        xEvent.xclient.type = Expose;

        sendXEvent(
            xEvent
            , _window
            , ExposureMask
        );
    }

    //TODO 共通化するべき
    bool poll(
        int _descriptor
    )
    {
        auto    pollFd = pollfd{
            _descriptor,
            POLLIN,
        };

        const auto  TIMEOUT = timespec{
            0,
            TIMEOUT_NANOSECONDS,
        };

        if( ppoll(
            &pollFd
            , 1
            , &TIMEOUT
            , nullptr
        ) <= 0 ) {
            return false;
        }

        return true;
    }
}

FgWindow::XDisplay::Unique FgWindow::XDisplay::create(
)
{
    auto    xDisplayUnique = FgWindow::XDisplay::Unique( XOpenDisplay( nullptr ) );
    if( xDisplayUnique.get() == nullptr ) {
        throw std::runtime_error( "XOpenDisplay()が失敗" );
    }

    return xDisplayUnique;
}

void FgWindow::XDisplay::destroy(
    FgWindow::XDisplay *    _this
)
{
    XCloseDisplay( &**_this );
}

void FgWindow::DestroyXWindow::operator()(
    FgWindow *  _windowPtr
) const
{
    auto &  window = *_windowPtr;

    destroyXWindow(
        **( window.xDisplayForEventUnique )
        , window.xWindow
    );

    flush( window );
}

FgWindow::FgWindow(
    const char *    _TITLE
    , int           _width
    , int           _height
)
    : xDisplayForEventUnique( FgWindow::XDisplay::create() )
    , xDisplayForPaintUnique( FgWindow::XDisplay::create() )
    , wmDeleteWindow( initializeWmDeleteWindow( **( this->xDisplayForEventUnique ) ) )
    , xWindow(
        createXWindow(
            **( this->xDisplayForEventUnique )
            , _width
            , _height
        )
    )
    , xWindowDestroyer( this )
{
    initializeXWindow(
        *this
        , _TITLE
        , _width
        , _height
    );

    flush( *this );
}

bool FgWindow::pollXEvent(
)
{
    auto    descriptor = XConnectionNumber( &**( this->xDisplayForEventUnique ) );

    return poll( descriptor );
}

void FgWindow::getXEvent(
    XEvent &    _xEvent
)
{
    XNextEvent(
        &**( this->xDisplayForEventUnique )
        , &_xEvent
    );
}

bool FgWindow::isWmDeleteWindow(
    const Atom &    _ATOM
) const
{
    return _ATOM == this->wmDeleteWindow;
}

Display & FgWindow::getXDisplayForPaint(
)
{
    return **( this->xDisplayForPaintUnique );
}

Window & FgWindow::getXWindow(
)
{
    return this->xWindow;
}

FgWindow * fgWindowCreate(
    const char *    _TITLE
    , int           _width
    , int           _height
)
{
    return new FgWindow(
        _TITLE
        , _width
        , _height
    );
}

void fgWindowDestroy(
    FgWindow *  _implPtr
)
{
    delete _implPtr;
}

void fgWindowRepaint(
    FgWindow *  _implPtr
)
{
    auto &  impl = *_implPtr;

    sendExposeEvent( impl );

    flush( impl );
}
