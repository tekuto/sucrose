﻿#include "fg/util/test.h"
#include "sucrose/window/eventprocessor.h"
#include "sucrose/window/xlib.h"
#include "sucrose/window/window.h"
#include "fg/window/window.h"
#include "fg/window/eventmanagers.h"
#include "fg/window/paintevent.h"
#include "fg/window/closeevent.h"
#include "fg/core/state/state.h"
#include "fg/core/state/creating.h"
#include "brownsugar/statemanager.h"

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <cstring>

TEST(
    WindowEventProcessorTest
    , Create
)
{
    sucrose::initializeXlib();

    auto    stateManagerUnique = brownsugar::StateManager::create();
    ASSERT_NE( nullptr, stateManagerUnique.get() );

    auto &  state = stateManagerUnique->getState();

    auto    windowUnique = fg::Window::create(
        "WindowEventProcessorTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    eventManagersUnique = fg::WindowEventManagers::create();
    ASSERT_NE( nullptr, eventManagersUnique.get() );
    auto &  eventManagers = *eventManagersUnique;

    const auto  EVENT_PROCESSOR_UNIQUE = fg::WindowEventProcessor::create(
        state
        , window
        , eventManagers
    );
    ASSERT_NE( nullptr, EVENT_PROCESSOR_UNIQUE.get() );
    const auto &    EVENT_PROCESSOR = **EVENT_PROCESSOR_UNIQUE;

    ASSERT_EQ( &state, &( EVENT_PROCESSOR.state ) );
    ASSERT_EQ( &window, &( EVENT_PROCESSOR.window ) );
    ASSERT_EQ( &eventManagers, &( EVENT_PROCESSOR.eventManagers ) );
    ASSERT_NE( nullptr, EVENT_PROCESSOR.stateJoinerUnique.get() );
    ASSERT_FALSE( EVENT_PROCESSOR.ended );
    ASSERT_NE( nullptr, EVENT_PROCESSOR.threadJoinerUnique.get() );
    ASSERT_NE( nullptr, EVENT_PROCESSOR.ender.get() );
}

struct PaintEventTestData : public fg::UniqueWrapper< PaintEventTestData >
{
    int &   i;

    PaintEventTestData(
        int &   _i
    )
        : i( _i )
    {
    }

    static Unique create(
        int &   _i
    )
    {
        return new PaintEventTestData( _i );
    }
};

void paintEventTestProc(
    fg::WindowPaintEvent< PaintEventTestData > &    _event
)
{
    _event.getState().getData().i = 20;
}

TEST(
    WindowEventProcessorTest
    , PaintEvent
)
{
    sucrose::initializeXlib();

    auto    stateManagerUnique = brownsugar::StateManager::create();
    ASSERT_NE( nullptr, stateManagerUnique.get() );

    auto &  rootState = stateManagerUnique->getState();

    auto    i = 10;

    auto    statePtr = static_cast< fg::State< PaintEventTestData > * >( nullptr );

    rootState.enter(
        [
            &i
            , &statePtr
        ]
        (
            fg::CreatingState< PaintEventTestData > &   _state
        )
        {
            statePtr = reinterpret_cast< fg::State< PaintEventTestData > * >( &_state );

            return PaintEventTestData::create( i ).release();
        }
        , PaintEventTestData::destroy
    );

    auto &  state = *statePtr;

    auto    windowUnique = fg::Window::create(
        "WindowEventProcessorTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    eventManagersUnique = fg::WindowEventManagers::create();
    ASSERT_NE( nullptr, eventManagersUnique.get() );
    auto &  eventManagers = *eventManagersUnique;

    auto &  paintEventManager = eventManagers.getPaintEventManager();

    auto    eventRegisterManagerUnique = fg::WindowPaintEventRegisterManager::create(
        paintEventManager
        , state
        , paintEventTestProc
    );
    ASSERT_NE( nullptr, eventRegisterManagerUnique.get() );

    auto    eventProcessorUnique = fg::WindowEventProcessor::create(
        rootState   // 無効ステートでも稼働できることを確認
        , window
        , eventManagers
    );
    ASSERT_NE( nullptr, eventProcessorUnique.get() );
    auto &  eventProcessor = *eventProcessorUnique;

    window.repaint();

    std::this_thread::sleep_for( std::chrono::milliseconds( 100 ) );    // イベント実行開始待機

    eventProcessor->stateJoinerUnique->join();

    ASSERT_EQ( 20, i );
}

struct CloseEventTestData : public fg::UniqueWrapper< CloseEventTestData >
{
    int &   i;

    CloseEventTestData(
        int &   _i
    )
        : i( _i )
    {
    }

    static Unique create(
        int &   _i
    )
    {
        return new CloseEventTestData( _i );
    }
};

void closeEventTestProc(
    fg::WindowCloseEvent< CloseEventTestData > &    _event
)
{
    _event.getState().getData().i = 20;
}

TEST(
    WindowEventProcessorTest
    , CloseEvent
)
{
    sucrose::initializeXlib();

    auto    stateManagerUnique = brownsugar::StateManager::create();
    ASSERT_NE( nullptr, stateManagerUnique.get() );

    auto &  rootState = stateManagerUnique->getState();

    auto    i = 10;

    auto    statePtr = static_cast< fg::State< CloseEventTestData > * >( nullptr );

    rootState.enter(
        [
            &i
            , &statePtr
        ]
        (
            fg::CreatingState< CloseEventTestData > &   _state
        )
        {
            statePtr = reinterpret_cast< fg::State< CloseEventTestData > * >( &_state );

            return CloseEventTestData::create( i ).release();
        }
        , CloseEventTestData::destroy
    );

    auto &  state = *statePtr;

    auto    windowUnique = fg::Window::create(
        "WindowEventProcessorTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    eventManagersUnique = fg::WindowEventManagers::create();
    ASSERT_NE( nullptr, eventManagersUnique.get() );
    auto &  eventManagers = *eventManagersUnique;

    auto &  closeEventManager = eventManagers.getCloseEventManager();

    auto    eventRegisterManagerUnique = fg::WindowCloseEventRegisterManager::create(
        closeEventManager
        , state
        , closeEventTestProc
    );
    ASSERT_NE( nullptr, eventRegisterManagerUnique.get() );

    auto    eventProcessorUnique = fg::WindowEventProcessor::create(
        rootState   // 無効ステートでも稼働できることを確認
        , window
        , eventManagers
    );
    ASSERT_NE( nullptr, eventProcessorUnique.get() );
    auto &  eventProcessor = *eventProcessorUnique;

    auto &  xDisplay = **( window->xDisplayForEventUnique );
    auto &  xWindow = window->xWindow;
    auto    xEvent = XEvent();
    xEvent.xclient.display = &xDisplay;
    xEvent.xclient.window = xWindow;
    xEvent.xclient.type = ClientMessage;
    xEvent.xclient.format = 32;
    std::memcpy(
        xEvent.xclient.data.l
        , &( window->wmDeleteWindow )
        , sizeof( window->wmDeleteWindow )
    );

    XSendEvent(
        &xDisplay
        , xWindow
        , False
        , 0
        , &xEvent
    );

    XFlush( &xDisplay );

    std::this_thread::sleep_for( std::chrono::milliseconds( 100 ) );    // イベント実行開始待機

    eventProcessor->stateJoinerUnique->join();

    ASSERT_EQ( 20, i );
}
