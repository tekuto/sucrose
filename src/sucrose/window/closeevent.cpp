﻿#include "fg/util/export.h"
#include "sucrose/window/closeevent.h"

FgWindow * fgWindowCloseEventDataGetWindow(
    FgWindowCloseEventData *    _implPtr
)
{
    return &*( _implPtr->window );
}
