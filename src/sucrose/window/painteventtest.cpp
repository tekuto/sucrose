﻿#include "fg/util/test.h"
#include "sucrose/window/paintevent.h"
#include "fg/window/window.h"

TEST(
    WindowPaintEventDataTest
    , GetWindow
)
{
    auto    dataImpl = FgWindowPaintEventData{ *reinterpret_cast< fg::Window * >( 10 ) };
    auto &  data = reinterpret_cast< fg::WindowPaintEventData & >( dataImpl );

    ASSERT_EQ( reinterpret_cast< fg::Window * >( 10 ), &( data.getWindow() ) );
}
