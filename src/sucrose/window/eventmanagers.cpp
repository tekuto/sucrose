﻿#include "fg/util/export.h"
#include "sucrose/window/eventmanagers.h"
#include "fg/core/state/eventmanager.h"

FgWindowEventManagers * fgWindowEventManagersCreate(
)
{
    return new FgWindowEventManagers{
        fg::WindowPaintEventManager::create(),
        fg::WindowCloseEventManager::create(),
    };
}

void fgWindowEventManagersDestroy(
    FgWindowEventManagers * _implPtr
)
{
    delete _implPtr;
}

FgStateEventManager * fgWindowEventManagersGetPaintEventManager(
    FgWindowEventManagers * _implPtr
)
{
    return &**( _implPtr->paintEventManagerUnique );
}

FgStateEventManager * fgWindowEventManagersGetCloseEventManager(
    FgWindowEventManagers * _implPtr
)
{
    return &**( _implPtr->closeEventManagerUnique );
}
