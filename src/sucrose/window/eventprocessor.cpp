﻿#include "fg/util/export.h"
#include "sucrose/window/eventprocessor.h"
#include "sucrose/window/window.h"
#include "fg/core/state/threadbackgroundsingleton.h"

#include <X11/Xlib.h>

namespace {
    bool isEnd(
        FgWindowEventProcessor &    _eventProcessor
    )
    {
        auto    lock = std::unique_lock< std::mutex >( _eventProcessor.mutex );

        return _eventProcessor.ended;
    }

    void end(
        FgWindowEventProcessor &    _eventProcessor
    )
    {
        auto    lock = std::unique_lock< std::mutex >( _eventProcessor.mutex );

        _eventProcessor.ended = true;
    }

    void processEventClientMessageWmDeleteWindow(
        FgWindowEventProcessor &    _eventProcessor
    )
    {
        auto &  eventManager = _eventProcessor.eventManagers.getCloseEventManager();
        auto &  eventData = _eventProcessor.eventData.closeEvent;

        eventManager.execute(
            *( _eventProcessor.stateJoinerUnique )
            , reinterpret_cast< fg::WindowCloseEventData & >( eventData )
        );
    }

    void processEventClientMessage(
        const XClientMessageEvent & _EVENT
        , FgWindowEventProcessor &  _eventProcessor
    )
    {
        const auto &    ATOM = static_cast< const Atom & >( _EVENT.data.l[ 0 ] );

        if( _eventProcessor.window->isWmDeleteWindow( ATOM ) == true ) {
            processEventClientMessageWmDeleteWindow( _eventProcessor );
        }
    }

    void processEventExpose(
        const XExposeEvent &
        , FgWindowEventProcessor &  _eventProcessor
    )
    {
        auto &  eventManager = _eventProcessor.eventManagers.getPaintEventManager();
        auto &  eventData = _eventProcessor.eventData.paintEvent;

        eventManager.execute(
            *( _eventProcessor.stateJoinerUnique )
            , reinterpret_cast< fg::WindowPaintEventData & >( eventData )
        );
    }

    void processEvent(
        const XEvent &              _X_EVENT
        , FgWindowEventProcessor &  _eventProcessor
    )
    {
        _eventProcessor.stateJoinerUnique->join();

        switch( _X_EVENT.type ) {
        case ClientMessage:
            processEventClientMessage(
                _X_EVENT.xclient
                , _eventProcessor
            );
            break;

        case Expose:
            processEventExpose(
                _X_EVENT.xexpose
                , _eventProcessor
            );
            break;

        default:
#ifdef  DEBUG
            std::printf( "D:非対応のイベント\n" );
#endif  // DEBUG

            break;
        }
    }

    void executePollEventThread(
        FgWindowEventProcessor &
    );

    void eventThread(
        fg::StateThreadBackgroundSingleton< void, FgWindowEventProcessor > &    _thread
    )
    {
        auto &  eventProcessor = _thread.getData();

        auto    xEvent = XEvent();

        eventProcessor.window->getXEvent( xEvent );

        processEvent(
            xEvent
            , eventProcessor
        );

        executePollEventThread( eventProcessor );
    }

    void executeEventThread(
        FgWindowEventProcessor &    _eventProcessor
    )
    {
        _eventProcessor.state.execute(
            eventThread
            , *( _eventProcessor.threadJoinerUnique )
            , _eventProcessor
        );
    }

    void pollEventThread(
        fg::StateThreadBackgroundSingleton< void, FgWindowEventProcessor > &    _thread
    )
    {
        auto &  eventProcessor = _thread.getData();

        if( isEnd( eventProcessor ) == true ) {
            return;
        }

        if( eventProcessor.window->pollXEvent() == false ) {
            executePollEventThread( eventProcessor );

            return;
        }

        executeEventThread( eventProcessor );
    }

    void executePollEventThread(
        FgWindowEventProcessor &    _eventProcessor
    )
    {
        _eventProcessor.state.execute(
            pollEventThread
            , *( _eventProcessor.threadJoinerUnique )
            , _eventProcessor
        );
    }
}

void FgWindowEventProcessor::EndThread::operator()(
    FgWindowEventProcessor *    _eventProcessorPtr
) const
{
    auto &  eventProcessor = *_eventProcessorPtr;

    end( eventProcessor );
}

FgWindowEventProcessor::EventData::EventData(
    fg::Window &    _window
)
    : windowPtr_( &_window )
{
}

FgWindowEventProcessor::FgWindowEventProcessor(
    fg::State<> &               _state
    , fg::Window &              _window
    , fg::WindowEventManagers & _eventManagers
)
    : state( _state )
    , window( _window )
    , eventManagers( _eventManagers )
    , eventData( _window )
    , stateJoinerUnique( fg::StateJoiner::create() )
    , ended( false )
    , threadJoinerUnique( fg::StateJoiner::create() )
    , ender( this )
{
    executePollEventThread( *this );
}

FgWindowEventProcessor * fgWindowEventProcessorCreate(
    FgState *                   _statePtr
    , FgWindow *                _windowPtr
    , FgWindowEventManagers *   _eventManagersPtr
)
{
    return new FgWindowEventProcessor(
        reinterpret_cast< fg::State<> & >( *_statePtr )
        , reinterpret_cast< fg::Window & >( *_windowPtr )
        , reinterpret_cast< fg::WindowEventManagers & >( *_eventManagersPtr )
    );
}

void fgWindowEventProcessorDestroy(
    FgWindowEventProcessor *    _implPtr
)
{
    delete _implPtr;
}
