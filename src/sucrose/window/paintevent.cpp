﻿#include "fg/util/export.h"
#include "sucrose/window/paintevent.h"

FgWindow * fgWindowPaintEventDataGetWindow(
    FgWindowPaintEventData *    _implPtr
)
{
    return &*( _implPtr->window );
}
