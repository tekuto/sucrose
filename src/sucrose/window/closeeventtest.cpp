﻿#include "fg/util/test.h"
#include "sucrose/window/closeevent.h"
#include "fg/window/window.h"

TEST(
    WindowCloseEventDataTest
    , GetWindow
)
{
    auto    dataImpl = FgWindowCloseEventData{ *reinterpret_cast< fg::Window * >( 10 ) };
    auto &  data = reinterpret_cast< fg::WindowCloseEventData & >( dataImpl );

    ASSERT_EQ( reinterpret_cast< fg::Window * >( 10 ), &( data.getWindow() ) );
}
