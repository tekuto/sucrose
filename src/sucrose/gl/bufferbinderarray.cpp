﻿#include "fg/util/export.h"
#include "fg/gl/bufferbinderarray.h"
#include "fg/gl/gl.h"
#include "sucrose/gl/currentcontext.h"
#include "sucrose/gl/buffers.h"

#include <stdexcept>

FgGLBufferBinderArray * fgGLBufferBinderArrayCreate(
    FgGLCurrentContext *    _glCurrentContextPtr
    , const FgGLBuffers *   _GL_BUFFERS_PTR
    , FgGLsizei             _index
)
{
    auto &          glCurrentContext = reinterpret_cast< fg::GLCurrentContext & >( *_glCurrentContextPtr );
    const auto &    GL_BUFFERS = reinterpret_cast< const fg::GLBuffers & >( *_GL_BUFFERS_PTR );

    if( &( glCurrentContext->getGLContext() ) != &( GL_BUFFERS->getGLContext() ) ) {
        throw std::runtime_error( "バッファ生成時とfg::GLContextが異なる" );
    }

    const auto  NAME = GL_BUFFERS.getName( _index );

    glCurrentContext.glGetError();
    glCurrentContext.glBindBuffer(
        fg::GL_ARRAY_BUFFER
        , NAME
    );
    if( glCurrentContext.glGetError() != fg::GL_NO_ERROR ) {
        throw std::runtime_error( "glBindBuffer()が失敗" );
    }

    return reinterpret_cast< FgGLBufferBinderArray * >( _glCurrentContextPtr );
}

void fgGLBufferBinderArrayDestroy(
    FgGLBufferBinderArray * _implPtr
)
{
    reinterpret_cast< fg::GLCurrentContext * >( _implPtr )->glBindBuffer(
        fg::GL_ARRAY_BUFFER
        , 0
    );
}
