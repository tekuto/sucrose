﻿#include "fg/util/export.h"
#include "sucrose/gl/contextargs.h"

namespace {
    enum {
        DEFAULT_RED_SIZE = 8,
        DEFAULT_GREEN_SIZE = 8,
        DEFAULT_BLUE_SIZE = 8,
        DEFAULT_ALPHA_SIZE = 8,
        DEFAULT_DEPTH_SIZE = 24,
        DEFAULT_STENCIL_SIZE = 8,
    };

    bool isExistsSize(
        const FgGLContextArgs::Size &   _SIZE
    )
    {
        return _SIZE.exists;
    }

    int getSize(
        const FgGLContextArgs::Size &   _SIZE
    )
    {
        return _SIZE.value;
    }

    void setSize(
        FgGLContextArgs::Size & _size
        , int                   _value
    )
    {
        _size.exists = true;
        _size.value = _value;
    }

    void clearSize(
        FgGLContextArgs::Size & _size
    )
    {
        _size.exists = false;
    }
}

FgGLContextArgs * fgGLContextArgsCreate(
)
{
    return new FgGLContextArgs{
        {
            true,
            DEFAULT_RED_SIZE,
        },
        {
            true,
            DEFAULT_GREEN_SIZE,
        },
        {
            true,
            DEFAULT_BLUE_SIZE,
        },
        {
            true,
            DEFAULT_ALPHA_SIZE,
        },
        {
            true,
            DEFAULT_DEPTH_SIZE,
        },
        {
            true,
            DEFAULT_STENCIL_SIZE,
        },
    };
}

FgGLContextArgs * fgGLContextArgsCreateBlank(
)
{
    return new FgGLContextArgs;
}

void fgGLContextArgsDestroy(
    FgGLContextArgs *   _implPtr
)
{
    delete _implPtr;
}

bool fgGLContextArgsIsExistsRedSize(
    const FgGLContextArgs * _IMPL_PTR
)
{
    return isExistsSize( _IMPL_PTR->redSize );
}

int fgGLContextArgsGetRedSize(
    const FgGLContextArgs * _IMPL_PTR
)
{
    return getSize( _IMPL_PTR->redSize );
}

void fgGLContextArgsSetRedSize(
    FgGLContextArgs *   _implPtr
    , int               _size
)
{
    setSize(
        _implPtr->redSize
        , _size
    );
}

void fgGLContextArgsClearRedSize(
    FgGLContextArgs *   _implPtr
)
{
    clearSize( _implPtr->redSize );
}

bool fgGLContextArgsIsExistsGreenSize(
    const FgGLContextArgs * _IMPL_PTR
)
{
    return isExistsSize( _IMPL_PTR->greenSize );
}

int fgGLContextArgsGetGreenSize(
    const FgGLContextArgs * _IMPL_PTR
)
{
    return getSize( _IMPL_PTR->greenSize );
}

void fgGLContextArgsSetGreenSize(
    FgGLContextArgs *   _implPtr
    , int               _size
)
{
    setSize(
        _implPtr->greenSize
        , _size
    );
}

void fgGLContextArgsClearGreenSize(
    FgGLContextArgs *   _implPtr
)
{
    clearSize( _implPtr->greenSize );
}

bool fgGLContextArgsIsExistsBlueSize(
    const FgGLContextArgs * _IMPL_PTR
)
{
    return isExistsSize( _IMPL_PTR->blueSize );
}

int fgGLContextArgsGetBlueSize(
    const FgGLContextArgs * _IMPL_PTR
)
{
    return getSize( _IMPL_PTR->blueSize );
}

void fgGLContextArgsSetBlueSize(
    FgGLContextArgs *   _implPtr
    , int               _size
)
{
    setSize(
        _implPtr->blueSize
        , _size
    );
}

void fgGLContextArgsClearBlueSize(
    FgGLContextArgs *   _implPtr
)
{
    clearSize( _implPtr->blueSize );
}

bool fgGLContextArgsIsExistsAlphaSize(
    const FgGLContextArgs * _IMPL_PTR
)
{
    return isExistsSize( _IMPL_PTR->alphaSize );
}

int fgGLContextArgsGetAlphaSize(
    const FgGLContextArgs * _IMPL_PTR
)
{
    return getSize( _IMPL_PTR->alphaSize );
}

void fgGLContextArgsSetAlphaSize(
    FgGLContextArgs *   _implPtr
    , int               _size
)
{
    setSize(
        _implPtr->alphaSize
        , _size
    );
}

void fgGLContextArgsClearAlphaSize(
    FgGLContextArgs *   _implPtr
)
{
    clearSize( _implPtr->alphaSize );
}

bool fgGLContextArgsIsExistsDepthSize(
    const FgGLContextArgs * _IMPL_PTR
)
{
    return isExistsSize( _IMPL_PTR->depthSize );
}

int fgGLContextArgsGetDepthSize(
    const FgGLContextArgs * _IMPL_PTR
)
{
    return getSize( _IMPL_PTR->depthSize );
}

void fgGLContextArgsSetDepthSize(
    FgGLContextArgs *   _implPtr
    , int               _size
)
{
    setSize(
        _implPtr->depthSize
        , _size
    );
}

void fgGLContextArgsClearDepthSize(
    FgGLContextArgs *   _implPtr
)
{
    clearSize( _implPtr->depthSize );
}

bool fgGLContextArgsIsExistsStencilSize(
    const FgGLContextArgs * _IMPL_PTR
)
{
    return isExistsSize( _IMPL_PTR->stencilSize );
}

int fgGLContextArgsGetStencilSize(
    const FgGLContextArgs * _IMPL_PTR
)
{
    return getSize( _IMPL_PTR->stencilSize );
}

void fgGLContextArgsSetStencilSize(
    FgGLContextArgs *   _implPtr
    , int               _size
)
{
    setSize(
        _implPtr->stencilSize
        , _size
    );
}

void fgGLContextArgsClearStencilSize(
    FgGLContextArgs *   _implPtr
)
{
    clearSize( _implPtr->stencilSize );
}
