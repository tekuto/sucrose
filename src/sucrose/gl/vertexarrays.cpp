﻿#include "fg/util/export.h"
#include "sucrose/gl/vertexarrays.h"
#include "fg/gl/gl.h"
#include "sucrose/gl/currentcontext.h"
#include "sucrose/gl/currentcontextremaker.h"
#include "sucrose/gl/context.h"

#include <stdexcept>

namespace {
    FgGLVertexArrays::Names generateGLVertexArrays(
        fg::GLCurrentContext &  _glCurrentContext
        , fg::GLsizei           _size
    )
    {
        auto    names = FgGLVertexArrays::Names( _size );

        _glCurrentContext.glGetError();

        _glCurrentContext.glGenVertexArrays(
            names.size()
            , names.data()
        );

        if( _glCurrentContext.glGetError() != fg::GL_NO_ERROR ) {
            throw std::runtime_error( "glGenVertexArrays()が失敗" );
        }

        return names;
    }
}

FgGLVertexArrays * fgGLVertexArraysCreate(
    FgGLCurrentContext *    _glCurrentContextPtr
    , FgGLsizei             _size
)
{
    if( _size <= 0 ) {
        throw std::out_of_range( "サイズが0以下" );
    }

    return new FgGLVertexArrays(
        reinterpret_cast< fg::GLCurrentContext & >( *_glCurrentContextPtr )
        , _size
    );
}

void fgGLVertexArraysDestroy(
    FgGLVertexArrays *  _implPtr
)
{
    delete _implPtr;
}

FgGLsizei fgGLVertexArraysGetSize(
    const FgGLVertexArrays *    _IMPL_PTR
)
{
    return _IMPL_PTR->names.size();
}

FgGLuint fgGLVertexArraysGetName(
    const FgGLVertexArrays *    _IMPL_PTR
    , FgGLsizei                 _index
)
{
    return _IMPL_PTR->names.at( _index );
}

void FgGLVertexArrays::DeleteNames::operator()(
    FgGLVertexArrays *  _glVertexArraysPtr
) const
{
    auto    glCurrentContextRemaker = sucrose::GLCurrentContextRemaker( FgGLContext::releaseGLCurrent() );

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( _glVertexArraysPtr->glContext );

    auto &  names = _glVertexArraysPtr->names;

    glCurrentContextUnique->glDeleteVertexArrays(
        names.size()
        , names.data()
    );
}

FgGLVertexArrays::FgGLVertexArrays(
    fg::GLCurrentContext &  _glCurrentContext
    , fg::GLsizei           _size
)
    : glContext( _glCurrentContext->getGLContext() )
    , names(
        generateGLVertexArrays(
            _glCurrentContext
            , _size
        )
    )
    , namesDeleter( this )
{
}

const fg::GLContext & FgGLVertexArrays::getGLContext(
) const
{
    return const_cast< FgGLVertexArrays * >( this )->getGLContext();
}

fg::GLContext & FgGLVertexArrays::getGLContext(
)
{
    return this->glContext;
}
