﻿#include "fg/util/test.h"
#include "sucrose/gl/buffers.h"
#include "fg/gl/gl.h"
#include "fg/gl/context.h"
#include "fg/gl/currentcontext.h"
#include "fg/window/window.h"
#include "sucrose/window/xlib.h"

#include <array>

TEST(
    GLBuffersTest
    , Create
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLBuffersTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );
    auto &  glContext = *glContextUnique;

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( glContext );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );

    auto    glBuffersUnique = fg::GLBuffers::create(
        *glCurrentContextUnique
        , 10
    );
    ASSERT_NE( nullptr, glBuffersUnique.get() );
    const auto &    GL_BUFFERS = *glBuffersUnique;

    ASSERT_EQ( &glContext, &( GL_BUFFERS->glContext ) );

    const auto &    NAMES = GL_BUFFERS->names;
    ASSERT_EQ( 10, NAMES.size() );
    for( const auto & NAME : NAMES ) {
        ASSERT_NE( 0, NAME );
    }

    ASSERT_EQ( &*GL_BUFFERS, GL_BUFFERS->namesDeleter.get() );
}

TEST(
    GLBuffersTest
    , Create_failedWhenIllegalSize
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLBuffersTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( *glContextUnique );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );
    auto &  glCurrentContext = *glCurrentContextUnique;

    try {
        auto    glBuffersUnique = fg::GLBuffers::create(
            glCurrentContext
            , 0
        );

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}

    try {
        auto    glBuffersUnique = fg::GLBuffers::create(
            glCurrentContext
            , -1
        );

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}
}

TEST(
    GLBuffersTest
    , Destroy
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLBuffersTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( *glContextUnique );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );

    auto    glBuffersUnique = fg::GLBuffers::create(
        *glCurrentContextUnique
        , 10
    );
    ASSERT_NE( nullptr, glBuffersUnique.get() );
    const auto &    GL_BUFFERS = *glBuffersUnique;

    const auto  NAME_BACKUP = GL_BUFFERS->names[ 0 ];

    glCurrentContextUnique->glBindBuffer(
        fg::GL_ARRAY_BUFFER
        , NAME_BACKUP
    );

    const fg::GLubyte   DUMMY_DATA[] = {
        10,
        20,
        30,
    };
    glCurrentContextUnique->glBufferData(
        fg::GL_ARRAY_BUFFER
        , sizeof( DUMMY_DATA )
        , DUMMY_DATA
        , fg::GL_STATIC_DRAW
    );

    glBuffersUnique.destroy();

    glCurrentContextUnique->glBindBuffer(
        fg::GL_ARRAY_BUFFER
        , NAME_BACKUP
    );

    auto    data = std::array< fg::GLubyte, 3 >();
    glCurrentContextUnique->glGetError();
    glCurrentContextUnique->glGetBufferSubData(
        fg::GL_ARRAY_BUFFER
        , 0
        , sizeof( decltype( data )::value_type ) * data.size()
        , data.data()
    );
    ASSERT_NE( fg::GL_NO_ERROR, glCurrentContextUnique->glGetError() );
}

TEST(
    GLBuffersTest
    , Destroy_afterCurrentContextDeleted
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLBuffersTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );
    auto &  glContext = *glContextUnique;

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( glContext );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );

    auto    glBuffersUnique = fg::GLBuffers::create(
        *glCurrentContextUnique
        , 10
    );
    ASSERT_NE( nullptr, glBuffersUnique.get() );
    const auto &    GL_BUFFERS = *glBuffersUnique;

    const auto  NAME_BACKUP = GL_BUFFERS->names[ 0 ];

    glCurrentContextUnique->glBindBuffer(
        fg::GL_ARRAY_BUFFER
        , NAME_BACKUP
    );

    const fg::GLubyte   DUMMY_DATA[] = {
        10,
        20,
        30,
    };
    glCurrentContextUnique->glBufferData(
        fg::GL_ARRAY_BUFFER
        , sizeof( DUMMY_DATA )
        , DUMMY_DATA
        , fg::GL_STATIC_DRAW
    );

    glCurrentContextUnique.destroy();

    glBuffersUnique.destroy();

    auto    glCurrentContextAfterDeleteBuffersUnique = fg::GLCurrentContext::create( glContext );
    ASSERT_NE( nullptr, glCurrentContextAfterDeleteBuffersUnique.get() );

    glCurrentContextAfterDeleteBuffersUnique->glBindBuffer(
        fg::GL_ARRAY_BUFFER
        , NAME_BACKUP
    );

    auto    data = std::array< fg::GLubyte, 3 >();
    glCurrentContextAfterDeleteBuffersUnique->glGetError();
    glCurrentContextAfterDeleteBuffersUnique->glGetBufferSubData(
        fg::GL_ARRAY_BUFFER
        , 0
        , sizeof( decltype( data )::value_type ) * data.size()
        , data.data()
    );
    ASSERT_NE( fg::GL_NO_ERROR, glCurrentContextAfterDeleteBuffersUnique->glGetError() );
}

TEST(
    GLBuffersTest
    , Destroy_afterMadeOtherCurrentContext
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLBuffersTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );
    auto &  glContext = *glContextUnique;

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( glContext );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );

    auto    glBuffersUnique = fg::GLBuffers::create(
        *glCurrentContextUnique
        , 10
    );
    ASSERT_NE( nullptr, glBuffersUnique.get() );
    const auto &    GL_BUFFERS = *glBuffersUnique;

    ASSERT_EQ( &glContext, &( GL_BUFFERS->glContext ) );

    const auto  NAME_BACKUP = GL_BUFFERS->names[ 0 ];

    glCurrentContextUnique->glBindBuffer(
        fg::GL_ARRAY_BUFFER
        , NAME_BACKUP
    );

    const fg::GLubyte   DUMMY_DATA[] = {
        10,
        20,
        30,
    };
    glCurrentContextUnique->glBufferData(
        fg::GL_ARRAY_BUFFER
        , sizeof( DUMMY_DATA )
        , DUMMY_DATA
        , fg::GL_STATIC_DRAW
    );

    glCurrentContextUnique.destroy();

    auto    glContext2Unique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContext2Unique.get() );
    auto &  glContext2 = *glContext2Unique;

    auto    glCurrentContext2Unique = fg::GLCurrentContext::create( glContext2 );
    ASSERT_NE( nullptr, glCurrentContext2Unique.get() );

    glCurrentContext2Unique->glBindBuffer(
        fg::GL_ARRAY_BUFFER
        , NAME_BACKUP
    );

    const fg::GLubyte   DUMMY_DATA2[] = {
        40,
        50,
        60,
    };
    glCurrentContext2Unique->glBufferData(
        fg::GL_ARRAY_BUFFER
        , sizeof( DUMMY_DATA2 )
        , DUMMY_DATA2
        , fg::GL_STATIC_DRAW
    );

    glBuffersUnique.destroy();

    glCurrentContext2Unique.destroy();

    auto    glCurrentContextAfterDeleteBuffersUnique = fg::GLCurrentContext::create( glContext );
    ASSERT_NE( nullptr, glCurrentContextAfterDeleteBuffersUnique.get() );

    glCurrentContextAfterDeleteBuffersUnique->glBindBuffer(
        fg::GL_ARRAY_BUFFER
        , NAME_BACKUP
    );

    auto    data = std::array< fg::GLubyte, 3 >();
    glCurrentContextAfterDeleteBuffersUnique->glGetError();
    glCurrentContextAfterDeleteBuffersUnique->glGetBufferSubData(
        fg::GL_ARRAY_BUFFER
        , 0
        , sizeof( decltype( data )::value_type ) * data.size()
        , data.data()
    );
    ASSERT_NE( fg::GL_NO_ERROR, glCurrentContextAfterDeleteBuffersUnique->glGetError() );

    glCurrentContextAfterDeleteBuffersUnique.destroy();

    auto    glCurrentContextAfterDeleteBuffers2Unique = fg::GLCurrentContext::create( glContext2 );
    ASSERT_NE( nullptr, glCurrentContextAfterDeleteBuffers2Unique.get() );

    glCurrentContextAfterDeleteBuffers2Unique->glBindBuffer(
        fg::GL_ARRAY_BUFFER
        , NAME_BACKUP
    );

    auto    data2 = std::array< fg::GLubyte, 3 >();
    glCurrentContextAfterDeleteBuffers2Unique->glGetError();
    glCurrentContextAfterDeleteBuffers2Unique->glGetBufferSubData(
        fg::GL_ARRAY_BUFFER
        , 0
        , sizeof( decltype( data2 )::value_type ) * data2.size()
        , data2.data()
    );
    ASSERT_EQ( fg::GL_NO_ERROR, glCurrentContextAfterDeleteBuffers2Unique->glGetError() );
    ASSERT_EQ( 40, data2[ 0 ] );
    ASSERT_EQ( 50, data2[ 1 ] );
    ASSERT_EQ( 60, data2[ 2 ] );
}

TEST(
    GLBuffersTest
    , GetSize
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLBuffersTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( *glContextUnique );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );

    auto    glBuffersUnique = fg::GLBuffers::create(
        *glCurrentContextUnique
        , 10
    );
    ASSERT_NE( nullptr, glBuffersUnique.get() );

    ASSERT_EQ( 10, glBuffersUnique->getSize() );
}

TEST(
    GLBuffersTest
    , GetName
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLBuffersTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( *glContextUnique );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );

    auto    glBuffersUnique = fg::GLBuffers::create(
        *glCurrentContextUnique
        , 10
    );
    ASSERT_NE( nullptr, glBuffersUnique.get() );
    const auto &    GL_BUFFERS = *glBuffersUnique;

    const auto  NAME = glBuffersUnique->getName( 0 );
    ASSERT_NE( 0, NAME );
    ASSERT_EQ( GL_BUFFERS->names[ 0 ], NAME );
}

TEST(
    GLBuffersTest
    , GetGLContext_const
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLBuffersTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );
    auto &  glContext = *glContextUnique;

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( glContext );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );

    auto    glBuffersUnique = fg::GLBuffers::create(
        *glCurrentContextUnique
        , 10
    );
    ASSERT_NE( nullptr, glBuffersUnique.get() );
    const auto &    GL_BUFFERS = *glBuffersUnique;

    ASSERT_EQ( &glContext, &( GL_BUFFERS->getGLContext() ) );
}

TEST(
    GLBuffersTest
    , GetGLContext
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLBuffersTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );
    auto &  glContext = *glContextUnique;

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( glContext );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );

    auto    glBuffersUnique = fg::GLBuffers::create(
        *glCurrentContextUnique
        , 10
    );
    ASSERT_NE( nullptr, glBuffersUnique.get() );
    auto &  glBuffers = *glBuffersUnique;

    ASSERT_EQ( &glContext, &glBuffers->getGLContext() );
}
