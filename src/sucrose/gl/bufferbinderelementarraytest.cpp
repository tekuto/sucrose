﻿#include "fg/util/test.h"
#include "fg/gl/bufferbinderelementarray.h"
#include "fg/gl/gl.h"
#include "fg/gl/context.h"
#include "fg/gl/currentcontext.h"
#include "fg/gl/buffers.h"
#include "fg/window/window.h"
#include "sucrose/gl/buffers.h"
#include "sucrose/window/xlib.h"

TEST(
    GLBufferBinderElementArrayTest
    , Create
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLBufferBinderElementArrayTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( *glContextUnique );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );
    auto &  glCurrentContext = *glCurrentContextUnique;

    auto    glBuffersUnique = fg::GLBuffers::create(
        glCurrentContext
        , 10
    );
    ASSERT_NE( nullptr, glBuffersUnique.get() );
    const auto &    GL_BUFFERS = *glBuffersUnique;

    auto    glBufferBinderElementArrayUnique = fg::GLBufferBinderElementArray::create(
        glCurrentContext
        , GL_BUFFERS
        , 5
    );
    ASSERT_NE( nullptr, glBufferBinderElementArrayUnique.get() );
    const auto &    GL_BUFFER_BINDER_ELEMENT_ARRAY = *glBufferBinderElementArrayUnique;

    ASSERT_EQ( &glCurrentContext, &reinterpret_cast< const fg::GLCurrentContext & >( GL_BUFFER_BINDER_ELEMENT_ARRAY ) );

    glCurrentContext.glGetError();
    auto    name = fg::GLuint();
    glCurrentContext.glGetIntegerv(
        fg::GL_ELEMENT_ARRAY_BUFFER_BINDING
        , &reinterpret_cast< fg::GLint & >( name )
    );
    ASSERT_EQ( fg::GL_NO_ERROR, glCurrentContext.glGetError() );
    ASSERT_NE( 0, name );
    ASSERT_EQ( GL_BUFFERS->names[ 5 ], name );
}

TEST(
    GLBufferBinderElementArrayTest
    , Create_failedWhenOtherContext
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLBufferBinderElementArrayTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( *glContextUnique );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );

    auto    glBuffersUnique = fg::GLBuffers::create(
        *glCurrentContextUnique
        , 10
    );
    ASSERT_NE( nullptr, glBuffersUnique.get() );
    const auto &    GL_BUFFERS = *glBuffersUnique;

    glCurrentContextUnique.destroy();

    auto    glContext2Unique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContext2Unique.get() );

    auto    glCurrentContext2Unique = fg::GLCurrentContext::create( *glContext2Unique );
    ASSERT_NE( nullptr, glCurrentContext2Unique.get() );

    try {
        auto    glBufferBinderElementArrayUnique = fg::GLBufferBinderElementArray::create(
            *glCurrentContext2Unique
            , GL_BUFFERS
            , 5
        );

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}
}

TEST(
    GLBufferBinderElementArrayTest
    , Destroy
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLBufferBinderElementArrayTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( *glContextUnique );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );
    auto &  glCurrentContext = *glCurrentContextUnique;

    auto    glBuffersUnique = fg::GLBuffers::create(
        glCurrentContext
        , 10
    );
    ASSERT_NE( nullptr, glBuffersUnique.get() );
    const auto &    GL_BUFFERS = *glBuffersUnique;

    auto    glBufferBinderElementArrayUnique = fg::GLBufferBinderElementArray::create(
        glCurrentContext
        , GL_BUFFERS
        , 5
    );
    ASSERT_NE( nullptr, glBufferBinderElementArrayUnique.get() );

    glBufferBinderElementArrayUnique.destroy();

    glCurrentContext.glGetError();
    auto    name = fg::GLuint();
    glCurrentContext.glGetIntegerv(
        fg::GL_ELEMENT_ARRAY_BUFFER_BINDING
        , &reinterpret_cast< fg::GLint & >( name )
    );
    ASSERT_EQ( fg::GL_NO_ERROR, glCurrentContext.glGetError() );
    ASSERT_EQ( 0, name );
}
