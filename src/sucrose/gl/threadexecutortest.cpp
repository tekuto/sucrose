﻿#include "fg/util/test.h"
#include "sucrose/gl/threadexecutor.h"
#include "fg/core/state/creating.h"
#include "fg/window/window.h"
#include "fg/gl/context.h"
#include "sucrose/window/xlib.h"
#include "brownsugar/statemanager.h"

void createTestProc(
    fg::GLThread<> &
)
{
}

void createWithUserDataTestProc(
    fg::GLThread< void, int > &
)
{
}

TEST(
    GLThreadExecutorTest
    , CreateForCreatingState
)
{
    sucrose::initializeXlib();

    auto    stateManagerUnique = brownsugar::StateManager::create();
    ASSERT_NE( nullptr, stateManagerUnique.get() );

    auto &  state = stateManagerUnique->getState();

    auto    windowUnique = fg::Window::create(
        "GLThreadExecutorTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );
    auto &  glContext = *glContextUnique;

    const auto  THREAD_EXECUTOR_UNIQUE = fg::GLThreadExecutor::create(
        reinterpret_cast< fg::CreatingState<> & >( state )
        , glContext
        , createTestProc
    );
    ASSERT_NE( nullptr, THREAD_EXECUTOR_UNIQUE.get() );
    const auto &    THREAD_EXECUTOR = *THREAD_EXECUTOR_UNIQUE;

    const auto &    THREAD = THREAD_EXECUTOR->thread;
    ASSERT_EQ( &state, &( THREAD.state ) );
    ASSERT_EQ( &glContext, &( THREAD.glContext ) );
    ASSERT_EQ( reinterpret_cast< FgGLThreadProc >( createTestProc ), THREAD.procPtr );
    ASSERT_EQ( nullptr, THREAD.userDataPtr );
    ASSERT_NE( nullptr, THREAD_EXECUTOR->joinerUnique.get() );
}

TEST(
    GLThreadExecutorTest
    , CreateForCreatingStateWithUserData
)
{
    sucrose::initializeXlib();

    auto    stateManagerUnique = brownsugar::StateManager::create();
    ASSERT_NE( nullptr, stateManagerUnique.get() );

    auto &  state = stateManagerUnique->getState();

    auto    windowUnique = fg::Window::create(
        "GLThreadExecutorTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );
    auto &  glContext = *glContextUnique;

    auto    i = 10;

    const auto  THREAD_EXECUTOR_UNIQUE = fg::GLThreadExecutor::create(
        reinterpret_cast< fg::CreatingState<> & >( state )
        , glContext
        , createWithUserDataTestProc
        , i
    );
    ASSERT_NE( nullptr, THREAD_EXECUTOR_UNIQUE.get() );
    const auto &    THREAD_EXECUTOR = *THREAD_EXECUTOR_UNIQUE;

    const auto &    THREAD = THREAD_EXECUTOR->thread;
    ASSERT_EQ( &state, &( THREAD.state ) );
    ASSERT_EQ( &glContext, &( THREAD.glContext ) );
    ASSERT_EQ( reinterpret_cast< FgGLThreadProc >( createWithUserDataTestProc ), THREAD.procPtr );
    ASSERT_EQ( 10, *static_cast< const int * >( THREAD.userDataPtr ) );
    ASSERT_NE( nullptr, THREAD_EXECUTOR->joinerUnique.get() );
}

TEST(
    GLThreadExecutorTest
    , CreateForState
)
{
    sucrose::initializeXlib();

    auto    stateManagerUnique = brownsugar::StateManager::create();
    ASSERT_NE( nullptr, stateManagerUnique.get() );

    auto &  state = stateManagerUnique->getState();

    auto    windowUnique = fg::Window::create(
        "GLThreadExecutorTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );
    auto &  glContext = *glContextUnique;

    const auto  THREAD_EXECUTOR_UNIQUE = fg::GLThreadExecutor::create(
        state
        , glContext
        , createTestProc
    );
    ASSERT_NE( nullptr, THREAD_EXECUTOR_UNIQUE.get() );
    const auto &    THREAD_EXECUTOR = *THREAD_EXECUTOR_UNIQUE;

    const auto &    THREAD = THREAD_EXECUTOR->thread;
    ASSERT_EQ( &state, &( THREAD.state ) );
    ASSERT_EQ( &glContext, &( THREAD.glContext ) );
    ASSERT_EQ( reinterpret_cast< FgGLThreadProc >( createTestProc ), THREAD.procPtr );
    ASSERT_EQ( nullptr, THREAD.userDataPtr );
    ASSERT_NE( nullptr, THREAD_EXECUTOR->joinerUnique.get() );
}

TEST(
    GLThreadExecutorTest
    , CreateForStateWithUserData
)
{
    sucrose::initializeXlib();

    auto    stateManagerUnique = brownsugar::StateManager::create();
    ASSERT_NE( nullptr, stateManagerUnique.get() );

    auto &  state = stateManagerUnique->getState();

    auto    windowUnique = fg::Window::create(
        "GLThreadExecutorTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );
    auto &  glContext = *glContextUnique;

    auto    i = 10;

    const auto  THREAD_EXECUTOR_UNIQUE = fg::GLThreadExecutor::create(
        state
        , glContext
        , createWithUserDataTestProc
        , i
    );
    ASSERT_NE( nullptr, THREAD_EXECUTOR_UNIQUE.get() );
    const auto &    THREAD_EXECUTOR = *THREAD_EXECUTOR_UNIQUE;

    const auto &    THREAD = THREAD_EXECUTOR->thread;
    ASSERT_EQ( &state, &( THREAD.state ) );
    ASSERT_EQ( &glContext, &( THREAD.glContext ) );
    ASSERT_EQ( reinterpret_cast< FgGLThreadProc >( createWithUserDataTestProc ), THREAD.procPtr );
    ASSERT_EQ( 10, *static_cast< const int * >( THREAD.userDataPtr ) );
    ASSERT_NE( nullptr, THREAD_EXECUTOR->joinerUnique.get() );
}

void executeTestProc(
    fg::GLThread< void, int > & _thread
)
{
    _thread.getData() = 20;
}

TEST(
    GLThreadExecutorTest
    , Execute
)
{
    sucrose::initializeXlib();

    auto    stateManagerUnique = brownsugar::StateManager::create();
    ASSERT_NE( nullptr, stateManagerUnique.get() );

    auto &  state = stateManagerUnique->getState();

    auto    windowUnique = fg::Window::create(
        "GLThreadExecutorTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );
    auto &  glContext = *glContextUnique;

    auto    i = 10;

    auto    threadExecutorUnique = fg::GLThreadExecutor::create(
        state
        , glContext
        , executeTestProc
        , i
    );
    ASSERT_NE( nullptr, threadExecutorUnique.get() );
    auto &  threadExecutor = *threadExecutorUnique;

    threadExecutor.execute();

    threadExecutor->joinerUnique->join();

    ASSERT_EQ( 20, i );
}
