﻿#include "fg/util/test.h"
#include "sucrose/gl/program.h"
#include "fg/gl/gl.h"
#include "fg/gl/context.h"
#include "fg/gl/currentcontext.h"
#include "fg/window/window.h"
#include "sucrose/window/xlib.h"

TEST(
    GLProgramTest
    , Create
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLProgramTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );
    auto &  glContext = *glContextUnique;

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( glContext );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );
    auto &  glCurrentContext = *glCurrentContextUnique;

    auto    glProgramUnique = fg::GLProgram::create( glCurrentContext );
    ASSERT_NE( nullptr, glProgramUnique.get() );
    const auto &    GL_PROGRAM = *glProgramUnique;

    ASSERT_EQ( &glContext, &( GL_PROGRAM->glContext ) );

    const auto &    OBJECT = GL_PROGRAM->object;
    ASSERT_NE( 0, OBJECT );
    ASSERT_EQ( fg::GL_TRUE, glCurrentContext.glIsProgram( OBJECT ) );

    ASSERT_EQ( &*GL_PROGRAM, GL_PROGRAM->objectDeleter.get() );
}

TEST(
    GLProgramTest
    , Destroy
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLProgramTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( *glContextUnique );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );
    auto &  glCurrentContext = *glCurrentContextUnique;

    auto    glProgramUnique = fg::GLProgram::create( glCurrentContext );
    ASSERT_NE( nullptr, glProgramUnique.get() );
    const auto &    GL_PROGRAM = *glProgramUnique;

    const auto  OBJECT = GL_PROGRAM->object;

    glProgramUnique.destroy();

    ASSERT_EQ( fg::GL_FALSE, glCurrentContext.glIsProgram( OBJECT ) );
}

TEST(
    GLProgramTest
    , Destroy_afterCurrentContextDeleted
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLProgramTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( *glContextUnique );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );
    auto &  glCurrentContext = *glCurrentContextUnique;

    auto    glProgramUnique = fg::GLProgram::create( glCurrentContext );
    ASSERT_NE( nullptr, glProgramUnique.get() );
    const auto &    GL_PROGRAM = *glProgramUnique;

    const auto  OBJECT = GL_PROGRAM->object;

    glCurrentContextUnique.destroy();

    glProgramUnique.destroy();

    ASSERT_EQ( fg::GL_FALSE, glCurrentContext.glIsProgram( OBJECT ) );
}

TEST(
    GLProgramTest
    , Destroy_afterMadeOtherCurrentContext
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLProgramTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );
    auto &  glContext = *glContextUnique;

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( glContext );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );

    auto    glProgramUnique = fg::GLProgram::create( *glCurrentContextUnique );
    ASSERT_NE( nullptr, glProgramUnique.get() );
    const auto &    GL_PROGRAM = *glProgramUnique;

    const auto  OBJECT = GL_PROGRAM->object;

    glCurrentContextUnique.destroy();

    auto    glContext2Unique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContext2Unique.get() );

    auto    glCurrentContext2Unique = fg::GLCurrentContext::create( *glContext2Unique );
    ASSERT_NE( nullptr, glCurrentContext2Unique.get() );

    glProgramUnique.destroy();

    glCurrentContext2Unique.destroy();

    auto    glCurrentContextAfterDeleteProgramUnique = fg::GLCurrentContext::create( glContext );
    ASSERT_NE( nullptr, glCurrentContextAfterDeleteProgramUnique.get() );

    ASSERT_EQ( fg::GL_FALSE, glCurrentContextAfterDeleteProgramUnique->glIsProgram( OBJECT ) );
}

TEST(
    GLProgramTest
    , GetObject
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLProgramTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( *glContextUnique );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );

    auto    glProgramUnique = fg::GLProgram::create( *glCurrentContextUnique );
    ASSERT_NE( nullptr, glProgramUnique.get() );
    const auto &    GL_PROGRAM = *glProgramUnique;

    ASSERT_EQ( GL_PROGRAM->object, glProgramUnique->getObject() );
}
