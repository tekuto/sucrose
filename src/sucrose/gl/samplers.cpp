﻿#include "fg/util/export.h"
#include "sucrose/gl/samplers.h"
#include "fg/gl/gl.h"
#include "sucrose/gl/currentcontext.h"
#include "sucrose/gl/currentcontextremaker.h"
#include "sucrose/gl/context.h"

#include <stdexcept>

namespace {
    FgGLSamplers::Names generateGLSamplers(
        fg::GLCurrentContext &  _glCurrentContext
        , fg::GLsizei           _size
    )
    {
        auto    names = FgGLSamplers::Names( _size );

        _glCurrentContext.glGetError();

        _glCurrentContext.glGenSamplers(
            names.size()
            , names.data()
        );

        if( _glCurrentContext.glGetError() != fg::GL_NO_ERROR ) {
            throw std::runtime_error( "glGenSamplers()が失敗" );
        }

        return names;
    }
}

FgGLSamplers * fgGLSamplersCreate(
    FgGLCurrentContext *    _glCurrentContextPtr
    , FgGLsizei             _size
)
{
    if( _size <= 0 ) {
        throw std::out_of_range( "サイズが0以下" );
    }

    return new FgGLSamplers(
        reinterpret_cast< fg::GLCurrentContext & >( *_glCurrentContextPtr )
        , _size
    );
}

void fgGLSamplersDestroy(
    FgGLSamplers *  _implPtr
)
{
    delete _implPtr;
}

FgGLsizei fgGLSamplersGetSize(
    const FgGLSamplers *    _IMPL_PTR
)
{
    return _IMPL_PTR->names.size();
}

FgGLuint fgGLSamplersGetName(
    const FgGLSamplers *    _IMPL_PTR
    , FgGLsizei             _index
)
{
    return _IMPL_PTR->names.at( _index );
}

void FgGLSamplers::DeleteNames::operator()(
    FgGLSamplers *  _glSamplersPtr
) const
{
    auto    glCurrentContextRemaker = sucrose::GLCurrentContextRemaker( FgGLContext::releaseGLCurrent() );

    auto    glCurrentContextUnique =  fg::GLCurrentContext::create( _glSamplersPtr->glContext );

    auto &  names = _glSamplersPtr->names;

    glCurrentContextUnique->glDeleteSamplers(
        names.size()
        , names.data()
    );
}

FgGLSamplers::FgGLSamplers(
    fg::GLCurrentContext &  _glCurrentContext
    , fg::GLsizei           _size
)
    : glContext( _glCurrentContext->getGLContext() )
    , names(
        generateGLSamplers(
            _glCurrentContext
            , _size
        )
    )
    , namesDeleter( this )
{
}
