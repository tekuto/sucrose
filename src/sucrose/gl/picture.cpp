﻿#include "fg/util/export.h"
#include "sucrose/gl/picture.h"

FgGLPicture * fgGLPictureCreate(
    const void *    _DATA_PTR
    , size_t        _size
    , FgGLsizei     _width
    , FgGLsizei     _height
    , FgGLenum      _format
    , FgGLenum      _dataType
)
{
    const auto  DATA_PTR = static_cast< const char * >( _DATA_PTR );

    return new FgGLPicture{
        FgGLPicture::Data(
            DATA_PTR
            , DATA_PTR + _size
        ),
        _width,
        _height,
        _format,
        _dataType,
    };
}

void fgGLPictureDestroy(
    FgGLPicture *   _implPtr
)
{
    delete _implPtr;
}

size_t fgGLPictureGetSize(
    const FgGLPicture * _IMPL_PTR
)
{
    return _IMPL_PTR->data.size();
}

const void * fgGLPictureGetData(
    const FgGLPicture * _IMPL_PTR
)
{
    return _IMPL_PTR->data.data();
}

FgGLsizei fgGLPictureGetWidth(
    const FgGLPicture * _IMPL_PTR
)
{
    return _IMPL_PTR->width;
}

FgGLsizei fgGLPictureGetHeight(
    const FgGLPicture * _IMPL_PTR
)
{
    return _IMPL_PTR->height;
}

FgGLenum fgGLPictureGetFormat(
    const FgGLPicture * _IMPL_PTR
)
{
    return _IMPL_PTR->format;
}

FgGLenum fgGLPictureGetDataType(
    const FgGLPicture * _IMPL_PTR
)
{
    return _IMPL_PTR->dataType;
}
