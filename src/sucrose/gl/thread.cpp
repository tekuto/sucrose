﻿#include "fg/util/export.h"
#include "sucrose/gl/thread.h"
#include "fg/core/state/state.h"
#include "fg/core/state/threadbackgroundsingleton.h"
#include "sucrose/gl/currentcontext.h"

namespace {
    void glThread(
        fg::StateThreadBackgroundSingleton< void, FgGLThread > &    _thread
    )
    {
        auto &  thread = _thread.getData();

        thread.procPtr( &thread );

        auto    glCurrentContextUnique = fg::GLCurrentContext::create( thread.glContext );
        auto &  glCurrentContext = *glCurrentContextUnique;

        glCurrentContext->swapBuffers();
    }
}

FgGLThread::FgGLThread(
    fg::State<> &       _state
    , fg::GLContext &   _glContext
    , FgGLThreadProc    _procPtr
    , void *            _userDataPtr
)
    : state( _state )
    , glContext( _glContext )
    , procPtr( _procPtr )
    , userDataPtr( _userDataPtr )
{
}

void FgGLThread::execute(
    fg::StateJoiner &   _joiner
)
{
    this->state.execute(
        glThread
        , _joiner
        , *this
    );
}

FgState * fgGLThreadGetState(
    FgGLThread *    _implPtr
)
{
    return &*( _implPtr->state );
}

FgGLContext * fgGLThreadGetGLContext(
    FgGLThread *    _implPtr
)
{
    return &*( _implPtr->glContext );
}

void * fgGLThreadGetData(
    FgGLThread *    _implPtr
)
{
    return _implPtr->userDataPtr;
}
