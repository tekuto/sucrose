﻿#include "fg/util/export.h"
#include "sucrose/gl/buffers.h"
#include "fg/gl/gl.h"
#include "sucrose/gl/currentcontext.h"
#include "sucrose/gl/currentcontextremaker.h"
#include "sucrose/gl/context.h"

#include <stdexcept>

namespace {
    FgGLBuffers::Names generateGLBuffers(
        fg::GLCurrentContext &  _glCurrentContext
        , fg::GLsizei           _size
    )
    {
        auto    names = FgGLBuffers::Names( _size );

        _glCurrentContext.glGetError();

        _glCurrentContext.glGenBuffers(
            names.size()
            , names.data()
        );

        if( _glCurrentContext.glGetError() != fg::GL_NO_ERROR ) {
            throw std::runtime_error( "glGenBuffers()が失敗" );
        }

        return names;
    }
}

FgGLBuffers * fgGLBuffersCreate(
    FgGLCurrentContext *    _glCurrentContextPtr
    , FgGLsizei             _size
)
{
    if( _size <= 0 ) {
        throw std::out_of_range( "サイズが0以下" );
    }

    return new FgGLBuffers(
        reinterpret_cast< fg::GLCurrentContext & >( *_glCurrentContextPtr )
        , _size
    );
}

void fgGLBuffersDestroy(
    FgGLBuffers *   _implPtr
)
{
    delete _implPtr;
}

FgGLsizei fgGLBuffersGetSize(
    const FgGLBuffers * _IMPL_PTR
)
{
    return _IMPL_PTR->names.size();
}

FgGLuint fgGLBuffersGetName(
    const FgGLBuffers * _IMPL_PTR
    , FgGLsizei         _index
)
{
    return _IMPL_PTR->names.at( _index );
}

void FgGLBuffers::DeleteNames::operator()(
    FgGLBuffers *   _glBuffersPtr
) const
{
    auto    glCurrentContextRemaker = sucrose::GLCurrentContextRemaker( FgGLContext::releaseGLCurrent() );

    auto    glCurrentContextUnique =  fg::GLCurrentContext::create( _glBuffersPtr->glContext );

    auto &  names = _glBuffersPtr->names;

    glCurrentContextUnique->glDeleteBuffers(
        names.size()
        , names.data()
    );
}

FgGLBuffers::FgGLBuffers(
    fg::GLCurrentContext &  _glCurrentContext
    , fg::GLsizei           _size
)
    : glContext( _glCurrentContext->getGLContext() )
    , names(
        generateGLBuffers(
            _glCurrentContext
            , _size
        )
    )
    , namesDeleter( this )
{
}

const fg::GLContext & FgGLBuffers::getGLContext(
) const
{
    return const_cast< FgGLBuffers * >( this )->getGLContext();
}

fg::GLContext & FgGLBuffers::getGLContext(
)
{
    return this->glContext;
}
