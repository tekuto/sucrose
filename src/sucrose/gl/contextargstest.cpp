﻿#include "fg/util/test.h"
#include "sucrose/gl/contextargs.h"

TEST(
    GLContextArgsTest
    , Create
)
{
    const auto  ARGS_UNIQUE = fg::GLContextArgs::create();
    ASSERT_NE( nullptr, ARGS_UNIQUE.get() );
    const auto &    ARGS = *ARGS_UNIQUE;

    const auto &    RED_SIZE = ARGS->redSize;
    ASSERT_TRUE( RED_SIZE.exists );
    ASSERT_EQ( 8, RED_SIZE.value );
    const auto &    GREEN_SIZE = ARGS->greenSize;
    ASSERT_TRUE( GREEN_SIZE.exists );
    ASSERT_EQ( 8, GREEN_SIZE.value );
    const auto &    BLUE_SIZE = ARGS->blueSize;
    ASSERT_TRUE( BLUE_SIZE.exists );
    ASSERT_EQ( 8, BLUE_SIZE.value );
    const auto &    ALPHA_SIZE = ARGS->alphaSize;
    ASSERT_TRUE( ALPHA_SIZE.exists );
    ASSERT_EQ( 8, ALPHA_SIZE.value );
    const auto &    DEPTH_SIZE = ARGS->depthSize;
    ASSERT_TRUE( DEPTH_SIZE.exists );
    ASSERT_EQ( 24, DEPTH_SIZE.value );
    const auto &    STENCIL_SIZE = ARGS->stencilSize;
    ASSERT_TRUE( STENCIL_SIZE.exists );
    ASSERT_EQ( 8, STENCIL_SIZE.value );
}

TEST(
    GLContextArgsTest
    , CreateBlank
)
{
    const auto  ARGS_UNIQUE = fg::GLContextArgs::createBlank();
    ASSERT_NE( nullptr, ARGS_UNIQUE.get() );
    const auto &    ARGS = *ARGS_UNIQUE;

    ASSERT_FALSE( ARGS->redSize.exists );
    ASSERT_FALSE( ARGS->greenSize.exists );
    ASSERT_FALSE( ARGS->blueSize.exists );
    ASSERT_FALSE( ARGS->alphaSize.exists );
    ASSERT_FALSE( ARGS->depthSize.exists );
    ASSERT_FALSE( ARGS->stencilSize.exists );
}

TEST(
    GLContextArgsTest
    , IsExistsRedSize
)
{
    auto    argsUnique = fg::GLContextArgs::createBlank();
    ASSERT_NE( nullptr, argsUnique.get() );

    argsUnique->setRedSize( 10 );

    ASSERT_TRUE( argsUnique->isExistsRedSize() );
}

TEST(
    GLContextArgsTest
    , GetRedSize
)
{
    auto    argsUnique = fg::GLContextArgs::createBlank();
    ASSERT_NE( nullptr, argsUnique.get() );

    argsUnique->setRedSize( 10 );

    ASSERT_EQ( 10, argsUnique->getRedSize() );
}

TEST(
    GLContextArgsTest
    , SetRedSize
)
{
    auto    argsUnique = fg::GLContextArgs::createBlank();
    ASSERT_NE( nullptr, argsUnique.get() );
    const auto &    ARGS = *argsUnique;

    argsUnique->setRedSize( 10 );

    const auto &    RED_SIZE = ARGS->redSize;
    ASSERT_TRUE( RED_SIZE.exists );
    ASSERT_EQ( 10, RED_SIZE.value );
}

TEST(
    GLContextArgsTest
    , ClearRedSize
)
{
    auto    argsUnique = fg::GLContextArgs::createBlank();
    ASSERT_NE( nullptr, argsUnique.get() );
    const auto &    ARGS = *argsUnique;

    argsUnique->setRedSize( 10 );

    argsUnique->clearRedSize();

    ASSERT_FALSE( ARGS->redSize.exists );
}

TEST(
    GLContextArgsTest
    , IsExistsGreenSize
)
{
    auto    argsUnique = fg::GLContextArgs::createBlank();
    ASSERT_NE( nullptr, argsUnique.get() );

    argsUnique->setGreenSize( 10 );

    ASSERT_TRUE( argsUnique->isExistsGreenSize() );
}

TEST(
    GLContextArgsTest
    , GetGreenSize
)
{
    auto    argsUnique = fg::GLContextArgs::createBlank();
    ASSERT_NE( nullptr, argsUnique.get() );

    argsUnique->setGreenSize( 10 );

    ASSERT_EQ( 10, argsUnique->getGreenSize() );
}

TEST(
    GLContextArgsTest
    , SetGreenSize
)
{
    auto    argsUnique = fg::GLContextArgs::createBlank();
    ASSERT_NE( nullptr, argsUnique.get() );
    const auto &    ARGS = *argsUnique;

    argsUnique->setGreenSize( 10 );

    const auto &    GREEN_SIZE = ARGS->greenSize;
    ASSERT_TRUE( GREEN_SIZE.exists );
    ASSERT_EQ( 10, GREEN_SIZE.value );
}

TEST(
    GLContextArgsTest
    , ClearGreenSize
)
{
    auto    argsUnique = fg::GLContextArgs::createBlank();
    ASSERT_NE( nullptr, argsUnique.get() );
    const auto &    ARGS = *argsUnique;

    argsUnique->setGreenSize( 10 );

    argsUnique->clearGreenSize();

    ASSERT_FALSE( ARGS->greenSize.exists );
}

TEST(
    GLContextArgsTest
    , IsExistsBlueSize
)
{
    auto    argsUnique = fg::GLContextArgs::createBlank();
    ASSERT_NE( nullptr, argsUnique.get() );

    argsUnique->setBlueSize( 10 );

    ASSERT_TRUE( argsUnique->isExistsBlueSize() );
}

TEST(
    GLContextArgsTest
    , GetBlueSize
)
{
    auto    argsUnique = fg::GLContextArgs::createBlank();
    ASSERT_NE( nullptr, argsUnique.get() );

    argsUnique->setBlueSize( 10 );

    ASSERT_EQ( 10, argsUnique->getBlueSize() );
}

TEST(
    GLContextArgsTest
    , SetBlueSize
)
{
    auto    argsUnique = fg::GLContextArgs::createBlank();
    ASSERT_NE( nullptr, argsUnique.get() );
    const auto &    ARGS = *argsUnique;

    argsUnique->setBlueSize( 10 );

    const auto &    BLUE_SIZE = ARGS->blueSize;
    ASSERT_TRUE( BLUE_SIZE.exists );
    ASSERT_EQ( 10, BLUE_SIZE.value );
}

TEST(
    GLContextArgsTest
    , ClearBlueSize
)
{
    auto    argsUnique = fg::GLContextArgs::createBlank();
    ASSERT_NE( nullptr, argsUnique.get() );
    const auto &    ARGS = *argsUnique;

    argsUnique->setBlueSize( 10 );

    argsUnique->clearBlueSize();

    ASSERT_FALSE( ARGS->blueSize.exists );
}

TEST(
    GLContextArgsTest
    , IsExistsAlphaSize
)
{
    auto    argsUnique = fg::GLContextArgs::createBlank();
    ASSERT_NE( nullptr, argsUnique.get() );

    argsUnique->setAlphaSize( 10 );

    ASSERT_TRUE( argsUnique->isExistsAlphaSize() );
}

TEST(
    GLContextArgsTest
    , GetAlphaSize
)
{
    auto    argsUnique = fg::GLContextArgs::createBlank();
    ASSERT_NE( nullptr, argsUnique.get() );

    argsUnique->setAlphaSize( 10 );

    ASSERT_EQ( 10, argsUnique->getAlphaSize() );
}

TEST(
    GLContextArgsTest
    , SetAlphaSize
)
{
    auto    argsUnique = fg::GLContextArgs::createBlank();
    ASSERT_NE( nullptr, argsUnique.get() );
    const auto &    ARGS = *argsUnique;

    argsUnique->setAlphaSize( 10 );

    const auto &    ALPHA_SIZE = ARGS->alphaSize;
    ASSERT_TRUE( ALPHA_SIZE.exists );
    ASSERT_EQ( 10, ALPHA_SIZE.value );
}

TEST(
    GLContextArgsTest
    , ClearAlphaSize
)
{
    auto    argsUnique = fg::GLContextArgs::createBlank();
    ASSERT_NE( nullptr, argsUnique.get() );
    const auto &    ARGS = *argsUnique;

    argsUnique->setAlphaSize( 10 );

    argsUnique->clearAlphaSize();

    ASSERT_FALSE( ARGS->alphaSize.exists );
}

TEST(
    GLContextArgsTest
    , IsExistsDepthSize
)
{
    auto    argsUnique = fg::GLContextArgs::createBlank();
    ASSERT_NE( nullptr, argsUnique.get() );

    argsUnique->setDepthSize( 10 );

    ASSERT_TRUE( argsUnique->isExistsDepthSize() );
}

TEST(
    GLContextArgsTest
    , GetDepthSize
)
{
    auto    argsUnique = fg::GLContextArgs::createBlank();
    ASSERT_NE( nullptr, argsUnique.get() );

    argsUnique->setDepthSize( 10 );

    ASSERT_EQ( 10, argsUnique->getDepthSize() );
}

TEST(
    GLContextArgsTest
    , SetDepthSize
)
{
    auto    argsUnique = fg::GLContextArgs::createBlank();
    ASSERT_NE( nullptr, argsUnique.get() );
    const auto &    ARGS = *argsUnique;

    argsUnique->setDepthSize( 10 );

    const auto &    DEPTH_SIZE = ARGS->depthSize;
    ASSERT_TRUE( DEPTH_SIZE.exists );
    ASSERT_EQ( 10, DEPTH_SIZE.value );
}

TEST(
    GLContextArgsTest
    , ClearDepthSize
)
{
    auto    argsUnique = fg::GLContextArgs::createBlank();
    ASSERT_NE( nullptr, argsUnique.get() );
    const auto &    ARGS = *argsUnique;

    argsUnique->setDepthSize( 10 );

    argsUnique->clearDepthSize();

    ASSERT_FALSE( ARGS->depthSize.exists );
}

TEST(
    GLContextArgsTest
    , IsExistsStencilSize
)
{
    auto    argsUnique = fg::GLContextArgs::createBlank();
    ASSERT_NE( nullptr, argsUnique.get() );

    argsUnique->setStencilSize( 10 );

    ASSERT_TRUE( argsUnique->isExistsStencilSize() );
}

TEST(
    GLContextArgsTest
    , GetStencilSize
)
{
    auto    argsUnique = fg::GLContextArgs::createBlank();
    ASSERT_NE( nullptr, argsUnique.get() );

    argsUnique->setStencilSize( 10 );

    ASSERT_EQ( 10, argsUnique->getStencilSize() );
}

TEST(
    GLContextArgsTest
    , SetStencilSize
)
{
    auto    argsUnique = fg::GLContextArgs::createBlank();
    ASSERT_NE( nullptr, argsUnique.get() );
    const auto &    ARGS = *argsUnique;

    argsUnique->setStencilSize( 10 );

    const auto &    STENCIL_SIZE = ARGS->stencilSize;
    ASSERT_TRUE( STENCIL_SIZE.exists );
    ASSERT_EQ( 10, STENCIL_SIZE.value );
}

TEST(
    GLContextArgsTest
    , ClearStencilSize
)
{
    auto    argsUnique = fg::GLContextArgs::createBlank();
    ASSERT_NE( nullptr, argsUnique.get() );
    const auto &    ARGS = *argsUnique;

    argsUnique->setStencilSize( 10 );

    argsUnique->clearStencilSize();

    ASSERT_FALSE( ARGS->stencilSize.exists );
}
