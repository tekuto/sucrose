﻿#include "fg/util/test.h"
#include "fg/gl/vertexarraybinder.h"
#include "fg/gl/gl.h"
#include "fg/gl/context.h"
#include "fg/gl/currentcontext.h"
#include "fg/gl/vertexarrays.h"
#include "fg/window/window.h"
#include "sucrose/gl/vertexarrays.h"
#include "sucrose/window/xlib.h"

TEST(
    GLVertexArrayBinderTest
    , Create
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLVertexArrayBinderTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );
    auto &  glContext = *glContextUnique;

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( glContext );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );
    auto &  glCurrentContext = *glCurrentContextUnique;

    auto    glVertexArraysUnique = fg::GLVertexArrays::create(
        glCurrentContext
        , 10
    );
    ASSERT_NE( nullptr, glVertexArraysUnique.get() );
    const auto &    GL_VERTEX_ARRAYS = *glVertexArraysUnique;

    const auto  GL_VERTEX_ARRAY_BINDER_UNIQUE = fg::GLVertexArrayBinder::create(
        glCurrentContext
        , GL_VERTEX_ARRAYS
        , 5
    );
    ASSERT_NE( nullptr, GL_VERTEX_ARRAY_BINDER_UNIQUE.get() );
    const auto &    GL_VERTEX_ARRAY_BINDER = *GL_VERTEX_ARRAY_BINDER_UNIQUE;

    ASSERT_EQ( &glCurrentContext, &reinterpret_cast< const fg::GLCurrentContext & >( GL_VERTEX_ARRAY_BINDER ) );

    glCurrentContext.glGetError();
    auto    name = fg::GLuint();
    glCurrentContext.glGetIntegerv(
        fg::GL_VERTEX_ARRAY_BINDING
        , &reinterpret_cast< fg::GLint & >( name )
    );
    ASSERT_EQ( fg::GL_NO_ERROR, glCurrentContext.glGetError() );
    ASSERT_NE( 0, name );
    ASSERT_EQ( GL_VERTEX_ARRAYS->names[ 5 ], name );
}

TEST(
    GLVertexArrayBinderTest
    , Create_failedWhenOtherContext
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLVertexArrayBinderTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );
    auto &  glContext = *glContextUnique;

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( glContext );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );
    auto &  glCurrentContext = *glCurrentContextUnique;

    auto    glVertexArraysUnique = fg::GLVertexArrays::create(
        glCurrentContext
        , 10
    );
    ASSERT_NE( nullptr, glVertexArraysUnique.get() );
    const auto &    GL_VERTEX_ARRAYS = *glVertexArraysUnique;

    glCurrentContextUnique.destroy();

    auto    glContext2Unique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContext2Unique.get() );
    auto &  glContext2 = *glContext2Unique;

    auto    glCurrentContext2Unique = fg::GLCurrentContext::create( glContext2 );
    ASSERT_NE( nullptr, glCurrentContext2Unique.get() );
    auto &  glCurrentContext2 = *glCurrentContext2Unique;

    try {
        auto    glVertexArrayBinderUnique = fg::GLVertexArrayBinder::create(
            glCurrentContext2
            , GL_VERTEX_ARRAYS
            , 5
        );

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}
}

TEST(
    GLVertexArrayBinderTest
    , Destroy
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLVertexArrayBinderTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );
    auto &  glContext = *glContextUnique;

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( glContext );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );
    auto &  glCurrentContext = *glCurrentContextUnique;

    auto    glVertexArraysUnique = fg::GLVertexArrays::create(
        glCurrentContext
        , 10
    );
    ASSERT_NE( nullptr, glVertexArraysUnique.get() );
    const auto &    GL_VERTEX_ARRAYS = *glVertexArraysUnique;

    auto    glVertexArrayBinderUnique = fg::GLVertexArrayBinder::create(
        glCurrentContext
        , GL_VERTEX_ARRAYS
        , 5
    );
    ASSERT_NE( nullptr, glVertexArrayBinderUnique.get() );

    glVertexArrayBinderUnique.destroy();

    glCurrentContext.glGetError();
    auto    name = fg::GLuint();
    glCurrentContext.glGetIntegerv(
        fg::GL_VERTEX_ARRAY_BINDING
        , &reinterpret_cast< fg::GLint & >( name )
    );
    ASSERT_EQ( fg::GL_NO_ERROR, glCurrentContext.glGetError() );
    ASSERT_EQ( 0, name );
}
