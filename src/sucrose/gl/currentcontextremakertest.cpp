﻿#include "fg/util/test.h"
#include "sucrose/gl/currentcontextremaker.h"
#include "fg/gl/context.h"
#include "fg/gl/currentcontext.h"
#include "fg/window/window.h"
#include "sucrose/gl/context.h"
#include "sucrose/window/xlib.h"

TEST(
    GLCurrentContextRemakerTest
    , Create
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLCurrentContextRemakerTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );
    auto &  glContext = *glContextUnique;

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( glContext );
    ASSERT_EQ( &glContext, reinterpret_cast< fg::GLContext * >( glCurrentContextUnique.get() ) );

    auto    glCurrentContextRemaker = sucrose::GLCurrentContextRemaker( FgGLContext::releaseGLCurrent() );
    ASSERT_EQ( &*glContext, glCurrentContextRemaker.get() );

    auto    glCurrentContext2Unique = fg::GLCurrentContext::create( glContext );
    ASSERT_EQ( &glContext, reinterpret_cast< fg::GLContext * >( glCurrentContext2Unique.get() ) );
    glCurrentContext2Unique.destroy();

    glCurrentContextRemaker.reset();

    try {
        auto    glCurrentContext3Unique = fg::GLCurrentContext::create( glContext );

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}
}
