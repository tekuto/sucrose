﻿#include "fg/util/export.h"
#include "sucrose/gl/currentcontext.h"
#include "fg/def/gl/functions.h"
#include "sucrose/gl/context.h"

#include <stdexcept>

FgGLCurrentContext * fgGLCurrentContextCreate(
    FgGLContext *   _glContextPtr
)
{
    _glContextPtr->makeGLCurrent();

    return reinterpret_cast< FgGLCurrentContext * >( _glContextPtr );
}

void fgGLCurrentContextDestroy(
    FgGLCurrentContext *    _implPtr
)
{
    FgGLContext::releaseGLCurrent();
}

#define GL_ARGS( ... ) \
    FgGLCurrentContext * _implPtr __VA_ARGS__
#define GL_VALUES( _dummy, ... ) \
    __VA_ARGS__
#define GL_CALL_FUNCTION( _name, _values ) \
    _implPtr->getGLContext()->gl##_name( GL_VALUES _values );
#define GL_DEFINE_FUNCTION( _proc, _name, _returnType, _args ) \
    _returnType fgGLCurrentContextGL##_name( GL_ARGS _args ) { _proc }
#define FG_GL_FUNCTION_NUM( _name, _returnType, _args, _values ) \
    GL_DEFINE_FUNCTION( return GL_CALL_FUNCTION( _name, _values ), _name, _returnType, _args )
#define FG_GL_FUNCTION_PTR( _name, _returnType, _args, _values ) \
    GL_DEFINE_FUNCTION( return GL_CALL_FUNCTION( _name, _values ), _name, _returnType *, _args )
#define FG_GL_FUNCTION_VOID( _name, _args, _values ) \
    GL_DEFINE_FUNCTION( GL_CALL_FUNCTION( _name, _values ), _name, void, _args )

// OpenGL関数のラッパー関数定義
FG_GL_FUNCTIONS

#undef  FG_GL_FUNCTION_VOID
#undef  FG_GL_FUNCTION_PTR
#undef  FG_GL_FUNCTION_NUM
#undef  GL_DEFINE_FUNCTION
#undef  GL_CALL_FUNCTION
#undef  GL_VALUES
#undef  GL_ARGS

fg::GLContext & FgGLCurrentContext::getGLContext(
)
{
    return reinterpret_cast< fg::GLContext & >( *this );
}

void FgGLCurrentContext::swapBuffers(
)
{
    this->getGLContext()->swapBuffers();
}
