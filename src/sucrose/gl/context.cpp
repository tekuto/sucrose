﻿#include "fg/util/export.h"
#include "sucrose/gl/context.h"
#include "fg/gl/contextargs.h"
#include "fg/def/gl/functions.h"
#include "sucrose/window/window.h"

#include <GL/glx.h>
#include <X11/Xlib.h>
#include <utility>
#include <vector>
#include <stdexcept>

namespace {
    thread_local auto   currentContextPtr = static_cast< FgGLContext * >( nullptr );

    FgGLContext::XVisualInfoUnique createXVisualInfo(
        const FgGLContextArgs & _ARGS
        , Display &             _xDisplay
    )
    {
        const auto &    ARGS = reinterpret_cast< const fg::GLContextArgs & >( _ARGS );

        auto    attrs = std::vector< int >(
            {
                GLX_USE_GL,
                GLX_RGBA,
                GLX_DOUBLEBUFFER,
                True,
            }
        );

        if( ARGS.isExistsRedSize() == true ) {
            attrs.push_back( GLX_RED_SIZE );
            attrs.push_back( ARGS.getRedSize() );
        }

        if( ARGS.isExistsGreenSize() == true ) {
            attrs.push_back( GLX_GREEN_SIZE );
            attrs.push_back( ARGS.getGreenSize() );
        }

        if( ARGS.isExistsBlueSize() == true ) {
            attrs.push_back( GLX_BLUE_SIZE );
            attrs.push_back( ARGS.getBlueSize() );
        }

        if( ARGS.isExistsAlphaSize() == true ) {
            attrs.push_back( GLX_ALPHA_SIZE );
            attrs.push_back( ARGS.getAlphaSize() );
        }

        if( ARGS.isExistsDepthSize() == true ) {
            attrs.push_back( GLX_DEPTH_SIZE );
            attrs.push_back( ARGS.getDepthSize() );
        }

        if( ARGS.isExistsStencilSize() == true ) {
            attrs.push_back( GLX_STENCIL_SIZE );
            attrs.push_back( ARGS.getStencilSize() );
        }

        attrs.push_back( None );

        const auto  X_DEFAULT_SCREEN = DefaultScreen( &_xDisplay );

        auto    thisUnique = FgGLContext::XVisualInfoUnique(
            glXChooseVisual(
                &_xDisplay
                , X_DEFAULT_SCREEN
                , attrs.data()
            )
        );
        if( thisUnique.get() == nullptr ) {
            throw std::runtime_error( "glXChooseVisual()が失敗" );
        }

        return thisUnique;
    }

    GLXContext createGLXContext(
        GLXContext      _shareList
        , Display &     _xDisplay
        , XVisualInfo & _xVisualInfo
    )
    {
        auto    glxContext = glXCreateContext(
            &_xDisplay
            , &_xVisualInfo
            , _shareList
            , True
        );
        if( glxContext == nullptr ) {
            throw std::runtime_error( "glXCreateContext()が失敗" );
        }

        return glxContext;
    }

    void destroyGLXContext(
        GLXContext  _glxContext
        , Display & _xDisplay
    )
    {
        glXDestroyContext(
            &_xDisplay
            , _glxContext
        );
    }

    FgGLContext::GLXContextManagerUnique createGLXContextManager(
        GLXContext      _shareList
        , Display &     _xDisplay
        , XVisualInfo & _xVisualInfo
    )
    {
        auto    thisUnique = FgGLContext::GLXContextManagerUnique(
            new FgGLContext::GLXContextManager{
                _xDisplay,
            }
        );
        auto &  glxContextManager = *thisUnique;

        glxContextManager.glxContext = createGLXContext(
            _shareList
            , _xDisplay
            , _xVisualInfo
        );
        glxContextManager.glxContextDestroyer.reset( &glxContextManager );

        return thisUnique;
    }

#define DUMMY_GL_FUNCTION_NAME( _name ) dummy_##_name

#define GL_ARGS( _dummy, ... ) \
    __VA_ARGS__

#define GL_DEFINE_FUNCTION( _proc, _name, _returnType, _args ) \
    _returnType DUMMY_GL_FUNCTION_NAME( _name )( GL_ARGS _args ) { _proc }

#define FG_GL_FUNCTION_NUM( _name, _returnType, _args, _values ) \
    GL_DEFINE_FUNCTION( return 0;, gl##_name, _returnType, _args )
#define FG_GL_FUNCTION_PTR( _name, _returnType, _args, _values ) \
    GL_DEFINE_FUNCTION( return nullptr;, gl##_name, _returnType *, _args )
#define FG_GL_FUNCTION_VOID( _name, _args, _values ) \
    GL_DEFINE_FUNCTION( , gl##_name, void, _args )

    // OpenGL関数のダミー追加
    FG_GL_FUNCTIONS

#undef  FG_GL_FUNCTION_VOID
#undef  FG_GL_FUNCTION_PTR
#undef  FG_GL_FUNCTION_NUM
#undef  GL_DEFINE_FUNCTION
#undef  GL_ARGS

    void initializeGL(
        fg::GLContext & _glContext
    )
    {
#define GL_FUNCTION_IMPL( _name ) \
    { \
        auto &  function = _glContext->_name; \
\
        function = reinterpret_cast< decltype( _glContext->_name ) >( glXGetProcAddress( reinterpret_cast< const fg::GLubyte * >( #_name ) ) ); \
        if( function == nullptr ) { \
            function = DUMMY_GL_FUNCTION_NAME( _name ); \
        } \
    }

#define FG_GL_FUNCTION_NUM( _name, ... ) GL_FUNCTION_IMPL( gl##_name )
#define FG_GL_FUNCTION_PTR( _name, ... ) GL_FUNCTION_IMPL( gl##_name )
#define FG_GL_FUNCTION_VOID( _name, ... ) GL_FUNCTION_IMPL( gl##_name )

        // OpenGL関数取得
        FG_GL_FUNCTIONS

#undef  FG_GL_FUNCTION_VOID
#undef  FG_GL_FUNCTION_PTR
#undef  FG_GL_FUNCTION_NUM

#undef  GL_FUNCTION_IMPL
    }

#undef  DUMMY_GL_FUNCTION_NAME

    FgGLContext * createGLContext(
        FgWindow &                  _window
        , const FgGLContextArgs &   _ARGS
        , GLXContext                _shareList
    )
    {
        auto &  xDisplay = _window.getXDisplayForPaint();

        auto    xVisualInfoUnique = createXVisualInfo(
            _ARGS
            , xDisplay
        );
        auto &  xVisualInfo = *xVisualInfoUnique;

        auto    glxContextManagerUnique = createGLXContextManager(
            _shareList
            , xDisplay
            , xVisualInfo
        );

        auto    thisUnique = fg::GLContext::Unique(
            new FgGLContext{
                reinterpret_cast< fg::Window & >( _window ),
                std::move( xVisualInfoUnique ),
                std::move( glxContextManagerUnique ),
            }
        );

        initializeGL( *thisUnique );

        return &**( thisUnique.release() );
    }

    FgGLContext * createGLContext(
        FgWindow &      _window
        , GLXContext    _shareList
    )
    {
        const auto  ARGS_UNIQUE = fg::GLContextArgs::create();

        return createGLContext(
            _window
            , **ARGS_UNIQUE
            , _shareList
        );
    }

    int getConfig(
        FgGLContext &   _glContext
        , int           _configName
    )
    {
        auto    value = int();
        if( glXGetConfig(
            &( _glContext.glxContextManagerUnique->xDisplay )
            , _glContext.xVisualInfoUnique.get()
            , _configName
            , &value
        ) != 0 ) {
            throw std::runtime_error( "glXGetConfig()が失敗" );
        }

        return value;
    }

    void glMakeCurrent(
        FgGLContext &   _glContext
        , Window        _xWindow
        , GLXContext    _glxContext
    )
    {
        if( glXMakeCurrent(
            &( _glContext.glxContextManagerUnique->xDisplay )
            , _xWindow
            , _glxContext
        ) != True ) {
            throw std::runtime_error( "glXMakeCurrent()が失敗" );
        }
    }
}

void FgGLContext::FreeXVisualInfo::operator()(
    XVisualInfo *   _xVisualInfoPtr
) const
{
    XFree( _xVisualInfoPtr );
}

void FgGLContext::GLXContextManager::DestroyGLXContext::operator()(
    FgGLContext::GLXContextManager *    _glxContextManagerPtr
) const
{
    destroyGLXContext(
        _glxContextManagerPtr->glxContext
        , _glxContextManagerPtr->xDisplay
    );
}

void FgGLContext::makeGLCurrent(
)
{
    if( currentContextPtr != nullptr ) {
        throw std::runtime_error( "カレントのGLコンテキストが既に存在する" );
    }

    glMakeCurrent(
        *this
        , this->window->getXWindow()
        , this->glxContextManagerUnique->glxContext
    );

    currentContextPtr = this;
}

FgGLContext * FgGLContext::releaseGLCurrent(
)
{
    if( currentContextPtr == nullptr ) {
        return nullptr;
    }
    auto &  currentContext = *currentContextPtr;

    glMakeCurrent(
        currentContext
        , None
        , nullptr
    );

    currentContextPtr = nullptr;

    return &currentContext;
}

void FgGLContext::swapBuffers(
)
{
    glXSwapBuffers(
        &( this->glxContextManagerUnique->xDisplay )
        , this->window->getXWindow()
    );
}

FgGLContext * fgGLContextCreate(
    FgWindow *  _windowPtr
)
{
    return createGLContext(
        *_windowPtr
        , nullptr
    );
}

FgGLContext * fgGLContextCreateWithShareList(
    FgWindow *      _windowPtr
    , FgGLContext * _shareListPtr
)
{
    return createGLContext(
        *_windowPtr
        , _shareListPtr->glxContextManagerUnique->glxContext
    );
}

FgGLContext * fgGLContextCreateWithArgs(
    FgWindow *                  _windowPtr
    , const FgGLContextArgs *   _ARGS_PTR
)
{
    return createGLContext(
        *_windowPtr
        , *_ARGS_PTR
        , nullptr
    );
}

FgGLContext * fgGLContextCreateWithShareListAndArgs(
    FgWindow *                  _windowPtr
    , FgGLContext *             _shareListPtr
    , const FgGLContextArgs *   _ARGS_PTR
)
{
    return createGLContext(
        *_windowPtr
        , *_ARGS_PTR
        , _shareListPtr->glxContextManagerUnique->glxContext
    );
}

void fgGLContextDestroy(
    FgGLContext *   _implPtr
)
{
    delete _implPtr;
}

FgWindow * fgGLContextGetWindow(
    FgGLContext *   _implPtr
)
{
    return &*( _implPtr->window );
}

int fgGLContextGetBufferSize(
    FgGLContext *   _implPtr
)
{
    return getConfig(
        *_implPtr
        , GLX_BUFFER_SIZE
    );
}

int fgGLContextGetRedSize(
    FgGLContext *   _implPtr
)
{
    return getConfig(
        *_implPtr
        , GLX_RED_SIZE
    );
}

int fgGLContextGetGreenSize(
    FgGLContext *   _implPtr
)
{
    return getConfig(
        *_implPtr
        , GLX_GREEN_SIZE
    );
}

int fgGLContextGetBlueSize(
    FgGLContext *   _implPtr
)
{
    return getConfig(
        *_implPtr
        , GLX_BLUE_SIZE
    );
}

int fgGLContextGetAlphaSize(
    FgGLContext *   _implPtr
)
{
    return getConfig(
        *_implPtr
        , GLX_ALPHA_SIZE
    );
}

int fgGLContextGetDepthSize(
    FgGLContext *   _implPtr
)
{
    return getConfig(
        *_implPtr
        , GLX_DEPTH_SIZE
    );
}

int fgGLContextGetStencilSize(
    FgGLContext *   _implPtr
)
{
    return getConfig(
        *_implPtr
        , GLX_STENCIL_SIZE
    );
}
