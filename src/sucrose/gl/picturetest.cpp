﻿#include "fg/util/test.h"
#include "sucrose/gl/picture.h"

#include <array>

TEST(
    GLPictureTest
    , Create
)
{
    const auto  PICTURE_UNIQUE = fg::GLPicture::create(
        std::array< char, 4 >(
            {
                10,
                20,
                30,
                40,
            }
        ).data()
        , 3
        , 50
        , 60
        , 70
        , 80
    );
    ASSERT_NE( nullptr, PICTURE_UNIQUE.get() );
    const auto &    PICTURE = *PICTURE_UNIQUE;

    const auto &    DATA = PICTURE->data;
    ASSERT_EQ( 3, DATA.size() );
    ASSERT_EQ( 10, DATA[ 0 ] );
    ASSERT_EQ( 20, DATA[ 1 ] );
    ASSERT_EQ( 30, DATA[ 2 ] );

    ASSERT_EQ( 50, PICTURE->width );
    ASSERT_EQ( 60, PICTURE->height );
    ASSERT_EQ( 70, PICTURE->format );
    ASSERT_EQ( 80, PICTURE->dataType );
}

TEST(
    GLPictureTest
    , GetSize
)
{
    const auto  PICTURE_UNIQUE = fg::GLPicture::create(
        std::array< char, 4 >(
            {
                10,
                20,
                30,
                40,
            }
        ).data()
        , 3
        , 50
        , 60
        , 70
        , 80
    );
    ASSERT_NE( nullptr, PICTURE_UNIQUE.get() );

    ASSERT_EQ( 3, PICTURE_UNIQUE->getSize() );
}

TEST(
    GLPictureTest
    , GetData
)
{
    const auto  PICTURE_UNIQUE = fg::GLPicture::create(
        std::array< char, 4 >(
            {
                10,
                20,
                30,
                40,
            }
        ).data()
        , 3
        , 50
        , 60
        , 70
        , 80
    );
    ASSERT_NE( nullptr, PICTURE_UNIQUE.get() );
    const auto &    PICTURE = *PICTURE_UNIQUE;

    const auto  DATA = static_cast< const char * >( PICTURE_UNIQUE->getData() );
    ASSERT_EQ( PICTURE->data.data(), DATA );
    ASSERT_EQ( 10, DATA[ 0 ] );
    ASSERT_EQ( 20, DATA[ 1 ] );
    ASSERT_EQ( 30, DATA[ 2 ] );
}

TEST(
    GLPictureTest
    , GetWidth
)
{
    const auto  PICTURE_UNIQUE = fg::GLPicture::create(
        std::array< char, 4 >(
            {
                10,
                20,
                30,
                40,
            }
        ).data()
        , 3
        , 50
        , 60
        , 70
        , 80
    );
    ASSERT_NE( nullptr, PICTURE_UNIQUE.get() );

    ASSERT_EQ( 50, PICTURE_UNIQUE->getWidth() );
}

TEST(
    GLPictureTest
    , GetHeight
)
{
    const auto  PICTURE_UNIQUE = fg::GLPicture::create(
        std::array< char, 4 >(
            {
                10,
                20,
                30,
                40,
            }
        ).data()
        , 3
        , 50
        , 60
        , 70
        , 80
    );
    ASSERT_NE( nullptr, PICTURE_UNIQUE.get() );

    ASSERT_EQ( 60, PICTURE_UNIQUE->getHeight() );
}

TEST(
    GLPictureTest
    , GetFormat
)
{
    const auto  PICTURE_UNIQUE = fg::GLPicture::create(
        std::array< char, 4 >(
            {
                10,
                20,
                30,
                40,
            }
        ).data()
        , 3
        , 50
        , 60
        , 70
        , 80
    );
    ASSERT_NE( nullptr, PICTURE_UNIQUE.get() );

    ASSERT_EQ( 70, PICTURE_UNIQUE->getFormat() );
}

TEST(
    GLPictureTest
    , GetDataType
)
{
    const auto  PICTURE_UNIQUE = fg::GLPicture::create(
        std::array< char, 4 >(
            {
                10,
                20,
                30,
                40,
            }
        ).data()
        , 3
        , 50
        , 60
        , 70
        , 80
    );
    ASSERT_NE( nullptr, PICTURE_UNIQUE.get() );

    ASSERT_EQ( 80, PICTURE_UNIQUE->getDataType() );
}
