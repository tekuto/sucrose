﻿#include "fg/util/test.h"
#include "sucrose/gl/samplers.h"
#include "fg/gl/gl.h"
#include "fg/gl/context.h"
#include "fg/gl/currentcontext.h"
#include "fg/window/window.h"
#include "sucrose/window/xlib.h"

TEST(
    GLSamplersTest
    , Create
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLSamplersTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );
    auto &  glContext = *glContextUnique;

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( glContext );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );
    auto &  glCurrentContext = *glCurrentContextUnique;

    const auto  GL_SAMPLERS_UNIQUE = fg::GLSamplers::create(
        glCurrentContext
        , 10
    );
    ASSERT_NE( nullptr, GL_SAMPLERS_UNIQUE.get() );
    const auto &    GL_SAMPLERS = *GL_SAMPLERS_UNIQUE;

    ASSERT_EQ( &glContext, &( GL_SAMPLERS->glContext ) );

    const auto &    NAMES = GL_SAMPLERS->names;
    ASSERT_EQ( 10, NAMES.size() );
    for( const auto & NAME : NAMES ) {
        ASSERT_NE( 0, NAME );
        ASSERT_EQ( fg::GL_TRUE, glCurrentContext.glIsSampler( NAME ) );
    }

    ASSERT_EQ( &*GL_SAMPLERS, GL_SAMPLERS->namesDeleter.get() );
}

TEST(
    GLSamplersTest
    , Create_failedWhenIllegalSize
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLSamplersTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );
    auto &  glContext = *glContextUnique;

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( glContext );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );
    auto &  glCurrentContext = *glCurrentContextUnique;

    try {
        auto    glSamplersUnique = fg::GLSamplers::create(
            glCurrentContext
            , 0
        );

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}

    try {
        auto    glSamplersUnique = fg::GLSamplers::create(
            glCurrentContext
            , -1
        );

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}
}

TEST(
    GLSamplersTest
    , Destroy
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLSamplersTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );
    auto &  glContext = *glContextUnique;

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( glContext );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );
    auto &  glCurrentContext = *glCurrentContextUnique;

    auto    glSamplersUnique = fg::GLSamplers::create(
        glCurrentContext
        , 10
    );
    ASSERT_NE( nullptr, glSamplersUnique.get() );
    const auto &    GL_SAMPLERS = *glSamplersUnique;

    const auto  NAMES_BACKUP = GL_SAMPLERS->names;

    glSamplersUnique.destroy();

    for( const auto & NAME : NAMES_BACKUP ) {
        ASSERT_EQ( fg::GL_FALSE, glCurrentContext.glIsSampler( NAME ) );
    }
}

TEST(
    GLSamplersTest
    , Destroy_afterCurrentContextDeleted
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLSamplersTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );
    auto &  glContext = *glContextUnique;

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( glContext );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );
    auto &  glCurrentContext = *glCurrentContextUnique;

    auto    glSamplersUnique = fg::GLSamplers::create(
        glCurrentContext
        , 10
    );
    ASSERT_NE( nullptr, glSamplersUnique.get() );
    const auto &    GL_SAMPLERS = *glSamplersUnique;

    const auto  NAMES_BACKUP = GL_SAMPLERS->names;

    glCurrentContextUnique.destroy();

    glSamplersUnique.destroy();

    auto    glCurrentContextAfterDeleteSamplersUnique = fg::GLCurrentContext::create( glContext );
    ASSERT_NE( nullptr, glCurrentContextAfterDeleteSamplersUnique.get() );

    for( const auto & NAME : NAMES_BACKUP ) {
        ASSERT_EQ( fg::GL_FALSE, glCurrentContextAfterDeleteSamplersUnique->glIsSampler( NAME ) );
    }
}

TEST(
    GLSamplersTest
    , Destroy_afterMadeOtherCurrentContext
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLSamplersTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );
    auto &  glContext = *glContextUnique;

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( glContext );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );
    auto &  glCurrentContext = *glCurrentContextUnique;

    auto    glSamplersUnique = fg::GLSamplers::create(
        glCurrentContext
        , 10
    );
    ASSERT_NE( nullptr, glSamplersUnique.get() );
    const auto &    GL_SAMPLERS = *glSamplersUnique;

    ASSERT_EQ( &glContext, &( GL_SAMPLERS->glContext ) );

    const auto  NAME_BACKUP = GL_SAMPLERS->names[ 0 ];

    glCurrentContext.glSamplerParameteri(
        NAME_BACKUP
        , fg::GL_TEXTURE_MIN_FILTER
        , fg::GL_NEAREST
    );

    glCurrentContextUnique.destroy();

    auto    glContext2Unique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContext2Unique.get() );
    auto &  glContext2 = *glContext2Unique;

    auto    glCurrentContext2Unique = fg::GLCurrentContext::create( glContext2 );
    ASSERT_NE( nullptr, glCurrentContext2Unique.get() );

    glSamplersUnique.destroy();

    glCurrentContext2Unique.destroy();

    auto    glCurrentContextAfterDeleteSamplersUnique = fg::GLCurrentContext::create( glContext );
    ASSERT_NE( nullptr, glCurrentContextAfterDeleteSamplersUnique.get() );

    auto    glTextureMinFilter = fg::GLint();
    glCurrentContextAfterDeleteSamplersUnique->glGetError();
    glCurrentContextAfterDeleteSamplersUnique->glGetSamplerParameteriv(
        NAME_BACKUP
        , fg::GL_TEXTURE_MIN_FILTER
        , &glTextureMinFilter
    );
    ASSERT_NE( fg::GL_NO_ERROR, glCurrentContextAfterDeleteSamplersUnique->glGetError() );
}

TEST(
    GLSamplersTest
    , GetSize
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLSamplersTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );
    auto &  glContext = *glContextUnique;

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( glContext );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );
    auto &  glCurrentContext = *glCurrentContextUnique;

    auto    glSamplersUnique = fg::GLSamplers::create(
        glCurrentContext
        , 10
    );
    ASSERT_NE( nullptr, glSamplersUnique.get() );

    ASSERT_EQ( 10, glSamplersUnique->getSize() );
}

TEST(
    GLSamplersTest
    , GetName
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLSamplersTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );
    auto &  glContext = *glContextUnique;

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( glContext );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );
    auto &  glCurrentContext = *glCurrentContextUnique;

    auto    glSamplersUnique = fg::GLSamplers::create(
        glCurrentContext
        , 10
    );
    ASSERT_NE( nullptr, glSamplersUnique.get() );
    const auto &    GL_SAMPLERS = *glSamplersUnique;

    const auto  NAME = GL_SAMPLERS.getName( 0 );
    ASSERT_NE( 0, NAME );
    ASSERT_EQ( GL_SAMPLERS->names[ 0 ], NAME );
}
