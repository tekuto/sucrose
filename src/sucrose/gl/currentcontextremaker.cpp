﻿#include "sucrose/gl/currentcontextremaker.h"
#include "sucrose/gl/context.h"

namespace sucrose {
    void RemakeGLCurrentContext::operator()(
        FgGLContext *   _glContextPtr
    ) const
    {
        _glContextPtr->makeGLCurrent();
    }
}
