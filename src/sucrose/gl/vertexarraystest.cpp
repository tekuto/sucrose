﻿#include "fg/util/test.h"
#include "sucrose/gl/vertexarrays.h"
#include "fg/gl/gl.h"
#include "fg/gl/context.h"
#include "fg/gl/currentcontext.h"
#include "fg/window/window.h"
#include "sucrose/window/xlib.h"

TEST(
    GLVertexArraysTest
    , Create
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLVertexArraysTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );
    auto &  glContext = *glContextUnique;

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( glContext );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );
    auto &  glCurrentContext = *glCurrentContextUnique;

    const auto  GL_VERTEX_ARRAYS_UNIQUE = fg::GLVertexArrays::create(
        glCurrentContext
        , 10
    );
    ASSERT_NE( nullptr, GL_VERTEX_ARRAYS_UNIQUE.get() );
    const auto &    GL_VERTEX_ARRAYS = *GL_VERTEX_ARRAYS_UNIQUE;

    ASSERT_EQ( &glContext, &( GL_VERTEX_ARRAYS->glContext ) );

    const auto &    NAMES = GL_VERTEX_ARRAYS->names;
    ASSERT_EQ( 10, NAMES.size() );
    for( const auto & NAME : NAMES ) {
        ASSERT_NE( 0, NAME );
    }

    ASSERT_EQ( &*GL_VERTEX_ARRAYS, GL_VERTEX_ARRAYS->namesDeleter.get() );
}

TEST(
    GLVertexArraysTest
    , Create_failedWhenIllegalSize
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLVertexArraysTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );
    auto &  glContext = *glContextUnique;

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( glContext );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );
    auto &  glCurrentContext = *glCurrentContextUnique;

    try {
        auto    glVertexArraysUnique = fg::GLVertexArrays::create(
            glCurrentContext
            , 0
        );

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}

    try {
        auto    glVertexArraysUnique = fg::GLVertexArrays::create(
            glCurrentContext
            , -1
        );
        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}
}

TEST(
    GLVertexArraysTest
    , Destroy
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLVertexArraysTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );
    auto &  glContext = *glContextUnique;

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( glContext );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );
    auto &  glCurrentContext = *glCurrentContextUnique;

    auto    glVertexArraysUnique = fg::GLVertexArrays::create(
        glCurrentContext
        , 10
    );
    ASSERT_NE( nullptr, glVertexArraysUnique.get() );
    const auto &    GL_VERTEX_ARRAYS = *glVertexArraysUnique;

    const auto  NAME_BACKUP = GL_VERTEX_ARRAYS->names[ 0 ];

    glCurrentContext.glBindVertexArray( NAME_BACKUP );

    glCurrentContext.glEnableVertexAttribArray( 5 );

    glVertexArraysUnique.destroy();

    glCurrentContext.glBindVertexArray( NAME_BACKUP );

    auto    enabled = fg::GLint();
    glCurrentContext.glGetError();
    glCurrentContext.glGetVertexAttribiv(
        5
        , fg::GL_VERTEX_ATTRIB_ARRAY_ENABLED
        , &enabled
    );
    ASSERT_EQ( fg::GL_NO_ERROR, glCurrentContext.glGetError() );
    ASSERT_EQ( 0, enabled );
}

TEST(
    GLVertexArraysTest
    , Destroy_afterCurrentContextDeleted
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLVertexArraysTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );
    auto &  glContext = *glContextUnique;

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( glContext );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );
    auto &  glCurrentContext = *glCurrentContextUnique;

    auto    glVertexArraysUnique = fg::GLVertexArrays::create(
        glCurrentContext
        , 10
    );
    ASSERT_NE( nullptr, glVertexArraysUnique.get() );
    const auto &    GL_VERTEX_ARRAYS = *glVertexArraysUnique;

    const auto  NAME_BACKUP = GL_VERTEX_ARRAYS->names[ 0 ];

    glCurrentContext.glBindVertexArray( NAME_BACKUP );

    glCurrentContext.glEnableVertexAttribArray( 5 );

    glCurrentContextUnique.destroy();

    glVertexArraysUnique.destroy();

    glCurrentContext.glBindVertexArray( NAME_BACKUP );

    auto    enabled = fg::GLint();
    glCurrentContext.glGetError();
    glCurrentContext.glGetVertexAttribiv(
        5
        , fg::GL_VERTEX_ATTRIB_ARRAY_ENABLED
        , &enabled
    );
    ASSERT_EQ( fg::GL_NO_ERROR, glCurrentContext.glGetError() );
    ASSERT_EQ( 0, enabled );
}

TEST(
    GLVertexArraysTest
    , Destroy_afterMadeOtherCurrentContext
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLVertexArraysTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );
    auto &  glContext = *glContextUnique;

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( glContext );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );
    auto &  glCurrentContext = *glCurrentContextUnique;

    auto    glVertexArraysUnique = fg::GLVertexArrays::create(
        glCurrentContext
        , 10
    );
    ASSERT_NE( nullptr, glVertexArraysUnique.get() );
    const auto &    GL_VERTEX_ARRAYS = *glVertexArraysUnique;

    ASSERT_EQ( &glContext, &( GL_VERTEX_ARRAYS->glContext ) );

    const auto  NAME_BACKUP = GL_VERTEX_ARRAYS->names[ 0 ];

    glCurrentContext.glBindVertexArray( NAME_BACKUP );

    glCurrentContext.glEnableVertexAttribArray( 5 );

    glCurrentContextUnique.destroy();

    auto    glContext2Unique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContext2Unique.get() );
    auto &  glContext2 = *glContext2Unique;

    auto    glCurrentContext2Unique = fg::GLCurrentContext::create( glContext2 );
    ASSERT_NE( nullptr, glCurrentContext2Unique.get() );
    auto &  glCurrentContext2 = *glCurrentContext2Unique;

    glCurrentContext2.glBindVertexArray( NAME_BACKUP );

    glCurrentContext2.glEnableVertexAttribArray( 5 );

    glVertexArraysUnique.destroy();

    glCurrentContext2Unique.destroy();

    auto    glCurrentContextAfterDeleteVertexArraysUnique = fg::GLCurrentContext::create( glContext );
    ASSERT_NE( nullptr, glCurrentContextAfterDeleteVertexArraysUnique.get() );
    auto &  glCurrentContextAfterDeleteVertexArrays = *glCurrentContextAfterDeleteVertexArraysUnique;

    glCurrentContextAfterDeleteVertexArrays.glBindVertexArray( NAME_BACKUP );

    auto    enabled = fg::GLint();
    glCurrentContextAfterDeleteVertexArrays.glGetError();
    glCurrentContextAfterDeleteVertexArrays.glGetVertexAttribiv(
        5
        , fg::GL_VERTEX_ATTRIB_ARRAY_ENABLED
        , &enabled
    );
    ASSERT_EQ( fg::GL_NO_ERROR, glCurrentContextAfterDeleteVertexArrays.glGetError() );
    ASSERT_EQ( 0, enabled );

    glCurrentContextAfterDeleteVertexArraysUnique.destroy();

    auto    glCurrentContextAfterDeleteVertexArrays2Unique = fg::GLCurrentContext::create( glContext2 );
    ASSERT_NE( nullptr, glCurrentContextAfterDeleteVertexArrays2Unique.get() );
    auto &  glCurrentContextAfterDeleteVertexArrays2 = *glCurrentContextAfterDeleteVertexArrays2Unique;

    glCurrentContextAfterDeleteVertexArrays2.glBindVertexArray( NAME_BACKUP );

    glCurrentContextAfterDeleteVertexArrays2.glGetError();
    glCurrentContextAfterDeleteVertexArrays2.glGetVertexAttribiv(
        5
        , fg::GL_VERTEX_ATTRIB_ARRAY_ENABLED
        , &enabled
    );
    ASSERT_EQ( fg::GL_NO_ERROR, glCurrentContextAfterDeleteVertexArrays2.glGetError() );
    ASSERT_NE( 0, enabled );
}

TEST(
    GLVertexArraysTest
    , GetSize
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLVertexArraysTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );
    auto &  glContext = *glContextUnique;

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( glContext );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );
    auto &  glCurrentContext = *glCurrentContextUnique;

    auto    glVertexArraysUnique = fg::GLVertexArrays::create(
        glCurrentContext
        , 10
    );
    ASSERT_NE( nullptr, glVertexArraysUnique.get() );
    const auto &    GL_VERTEX_ARRAYS = *glVertexArraysUnique;

    ASSERT_EQ( 10, GL_VERTEX_ARRAYS.getSize() );
}

TEST(
    GLVertexArraysTest
    , GetName
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLVertexArraysTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );
    auto &  glContext = *glContextUnique;

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( glContext );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );
    auto &  glCurrentContext = *glCurrentContextUnique;

    auto    glVertexArraysUnique = fg::GLVertexArrays::create(
        glCurrentContext
        , 10
    );
    ASSERT_NE( nullptr, glVertexArraysUnique.get() );
    const auto &    GL_VERTEX_ARRAYS = *glVertexArraysUnique;

    const auto  NAME = GL_VERTEX_ARRAYS.getName( 0 );
    ASSERT_NE( 0, NAME );
    ASSERT_EQ( GL_VERTEX_ARRAYS->names[ 0 ], NAME );
}

TEST(
    GLVertexArraysTest
    , GetGLContext_const
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLVertexArraysTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );
    auto &  glContext = *glContextUnique;

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( glContext );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );
    auto &  glCurrentContext = *glCurrentContextUnique;

    auto    glVertexArraysUnique = fg::GLVertexArrays::create(
        glCurrentContext
        , 10
    );
    ASSERT_NE( nullptr, glVertexArraysUnique.get() );
    const auto &    GL_VERTEX_ARRAYS = *glVertexArraysUnique;

    ASSERT_EQ( &glContext, &( GL_VERTEX_ARRAYS->getGLContext() ) );
}

TEST(
    GLVertexArraysTest
    , GetGLContext
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLVertexArraysTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );
    auto &  glContext = *glContextUnique;

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( glContext );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );
    auto &  glCurrentContext = *glCurrentContextUnique;

    auto    glVertexArraysUnique = fg::GLVertexArrays::create(
        glCurrentContext
        , 10
    );
    ASSERT_NE( nullptr, glVertexArraysUnique.get() );
    auto &  glVertexArrays = *glVertexArraysUnique;

    ASSERT_EQ( &glContext, &( glVertexArrays->getGLContext() ) );
}
