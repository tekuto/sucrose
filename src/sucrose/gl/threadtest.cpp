﻿#include "fg/util/test.h"
#include "sucrose/gl/thread.h"
#include "fg/core/state/creating.h"
#include "fg/core/state/joiner.h"
#include "fg/window/window.h"
#include "fg/gl/context.h"
#include "fg/common/unique.h"
#include "sucrose/window/xlib.h"
#include "brownsugar/statemanager.h"

struct GLThreadTestData : public fg::UniqueWrapper< GLThreadTestData >
{
    static Unique create(
    )
    {
        return new GLThreadTestData;
    }
};

void glThreadTestProc(
    FgGLThread *
)
{
}

TEST(
    GLThreadTest
    , Constructor
)
{
    auto    i = 10;

    const auto  THREAD = FgGLThread(
        *reinterpret_cast< fg::State<> * >( 20 )
        , *reinterpret_cast< fg::GLContext * >( 30 )
        , glThreadTestProc
        , &i
    );

    ASSERT_EQ( reinterpret_cast< fg::State<> * >( 20 ), &( THREAD.state ) );
    ASSERT_EQ( reinterpret_cast< fg::GLContext * >( 30 ), &( THREAD.glContext ) );
    ASSERT_EQ( glThreadTestProc, THREAD.procPtr );
    ASSERT_EQ( 10, *static_cast< const int * >( THREAD.userDataPtr ) );
}

void executeTestProc(
    fg::GLThread< void, int > & _thread
)
{
    _thread.getData() = 20;
}

TEST(
    GLThreadTest
    , Execute
)
{
    sucrose::initializeXlib();

    auto    stateManagerUnique = brownsugar::StateManager::create();
    ASSERT_NE( nullptr, stateManagerUnique.get() );

    auto &  rootState = stateManagerUnique->getState();

    rootState.enter(
        [](
            fg::CreatingState<> &
        )
        {
        }
    );

    auto    windowUnique = fg::Window::create(
        "GLThreadTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );
    auto &  glContext = *glContextUnique;

    auto    joinerUnique = fg::StateJoiner::create();
    ASSERT_NE( nullptr, joinerUnique.get() );
    auto &  joiner = *joinerUnique;

    auto    i = 10;
    auto    thread = FgGLThread(
        rootState   // 無効ステートでも稼働することを確認
        , glContext
        , reinterpret_cast< FgGLThreadProc >( executeTestProc )
        , &i
    );

    thread.execute( joiner );

    joiner.join();

    ASSERT_EQ( 20, i );
}

TEST(
    GLThreadTest
    , GetState
)
{
    auto    i = 10;

    auto    threadImpl = FgGLThread(
        *reinterpret_cast< fg::State<> * >( 20 )
        , *reinterpret_cast< fg::GLContext * >( 30 )
        , glThreadTestProc
        , &i
    );
    auto &  thread = reinterpret_cast< fg::GLThread< GLThreadTestData, int > & >( threadImpl );

    ASSERT_EQ( reinterpret_cast< fg::State< GLThreadTestData > * >( 20 ), &( thread.getState() ) );
}

TEST(
    GLThreadTest
    , GetState_withoutUserData
)
{
    auto    threadImpl = FgGLThread(
        *reinterpret_cast< fg::State<> * >( 10 )
        , *reinterpret_cast< fg::GLContext * >( 20 )
        , glThreadTestProc
        , nullptr
    );
    auto &  thread = reinterpret_cast< fg::GLThread< GLThreadTestData > & >( threadImpl );

    ASSERT_EQ( reinterpret_cast< fg::State< GLThreadTestData > * >( 10 ), &( thread.getState() ) );
}

TEST(
    GLThreadTest
    , GetGLContext
)
{
    auto    i = 10;

    auto    threadImpl = FgGLThread(
        *reinterpret_cast< fg::State<> * >( 20 )
        , *reinterpret_cast< fg::GLContext * >( 30 )
        , glThreadTestProc
        , &i
    );
    auto &  thread = reinterpret_cast< fg::GLThread< GLThreadTestData, int > & >( threadImpl );

    ASSERT_EQ( reinterpret_cast< fg::GLContext * >( 30 ), &( thread.getGLContext() ) );
}

TEST(
    GLThreadTest
    , GetGLContext_withoutUserData
)
{
    auto    threadImpl = FgGLThread(
        *reinterpret_cast< fg::State<> * >( 10 )
        , *reinterpret_cast< fg::GLContext * >( 20 )
        , glThreadTestProc
        , nullptr
    );
    auto &  thread = reinterpret_cast< fg::GLThread< GLThreadTestData > & >( threadImpl );

    ASSERT_EQ( reinterpret_cast< fg::GLContext * >( 20 ), &( thread.getGLContext() ) );
}

TEST(
    GLThreadTest
    , GetData
)
{
    auto    i = 10;

    auto    threadImpl = FgGLThread(
        *reinterpret_cast< fg::State<> * >( 20 )
        , *reinterpret_cast< fg::GLContext * >( 30 )
        , glThreadTestProc
        , &i
    );
    auto &  thread = reinterpret_cast< fg::GLThread< GLThreadTestData, int > & >( threadImpl );

    ASSERT_EQ( 10, thread.getData() );
}
