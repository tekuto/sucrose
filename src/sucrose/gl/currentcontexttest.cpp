﻿#include "fg/util/test.h"
#include "sucrose/gl/currentcontext.h"
#include "fg/gl/context.h"
#include "fg/window/window.h"
#include "sucrose/window/xlib.h"

#include <thread>

TEST(
    GLCurrentContextTest
    , Create
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLCurrentContextTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );
    auto &  glContext = *glContextUnique;

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( glContext );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );
    const auto &    GL_CURRENT_CONTEXT = *glCurrentContextUnique;

    ASSERT_EQ( &glContext, &reinterpret_cast< const fg::GLContext & >( GL_CURRENT_CONTEXT ) );
}

TEST(
    GLCurrentContextTest
    , Create_failedDuplicate
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLCurrentContextTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );
    auto &  glContext = *glContextUnique;

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( glContext );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );

    try {
        auto    glCurrentContext2Unique = fg::GLCurrentContext::create( glContext );

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}
}

TEST(
    GLCurrentContextTest
    , Create_notDuplicate
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLCurrentContextTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );
    auto &  glContext = *glContextUnique;

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( glContext );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );

    glCurrentContextUnique.destroy();

    auto    glCurrentContext2Unique = fg::GLCurrentContext::create( glContext );
    ASSERT_NE( nullptr, glCurrentContext2Unique.get() );
}

TEST(
    GLCurrentContextTest
    , Create_multiThread
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLCurrentContextTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );
    auto &  glContext = *glContextUnique;

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( glContext );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );

    auto    glCurrentContext2Ptr = static_cast< fg::GLCurrentContext * >( nullptr );

    auto    thread = std::thread(
        [
            &glContext
            , &glCurrentContext2Ptr
        ]
        {
            auto    glCurrentContextUnique = fg::GLCurrentContext::create( glContext );

            glCurrentContext2Ptr = glCurrentContextUnique.get();
        }
    );
    thread.join();

    const auto &    GL_CURRENT_CONTEXT2 = *glCurrentContext2Ptr;

    ASSERT_EQ( &glContext, &reinterpret_cast< const fg::GLContext & >( GL_CURRENT_CONTEXT2 ) );
}

TEST(
    GLCurrentContextTest
    , GetGLContext
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLCurrentContextTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );
    auto &  glContext = *glContextUnique;

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( glContext );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );
    auto &  glCurrentContext = *glCurrentContextUnique;

    ASSERT_EQ( &glContext, &( glCurrentContext->getGLContext() ) );
}
