﻿#include "fg/util/test.h"
#include "sucrose/gl/fragmentshader.h"
#include "fg/gl/gl.h"
#include "fg/gl/context.h"
#include "fg/gl/currentcontext.h"
#include "fg/window/window.h"
#include "sucrose/window/xlib.h"

TEST(
    GLFragmentShaderTest
    , Create
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLFragmentShaderTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );
    auto &  glContext = *glContextUnique;

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( glContext );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );
    auto &  glCurrentContext = *glCurrentContextUnique;

    auto    glFragmentShaderUnique = fg::GLFragmentShader::create( glCurrentContext );
    ASSERT_NE( nullptr, glFragmentShaderUnique.get() );
    const auto &    GL_FRAGMENT_SHADER = *glFragmentShaderUnique;

    ASSERT_EQ( &glContext, &( GL_FRAGMENT_SHADER->glContext ) );

    const auto &    OBJECT = GL_FRAGMENT_SHADER->object;
    ASSERT_EQ( fg::GL_TRUE, glCurrentContext.glIsShader( OBJECT ) );

    auto    shaderType = fg::GLint();
    glCurrentContext.glGetError();
    glCurrentContext.glGetShaderiv(
        OBJECT
        , fg::GL_SHADER_TYPE
        , &shaderType
    );
    ASSERT_EQ( fg::GL_NO_ERROR, glCurrentContext.glGetError() );
    ASSERT_EQ( fg::GL_FRAGMENT_SHADER, shaderType );

    ASSERT_EQ( &*GL_FRAGMENT_SHADER, GL_FRAGMENT_SHADER->objectDeleter.get() );
}

TEST(
    GLFragmentShaderTest
    , Destroy
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLFragmentShaderTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );
    auto &  glContext = *glContextUnique;

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( glContext );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );
    auto &  glCurrentContext = *glCurrentContextUnique;

    auto    glFragmentShaderUnique = fg::GLFragmentShader::create( glCurrentContext );
    ASSERT_NE( nullptr, glFragmentShaderUnique.get() );
    const auto &    GL_FRAGMENT_SHADER = *glFragmentShaderUnique;

    const auto  OBJECT = GL_FRAGMENT_SHADER->object;

    glFragmentShaderUnique.destroy();

    ASSERT_EQ( fg::GL_FALSE, glCurrentContext.glIsShader( OBJECT ) );
}

TEST(
    GLFragmentShaderTest
    , Destroy_afterCurrentContextDeleted
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLFragmentShaderTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );
    auto &  glContext = *glContextUnique;

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( glContext );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );
    auto &  glCurrentContext = *glCurrentContextUnique;

    auto    glFragmentShaderUnique = fg::GLFragmentShader::create( glCurrentContext );
    ASSERT_NE( nullptr, glFragmentShaderUnique.get() );
    const auto &    GL_FRAGMENT_SHADER = *glFragmentShaderUnique;

    const auto  OBJECT = GL_FRAGMENT_SHADER->object;

    glCurrentContextUnique.destroy();

    glFragmentShaderUnique.destroy();

    ASSERT_EQ( fg::GL_FALSE, glCurrentContext.glIsShader( OBJECT ) );
}

TEST(
    GLFragmentShaderTest
    , Destroy_afterMadeOtherCurrentContext
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLFragmentShaderTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );
    auto &  glContext = *glContextUnique;

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( glContext );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );

    auto    glFragmentShaderUnique = fg::GLFragmentShader::create( *glCurrentContextUnique );
    ASSERT_NE( nullptr, glFragmentShaderUnique.get() );
    const auto &    GL_FRAGMENT_SHADER = *glFragmentShaderUnique;

    const auto  OBJECT = GL_FRAGMENT_SHADER->object;

    glCurrentContextUnique.destroy();

    auto    glContext2Unique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContext2Unique.get() );

    auto    glCurrentContext2Unique = fg::GLCurrentContext::create( *glContext2Unique );
    ASSERT_NE( nullptr, glCurrentContext2Unique.get() );

    glFragmentShaderUnique.destroy();

    glCurrentContext2Unique.destroy();

    auto    glCurrentContextAfterDeleteFragmentShaderUnique = fg::GLCurrentContext::create( glContext );
    ASSERT_NE( nullptr, glCurrentContextAfterDeleteFragmentShaderUnique.get() );

    ASSERT_EQ( fg::GL_FALSE, glCurrentContextAfterDeleteFragmentShaderUnique->glIsShader( OBJECT ) );
}

TEST(
    GLFragmentShaderTest
    , GetObject
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLFragmentShaderTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( *glContextUnique );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );

    auto    glFragmentShaderUnique = fg::GLFragmentShader::create( *glCurrentContextUnique );
    ASSERT_NE( nullptr, glFragmentShaderUnique.get() );
    const auto &    GL_FRAGMENT_SHADER = *glFragmentShaderUnique;

    ASSERT_EQ( GL_FRAGMENT_SHADER->object, glFragmentShaderUnique->getObject() );
}
