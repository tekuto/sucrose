﻿#include "fg/util/export.h"
#include "fg/gl/vertexarraybinder.h"
#include "fg/gl/gl.h"
#include "sucrose/gl/currentcontext.h"
#include "sucrose/gl/vertexarrays.h"

#include <stdexcept>

FgGLVertexArrayBinder * fgGLVertexArrayBinderCreate(
    FgGLCurrentContext *        _glCurrentContextPtr
    , const FgGLVertexArrays *  _GL_VERTEX_ARRAYS_PTR
    , FgGLsizei                 _index
)
{
    auto &          glCurrentContext = reinterpret_cast< fg::GLCurrentContext & >( *_glCurrentContextPtr );
    const auto &    GL_VERTEX_ARRAYS = reinterpret_cast< const fg::GLVertexArrays & >( *_GL_VERTEX_ARRAYS_PTR );

    if( &( glCurrentContext->getGLContext() ) != &( GL_VERTEX_ARRAYS->getGLContext() ) ) {
        throw std::runtime_error( "頂点配列生成時とfg::GLContextが異なる" );
    }

    const auto  NAME = GL_VERTEX_ARRAYS.getName( _index );

    glCurrentContext.glGetError();
    glCurrentContext.glBindVertexArray( NAME );
    if( glCurrentContext.glGetError() != fg::GL_NO_ERROR ) {
        throw std::runtime_error( "glBindVertexArray()が失敗" );
    }

    return reinterpret_cast< FgGLVertexArrayBinder * >( _glCurrentContextPtr );
}

void fgGLVertexArrayBinderDestroy(
    FgGLVertexArrayBinder * _implPtr
)
{
    reinterpret_cast< fg::GLCurrentContext * >( _implPtr )->glBindVertexArray( 0 );
}
