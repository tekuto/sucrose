﻿#include "fg/util/test.h"
#include "sucrose/gl/vertexshader.h"
#include "fg/gl/gl.h"
#include "fg/gl/context.h"
#include "fg/gl/currentcontext.h"
#include "fg/window/window.h"
#include "sucrose/window/xlib.h"

TEST(
    GLVertexShaderTest
    , Create
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLVertexShaderTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );
    auto &  glContext = *glContextUnique;

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( glContext );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );
    auto &  glCurrentContext = *glCurrentContextUnique;

    auto    glVertexShaderUnique = fg::GLVertexShader::create( glCurrentContext );
    ASSERT_NE( nullptr, glVertexShaderUnique.get() );
    const auto &    GL_VERTEX_SHADER = *glVertexShaderUnique;

    ASSERT_EQ( &glContext, &( GL_VERTEX_SHADER->glContext ) );

    const auto &    OBJECT = GL_VERTEX_SHADER->object;
    ASSERT_EQ( fg::GL_TRUE, glCurrentContext.glIsShader( OBJECT ) );
    auto    shaderType = fg::GLint();
    glCurrentContext.glGetError();
    glCurrentContext.glGetShaderiv(
        OBJECT
        , fg::GL_SHADER_TYPE
        , &shaderType
    );
    ASSERT_EQ( fg::GL_NO_ERROR, glCurrentContext.glGetError() );
    ASSERT_EQ( fg::GL_VERTEX_SHADER, shaderType );

    ASSERT_EQ( &*GL_VERTEX_SHADER, GL_VERTEX_SHADER->objectDeleter.get() );
}

TEST(
    GLVertexShaderTest
    , Destroy
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLVertexShaderTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( *glContextUnique );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );
    auto &  glCurrentContext = *glCurrentContextUnique;

    auto    glVertexShaderUnique = fg::GLVertexShader::create( glCurrentContext );
    ASSERT_NE( nullptr, glVertexShaderUnique.get() );
    const auto &    GL_VERTEX_SHADER = *glVertexShaderUnique;

    const auto  OBJECT = GL_VERTEX_SHADER->object;

    glVertexShaderUnique.destroy();

    ASSERT_EQ( fg::GL_FALSE, glCurrentContext.glIsShader( OBJECT ) );
}

TEST(
    GLVertexShaderTest
    , Destroy_afterCurrentContextDeleted
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLVertexShaderTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( *glContextUnique );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );
    auto &  glCurrentContext = *glCurrentContextUnique;

    auto    glVertexShaderUnique = fg::GLVertexShader::create( glCurrentContext );
    ASSERT_NE( nullptr, glVertexShaderUnique.get() );
    const auto &    GL_VERTEX_SHADER = *glVertexShaderUnique;

    const auto  OBJECT = GL_VERTEX_SHADER->object;

    glCurrentContextUnique.destroy();

    glVertexShaderUnique.destroy();

    ASSERT_EQ( fg::GL_FALSE, glCurrentContext.glIsShader( OBJECT ) );
}

TEST(
    GLVertexShaderTest
    , Destroy_afterMadeOtherCurrentContext
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLVertexShaderTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );
    auto &  glContext = *glContextUnique;

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( glContext );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );

    auto    glVertexShaderUnique = fg::GLVertexShader::create( *glCurrentContextUnique );
    ASSERT_NE( nullptr, glVertexShaderUnique.get() );
    const auto &    GL_VERTEX_SHADER = *glVertexShaderUnique;

    const auto  OBJECT = GL_VERTEX_SHADER->object;

    glCurrentContextUnique.destroy();

    auto    glContext2Unique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContext2Unique.get() );

    auto    glCurrentContext2Unique = fg::GLCurrentContext::create( *glContext2Unique );
    ASSERT_NE( nullptr, glCurrentContext2Unique.get() );

    glVertexShaderUnique.destroy();

    glCurrentContext2Unique.destroy();

    auto    glCurrentContextAfterDeleteVertexShaderUnique = fg::GLCurrentContext::create( glContext );
    ASSERT_NE( nullptr, glCurrentContextAfterDeleteVertexShaderUnique.get() );

    ASSERT_EQ( fg::GL_FALSE, glCurrentContextAfterDeleteVertexShaderUnique->glIsShader( OBJECT ) );
}

TEST(
    GLVertexShaderTest
    , GetObject
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLVertexShaderTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( *glContextUnique );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );

    auto    glVertexShaderUnique = fg::GLVertexShader::create( *glCurrentContextUnique );
    ASSERT_NE( nullptr, glVertexShaderUnique.get() );
    const auto &    GL_VERTEX_SHADER = *glVertexShaderUnique;

    ASSERT_EQ( GL_VERTEX_SHADER->object, glVertexShaderUnique->getObject() );
}
