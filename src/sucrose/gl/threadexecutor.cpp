﻿#include "fg/util/export.h"
#include "sucrose/gl/threadexecutor.h"
#include "fg/core/state/joiner.h"
#include "sucrose/gl/thread.h"

FgGLThreadExecutor * fgGLThreadExecutorCreateForCreatingState(
    FgCreatingState *   _statePtr
    , FgGLContext *     _glContextPtr
    , FgGLThreadProc    _procPtr
)
{
    return fgGLThreadExecutorCreateForState(
        reinterpret_cast< FgState * >( _statePtr )
        , _glContextPtr
        , _procPtr
    );
}

FgGLThreadExecutor * fgGLThreadExecutorCreateForCreatingStateWithUserData(
    FgCreatingState *   _statePtr
    , FgGLContext *     _glContextPtr
    , FgGLThreadProc    _procPtr
    , void *            _userDataPtr
)
{
    return fgGLThreadExecutorCreateForStateWithUserData(
        reinterpret_cast< FgState * >( _statePtr )
        , _glContextPtr
        , _procPtr
        , _userDataPtr
    );
}

FgGLThreadExecutor * fgGLThreadExecutorCreateForState(
    FgState *           _statePtr
    , FgGLContext *     _glContextPtr
    , FgGLThreadProc    _procPtr
)
{
    return fgGLThreadExecutorCreateForStateWithUserData(
        _statePtr
        , _glContextPtr
        , _procPtr
        , nullptr
    );
}

FgGLThreadExecutor * fgGLThreadExecutorCreateForStateWithUserData(
    FgState *           _statePtr
    , FgGLContext *     _glContextPtr
    , FgGLThreadProc    _procPtr
    , void *            _userDataPtr
)
{
    auto    joinerUnique = fg::StateJoiner::create();

    return new FgGLThreadExecutor{
        FgGLThread(
            reinterpret_cast< fg::State<> & >( *_statePtr )
            , reinterpret_cast< fg::GLContext & >( *_glContextPtr )
            , _procPtr
            , _userDataPtr
        ),
        std::move( joinerUnique ),
    };
}

void fgGLThreadExecutorDestroy(
    FgGLThreadExecutor *    _implPtr
)
{
    delete _implPtr;
}

void fgGLThreadExecutorExecute(
    FgGLThreadExecutor *    _implPtr
)
{
    _implPtr->thread.execute( *( _implPtr->joinerUnique ) );
}
