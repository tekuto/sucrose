﻿#include "fg/util/export.h"
#include "sucrose/gl/program.h"
#include "fg/gl/gl.h"
#include "sucrose/gl/currentcontext.h"
#include "sucrose/gl/currentcontextremaker.h"
#include "sucrose/gl/context.h"

#include <utility>
#include <stdexcept>

namespace {
    FgGLProgram::Object createObject(
        fg::GLCurrentContext &  _glCurrentContext
    )
    {
        auto    object = _glCurrentContext.glCreateProgram();
        if( object == 0 ) {
            throw std::runtime_error( "glCreateProgram()が失敗" );
        }

        return std::move( object );
    }
}

FgGLProgram * fgGLProgramCreate(
    FgGLCurrentContext *    _glCurrentContextPtr
)
{
    return new FgGLProgram( reinterpret_cast< fg::GLCurrentContext & >( *_glCurrentContextPtr ) );
}

void fgGLProgramDestroy(
    FgGLProgram *   _implPtr
)
{
    delete _implPtr;
}

FgGLuint fgGLProgramGetObject(
    const FgGLProgram * _IMPL_PTR
)
{
    return _IMPL_PTR->object;
}

void FgGLProgram::DeleteObject::operator()(
    FgGLProgram *   _glProgramPtr
) const
{
    auto    glCurrentContextRemaker = sucrose::GLCurrentContextRemaker( FgGLContext::releaseGLCurrent() );

    auto    glCurrentContextUnique =  fg::GLCurrentContext::create( _glProgramPtr->glContext );

    glCurrentContextUnique->glDeleteProgram( _glProgramPtr->object );
}

FgGLProgram::FgGLProgram(
    fg::GLCurrentContext &  _glCurrentContext
)
    : glContext( _glCurrentContext->getGLContext() )
    , object( createObject( _glCurrentContext ) )
    , objectDeleter( this )
{
}
