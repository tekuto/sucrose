﻿#include "fg/util/test.h"
#include "sucrose/gl/textures.h"
#include "fg/gl/gl.h"
#include "fg/gl/context.h"
#include "fg/gl/currentcontext.h"
#include "fg/window/window.h"
#include "sucrose/window/xlib.h"

#include <array>

TEST(
    GLTexturesTest
    , Create
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLTexturesTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );
    auto &  glContext = *glContextUnique;

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( glContext );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );

    const auto  GL_TEXTURES_UNIQUE = fg::GLTextures::create(
        *glCurrentContextUnique
        , 10
    );
    ASSERT_NE( nullptr, GL_TEXTURES_UNIQUE.get() );
    const auto &    GL_TEXTURES = *GL_TEXTURES_UNIQUE;

    ASSERT_EQ( &glContext, &( GL_TEXTURES->glContext ) );

    const auto &    NAMES = GL_TEXTURES->names;
    ASSERT_EQ( 10, NAMES.size() );
    for( const auto & NAME : NAMES ) {
        ASSERT_NE( 0, NAME );
    }

    ASSERT_EQ( &*GL_TEXTURES, GL_TEXTURES->namesDeleter.get() );
}

TEST(
    GLTexturesTest
    , Create_failedWhenIllegalSize
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLTexturesTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( *glContextUnique );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );
    auto &  glCurrentContext = *glCurrentContextUnique;

    try {
        auto    glTexturesUnique = fg::GLTextures::create(
            glCurrentContext
            , 0
        );

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}

    try {
        auto    glTexturesUnique = fg::GLTextures::create(
            glCurrentContext
            , -1
        );

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}
}

TEST(
    GLTexturesTest
    , Destroy
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLTexturesTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( *glContextUnique );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );
    auto &  glCurrentContext = *glCurrentContextUnique;

    auto    glTexturesUnique = fg::GLTextures::create(
        glCurrentContext
        , 10
    );
    ASSERT_NE( nullptr, glTexturesUnique.get() );
    const auto &    GL_TEXTURES = *glTexturesUnique;

    const auto  NAME_BACKUP = GL_TEXTURES->names[ 0 ];

    glCurrentContext.glBindTexture(
        fg::GL_TEXTURE_2D
        , NAME_BACKUP
    );

    const auto  DUMMY_TEXIMAGE = std::array< unsigned char, 3 >{
        10,
        20,
        30,
    };
    glCurrentContext.glTexImage2D(
        fg::GL_TEXTURE_2D
        , 0
        , fg::GL_RGB
        , 1
        , 1
        , 0
        , fg::GL_RGB
        , fg::GL_UNSIGNED_BYTE
        , DUMMY_TEXIMAGE.data()
    );

    glTexturesUnique.destroy();

    glCurrentContext.glBindTexture(
        fg::GL_TEXTURE_2D
        , NAME_BACKUP
    );

    auto    texImage = std::array< unsigned char, 3 >();
    glCurrentContext.glGetTexImage(
        fg::GL_TEXTURE_2D
        , 0
        , fg::GL_RGB
        , fg::GL_UNSIGNED_BYTE
        , texImage.data()
    );
    ASSERT_NE( 10, texImage[ 0 ] );
    ASSERT_NE( 20, texImage[ 1 ] );
    ASSERT_NE( 30, texImage[ 2 ] );
}

TEST(
    GLTexturesTest
    , Destroy_afterCurrentContextDeleted
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLTexturesTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );
    auto &  glContext = *glContextUnique;

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( glContext );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );
    auto &  glCurrentContext = *glCurrentContextUnique;

    auto    glTexturesUnique = fg::GLTextures::create(
        glCurrentContext
        , 10
    );
    ASSERT_NE( nullptr, glTexturesUnique.get() );
    const auto &    GL_TEXTURES = *glTexturesUnique;

    const auto  NAME_BACKUP = GL_TEXTURES->names[ 0 ];

    glCurrentContext.glBindTexture(
        fg::GL_TEXTURE_2D
        , NAME_BACKUP
    );

    const auto  DUMMY_TEXIMAGE = std::array< unsigned char, 3 >{
        10,
        20,
        30,
    };
    glCurrentContext.glTexImage2D(
        fg::GL_TEXTURE_2D
        , 0
        , fg::GL_RGB
        , 1
        , 1
        , 0
        , fg::GL_RGB
        , fg::GL_UNSIGNED_BYTE
        , DUMMY_TEXIMAGE.data()
    );

    glCurrentContextUnique.destroy();

    glTexturesUnique.destroy();

    auto    glCurrentContextAfterDeleteTexturesUnique = fg::GLCurrentContext::create( glContext );
    ASSERT_NE( nullptr, glCurrentContextAfterDeleteTexturesUnique.get() );

    glCurrentContextAfterDeleteTexturesUnique->glBindTexture(
        fg::GL_TEXTURE_2D
        , NAME_BACKUP
    );

    auto    texImage = std::array< unsigned char, 3 >();
    glCurrentContextAfterDeleteTexturesUnique->glGetTexImage(
        fg::GL_TEXTURE_2D
        , 0
        , fg::GL_RGB
        , fg::GL_UNSIGNED_BYTE
        , texImage.data()
    );
    ASSERT_NE( 10, texImage[ 0 ] );
    ASSERT_NE( 20, texImage[ 1 ] );
    ASSERT_NE( 30, texImage[ 2 ] );
}

TEST(
    GLTexturesTest
    , Destroy_afterMadeOtherCurrentContext
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLTexturesTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );
    auto &  glContext = *glContextUnique;

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( glContext );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );
    auto &  glCurrentContext = *glCurrentContextUnique;

    auto    glTexturesUnique = fg::GLTextures::create(
        glCurrentContext
        , 10
    );
    ASSERT_NE( nullptr, glTexturesUnique.get() );
    const auto &    GL_TEXTURES = *glTexturesUnique;

    ASSERT_EQ( &glContext, &( GL_TEXTURES->glContext ) );

    const auto  NAME_BACKUP = GL_TEXTURES->names[ 0 ];

    glCurrentContext.glBindTexture(
        fg::GL_TEXTURE_2D
        , NAME_BACKUP
    );

    const auto  DUMMY_TEXIMAGE = std::array< unsigned char, 3 >{
        10,
        20,
        30,
    };
    glCurrentContext.glTexImage2D(
        fg::GL_TEXTURE_2D
        , 0
        , fg::GL_RGB
        , 1
        , 1
        , 0
        , fg::GL_RGB
        , fg::GL_UNSIGNED_BYTE
        , DUMMY_TEXIMAGE.data()
    );

    glCurrentContextUnique.destroy();

    auto    glContext2Unique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContext2Unique.get() );
    auto &  glContext2 = *glContext2Unique;

    auto    glCurrentContext2Unique = fg::GLCurrentContext::create( glContext2 );
    ASSERT_NE( nullptr, glCurrentContext2Unique.get() );

    glCurrentContext2Unique->glBindTexture(
        fg::GL_TEXTURE_2D
        , NAME_BACKUP
    );

    const auto  DUMMY_TEXIMAGE2 = std::array< unsigned char, 3 >{
        40,
        50,
        60,
    };
    glCurrentContext2Unique->glTexImage2D(
        fg::GL_TEXTURE_2D
        , 0
        , fg::GL_RGB
        , 1
        , 1
        , 0
        , fg::GL_RGB
        , fg::GL_UNSIGNED_BYTE
        , DUMMY_TEXIMAGE2.data()
    );

    glTexturesUnique.destroy();

    glCurrentContext2Unique.destroy();

    auto    glCurrentContextAfterDeleteTexturesUnique = fg::GLCurrentContext::create( glContext );
    ASSERT_NE( nullptr, glCurrentContextAfterDeleteTexturesUnique.get() );

    glCurrentContextAfterDeleteTexturesUnique->glBindTexture(
        fg::GL_TEXTURE_2D
        , NAME_BACKUP
    );

    auto    texImage = std::array< unsigned char, 3 >();
    glCurrentContextAfterDeleteTexturesUnique->glGetTexImage(
        fg::GL_TEXTURE_2D
        , 0
        , fg::GL_RGB
        , fg::GL_UNSIGNED_BYTE
        , texImage.data()
    );
    ASSERT_NE( 10, texImage[ 0 ] );
    ASSERT_NE( 20, texImage[ 1 ] );
    ASSERT_NE( 30, texImage[ 2 ] );

    glCurrentContextAfterDeleteTexturesUnique.destroy();

    auto    glCurrentContextAfterDeleteTextures2Unique = fg::GLCurrentContext::create( glContext2 );
    ASSERT_NE( nullptr, glCurrentContextAfterDeleteTextures2Unique.get() );

    glCurrentContextAfterDeleteTextures2Unique->glBindTexture(
        fg::GL_TEXTURE_2D
        , NAME_BACKUP
    );

    auto    texImage2 = std::array< unsigned char, 3 >();
    glCurrentContextAfterDeleteTextures2Unique->glGetError();
    glCurrentContextAfterDeleteTextures2Unique->glGetTexImage(
        fg::GL_TEXTURE_2D
        , 0
        , fg::GL_RGB
        , fg::GL_UNSIGNED_BYTE
        , texImage2.data()
    );
    ASSERT_EQ( fg::GL_NO_ERROR, glCurrentContextAfterDeleteTextures2Unique->glGetError() );
    ASSERT_EQ( 40, texImage2[ 0 ] );
    ASSERT_EQ( 50, texImage2[ 1 ] );
    ASSERT_EQ( 60, texImage2[ 2 ] );
}

TEST(
    GLTexturesTest
    , GetSize
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLTexturesTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( *glContextUnique );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );

    auto    glTexturesUnique = fg::GLTextures::create(
        *glCurrentContextUnique
        , 10
    );
    ASSERT_NE( nullptr, glTexturesUnique.get() );

    ASSERT_EQ( 10, glTexturesUnique->getSize() );
}

TEST(
    GLTexturesTest
    , GetName
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLTexturesTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( *glContextUnique );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );

    auto    glTexturesUnique = fg::GLTextures::create(
        *glCurrentContextUnique
        , 10
    );
    ASSERT_NE( nullptr, glTexturesUnique.get() );
    const auto &    GL_TEXTURES = *glTexturesUnique;

    const auto  NAME = GL_TEXTURES.getName( 0 );
    ASSERT_NE( 0, NAME );
    ASSERT_EQ( GL_TEXTURES->names[ 0 ], NAME );
}

TEST(
    GLTexturesTest
    , GetGLContext_const
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLTexturesTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );
    auto &  glContext = *glContextUnique;

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( glContext );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );

    auto    glTexturesUnique = fg::GLTextures::create(
        *glCurrentContextUnique
        , 10
    );
    ASSERT_NE( nullptr, glTexturesUnique.get() );
    const auto &    GL_TEXTURES = *glTexturesUnique;

    ASSERT_EQ( &glContext, &( GL_TEXTURES->getGLContext() ) );
}

TEST(
    GLTexturesTest
    , GetGLContext
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLTexturesTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );
    auto &  glContext = *glContextUnique;

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( glContext );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );

    auto    glTexturesUnique = fg::GLTextures::create(
        *glCurrentContextUnique
        , 10
    );
    ASSERT_NE( nullptr, glTexturesUnique.get() );
    auto &  glTextures = *glTexturesUnique;

    ASSERT_EQ( &glContext, &( glTextures->getGLContext() ) );
}
