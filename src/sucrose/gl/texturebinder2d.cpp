﻿#include "fg/util/export.h"
#include "fg/gl/texturebinder2d.h"
#include "fg/gl/gl.h"
#include "sucrose/gl/currentcontext.h"
#include "sucrose/gl/textures.h"

#include <stdexcept>

FgGLTextureBinder2D * fgGLTextureBinder2DCreate(
    FgGLCurrentContext *    _glCurrentContextPtr
    , const FgGLTextures *  _GL_TEXTURES_PTR
    , FgGLsizei             _index
)
{
    auto &          glCurrentContext = reinterpret_cast< fg::GLCurrentContext & >( *_glCurrentContextPtr );
    const auto &    GL_TEXTURES = reinterpret_cast< const fg::GLTextures & >( *_GL_TEXTURES_PTR );

    if( &( glCurrentContext->getGLContext() ) != &( GL_TEXTURES->getGLContext() ) ) {
        throw std::runtime_error( "テクスチャ生成時とfg::GLContextが異なる" );
    }

    const auto  NAME = GL_TEXTURES.getName( _index );

    glCurrentContext.glGetError();
    glCurrentContext.glBindTexture(
        fg::GL_TEXTURE_2D
        , NAME
    );
    if( glCurrentContext.glGetError() != fg::GL_NO_ERROR ) {
        throw std::runtime_error( "glBindTexture()が失敗" );
    }

    return reinterpret_cast< FgGLTextureBinder2D * >( _glCurrentContextPtr );
}

void fgGLTextureBinder2DDestroy(
    FgGLTextureBinder2D *   _implPtr
)
{
    reinterpret_cast< fg::GLCurrentContext * >( _implPtr )->glBindTexture(
        fg::GL_TEXTURE_2D
        , 0
    );
}
