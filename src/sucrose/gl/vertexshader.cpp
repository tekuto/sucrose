﻿#include "fg/util/export.h"
#include "sucrose/gl/vertexshader.h"
#include "fg/gl/gl.h"
#include "sucrose/gl/currentcontext.h"
#include "sucrose/gl/currentcontextremaker.h"
#include "sucrose/gl/context.h"

#include <utility>
#include <stdexcept>

namespace {
    FgGLVertexShader::Object createObject(
        fg::GLCurrentContext &  _glCurrentContext
    )
    {
        _glCurrentContext.glGetError();

        auto    object = _glCurrentContext.glCreateShader( fg::GL_VERTEX_SHADER );
        if( _glCurrentContext.glGetError() != fg::GL_NO_ERROR ) {
            throw std::runtime_error( "glCreateShader()が失敗" );
        }

        return std::move( object );
    }
}

FgGLVertexShader * fgGLVertexShaderCreate(
    FgGLCurrentContext *    _glCurrentContextPtr
)
{
    return new FgGLVertexShader( reinterpret_cast< fg::GLCurrentContext & >( *_glCurrentContextPtr ) );
}

void fgGLVertexShaderDestroy(
    FgGLVertexShader *  _implPtr
)
{
    delete _implPtr;
}

FgGLuint fgGLVertexShaderGetObject(
    const FgGLVertexShader *    _IMPL_PTR
)
{
    return _IMPL_PTR->object;
}

void FgGLVertexShader::DeleteObject::operator()(
    FgGLVertexShader *  _glVertexShaderPtr
) const
{
    auto    glCurrentContextRemaker = sucrose::GLCurrentContextRemaker( FgGLContext::releaseGLCurrent() );

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( _glVertexShaderPtr->glContext );

    glCurrentContextUnique->glDeleteShader( _glVertexShaderPtr->object );
}

FgGLVertexShader::FgGLVertexShader(
    fg::GLCurrentContext &  _glCurrentContext
)
    : glContext( _glCurrentContext->getGLContext() )
    , object( createObject( _glCurrentContext ) )
    , objectDeleter( this )
{
}
