﻿#include "fg/util/export.h"
#include "sucrose/gl/fragmentshader.h"
#include "fg/gl/gl.h"
#include "sucrose/gl/currentcontext.h"
#include "sucrose/gl/currentcontextremaker.h"
#include "sucrose/gl/context.h"

#include <utility>
#include <stdexcept>

namespace {
    FgGLFragmentShader::Object createObject(
        fg::GLCurrentContext &  _glCurrentContext
    )
    {
        _glCurrentContext.glGetError();

        auto    object = _glCurrentContext.glCreateShader( fg::GL_FRAGMENT_SHADER );
        if( _glCurrentContext.glGetError() != fg::GL_NO_ERROR ) {
            throw std::runtime_error( "glCreateShader()が失敗" );
        }

        return std::move( object );
    }
}

FgGLFragmentShader * fgGLFragmentShaderCreate(
    FgGLCurrentContext *    _glCurrentContextPtr
)
{
    return new FgGLFragmentShader( reinterpret_cast< fg::GLCurrentContext & >( *_glCurrentContextPtr ) );
}

void fgGLFragmentShaderDestroy(
    FgGLFragmentShader *    _implPtr
)
{
    delete _implPtr;
}

FgGLuint fgGLFragmentShaderGetObject(
    const FgGLFragmentShader *  _IMPL_PTR
)
{
    return _IMPL_PTR->object;
}

void FgGLFragmentShader::DeleteObject::operator()(
    FgGLFragmentShader *    _glFragmentShaderPtr
) const
{
    auto    glCurrentContextRemaker = sucrose::GLCurrentContextRemaker( FgGLContext::releaseGLCurrent() );

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( _glFragmentShaderPtr->glContext );

    glCurrentContextUnique->glDeleteShader( _glFragmentShaderPtr->object );
}

FgGLFragmentShader::FgGLFragmentShader(
    fg::GLCurrentContext &  _glCurrentContext
)
    : glContext( _glCurrentContext->getGLContext() )
    , object( createObject( _glCurrentContext ) )
    , objectDeleter( this )
{
}
