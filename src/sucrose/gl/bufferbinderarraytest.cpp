﻿#include "fg/util/test.h"
#include "fg/gl/bufferbinderarray.h"
#include "fg/gl/gl.h"
#include "fg/gl/context.h"
#include "fg/gl/currentcontext.h"
#include "fg/gl/buffers.h"
#include "fg/window/window.h"
#include "sucrose/gl/buffers.h"
#include "sucrose/window/xlib.h"

TEST(
    GLBufferBinderArrayTest
    , Create
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLBufferBinderArrayTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( *glContextUnique );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );
    auto &  glCurrentContext = *glCurrentContextUnique;

    auto    glBuffersUnique = fg::GLBuffers::create(
        glCurrentContext
        , 10
    );
    ASSERT_NE( nullptr, glBuffersUnique.get() );
    const auto &    GL_BUFFERS = *glBuffersUnique;

    auto    glBufferBinderArrayUnique = fg::GLBufferBinderArray::create(
        glCurrentContext
        , GL_BUFFERS
        , 5
    );
    ASSERT_NE( nullptr, glBufferBinderArrayUnique.get() );
    const auto &    GL_BUFFER_BINDER_ARRAY = *glBufferBinderArrayUnique;

    ASSERT_EQ( &glCurrentContext, &reinterpret_cast< const fg::GLCurrentContext & >( GL_BUFFER_BINDER_ARRAY ) );

    glCurrentContext.glGetError();
    auto    name = fg::GLuint();
    glCurrentContext.glGetIntegerv(
        fg::GL_ARRAY_BUFFER_BINDING
        , &reinterpret_cast< fg::GLint & >( name )
    );
    ASSERT_EQ( fg::GL_NO_ERROR, glCurrentContext.glGetError() );
    ASSERT_NE( 0, name );
    ASSERT_EQ( GL_BUFFERS->names[ 5 ], name );
}

TEST(
    GLBufferBinderArrayTest
    , Create_failedWhenOtherContext
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLBufferBinderArrayTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( *glContextUnique );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );

    auto    glBuffersUnique = fg::GLBuffers::create(
        *glCurrentContextUnique
        , 10
    );
    ASSERT_NE( nullptr, glBuffersUnique.get() );
    const auto &    GL_BUFFERS = *glBuffersUnique;

    glCurrentContextUnique.destroy();

    auto    glContext2Unique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContext2Unique.get() );

    auto    glCurrentContext2Unique = fg::GLCurrentContext::create( *glContext2Unique );
    ASSERT_NE( nullptr, glCurrentContext2Unique.get() );

    try {
        auto    glBufferBinderArrayUnique = fg::GLBufferBinderArray::create(
            *glCurrentContext2Unique
            , GL_BUFFERS
            , 5
        );

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}
}

TEST(
    GLBufferBinderArrayTest
    , Destroy
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLBufferBinderArrayTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( *glContextUnique );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );
    auto &  glCurrentContext = *glCurrentContextUnique;

    auto    glBuffersUnique = fg::GLBuffers::create(
        glCurrentContext
        , 10
    );
    ASSERT_NE( nullptr, glBuffersUnique.get() );
    const auto &    GL_BUFFERS = *glBuffersUnique;

    auto    glBufferBinderArrayUnique = fg::GLBufferBinderArray::create(
        glCurrentContext
        , GL_BUFFERS
        , 5
    );
    ASSERT_NE( nullptr, glBufferBinderArrayUnique.get() );

    glBufferBinderArrayUnique.destroy();

    glCurrentContext.glGetError();
    auto    name = fg::GLuint();
    glCurrentContext.glGetIntegerv(
        fg::GL_ARRAY_BUFFER_BINDING
        , &reinterpret_cast< fg::GLint & >( name )
    );
    ASSERT_EQ( fg::GL_NO_ERROR, glCurrentContext.glGetError() );
    ASSERT_EQ( 0, name );
}
