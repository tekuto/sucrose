﻿#include "fg/util/test.h"
#include "sucrose/gl/context.h"
#include "fg/gl/gl.h"
#include "fg/gl/contextargs.h"
#include "fg/gl/currentcontext.h"
#include "fg/gl/textures.h"
#include "fg/gl/texturebinder2d.h"
#include "fg/window/window.h"
#include "sucrose/window/xlib.h"
#include "sucrose/window/window.h"

#include <memory>
#include <thread>
#include <GL/glx.h>

#undef  GL_TRUE

TEST(
    GLContextTest
    , Create
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLContextTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto &  xDisplay = window->getXDisplayForPaint();

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );
    auto &  glContext = *glContextUnique;

    ASSERT_EQ( &window, &( glContext->window ) );

    auto &  xVisualInfoUnique = glContext->xVisualInfoUnique;
    ASSERT_NE( nullptr, xVisualInfoUnique.get() );
    auto &  xVisualInfo = *xVisualInfoUnique;

    const auto &    GLX_CONTEXT_MANAGER_UNIQUE = glContext->glxContextManagerUnique;
    ASSERT_NE( nullptr, GLX_CONTEXT_MANAGER_UNIQUE.get() );
    const auto &    GLX_CONTEXT_MANAGER = *GLX_CONTEXT_MANAGER_UNIQUE;
    ASSERT_EQ( &xDisplay, &( GLX_CONTEXT_MANAGER.xDisplay ) );
    ASSERT_NE( nullptr, GLX_CONTEXT_MANAGER.glxContext );
    ASSERT_EQ( &GLX_CONTEXT_MANAGER, GLX_CONTEXT_MANAGER.glxContextDestroyer.get() );

    auto    redSize = int( 10 );
    ASSERT_EQ(
        0
        , glXGetConfig(
            &xDisplay
            , &xVisualInfo
            , GLX_RED_SIZE
            , &redSize
        )
    );
    ASSERT_EQ( 8, redSize );

    auto    greenSize = int( 20 );
    ASSERT_EQ(
        0
        , glXGetConfig(
            &xDisplay
            , &xVisualInfo
            , GLX_GREEN_SIZE
            , &greenSize
        )
    );
    ASSERT_EQ( 8, greenSize );

    auto    blueSize = int( 30 );
    ASSERT_EQ(
        0
        , glXGetConfig(
            &xDisplay
            , &xVisualInfo
            , GLX_BLUE_SIZE
            , &blueSize
        )
    );
    ASSERT_EQ( 8, blueSize );

    auto    alphaSize = int( 40 );
    ASSERT_EQ(
        0
        , glXGetConfig(
            &xDisplay
            , &xVisualInfo
            , GLX_ALPHA_SIZE
            , &alphaSize
        )
    );
    ASSERT_EQ( 8, alphaSize );

    auto    depthSize = int( 50 );
    ASSERT_EQ(
        0
        , glXGetConfig(
            &xDisplay
            , &xVisualInfo
            , GLX_DEPTH_SIZE
            , &depthSize
        )
    );
    ASSERT_EQ( 24, depthSize );

    auto    stencilSize = int( 60 );
    ASSERT_EQ(
        0
        , glXGetConfig(
            &xDisplay
            , &xVisualInfo
            , GLX_STENCIL_SIZE
            , &stencilSize
        )
    );
    ASSERT_EQ( 8, stencilSize );
}

TEST(
    GLContextTest
    , CreateWithShareList
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLContextTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto &  xDisplay = window->getXDisplayForPaint();

    auto    otherContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, otherContextUnique.get() );
    auto &  otherContext = *otherContextUnique;

    auto    glContextUnique = fg::GLContext::create(
        window
        , otherContext
    );
    ASSERT_NE( nullptr, glContextUnique.get() );
    auto &  glContext = *glContextUnique;

    auto    glTexturesUnique = fg::GLTextures::Unique();
    {
        auto    glCurrentContextUnique = fg::GLCurrentContext::create( otherContext );
        ASSERT_NE( nullptr, glCurrentContextUnique.get() );
        auto &  glCurrentContext = *glCurrentContextUnique;

        glTexturesUnique = fg::GLTextures::create(
            glCurrentContext
            , 10
        );
        ASSERT_NE( nullptr, glTexturesUnique.get() );

        const auto  GL_TEXTURE_BINDER_2D_UNIQUE = fg::GLTextureBinder2D::create(
            glCurrentContext
            , *glTexturesUnique
            , 0
        );
        ASSERT_NE( nullptr, GL_TEXTURE_BINDER_2D_UNIQUE.get() );
    }

    ASSERT_EQ( &window, &( glContext->window ) );

    auto &  xVisualInfoUnique = glContext->xVisualInfoUnique;
    ASSERT_NE( nullptr, xVisualInfoUnique.get() );
    auto &  xVisualInfo = *xVisualInfoUnique;

    const auto &    GLX_CONTEXT_MANAGER_UNIQUE = glContext->glxContextManagerUnique;
    ASSERT_NE( nullptr, GLX_CONTEXT_MANAGER_UNIQUE.get() );
    const auto &    GLX_CONTEXT_MANAGER = *GLX_CONTEXT_MANAGER_UNIQUE;
    ASSERT_EQ( &xDisplay, &( GLX_CONTEXT_MANAGER.xDisplay ) );
    ASSERT_NE( nullptr, GLX_CONTEXT_MANAGER.glxContext );
    ASSERT_EQ( &GLX_CONTEXT_MANAGER, GLX_CONTEXT_MANAGER.glxContextDestroyer.get() );

    auto    redSize = int( 10 );
    ASSERT_EQ(
        0
        , glXGetConfig(
            &xDisplay
            , &xVisualInfo
            , GLX_RED_SIZE
            , &redSize
        )
    );
    ASSERT_EQ( 8, redSize );

    auto    greenSize = int( 20 );
    ASSERT_EQ(
        0
        , glXGetConfig(
            &xDisplay
            , &xVisualInfo
            , GLX_GREEN_SIZE
            , &greenSize
        )
    );
    ASSERT_EQ( 8, greenSize );

    auto    blueSize = int( 30 );
    ASSERT_EQ(
        0
        , glXGetConfig(
            &xDisplay
            , &xVisualInfo
            , GLX_BLUE_SIZE
            , &blueSize
        )
    );
    ASSERT_EQ( 8, blueSize );

    auto    alphaSize = int( 40 );
    ASSERT_EQ(
        0
        , glXGetConfig(
            &xDisplay
            , &xVisualInfo
            , GLX_ALPHA_SIZE
            , &alphaSize
        )
    );
    ASSERT_EQ( 8, alphaSize );

    auto    depthSize = int( 50 );
    ASSERT_EQ(
        0
        , glXGetConfig(
            &xDisplay
            , &xVisualInfo
            , GLX_DEPTH_SIZE
            , &depthSize
        )
    );
    ASSERT_EQ( 24, depthSize );

    auto    stencilSize = int( 60 );
    ASSERT_EQ(
        0
        , glXGetConfig(
            &xDisplay
            , &xVisualInfo
            , GLX_STENCIL_SIZE
            , &stencilSize
        )
    );
    ASSERT_EQ( 8, stencilSize );

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( glContext );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );

    ASSERT_EQ( fg::GL_TRUE, glCurrentContextUnique->glIsTexture( glTexturesUnique->getName( 0 ) ) );
}

TEST(
    GLContextTest
    , CreateWithArgs
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLContextTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto &  xDisplay = window->getXDisplayForPaint();

    auto    argsUnique = fg::GLContextArgs::createBlank();
    ASSERT_NE( nullptr, argsUnique.get() );

    argsUnique->setRedSize( 8 );
    argsUnique->setGreenSize( 8 );
    argsUnique->setBlueSize( 8 );
    argsUnique->setAlphaSize( 0 );
    argsUnique->setDepthSize( 0 );
    argsUnique->setStencilSize( 0 );

    auto    glContextUnique = fg::GLContext::create(
        window
        , *argsUnique
    );
    ASSERT_NE( nullptr, glContextUnique.get() );
    auto &  glContext = *glContextUnique;

    ASSERT_EQ( &window, &( glContext->window ) );

    auto &  xVisualInfoUnique = glContext->xVisualInfoUnique;
    ASSERT_NE( nullptr, xVisualInfoUnique.get() );
    auto &  xVisualInfo = *xVisualInfoUnique;

    const auto &    GLX_CONTEXT_MANAGER_UNIQUE = glContext->glxContextManagerUnique;
    ASSERT_NE( nullptr, GLX_CONTEXT_MANAGER_UNIQUE.get() );
    const auto &    GLX_CONTEXT_MANAGER = *GLX_CONTEXT_MANAGER_UNIQUE;
    ASSERT_EQ( &xDisplay, &( GLX_CONTEXT_MANAGER.xDisplay ) );
    ASSERT_NE( nullptr, GLX_CONTEXT_MANAGER.glxContext );
    ASSERT_EQ( &GLX_CONTEXT_MANAGER, GLX_CONTEXT_MANAGER.glxContextDestroyer.get() );

    auto    redSize = int( 10 );
    ASSERT_EQ(
        0
        , glXGetConfig(
            &xDisplay
            , &xVisualInfo
            , GLX_RED_SIZE
            , &redSize
        )
    );
    ASSERT_EQ( 8, redSize );

    auto    greenSize = int( 20 );
    ASSERT_EQ(
        0
        , glXGetConfig(
            &xDisplay
            , &xVisualInfo
            , GLX_GREEN_SIZE
            , &greenSize
        )
    );
    ASSERT_EQ( 8, greenSize );

    auto    blueSize = int( 30 );
    ASSERT_EQ(
        0
        , glXGetConfig(
            &xDisplay
            , &xVisualInfo
            , GLX_BLUE_SIZE
            , &blueSize
        )
    );
    ASSERT_EQ( 8, blueSize );

    auto    alphaSize = int( 40 );
    ASSERT_EQ(
        0
        , glXGetConfig(
            &xDisplay
            , &xVisualInfo
            , GLX_ALPHA_SIZE
            , &alphaSize
        )
    );
    ASSERT_EQ( 0, alphaSize );

    auto    depthSize = int( 50 );
    ASSERT_EQ(
        0
        , glXGetConfig(
            &xDisplay
            , &xVisualInfo
            , GLX_DEPTH_SIZE
            , &depthSize
        )
    );
    ASSERT_EQ( 0, depthSize );

    auto    stencilSize = int( 60 );
    ASSERT_EQ(
        0
        , glXGetConfig(
            &xDisplay
            , &xVisualInfo
            , GLX_STENCIL_SIZE
            , &stencilSize
        )
    );
    ASSERT_EQ( 0, stencilSize );
}

TEST(
    GLContextTest
    , CreateWithShareListAndArgs
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLContextTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto &  xDisplay = window->getXDisplayForPaint();

    auto    otherContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, otherContextUnique.get() );
    auto &  otherContext = *otherContextUnique;

    auto    argsUnique = fg::GLContextArgs::createBlank();
    ASSERT_NE( nullptr, argsUnique.get() );

    argsUnique->setRedSize( 8 );
    argsUnique->setGreenSize( 8 );
    argsUnique->setBlueSize( 8 );
    argsUnique->setAlphaSize( 0 );
    argsUnique->setDepthSize( 0 );
    argsUnique->setStencilSize( 0 );

    auto    glContextUnique = fg::GLContext::create(
        window
        , otherContext
        , *argsUnique
    );
    ASSERT_NE( nullptr, glContextUnique.get() );
    auto &  glContext = *glContextUnique;

    auto    glTexturesUnique = fg::GLTextures::Unique();
    {
        auto    glCurrentContextUnique = fg::GLCurrentContext::create( otherContext );
        ASSERT_NE( nullptr, glCurrentContextUnique.get() );
        auto &  glCurrentContext = *glCurrentContextUnique;

        glTexturesUnique = fg::GLTextures::create(
            glCurrentContext
            , 10
        );
        ASSERT_NE( nullptr, glTexturesUnique.get() );

        const auto  GL_TEXTURE_BINDER_2D_UNIQUE = fg::GLTextureBinder2D::create(
            glCurrentContext
            , *glTexturesUnique
            , 0
        );
        ASSERT_NE( nullptr, GL_TEXTURE_BINDER_2D_UNIQUE.get() );
    }

    ASSERT_EQ( &window, &( glContext->window ) );

    auto &  xVisualInfoUnique = glContext->xVisualInfoUnique;
    ASSERT_NE( nullptr, xVisualInfoUnique.get() );
    auto &  xVisualInfo = *xVisualInfoUnique;

    const auto &    GLX_CONTEXT_MANAGER_UNIQUE = glContext->glxContextManagerUnique;
    ASSERT_NE( nullptr, GLX_CONTEXT_MANAGER_UNIQUE.get() );
    const auto &    GLX_CONTEXT_MANAGER = *GLX_CONTEXT_MANAGER_UNIQUE;
    ASSERT_EQ( &xDisplay, &( GLX_CONTEXT_MANAGER.xDisplay ) );
    ASSERT_NE( nullptr, GLX_CONTEXT_MANAGER.glxContext );
    ASSERT_EQ( &GLX_CONTEXT_MANAGER, GLX_CONTEXT_MANAGER.glxContextDestroyer.get() );

    auto    redSize = int( 10 );
    ASSERT_EQ(
        0
        , glXGetConfig(
            &xDisplay
            , &xVisualInfo
            , GLX_RED_SIZE
            , &redSize
        )
    );
    ASSERT_EQ( 8, redSize );

    auto    greenSize = int( 20 );
    ASSERT_EQ(
        0
        , glXGetConfig(
            &xDisplay
            , &xVisualInfo
            , GLX_GREEN_SIZE
            , &greenSize
        )
    );
    ASSERT_EQ( 8, greenSize );

    auto    blueSize = int( 30 );
    ASSERT_EQ(
        0
        , glXGetConfig(
            &xDisplay
            , &xVisualInfo
            , GLX_BLUE_SIZE
            , &blueSize
        )
    );
    ASSERT_EQ( 8, blueSize );

    auto    alphaSize = int( 40 );
    ASSERT_EQ(
        0
        , glXGetConfig(
            &xDisplay
            , &xVisualInfo
            , GLX_ALPHA_SIZE
            , &alphaSize
        )
    );
    ASSERT_EQ( 0, alphaSize );

    auto    depthSize = int( 50 );
    ASSERT_EQ(
        0
        , glXGetConfig(
            &xDisplay
            , &xVisualInfo
            , GLX_DEPTH_SIZE
            , &depthSize
        )
    );
    ASSERT_EQ( 0, depthSize );

    auto    stencilSize = int( 60 );
    ASSERT_EQ(
        0
        , glXGetConfig(
            &xDisplay
            , &xVisualInfo
            , GLX_STENCIL_SIZE
            , &stencilSize
        )
    );
    ASSERT_EQ( 0, stencilSize );

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( glContext );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );

    ASSERT_EQ( fg::GL_TRUE, glCurrentContextUnique->glIsTexture( glTexturesUnique->getName( 0 ) ) );
}

TEST(
    GLContextTest
    , GetWindow
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLContextTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );

    ASSERT_EQ( &window, &( glContextUnique->getWindow() ) );
}

TEST(
    GLContextTest
    , GetBufferSize
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLContextTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    argsUnique = fg::GLContextArgs::createBlank();
    ASSERT_NE( nullptr, argsUnique.get() );
    auto &  args = *argsUnique;

    args.setRedSize( 8 );
    args.setGreenSize( 8 );
    args.setBlueSize( 8 );
    args.setAlphaSize( 8 );
    args.setDepthSize( 24 );
    args.setStencilSize( 8 );

    auto    glContextUnique = fg::GLContext::create(
        window
        , args
    );
    ASSERT_NE( nullptr, glContextUnique.get() );

    ASSERT_EQ( 32, glContextUnique->getBufferSize() );
}

TEST(
    GLContextTest
    , GetRedSize
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLContextTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    argsUnique = fg::GLContextArgs::createBlank();
    ASSERT_NE( nullptr, argsUnique.get() );
    auto &  args = *argsUnique;

    args.setRedSize( 8 );
    args.setGreenSize( 8 );
    args.setBlueSize( 8 );
    args.setAlphaSize( 8 );
    args.setDepthSize( 24 );
    args.setStencilSize( 8 );

    auto    glContextUnique = fg::GLContext::create(
        window
        , args
    );
    ASSERT_NE( nullptr, glContextUnique.get() );

    ASSERT_EQ( 8, glContextUnique->getRedSize() );
}

TEST(
    GLContextTest
    , GetGreenSize
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLContextTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    argsUnique = fg::GLContextArgs::createBlank();
    ASSERT_NE( nullptr, argsUnique.get() );
    auto &  args = *argsUnique;

    args.setRedSize( 8 );
    args.setGreenSize( 8 );
    args.setBlueSize( 8 );
    args.setAlphaSize( 8 );
    args.setDepthSize( 24 );
    args.setStencilSize( 8 );

    auto    glContextUnique = fg::GLContext::create(
        window
        , args
    );
    ASSERT_NE( nullptr, glContextUnique.get() );

    ASSERT_EQ( 8, glContextUnique->getGreenSize() );
}

TEST(
    GLContextTest
    , GetBlueSize
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLContextTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    argsUnique = fg::GLContextArgs::createBlank();
    ASSERT_NE( nullptr, argsUnique.get() );
    auto &  args = *argsUnique;

    args.setRedSize( 8 );
    args.setGreenSize( 8 );
    args.setBlueSize( 8 );
    args.setAlphaSize( 8 );
    args.setDepthSize( 24 );
    args.setStencilSize( 8 );

    auto    glContextUnique = fg::GLContext::create(
        window
        , args
    );
    ASSERT_NE( nullptr, glContextUnique.get() );

    ASSERT_EQ( 8, glContextUnique->getBlueSize() );
}

TEST(
    GLContextTest
    , GetAlphaSize
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLContextTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    argsUnique = fg::GLContextArgs::createBlank();
    ASSERT_NE( nullptr, argsUnique.get() );
    auto &  args = *argsUnique;

    args.setRedSize( 8 );
    args.setGreenSize( 8 );
    args.setBlueSize( 8 );
    args.setAlphaSize( 8 );
    args.setDepthSize( 24 );
    args.setStencilSize( 8 );

    auto    glContextUnique = fg::GLContext::create(
        window
        , args
    );
    ASSERT_NE( nullptr, glContextUnique.get() );

    ASSERT_EQ( 8, glContextUnique->getAlphaSize() );
}

TEST(
    GLContextTest
    , GetDepthSize
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLContextTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    argsUnique = fg::GLContextArgs::createBlank();
    ASSERT_NE( nullptr, argsUnique.get() );
    auto &  args = *argsUnique;

    args.setRedSize( 8 );
    args.setGreenSize( 8 );
    args.setBlueSize( 8 );
    args.setAlphaSize( 8 );
    args.setDepthSize( 24 );
    args.setStencilSize( 8 );

    auto    glContextUnique = fg::GLContext::create(
        window
        , args
    );
    ASSERT_NE( nullptr, glContextUnique.get() );

    ASSERT_EQ( 24, glContextUnique->getDepthSize() );
}

TEST(
    GLContextTest
    , GetStencilSize
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLContextTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    argsUnique = fg::GLContextArgs::createBlank();
    ASSERT_NE( nullptr, argsUnique.get() );
    auto &  args = *argsUnique;

    args.setRedSize( 8 );
    args.setGreenSize( 8 );
    args.setBlueSize( 8 );
    args.setAlphaSize( 8 );
    args.setDepthSize( 24 );
    args.setStencilSize( 8 );

    auto    glContextUnique = fg::GLContext::create(
        window
        , args
    );
    ASSERT_NE( nullptr, glContextUnique.get() );

    ASSERT_EQ( 8, glContextUnique->getStencilSize() );
}

struct ReleaseCurrent
{
    void operator()(
        fg::GLContext *
    ) const
    {
        FgGLContext::releaseGLCurrent();
    }
};

using GLCurrentContextReleaser = std::unique_ptr<
    fg::GLContext
    , ReleaseCurrent
>;

TEST(
    GLContextTest
    , MakeCurrent
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLContextTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );
    auto &  glContext = *glContextUnique;

    glContext->makeGLCurrent();
    auto    releaser = GLCurrentContextReleaser( &glContext );
}

TEST(
    GLContextTest
    , MakeCurrent_failedDuplicate
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLContextTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );
    auto &  glContext = *glContextUnique;

    glContext->makeGLCurrent();
    auto    releaser = GLCurrentContextReleaser( &glContext );

    try {
        glContext->makeGLCurrent();

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}
}

TEST(
    GLContextTest
    , MakeCurrent_notDuplicate
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLContextTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );
    auto &  glContext = *glContextUnique;

    glContext->makeGLCurrent();
    auto    releaser = GLCurrentContextReleaser( &glContext );
    releaser.reset();

    glContext->makeGLCurrent();
    auto    releaser2 = GLCurrentContextReleaser( &glContext );
}

TEST(
    GLContextTest
    , MakeCurrent_multiThread
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLContextTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );
    auto &  glContext = *glContextUnique;

    glContext->makeGLCurrent();
    auto    releaser = GLCurrentContextReleaser( &glContext );

    auto    thread = std::thread(
        [
            &glContext
        ]
        {
            glContext->makeGLCurrent();
            auto    releaser = GLCurrentContextReleaser( &glContext );
        }
    );
    thread.join();
}
