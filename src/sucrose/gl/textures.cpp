﻿#include "fg/util/export.h"
#include "sucrose/gl/textures.h"
#include "fg/gl/gl.h"
#include "sucrose/gl/currentcontext.h"
#include "sucrose/gl/currentcontextremaker.h"
#include "sucrose/gl/context.h"

#include <stdexcept>

namespace {
    FgGLTextures::Names generateGLTextures(
        fg::GLCurrentContext &  _glCurrentContext
        , fg::GLsizei           _size
    )
    {
        auto    names = FgGLTextures::Names( _size );

        _glCurrentContext.glGetError();

        _glCurrentContext.glGenTextures(
            names.size()
            , names.data()
        );

        if( _glCurrentContext.glGetError() != fg::GL_NO_ERROR ) {
            throw std::runtime_error( "glGenTextures()が失敗" );
        }

        return names;
    }
}

FgGLTextures * fgGLTexturesCreate(
    FgGLCurrentContext *    _glCurrentContextPtr
    , FgGLsizei             _size
)
{
    if( _size <= 0 ) {
        throw std::out_of_range( "サイズが0以下" );
    }

    return new FgGLTextures(
        reinterpret_cast< fg::GLCurrentContext & >( *_glCurrentContextPtr )
        , _size
    );
}

void fgGLTexturesDestroy(
    FgGLTextures *  _implPtr
)
{
    delete _implPtr;
}

FgGLsizei fgGLTexturesGetSize(
    const FgGLTextures *    _IMPL_PTR
)
{
    return _IMPL_PTR->names.size();
}

FgGLuint fgGLTexturesGetName(
    const FgGLTextures *    _IMPL_PTR
    , FgGLsizei             _index
)
{
    return _IMPL_PTR->names.at( _index );
}

void FgGLTextures::DeleteNames::operator()(
    FgGLTextures *  _glTexturesPtr
) const
{
    auto    glCurrentContextRemaker = sucrose::GLCurrentContextRemaker( FgGLContext::releaseGLCurrent() );

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( _glTexturesPtr->glContext );

    auto &  names = _glTexturesPtr->names;

    glCurrentContextUnique->glDeleteTextures(
        names.size()
        , names.data()
    );
}

FgGLTextures::FgGLTextures(
    fg::GLCurrentContext &  _glCurrentContext
    , fg::GLsizei           _size
)
    : glContext( _glCurrentContext->getGLContext() )
    , names(
        generateGLTextures(
            _glCurrentContext
            , _size
        )
    )
    , namesDeleter( this )
{
}

const fg::GLContext & FgGLTextures::getGLContext(
) const
{
    return const_cast< FgGLTextures * >( this )->getGLContext();
}

fg::GLContext & FgGLTextures::getGLContext(
)
{
    return this->glContext;
}
