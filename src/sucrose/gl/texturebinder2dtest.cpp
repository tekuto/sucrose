﻿#include "fg/util/test.h"
#include "fg/gl/texturebinder2d.h"
#include "fg/gl/gl.h"
#include "fg/gl/context.h"
#include "fg/gl/currentcontext.h"
#include "fg/gl/textures.h"
#include "fg/window/window.h"
#include "sucrose/gl/textures.h"
#include "sucrose/window/xlib.h"

TEST(
    GLTextureBinder2DTest
    , Create
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLTextureBinder2DTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( *glContextUnique );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );
    auto &  glCurrentContext = *glCurrentContextUnique;

    auto    glTexturesUnique = fg::GLTextures::create(
        glCurrentContext
        , 10
    );
    ASSERT_NE( nullptr, glTexturesUnique.get() );
    const auto &    GL_TEXTURES = *glTexturesUnique;

    const auto  GL_TEXTURE_BINDER_2D_UNIQUE = fg::GLTextureBinder2D::create(
        glCurrentContext
        , GL_TEXTURES
        , 5
    );
    ASSERT_NE( nullptr, GL_TEXTURE_BINDER_2D_UNIQUE.get() );

    ASSERT_EQ( &glCurrentContext, &reinterpret_cast< const fg::GLCurrentContext & >( *GL_TEXTURE_BINDER_2D_UNIQUE ) );

    glCurrentContext.glGetError();
    auto    name = fg::GLuint();
    glCurrentContext.glGetIntegerv(
        fg::GL_TEXTURE_BINDING_2D
        , &reinterpret_cast< fg::GLint & >( name )
    );
    ASSERT_EQ( fg::GL_NO_ERROR, glCurrentContext.glGetError() );
    ASSERT_NE( 0, name );
    ASSERT_EQ( GL_TEXTURES->names[ 5 ], name );
}

TEST(
    GLTextureBinder2DTest
    , Create_failedWhenOtherContext
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLTextureBinder2DTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( *glContextUnique );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );
    auto &  glCurrentContext = *glCurrentContextUnique;

    auto    glTexturesUnique = fg::GLTextures::create(
        glCurrentContext
        , 10
    );
    ASSERT_NE( nullptr, glTexturesUnique.get() );
    const auto &    GL_TEXTURES = *glTexturesUnique;

    glCurrentContextUnique.destroy();

    auto    glContext2Unique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContext2Unique.get() );

    auto    glCurrentContext2Unique = fg::GLCurrentContext::create( *glContext2Unique );
    ASSERT_NE( nullptr, glCurrentContext2Unique.get() );
    auto &  glCurrentContext2 = *glCurrentContext2Unique;

    try {
        auto    glTextureBinder2DUnique = fg::GLTextureBinder2D::create(
            glCurrentContext2
            , GL_TEXTURES
            , 5
        );

        ASSERT_FALSE( true );   // ここには到達しない
    } catch( ... ) {}
}

TEST(
    GLTextureBinder2DTest
    , Destroy
)
{
    sucrose::initializeXlib();

    auto    windowUnique = fg::Window::create(
        "GLTextureBinder2DTest"
        , 100
        , 200
    );
    ASSERT_NE( nullptr, windowUnique.get() );
    auto &  window = *windowUnique;

    auto    glContextUnique = fg::GLContext::create( window );
    ASSERT_NE( nullptr, glContextUnique.get() );

    auto    glCurrentContextUnique = fg::GLCurrentContext::create( *glContextUnique );
    ASSERT_NE( nullptr, glCurrentContextUnique.get() );
    auto &  glCurrentContext = *glCurrentContextUnique;

    auto    glTexturesUnique = fg::GLTextures::create(
        glCurrentContext
        , 10
    );
    ASSERT_NE( nullptr, glTexturesUnique.get() );
    const auto &    GL_TEXTURES = *glTexturesUnique;

    auto    glTextureBinder2DUnique = fg::GLTextureBinder2D::create(
        glCurrentContext
        , GL_TEXTURES
        , 5
    );
    ASSERT_NE( nullptr, glTextureBinder2DUnique.get() );

    glTextureBinder2DUnique.destroy();

    glCurrentContext.glGetError();
    auto    name = fg::GLuint();
    glCurrentContext.glGetIntegerv(
        fg::GL_TEXTURE_BINDING_2D
        , &reinterpret_cast< fg::GLint & >( name )
    );
    ASSERT_EQ( fg::GL_NO_ERROR, glCurrentContext.glGetError() );
    ASSERT_EQ( 0, name );
}
