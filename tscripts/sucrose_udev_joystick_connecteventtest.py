# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'sucrose-udev-joystick-connecteventtest'

module.SOURCE = {
    'udev' : {
        'joystick' : [
            'connecteventtest.cpp',
            'connectevent.cpp',
            'id.cpp',
        ],
    },
}
