# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.DEPENDS = [
    'sucrose_files',
    'sucrose_gl_contextargstest',
    'sucrose_gl_contexttest',
    'sucrose_gl_currentcontexttest',
    'sucrose_gl_currentcontextremakertest',
    'sucrose_gl_threadtest',
    'sucrose_gl_threadexecutortest',
    'sucrose_gl_texturestest',
    'sucrose_gl_texturebinder2dtest',
    'sucrose_gl_samplerstest',
    'sucrose_gl_bufferstest',
    'sucrose_gl_bufferbinderarraytest',
    'sucrose_gl_bufferbinderelementarraytest',
    'sucrose_gl_vertexarraystest',
    'sucrose_gl_vertexarraybindertest',
    'sucrose_gl_vertexshadertest',
    'sucrose_gl_fragmentshadertest',
    'sucrose_gl_programtest',
    'sucrose_gl_picturetest',
]

module.BUILDER = cpp.shlib

module.TARGET = 'sucrose-gl'

module.SOURCE = {
    'gl' : [
        'contextargs.cpp',
        'context.cpp',
        'currentcontext.cpp',
        'currentcontextremaker.cpp',
        'thread.cpp',
        'threadexecutor.cpp',
        'textures.cpp',
        'texturebinder2d.cpp',
        'samplers.cpp',
        'buffers.cpp',
        'bufferbinderarray.cpp',
        'bufferbinderelementarray.cpp',
        'vertexarrays.cpp',
        'vertexarraybinder.cpp',
        'vertexshader.cpp',
        'fragmentshader.cpp',
        'program.cpp',
        'picture.cpp',
    ]
}

module.LIB = [
    'GL',
    'X11',
]
