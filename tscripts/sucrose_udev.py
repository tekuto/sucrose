# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.DEPENDS = [
    'sucrose_files',
    'sucrose_udev_managertest',
    'sucrose_udev_eventparameterstest',
    'sucrose_udev_eventtest',
    'sucrose_udev_eventpropertynamestest',
]

module.BUILDER = cpp.shlib

module.TARGET = 'sucrose-udev'

module.SOURCE = {
    'udev' : [
        'manager.cpp',
        'eventparameters.cpp',
        'event.cpp',
        'eventpropertynames.cpp',
    ]
}

module.LIB = [
    'udev',
]
