# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'sucrose-window-xlibtest'

module.SOURCE = {
    'window' : [
        'xlibtest.cpp',
        'xlib.cpp',
    ],
}

module.LIB = [
    'X11',
]
