# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.DEPENDS = [
    'sucrose_files',
    'sucrose_window_xlibtest',
    'sucrose_window_windowtest',
    'sucrose_window_eventmanagerstest',
    'sucrose_window_painteventtest',
    'sucrose_window_closeeventtest',
    'sucrose_window_eventprocessortest',
]

module.BUILDER = cpp.shlib

module.TARGET = 'sucrose-window'

module.SOURCE = {
    'window' : [
        'module.cpp',
        'xlib.cpp',
        'window.cpp',
        'eventmanagers.cpp',
        'paintevent.cpp',
        'closeevent.cpp',
        'eventprocessor.cpp',
    ]
}

module.LIB = [
    'X11',
]
