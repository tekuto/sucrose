# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'sucrose-udev-joystick-eventparameterstest'

module.SOURCE = {
    'udev' : {
        'joystick' : [
            'eventparameterstest.cpp',
            'eventparameters.cpp',
            'id.cpp',
            'connectevent.cpp',
            'disconnectevent.cpp',
            'buttonevent.cpp',
            'axisevent.cpp',
        ],
    },
}
