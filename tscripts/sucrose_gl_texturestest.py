# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.DEPENDS = [
    'sucrose_gl',
    'sucrose_window',
]

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'sucrose-gl-texturestest'

module.SOURCE = {
    'gl' : [
        'texturestest.cpp',
        'textures.cpp',
        'context.cpp',
        'currentcontext.cpp',
        'currentcontextremaker.cpp',
    ],
    'window' : [
        'xlib.cpp',
    ],
}

module.LIB = [
    'GL',
    'X11',
    'brownsugar-statemanager',
]

module.USE = [
    'sucrose-gl',
    'sucrose-window',
]
