# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'sucrose-udev-managertest'

module.SOURCE = {
    'udev' : [
        'managertest.cpp',
        'manager.cpp',
        'event.cpp',
        'eventparameters.cpp',
    ],
}

module.LIB = [
    'udev',
    'brownsugar-statemanager',
]
