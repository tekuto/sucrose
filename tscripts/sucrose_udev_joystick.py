# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.DEPENDS = [
    'sucrose_files',
    'sucrose_udev_joystick_managertest',
    'sucrose_udev_joystick_joysticktest',
    'sucrose_udev_joystick_eventparameterstest',
    'sucrose_udev_joystick_idtest',
    'sucrose_udev_joystick_connecteventtest',
    'sucrose_udev_joystick_disconnecteventtest',
    'sucrose_udev_joystick_buttoneventtest',
    'sucrose_udev_joystick_axiseventtest',
]

module.BUILDER = cpp.shlib

module.TARGET = 'sucrose-udev-joystick'

module.SOURCE = {
    'udev' : {
        'joystick' : [
            'manager.cpp',
            'joystick.cpp',
            'eventparameters.cpp',
            'id.cpp',
            'connectevent.cpp',
            'disconnectevent.cpp',
            'buttonevent.cpp',
            'axisevent.cpp',
        ]
    }
}
