# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'sucrose-udev-joystick-axiseventtest'

module.SOURCE = {
    'udev' : {
        'joystick' : [
            'axiseventtest.cpp',
            'axisevent.cpp',
            'id.cpp',
        ],
    },
}
