# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.DEPENDS = [
    'sucrose_window',
]

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'sucrose-window-eventprocessortest'

module.SOURCE = {
    'window' : [
        'eventprocessortest.cpp',
        'xlib.cpp',
    ],
}

module.LIB = [
    'X11',
    'brownsugar-statemanager',
]

module.USE = [
    'sucrose-window',
]
