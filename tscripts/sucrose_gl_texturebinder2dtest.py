# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.DEPENDS = [
    'sucrose_gl',
    'sucrose_window',
]

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'sucrose-gl-texturebinder2dtest'

module.SOURCE = {
    'gl' : [
        'texturebinder2dtest.cpp',
    ],
    'window' : [
        'xlib.cpp',
    ],
}

module.LIB = [
    'X11',
    'brownsugar-statemanager',
]

module.USE = [
    'sucrose-gl',
    'sucrose-window',
]
