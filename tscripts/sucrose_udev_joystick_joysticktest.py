# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.DEPENDS = [
    'sucrose_testdata_udevjoysticktest',
]

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'sucrose-udev-joystick-joysticktest'

module.SOURCE = {
    'udev' : {
        'joystick' : [
            'joysticktest.cpp',
            'joystick.cpp',
            'eventparameters.cpp',
            'id.cpp',
            'buttonevent.cpp',
            'axisevent.cpp',
        ],
    },
}

module.LIB = [
    'brownsugar-statemanager',
]
