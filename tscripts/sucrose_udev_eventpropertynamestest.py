# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'sucrose-udev-eventpropertynamestest'

module.SOURCE = {
    'udev' : [
        'eventpropertynamestest.cpp',
        'eventpropertynames.cpp',
        'event.cpp',
    ],
}
