# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'sucrose-udev-joystick-disconnecteventtest'

module.SOURCE = {
    'udev' : {
        'joystick' : [
            'disconnecteventtest.cpp',
            'disconnectevent.cpp',
            'id.cpp',
        ],
    },
}
