# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.DEPENDS = [
    'sucrose_udev',
]

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'sucrose-udev-eventparameterstest'

module.SOURCE = {
    'udev' : [
        'eventparameterstest.cpp',
        'eventparameters.cpp',
        'event.cpp',
    ],
}

module.LIB = [
    'brownsugar-statemanager',
]

module.USE = [
    'sucrose-udev',
]
