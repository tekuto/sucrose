# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.DEPENDS = [
    'sucrose_controller',
]

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'sucrose-controller-raw-actionbuffertest'

module.SOURCE = {
    'controller' : {
        'raw' : [
            'actionbuffertest.cpp',
            'actionbuffer.cpp',
            'action.cpp',
            'connectaction.cpp',
        ],
    },
}

module.LIB = [
    'brownsugar-statemanager',
]

module.USE = [
    'sucrose-controller',
]
