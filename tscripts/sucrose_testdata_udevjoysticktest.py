# -*- coding: utf-8 -*-

from taf import *
from taf.tools import copy

module.TYPE = module.test

module.BUILDER = copy.files

module.TARGET = 'udevjoysticktest'

module.SOURCE = {
    'testdata' : {
        'udevjoysticktest' : [
            'new.dat',
            'buttonpress.dat',
            'axisoperate.dat',
            'js10',
        ]
    }
}
