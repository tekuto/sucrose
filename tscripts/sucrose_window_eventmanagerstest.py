# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.DEPENDS = [
    'sucrose_window',
]

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'sucrose-window-eventmanagerstest'

module.SOURCE = {
    'window' : [
        'eventmanagerstest.cpp',
    ],
}

module.LIB = [
    'brownsugar-statemanager',
]

module.USE = [
    'sucrose-window',
]
