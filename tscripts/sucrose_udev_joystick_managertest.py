# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.DEPENDS = [
    'sucrose_testdata_udevjoysticktest',
    'sucrose_udev_joystick',
]

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'sucrose-udev-joystick-managertest'

module.SOURCE = {
    'udev' : [
        {
            'joystick' : [
                'managertest.cpp',
            ],
        },
        'event.cpp',
    ],
}

module.LIB = [
    'brownsugar-statemanager',
]

module.USE = [
    'sucrose-udev-joystick',
]
