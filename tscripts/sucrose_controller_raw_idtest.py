# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.DEPENDS = [
    'sucrose_controller',
]

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'sucrose-controller-raw-idtest'

module.SOURCE = {
    'controller' : {
        'raw' : [
            'idtest.cpp',
        ],
    },
}

module.LIB = [
    'brownsugar-statemanager',
]

module.USE = [
    'sucrose-controller',
]
