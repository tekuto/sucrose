# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'sucrose-udev-joystick-buttoneventtest'

module.SOURCE = {
    'udev' : {
        'joystick' : [
            'buttoneventtest.cpp',
            'buttonevent.cpp',
            'id.cpp',
        ],
    },
}
