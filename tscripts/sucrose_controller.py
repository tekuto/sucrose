# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.DEPENDS = [
    'sucrose_files',
    'sucrose_controller_raw_idtest',
    'sucrose_controller_raw_buttontest',
    'sucrose_controller_raw_axistest',
    'sucrose_controller_raw_connectactiontest',
    'sucrose_controller_raw_buttonactiontest',
    'sucrose_controller_raw_axisactiontest',
    'sucrose_controller_raw_actiontest',
    'sucrose_controller_raw_actionbuffertest',
    'sucrose_controller_raw_managertest',
]

module.BUILDER = cpp.shlib

module.TARGET = 'sucrose-controller'

module.SOURCE = {
    'controller' : [
        {
            'raw' : [
                'id.cpp',
                'button.cpp',
                'axis.cpp',
                'connectaction.cpp',
                'buttonaction.cpp',
                'axisaction.cpp',
                'action.cpp',
                'actionbuffer.cpp',
                'manager.cpp',
            ],
        },
    ]
}
