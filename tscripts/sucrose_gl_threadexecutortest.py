# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.DEPENDS = [
    'sucrose_gl',
    'sucrose_window',
]

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'sucrose-gl-threadexecutortest'

module.SOURCE = {
    'gl' : [
        'threadexecutortest.cpp',
    ],
    'window' : [
        'xlib.cpp',
    ],
}

module.LIB = [
    'X11',
    'brownsugar-statemanager',
]

module.USE = [
    'sucrose-gl',
    'sucrose-window',
]
