# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.DEPENDS = [
    'sucrose_controller',
]

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'sucrose-controller-raw-axisactiontest'

module.SOURCE = {
    'controller' : {
        'raw' : [
            'axisactiontest.cpp',
            'axisaction.cpp',
            'axis.cpp',
        ],
    },
}

module.LIB = [
    'brownsugar-statemanager',
]

module.USE = [
    'sucrose-controller',
]
