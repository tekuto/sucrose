# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.DEPENDS = [
    'sucrose_gl',
    'sucrose_window',
]

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'sucrose-gl-contextargstest'

module.SOURCE = {
    'gl' : [
        'contextargstest.cpp',
    ],
}

module.LIB = [
    'brownsugar-statemanager',
]

module.USE = [
    'sucrose-gl',
    'sucrose-window',
]
