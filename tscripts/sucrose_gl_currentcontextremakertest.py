﻿# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.DEPENDS = [
    'sucrose_gl',
    'sucrose_window',
]

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'sucrose-gl-currentcontextremakertest'

module.SOURCE = {
    'gl' : [
        'currentcontextremakertest.cpp',
        'currentcontextremaker.cpp',
        'context.cpp',
        'currentcontext.cpp',
    ],
    'window' : [
        'xlib.cpp',
    ],
}

module.LIB = [
    'GL',
    'X11',
    'brownsugar-statemanager',
]

module.USE = [
    'sucrose-gl',
    'sucrose-window',
]
