# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'sucrose-udev-joystick-idtest'

module.SOURCE = {
    'udev' : {
        'joystick' : [
            'idtest.cpp',
            'id.cpp',
        ],
    },
}
